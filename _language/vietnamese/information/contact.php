<?php
// Heading
$_['heading_title']      = 'Liên hệ với chúng tôi';

// Text
$_['text_location']      = 'Thông tin của chúng tôi';
$_['text_contact']       = 'Liên hệ trực tuyến';
$_['text_address']      = 'Địa chỉ';
$_['text_email']         = 'E-mail:';
$_['text_phone']     	 = 'Điện thoại';
$_['text_fax']           = 'Fax';
$_['text_map']           = 'Bản đồ';
$_['text_message']       = '<p>Cảm ơn bạn. Thư liên hệ của bạn đã được gửi đến chúng tôi!</p>';

// Entry Fields
$_['entry_name']         = 'Tên của bạn';
$_['entry_email']        = 'E-mail';
$_['entry_enquiry']      = 'Nội dung';
$_['entry_submit']       = 'Gửi liên hệ';
$_['entry_captcha']	 	 = 'Nhập mã xác nhận';

// Email
$_['email_subject']      = 'Tiêu đề %s';

// Errors
$_['error_name']         = 'Tên của bạn phải nhiều hơn 3 và nhỏ hơn 32 ký tự!';
$_['error_email']        = 'Bạn phải điền chính xác Email!';
$_['error_enquiry']      = 'Nội dung liên hệ phải nhiều hơn 10 và nhỏ hơn 3000 ký tự!';
$_['error_captcha']	 = 'Bạn đã nhập mã xác nhận không chính xác!';
?>
