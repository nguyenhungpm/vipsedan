<?php
$_['text_airport'] = 'Sân bay';
$_['text_longtrip'] = 'Đường dài';
$_['text_departure'] = 'Điểm đi';
$_['text_departure_placeholder'] = 'Điểm đón: tỉnh thành, khách sạn, địa điểm khác';
$_['text_destination'] = 'Điểm đến';
$_['text_destination_placeholder'] = 'Điểm đến: tỉnh thành, khách sạn, địa điểm khác';
$_['text_cartype'] = 'Loại xe';
$_['text_pickuptime'] = 'Thời gian đón';
$_['text_flightnumber'] = 'Mã chuyến bay';
$_['text_oneway'] = '1 chiều';
$_['text_roundtrip'] = '2 chiều';
$_['text_booknow'] = 'Đặt xe ngay';
$_['text_calculatefare'] = 'Tính giá';
$_['text_promotion'] = '<b>Khuyến mại</b>: Xe đi tỉnh được giảm giá 70% giá cước chiều về';
?>