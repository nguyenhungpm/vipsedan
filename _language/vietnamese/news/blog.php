<?php
// Text
$_['text_news']      = 'Blog';
$_['text_error']        = 'Không tìm thấy bài viết!';
$_['text_empty']        = 'Chưa có bài viết trong mục này!';
$_['text_limit']        = 'Hiển thị:';
$_['text_post_on']        = 'Ngày đăng tin:';
$_['text_viewed']        = 'lượt xem';
$_['text_readmore']        = 'Đọc thêm';
?>
