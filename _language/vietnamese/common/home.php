<?php
$_['text_bestprice'] = 'Giá tốt nhất';
$_['text_bestprice_content'] = 'Giá tốt nhất với nhiều ưu đãi khác nhau. Dịch vụ tiện chuyến giá siêu rẻ';
$_['text_bestservice'] = 'Dịch vụ tốt nhất';
$_['text_bestservice_content'] = 'Có nhiều loại xe để lựa chọn. Luôn có nhiều chương trình ưu đãi, khuyến mại. Thời gian chờ lên tới 45 phút.';
$_['text_bestsuport'] = 'Tậm tâm phục vụ';
$_['text_bestsuport_content'] = 'Bạn được đón tiếp như người thân. Bạn biết rõ thông tin về lái xe. Các lái xe được tuyển chọn kĩ càng';
?>