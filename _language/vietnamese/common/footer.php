<?php
// Text
$_['text_information']  = 'Thông tin';
$_['text_service']      = 'Chăm sóc khách hàng';
$_['text_extra']        = 'Chức năng khác';
$_['text_contact']      = 'Liên hệ';
$_['text_return']       = 'Trả hàng';
$_['text_sitemap']      = 'Sơ đồ trang';
$_['text_manufacturer'] = 'Thương hiệu';
$_['text_voucher']      = 'Phiếu quà tặng';
$_['text_affiliate']    = 'Đại lý';
$_['text_special']      = 'Khuyến mãi';
$_['text_account']      = 'Tài khoản của tôi';
$_['text_order']        = 'Lịch sử đơn hàng';
$_['text_wishlist']     = 'Danh sách yêu thích';
$_['text_newsletter']   = 'Thư thông báo';
$_['text_powered']      = 'Bản quyền của <a href="http://www.opencart.com">OpenCart</a><br /> %s &copy; %s';
$_['text_footer']       = '<p></p>

<p>.</p>

<p></p>

<p></p>
';
$_['text_head_office']   = 'Địa điểm giao dịch';
$_['text_representative_office']   = 'Các cơ sở tại Hà Nội';
$_['text_representative_office_content']   = '<span>- Số 1 Quang Trung, Hà Đông, Hà Nội</span><br />
<span>- 227 Trần Thái Tông, Cầu Giấy, Hà Nội</span><br />
<span>- 68 Phạm Ngọc Thạch, Đống Đa, Hà Nội</span><br />
<span>- 27 Phan Huy Chú, Hoàn Kiếm, Hà Nội</span><br />
<span>- 128 Phùng Hưng, Hoàn Kiếm, Hà Nội</span><br />
<span>- Park 3, Time City, Hai Bà Trưng, Hà Nội</span><br />';
$_['text_representative_office_other']   = 'Các cơ sở khác';
$_['text_representative_office_other_content']   = '<span>- số 268 Cao thắng,tp Hạ Long ,Quảng Ninh</span><br />
<span>- Sảnh khách sạn Tràng An,Số 1 Lê Hồng Phong,Phường Vân Giang,TP Ninh Bình</span><br />
<span>- 24D Đường hoàng hoa thám,Lộc Thọ, Nha Trang , khánh hoà</span><br />
<span>- 168 đường trần hưng đạo, ấp cửa lấp,xã Dương Tơ, Phú Quốc ,Kiên Giang</span><br />
<span>- 158 Tôn đức thắng, Quận Liên Chiểu,TP Đà Nẵng</span><br />
<span>- 268B Lê Hồng Phong, Quận 5,Tp Hồ Chí Minh</span><br />
<span>- 389 Đường Bùi Thị Xuân,Phường 2,Tp Đà Lạt, Lâm Đồng</span><br />';
$_['text_headquarters']   = 'Trụ sở';
$_['text_taxcode']   = 'MST';
?>