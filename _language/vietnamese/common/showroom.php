<?php
// Heading
$_['heading_title']  	= 'Mạng lưới bán hàng';

// Text 
$_['text_north']  		= 'Miền Bắc';
$_['text_south']   		= 'Miền Nam';
$_['text_central']   	= 'Miền Trung';
$_['text_select']   	= 'Chọn tỉnh/thành phố';
$_['text_district']     = 'Quận/huyện';
$_['text_name']     	= 'Tên đại lý';
$_['text_email']     	= 'E-mail';
$_['text_phone'] 		= 'Điện thoại';
$_['text_address'] 		= 'Địa chỉ';
$_['no_result'] 		= 'Không có cửa hàng nào tại khu vực này';
?>