<?php
// Heading
$_['heading_title'] = 'Subscribe for newsletter';

// Text
$_['text_subscribe']     = 'Subscribe to receive the latest news via email.';
$_['text_submit']     = 'Subscribe';
$_['text_email']     = 'Your email';
?>
