<?php
$_['text_airport'] = 'Airport';
$_['text_longtrip'] = 'Long Trip';
$_['text_departure'] = 'Departure';
$_['text_departure_placeholder'] = 'Departure: City, hotel, other locatoin';
$_['text_destination'] = 'Destination';
$_['text_destination_placeholder'] = 'Destination: City, hotel, other locatoin';
$_['text_cartype'] = 'Car type';
$_['text_pickuptime'] = 'Pick up time';
$_['text_flightnumber'] = 'Flight number';
$_['text_oneway'] = 'One way';
$_['text_roundtrip'] = 'Round trip';
$_['text_booknow'] = 'Book now';
$_['text_calculatefare'] = 'Calculate fare';
$_['text_promotion'] = '<b>Promotion</b>: Cars going to the province are discounted 70% of the return fare';
?>