<?php
// Text
$_['text_information']  = 'Information';
$_['text_service']      = 'Customer Service';
$_['text_extra']        = 'Extras';
$_['text_contact']      = 'Contact Us';
$_['text_return']       = 'Returns';
$_['text_sitemap']      = 'Site Map';
$_['text_manufacturer'] = 'Brands';
$_['text_voucher']      = 'Gift Vouchers';
$_['text_affiliate']    = 'Affiliates';
$_['text_special']      = 'Specials';
$_['text_account']      = 'My Account';
$_['text_order']        = 'Order History';
$_['text_wishlist']     = 'Wish List';
$_['text_newsletter']   = 'Newsletter';
$_['text_powered']      = 'Powered By <a href="http://www.opencart.com">OpenCart</a><br /> %s &copy; %s';


$_['text_address']      = 'Address';
$_['text_telephone']      = 'Tel.';
$_['text_follow']      = 'Follow us';
$_['text_copyright']      = 'All rights reserved';
$_['text_back_top']      = 'Back to top';
$_['text_head_office']   = 'Head Office';
$_['text_representative_office']   = 'Representative Office';
$_['text_representative_office_content']   = '<span>- No 1 Quang Trung, Ha Dong District, Hanoi</span><br />
<span>- 227 Tran Thai Tong street, Cau Giay District, Hanoi</span><br />
<span>- 68 Pham Ngoc Thach street, Dong Da District, Hanoi</span><br />
<span>- 27 Phan Huy Chu street, Hoan Kiem District, Hanoi</span><br />
<span>- 128 Phung Hung street, Hoan Kiem District, Hanoi</span><br />
<span>- Park 3, Vinhomes Times City, Hai Ba Trung District, Hanoi</span><br />';
$_['text_representative_office_other']   = 'Other Representative Office';
$_['text_representative_office_other_content']   = '<span>- 268 Cao Thang Street, Ha Long City, Quang Ninh Province</span><br />
<span>- Trang An Hotel, 1 Le Hong Phong Street, Van Giang Ward, Ninh Binh City</span><br />
<span>- 24D Hoang Hoa Tham street, Loc Tho Ward, Nha Trang City, Khanh Hoa Province</span><br />
<span>- 168 Tran Hung Dao Street, Cua Lap Residential Group, Duong To Commune, Phu Quoc City, Kien Giang Province</span><br />
<span>- 158 Ton Duc Thang Street, Lien Chieu District, Da Nang City</span><br />
<span>- 268B Le Hong Phong Street, 5 District, Ho Chi Minh City</span><br />
<span>- 389 Bui Thi Xuan Street, District 2, Da Lat City, Lam Dong Province</span><br />';
$_['text_headquarters']   = 'Headquarters';
$_['text_taxcode']   = 'Taxcode';
?>