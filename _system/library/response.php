<?php
require('Fastimage.php');
class Response {
	private $headers = array();
	private $level = 0;
	private $output;

	public function addHeader($header) {
		$this->headers[] = $header;
	}

	public function redirect($url) {
		header('Location: ' . $url);
		exit;
	}

	public function setCompression($level) {
		$this->level = $level;
	}

	public function setOutput($output) {
		$this->output = $output;
	}

	private function compress($data, $level = 0) {
		if (isset($_SERVER['HTTP_ACCEPT_ENCODING']) && (strpos($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip') !== false)) {
			$encoding = 'gzip';
		}

		if (isset($_SERVER['HTTP_ACCEPT_ENCODING']) && (strpos($_SERVER['HTTP_ACCEPT_ENCODING'], 'x-gzip') !== false)) {
			$encoding = 'x-gzip';
		}

		if (!isset($encoding)) {
			return $data;
		}

		if (!extension_loaded('zlib') || ini_get('zlib.output_compression')) {
			return $data;
		}

		if (headers_sent()) {
			return $data;
		}

		if (connection_status()) {
			return $data;
		}

		$this->addHeader('Content-Encoding: ' . $encoding);

		return gzencode($data, (int)$level);
	}

	public function output() {
		if ($this->output) {
			if ($this->level) {
				$output = $this->compress($this->output, $this->level);
			} else {
				$output = $this->output;
			}

			if (!headers_sent()) {
				foreach ($this->headers as $header) {
					header($header, true);
				}
			}

			echo $output;
		}
	}
	
	// public function output() {

		// if ($this->output) {
			// if(strpos(HTTP_SERVER,'/oadmin/') || strpos($_SERVER['REQUEST_URI'],'amp-page/')){
				// $output = $this->output;
			// }else{
				// if ($this->level) {
					// $output = $this->compress($this->minify_html($this->output), $this->level);
				// } else {
					// $output = $this->minify_html($this->output);
					// $output = str_replace('og:http:', 'og: http:', $output);
				// }
			// }


			// if (!headers_sent()) {
				// foreach ($this->headers as $header) {
					// header($header, true);
				// }
			// }

			// echo $output;
		// }
	// }

	public function ClearCache(){
		require_once(DIR_SYSTEM . 'library/pagecache.php');
		$pagecache = new PageCache();
		$vals=$pagecache->Settings();
		$expire=$vals['expire'];
		$cachefolder=$vals['cachefolder'];
		$range=array( '0','1','2','3','4','5','6','7',
					  '8','9','a','b','c','d','e','f');
		$count=0;
		foreach ($range as $f) {
			foreach ($range as $s) {
				$dname=$cachefolder . $f . '/' . $s;
				if (is_dir($dname) && @$dir=opendir($dname)) {
					while (false !== ($file = readdir($dir))) {
					   // only purge files that end in .cache
					   if (substr($file,-6) == '.cache') {
						   $fpath=$dname . '/' . $file;
						   unlink($fpath);
						   $count++;
					   }
					}
				}
			}
		}
	}


	public function amphtml($html = ''){
		$html = str_ireplace(['<video','/video>','<audio','/audio>'],['<amp-video','/amp-video>','<amp-audio','/amp-audio>'],$html);

		$html = $this->_ampify_img($html);
		$html = strip_tags($html,'<h1><h2><h3><h4><h5><h6><a><p><ul><ol><li><blockquote><q><cite><ins><del><strong><em><code><pre><svg><table><thead><tbody><tfoot><th><tr><td><dl><dt><dd><article><section><header><footer><aside><figure><time><abbr><div><span><hr><small><br><amp-img><amp-audio><amp-video><amp-ad><amp-anim><amp-carousel><amp-fit-rext><amp-image-lightbox><amp-instagram><amp-lightbox><amp-twitter><amp-youtube>');

		$html = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $html);

		$allowed_tags = '<h1><h2><h3><h4><h5><h6><a><p><ul><ol><li><blockquote><q><cite><ins><del><strong><em><code><pre><svg><table><thead><tbody><tfoot><th><tr><td><dl><dt><dd><article><section><header><footer><aside><figure><time><abbr><div><span><hr><small><br><amp-img><amp-audio><amp-video><amp-ad><amp-anim><amp-carousel><amp-fit-rext><amp-image-lightbox><amp-instagram><amp-lightbox><amp-twitter><amp-youtube>';

		mb_regex_encoding('UTF-8');
		$search = array('/&lsquo;/u', '/&rsquo;/u', '/&ldquo;/u', '/&rdquo;/u', '/&mdash;/u');
		$replace = array('\'', '\'', '"', '"', '-');
		$html = preg_replace($search, $replace, $html);
		if(mb_stripos($html, '/*') !== FALSE){
			$html = mb_eregi_replace('#/\*.*?\*/#s', '', $html, 'm');
		}
		$html = preg_replace(array('/<([0-9]+)/'), array('< $1'), $html);
		$html = strip_tags($html, $allowed_tags);
		$html = preg_replace(array('/^\s\s+/', '/\s\s+$/', '/\s\s+/u'), array('', '', ' '), $html);
		$search = array('#<(strong|b)[^>]*>(.*?)</(strong|b)>#isu', '#<(em|i)[^>]*>(.*?)</(em|i)>#isu', '#<u[^>]*>(.*?)</u>#isu');
		$replace = array('<b>$2</b>', '<i>$2</i>', '<u>$1</u>');
		$html = preg_replace($search, $replace, $html);
		$num_matches = preg_match_all("/\<!--/u", $html, $matches);
		if($num_matches){
			$html = preg_replace('/\<!--(.)*--\>/isu', '', $html);
		}
		$html = preg_replace('/<\/font[^>]*>/', '', preg_replace('/<font[^>]*>/', '', $html));
		return $html;
	}


	function getimgsize($url, $referer = ''){
			$headers = array(
											'Range: bytes=0-32768'
											);

			/* Hint: you could extract the referer from the url */
			if (!empty($referer)) array_push($headers, 'Referer: '.$referer);

			$curl = curl_init($url);
			curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			$data = curl_exec($curl);
			curl_close($curl);

			$image = imagecreatefromstring($data);

			$return = array(imagesx($image), imagesy($image));

			imagedestroy($image);

			return $return;
	}
	function _ampify_img ($html) {
      preg_match_all("#<img(.*?)\\/?>#", $html, $img_matches);

      foreach ($img_matches[1] as $key => $img_tag) {
        preg_match_all('/(alt|src|width|height)=["\'](.*?)["\']/i', $img_tag, $attribute_matches);
        $attributes = array_combine($attribute_matches[1], $attribute_matches[2]);

        if (!array_key_exists('width', $attributes) || !array_key_exists('height', $attributes)) {
          if (array_key_exists('src', $attributes)) {
              $image = new FastImage($attributes['src']);
              list($w, $h) = $image->getSize();
              $attributes['width'] = $w;
              $attributes['height'] = $h;
          }
        }

        $amp_tag = '<amp-img ';
        foreach ($attributes as $attribute => $val) {
          $amp_tag .= $attribute .'="'. $val .'" ';
        }

        $amp_tag .= 'layout="responsive"';
        $amp_tag .= '>';
        $amp_tag .= '</amp-img>';

        $html = str_replace($img_matches[0][$key], $amp_tag, $html);
      }

      return $html;
  }

	// HTML Minifier
	public function minify_html($input) {
		if(trim($input) === "") return $input;
		$replace = array(
			' </s>' => ' </s>',
			'<oembed url=' => '<iframe src=',
			'</oembed>' => '</iframe>',
			'youtube.com/watch?v=' => 'youtube.com/embed/',
			'youtu.be/' => 'youtube.com/embed/',
			'<s> ' => '<s> ',
			'<u> ' => ' <u>',
			' </u>' => '</u> ',
			' </i>' => '</i> ',
			'<i> ' => ' <i>',
			'<em> ' => '<em> ',
			' </em>' => '</em> ',
			' </strong>' => '</strong> ',
			'<strong> ' => ' <strong>',
			'&nbsp;' => '&#160;',
			'og:http' => 'og: http'
		);

		$input = strtr($input, $replace);
		// Remove extra white-space(s) between HTML attribute(s)
		$input = preg_replace_callback('#<([^\/\s<>!]+)(?:\s+([^<>]*?)\s*|\s*)(\/?)>#s', function($matches) {
			return '<' . $matches[1] . preg_replace('#([^\s=]+)(\=([\'"]?)(.*?)\3)?(\s+|$)#s', ' $1$2', $matches[2]) . $matches[3] . '>';
		}, str_replace("\r", "", $input));
		// Minify inline CSS declaration(s)
		if(strpos($input, ' style=') !== false) {
			$input = preg_replace_callback('#<([^<]+?)\s+style=([\'"])(.*?)\2(?=[\/\s>])#s', function($matches) {
				return '<' . $matches[1] . ' style=' . $matches[2] . $this->minify_css($matches[3]) . $matches[2];
			}, $input);
		}
		if(strpos($input, '</style>') !== false) {
		  $input = preg_replace_callback('#<style(.*?)>(.*?)</style>#is', function($matches) {
			return '<style' . $matches[1] .'>'. $this->minify_css($matches[2]) . '</style>';
		  }, $input);
		}
		if(strpos($input, '</script>') !== false) {
		  $input = preg_replace_callback('#<script(.*?)>(.*?)</script>#is', function($matches) {
			return '<script' . $matches[1] .'>'. $this->minify_js($matches[2]) . '</script>';
		  }, $input);
		}
		return preg_replace(
			array(
				// t = text
				// o = tag open
				// c = tag close
				// Keep important white-space(s) after self-closing HTML tag(s)
				'#<(img|input)(>| .*?>)#s',
				// Remove a line break and two or more white-space(s) between tag(s)
				'#(<!--.*?-->)|(>)(?:\n*|\s{2,})(<)|^\s*|\s*$#s',
				'#(<!--.*?-->)|(?<!\>)\s+(<\/.*?>)|(<[^\/]*?>)\s+(?!\<)#s', // t+c || o+t
				'#(<!--.*?-->)|(<[^\/]*?>)\s+(<[^\/]*?>)|(<\/.*?>)\s+(<\/.*?>)#s', // o+o || c+c
				'#(<!--.*?-->)|(<\/.*?>)\s+(\s)(?!\<)|(?<!\>)\s+(\s)(<[^\/]*?\/?>)|(<[^\/]*?\/?>)\s+(\s)(?!\<)#s', // c+t || t+o || o+t -- separated by long white-space(s)
				'#(<!--.*?-->)|(<[^\/]*?>)\s+(<\/.*?>)#s', // empty tag
				'#<(img|input)(>| .*?>)<\/\1>#s', // reset previous fix
				'#(&nbsp;)&nbsp;(?![<\s])#', // clean up ...
				'#(?<=\>)(&nbsp;)(?=\<)#', // --ibid
				// Remove HTML comment(s) except IE comment(s)
				'#\s*<!--(?!\[if\s).*?-->\s*|(?<!\>)\n+(?=\<[^!])#s'
			),
			array(
				'<$1$2</$1>',
				'$1$2$3',
				'$1$2$3',
				'$1$2$3$4$5',
				'$1$2$3$4$5$6$7',
				'$1$2$3',
				'<$1$2',
				'$1 ',
				'$1',
				""
			),
		$input);
	}
	// CSS Minifier => http://ideone.com/Q5USEF + improvement(s)
	public function minify_css($input) {
		if(trim($input) === "") return $input;
		return preg_replace(
			array(
				// Remove comment(s)
				'#("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\')|\/\*(?!\!)(?>.*?\*\/)|^\s*|\s*$#s',
				// Remove unused white-space(s)
				'#("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\'|\/\*(?>.*?\*\/))|\s*+;\s*+(})\s*+|\s*+([*$~^|]?+=|[{};,>~+]|\s*+-(?![0-9\.])|!important\b)\s*+|([[(:])\s++|\s++([])])|\s++(:)\s*+(?!(?>[^{}"\']++|"(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\')*+{)|^\s++|\s++\z|(\s)\s+#si',
				// Replace `0(cm|em|ex|in|mm|pc|pt|px|vh|vw|%)` with `0`
				'#(?<=[\s:])(0)(cm|em|ex|in|mm|pc|pt|px|vh|vw|%)#si',
				// Replace `:0 0 0 0` with `:0`
				'#:(0\s+0|0\s+0\s+0\s+0)(?=[;\}]|\!important)#i',
				// Replace `background-position:0` with `background-position:0 0`
				'#(background-position):0(?=[;\}])#si',
				// Replace `0.6` with `.6`, but only when preceded by `:`, `,`, `-` or a white-space
				'#(?<=[\s:,\-])0+\.(\d+)#s',
				// Minify string value
				'#(\/\*(?>.*?\*\/))|(?<!content\:)([\'"])([a-z_][a-z0-9\-_]*?)\2(?=[\s\{\}\];,])#si',
				'#(\/\*(?>.*?\*\/))|(\burl\()([\'"])([^\s]+?)\3(\))#si',
				// Minify HEX color code
				'#(?<=[\s:,\-]\#)([a-f0-6]+)\1([a-f0-6]+)\2([a-f0-6]+)\3#i',
				// Replace `(border|outline):none` with `(border|outline):0`
				'#(?<=[\{;])(border|outline):none(?=[;\}\!])#',
				// Remove empty selector(s)
				'#(\/\*(?>.*?\*\/))|(^|[\{\}])(?:[^\s\{\}]+)\{\}#s'
			),
			array(
				'$1',
				'$1$2$3$4$5$6$7',
				'$1',
				':0',
				'$1:0 0',
				'.$1',
				'$1$3',
				'$1$2$4$5',
				'$1$2$3',
				'$1:0',
				'$1$2'
			),
		$input);
	}
	// JavaScript Minifier
	public function minify_js($input) {
		if(trim($input) === "") return $input;
		return preg_replace(
			array(
				// Remove comment(s)
				'#\s*("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\')\s*|\s*\/\*(?!\!|@cc_on)(?>[\s\S]*?\*\/)\s*|\s*(?<![\:\=])\/\/.*(?=[\n\r]|$)|^\s*|\s*$#',
				// Remove white-space(s) outside the string and regex
				'#("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\'|\/\*(?>.*?\*\/)|\/(?!\/)[^\n\r]*?\/(?=[\s.,;]|[gimuy]|$))|\s*([!%&*\(\)\-=+\[\]\{\}|;:,.<>?\/])\s*#s',
				// Remove the last semicolon
				'#;+\}#',
				// Minify object attribute(s) except JSON attribute(s). From `{'foo':'bar'}` to `{foo:'bar'}`
				'#([\{,])([\'])(\d+|[a-z_][a-z0-9_]*)\2(?=\:)#i',
				// --ibid. From `foo['bar']` to `foo.bar`
				'#([a-z0-9_\)\]])\[([\'"])([a-z_][a-z0-9_]*)\2\]#i'
			),
			array(
				'$1',
				'$1$2',
				'}',
				'$1$3',
				'$1.$3'
			),
		$input);
	}
}

define('SAFE', 1);
define('EXTREME', 2);
define('EXTREME_SAVE_COMMENTS', 4);
define('EXTREME_SAVE_PRE', 3);

function minify($html, $level=4)
{
	switch((int)$level)
	{
		case 0:
			//Don't minify
		break;

		case 1: //Safe Minify

			// Replace all whitespace characters between tags with a single space
			$html = preg_replace("`>\s+<`", "> <", $html);
		break;

		case 2: //Extreme Minify

			// Placeholder to save conditional comment hack, <pre> and <code> tags
			$place_holders = array(
				'<!-->' => '_!--_',
			);

			//Set placeholders
			$html = strtr($html, $place_holders);

			// Remove all normal comments - save conditionals
			$html = preg_replace('/<!--[^(\[|(<!))](.*)-->/Uis', '', $html);

			// Replace all whitespace characters with a single space
			$html = preg_replace("`\s+`", " ", $html);

			// Remove the spaces between adjacent html tags
			$html = preg_replace("`> <`", "><", $html);

			// Replace space between adjacent a tags for readability
			$html = str_replace("</a><a", "</a> <a", $html);

			// Restore placeholders
			$html = strtr($html, array_flip($place_holders));
		break;

		case 3: //Extreme, save pre and code tags
			// Placeholder to save conditional comment hack, <pre> and <code> tags
			$place_holders = array(
				'<!-->' => '_!--_',
				'<pre>' => '_pre_',
				'</pre>' => '_/pre_',
				'<code>' => '_code_',
				'</code>' => '_/code_'
			);

			//Set placeholders
			$html = strtr($html, $place_holders);

			// Remove all normal comments - save conditionals
			$html = preg_replace('/<!--[^(\[|(<!))](.*)-->/Uis', '', $html);

			// Replace all whitespace characters with a single space
			$html = preg_replace(">`\s+`<", "> <", $html);

			// Remove the spaces between adjacent html tags
			$html = preg_replace("`> <`", "><", $html);

			// Replace space between adjacent a tags for readability
			$html = str_replace("</a><a", "</a> <a", $html);

			// Restore placeholders
			$html = strtr($html, array_flip($place_holders));

		break;

		case 4: //Extreme minify, save comments

			// Replace all whitespace characters with a single space
			$html = preg_replace("`\s+`", " ", $html);

			// Remove spaces between adjacent html tags
			$html = preg_replace("`> <`", "><", $html);

			// Restore space between ajacent a tags
			$html = str_replace("</a><a", "</a> <a", $html);
		break;
	}

	//Normalize ampersands
	//$html = str_replace("&amp;", "&", $html);
	//$html = str_replace("&", "&amp;", $html);

	//Replace common entities with more compatible versions
	$replace = array(
		'&nbsp;' => '&#160;',
		'&copy;' => '&#169;',
		'&acirc;' => '&#226;',
		'&cent;' => '&#162;',
		'&raquo;' => '&#187;',
		'&laquo;' => '&#171;',
		'og:http' => 'og: http'
	);

	//$html = strtr($html, $replace);

	//Return minified html
	return $html;
}
function _ampify_img ($html) {
		preg_match_all("#<img(.*?)\\/?>#", $html, $img_matches);

		foreach ($img_matches[1] as $key => $img_tag) {
		  preg_match_all('/(alt|src|width|height)=["\'](.*?)["\']/i', $img_tag, $attribute_matches);
		  $attributes = array_combine($attribute_matches[1], $attribute_matches[2]);

		  if (!array_key_exists('width', $attributes) || !array_key_exists('height', $attributes)) {
			if (array_key_exists('src', $attributes)) {
			  list($width, $height) = getimagesize($attributes['src']);
			  $attributes['width'] = $width;
			  $attributes['height'] = $height;
			}
		  }

		  $amp_tag = '<amp-img ';
		  foreach ($attributes as $attribute => $val) {
			$amp_tag .= $attribute .'="'. $val .'" ';
		  }

		  $amp_tag .= 'layout="responsive"';
		  $amp_tag .= '>';
		  $amp_tag .= '</amp-img>';

		  $html = str_replace($img_matches[0][$key], $amp_tag, $html);
		}

		return $html;
	}
?>
