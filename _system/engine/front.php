<?php
final class Front {
	protected $registry;
	protected $pre_action = array();
	protected $error;

	public function __construct($registry) {
		$this->registry = $registry;
		// LANGUAGE_CODE_IN_URL start
		if (isset($this->registry->get('request')->get['_route_']) && empty($this->registry->get('request')->post['language_code'])) {
			$languages = array();

			$query = $this->registry->get('db')->query("SELECT * FROM " . DB_PREFIX . "language WHERE status = '1'");

			foreach ($query->rows as $result) {
				$languages[strtolower($result['code'])] = $result;
			}

			$uri_parts = explode('/', $this->registry->get('request')->get['_route_']);

			foreach ($uri_parts as $key => $part) {
				if ($part && isset($languages[strtolower($part)])) {
					if (strtolower($part) != strtolower($this->registry->get('session')->data['language'])) {
						$language_data = $languages[strtolower($part)];
						$code = $language_data['code'];

						setcookie('language', $code, time() + 60 * 60 * 24 * 30, '/', $this->registry->get('request')->server['HTTP_HOST']);
						$this->registry->get('session')->data['language'] = $code;

						$this->registry->get('config')->set('config_language_id', $language_data['language_id']);
						$this->registry->get('config')->set('config_language', $code);

						$language = new Language($language_data['directory']);
						$language->load($language_data['filename']);
						$this->registry->set('language', $language);
					}

					unset($uri_parts[$key]);

					break;
				}
			}

			$this->registry->get('request')->get['_route_'] = implode('/', $uri_parts);

			if ($this->registry->get('request')->get['_route_'] == 'index.php') {
				unset($this->registry->get('request')->get['_route_']);
			}
		}
		// LANGUAGE_CODE_IN_URL end
	}

	public function addPreAction($pre_action) {
		$this->pre_action[] = $pre_action;
	}

	public function dispatch($action, $error) {
		$this->error = $error;

		foreach ($this->pre_action as $pre_action) {
			$result = $this->execute($pre_action);

			if ($result) {
				$action = $result;

				break;
			}
		}

		while ($action) {
			$action = $this->execute($action);
		}
	}

	private function execute($action) {
		if (file_exists($action->getFile())) {
			require_once($action->getFile());

			$class = $action->getClass();

			$controller = new $class($this->registry);

			if (is_callable(array($controller, $action->getMethod()))) {
				$action = call_user_func_array(array($controller, $action->getMethod()), $action->getArgs());
			} else {
				$action = $this->error;

				$this->error = '';
			}
		} else {
			$action = $this->error;

			$this->error = '';
		}

		return $action;
	}
}
?>
