<?php
class ModelModuleNewsletter extends Model {
	public function checkmailid($data) {

	   $query=$this->db->query("SELECT * FROM " . DB_PREFIX . "subscribe where email='".$this->db->escape($data['subscribe_email'])."'");
	   return $query->num_rows;
	}

	public function checkmaildaily($data) {

	   $query=$this->db->query("SELECT * FROM " . DB_PREFIX . "subscribe where email='".$data['daily_email']."' AND type='Đại lý'");
	   return $query->num_rows;
	}

	public function subscribe($data) {

		$this->db->query("INSERT INTO " . DB_PREFIX . "subscribe SET email='".$this->db->escape($data['subscribe_email'])."', date_added = NOW(), ip = '". $_SERVER['REMOTE_ADDR'] ."'");
	}

	public function adddaily($data) {

		$this->db->query("INSERT INTO " . DB_PREFIX . "subscribe SET email='".$data['daily_email']."', date_added = NOW(), name='".$this->db->escape($data['daily_name']) . "', phone='".$this->db->escape($data['daily_phone'])."', type='Đại lý', ip = '". $_SERVER['REMOTE_ADDR'] ."'");
		if ($this->config->get('config_email') != '') {
				$subject = 'Yêu cầu trở thàng đại lý từ website '. $this->config->get('config_name')[$this->config->get('config_language_id')];
				$content_notify  = '<b>Tên người gửi:</b> ' .  $data['daily_name'];
				$content_notify .= '<br /><b>Điện thoại:</b> ' . 	$data['daily_phone'];
				$content_notify .= '<br /><b>Thư điện tử:</b> ' .   $data['daily_email'];
				//$content_notify .= '<br /><b>Nội dung: </b><br />'. html_entity_decode($question, ENT_QUOTES, 'UTF-8');
				$content_notify .= '<br/><br/>Đây là email tự động mang tính chất thông báo.';

				$mail = new Mail();
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->hostname = $this->config->get('config_smtp_host');
				$mail->username = $this->config->get('config_smtp_username');
				$mail->password = $this->config->get('config_smtp_password');
				$mail->port = $this->config->get('config_smtp_port');
				$mail->timeout = $this->config->get('config_smtp_timeout');
				$mail->setTo($this->config->get('config_email'));
				$mail->setFrom($this->config->get('config_mail_from'));
				$mail->setSender($this->config->get('config_name')[$this->config->get('config_language_id')]);
				$mail->setSubject($subject);
				$mail->setHtml($content_notify);
				$mail->send();
	 }
	}

	public function unsubscribe($token) {

		$this->db->query("DELETE FROM " . DB_PREFIX . "subscribe WHERE token='".$token."'");
	}
	public function unsubscribe_($email) {

		$this->db->query("INSERT INTO " . DB_PREFIX . "unsubscribe SET email='".$email."'");
	}
}
?>
