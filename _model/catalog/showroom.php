<?php

class ModelCatalogShowroom extends Model {

    public function getAreas() {
        $sql = "SELECT * FROM " . DB_PREFIX . "area a LEFT JOIN " . DB_PREFIX . "area_description ad ON (a.area_id=ad.area_id) WHERE ad.language_id = '" . (int) $this->config->get('config_language_id') . "'";
        $sql .= " ORDER BY a.sort_order ASC, ad.name ASC";
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getProvinceByArea($area_id) {
        $sql = "SELECT * FROM `" . DB_PREFIX . "province` p LEFT JOIN " . DB_PREFIX . "province_description pd ON (p.provinceid=pd.province_id) WHERE pd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND p.provinceid IN (SELECT province_id FROM " . DB_PREFIX . "dealer WHERE province_id <>0)";
        if (!empty($area_id)) {
            $sql .= " AND `area_id`='" . $area_id . "'";
            $sql .= " ORDER BY p.sort_order ASC, pd.name ASC";
        }
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getNameProvinceById($province_id) {
        $sql = "SELECT * FROM `" . DB_PREFIX . "province` p LEFT JOIN " . DB_PREFIX . "province_description pd ON (p.provinceid=pd.province_id) WHERE pd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND (p.`provinceid`='" . $province_id . "' OR p.`provinceid` ='0')";
        // $sql = "SELECT * FROM `" . DB_PREFIX . "province` WHERE `provinceid`='".$province_id."'";
        $query = $this->db->query($sql);
        return $query->row;
    }

    public function getNameProvinceByDistrict($district_id) {
        $sql = "SELECT * FROM `" . DB_PREFIX . "province` p LEFT JOIN " . DB_PREFIX . "province_description pd ON (p.provinceid=pd.province_id) WHERE pd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND p.`provinceid` in (SELECT province_id FROM `" . DB_PREFIX . "district` p LEFT JOIN " . DB_PREFIX . "district_description pd ON (p.district_id=pd.district_id) WHERE pd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND p.`district_id`='" . $district_id . "')";


        // $sql = "SELECT province_id FROM `" . DB_PREFIX . "district` p LEFT JOIN " . DB_PREFIX . "district_description pd ON (p.district_id=pd.district_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.`district_id`='".$district_id."'";
        // $sql = "SELECT * FROM `" . DB_PREFIX . "province` WHERE `provinceid`='".$province_id."'";
        $query = $this->db->query($sql);
        return $query->row;
    }

    public function getNameDistrictById($district_id) {
        $sql = "SELECT * FROM `" . DB_PREFIX . "district` p LEFT JOIN " . DB_PREFIX . "district_description pd ON (p.district_id=pd.district_id) WHERE pd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND p.`district_id`='" . $district_id . "'";
        // $sql = "SELECT * FROM `" . DB_PREFIX . "province` WHERE `provinceid`='".$province_id."'";
        $query = $this->db->query($sql);
        return $query->row;
    }

    public function getDealerByDistrict($district_id) {
    // public function getDealerByProvince($district_id)
        $sql = "SELECT * FROM `" . DB_PREFIX . "dealer` d LEFT JOIN " . DB_PREFIX . "dealer_description dd ON (d.dealer_id=dd.dealer_id) WHERE dd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND  d.district_id='" . $district_id . "'";
        $sql .= " ORDER BY d.sort_order ASC, dd.name ASC";
        // var_dump($sql);
        $query = $this->db->query($sql);
        return $query->rows;
        /* return $sql; */
    }

    public function getDealerByProvinceID($data) {
        $sql = "SELECT * FROM `" . DB_PREFIX . "dealer` d LEFT JOIN " . DB_PREFIX . "dealer_description dd ON (d.dealer_id=dd.dealer_id) WHERE dd.language_id = '" . (int) $this->config->get('config_language_id') . "'";

    		if(!empty($data['province_id'])){
    			$sql .= " AND (d.province_id='" . $data['province_id'] . "' OR d.province_id=0)";
    		}

        if(!empty($data['district_id'])){
    			$sql .= " AND (d.district_id='" . $data['district_id'] . "')";
    		}

        if(!empty($data['brand'])){
          $sql .= " AND dd.name like '%". $this->db->escape(utf8_strtolower($data['brand'])) ."%'";
        }

        if(!empty($data['man_id'])){
            $sql .= " AND FIND_IN_SET(". (int)$data['man_id'] . ", d.product )";
        }
        $sql .= " ORDER BY d.sort_order ASC, dd.name ASC";

		    if (!isset($data['sitemap']) && (isset($data['start']) || isset($data['limit']))) {
            if (isset($data['start']) < 0) {
                $data['start'] = 0;
            }
            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getTotalDealerByProvinceID($data) {
        $sql = "SELECT COUNT(*) as total FROM `" . DB_PREFIX . "dealer` d LEFT JOIN " . DB_PREFIX . "dealer_description dd ON (d.dealer_id=dd.dealer_id) WHERE dd.language_id = '" . (int) $this->config->get('config_language_id') . "'";

		if(!empty($data['province_id'])){
			$sql .= " AND  d.province_id='" . $data['province_id'] . "'";
		}
        if(!empty($data['brand'])){
          $sql .= " AND dd.name like '%". $this->db->escape(utf8_strtolower($data['brand'])) ."%'";
        }
        if(!empty($data['man_id'])){
            $sql .= " AND FIND_IN_SET(". (int)$data['man_id'] . ", d.product )";
        }

        $query = $this->db->query($sql);
        return $query->row['total'];
    }

    public function getDistrictByProvince($province_id) {
        $sql = "SELECT * FROM `" . DB_PREFIX . "district` d LEFT JOIN " . DB_PREFIX . "district_description dd ON (d.district_id=dd.district_id) WHERE dd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND  d.province_id='" . $province_id . "'";
        $sql .= " ORDER BY d.sort_order ASC, dd.name ASC";
        // print_r($sql);
        $query = $this->db->query($sql);
        return $query->rows;
        /* return $sql; */
    }

    public function getProvinceByLocal($area_id) {
        $sql = "SELECT * FROM `" . DB_PREFIX . "province` WHERE `area_id`=" . $area_id;
        $query = $this->db->query($sql);
        return $query->rows;
    }

    // public function getDistrictByProvince($province_id)
    // {
    // $sql = "SELECT * FROM `" . DB_PREFIX . "district` WHERE `provinceid`=".$province_id;
    // $query = $this->db->query($sql);
    // return $query->rows;
    // }




    public function getProvinces() {
        $sql = "SELECT * FROM " . DB_PREFIX . "province WHERE provinceid IN (SELECT province_id FROM " . DB_PREFIX . "dealer WHERE province_id <>0)";
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getTotalDealer($data = array()) {
        $sql = "SELECT COUNT(*) as total FROM ever_dealer dl LEFT JOIN ever_district dt ON (dl.district_id=dt.district_id) LEFT JOIN ever_province pr ON (dt.province_id=pr.provinceid) WHERE 1 ";
        if (isset($data['province_id'])) {
            $sql .= " AND pr.provinceid = '" . $data['province_id'] . "'";
        }
        if (isset($data['district_id'])) {
            $sql .= " AND dt.district_id = '" . $data['district_id'] . "'";
        }
        if(!empty($data['man_id'])){
            $sql .= " AND FIND_IN_SET(". (int)$data['man_id'] . ", d.product )";
        }
        $query = $this->db->query($sql);
        return $query->row['total'];
    }

}

?>
