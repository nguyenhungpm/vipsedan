<?php
class ModelDesignBanner extends Model {
	public function getBanner($banner_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "banner_image bi INNER JOIN " . DB_PREFIX . "banner_image_description bid ON (bi.banner_image_id  = bid.banner_image_id) WHERE bi.banner_id = '" . (int)$banner_id . "' AND bid.language_id = '" . (int)$this->config->get('config_language_id') . "' AND bid.image != 'no_image.jpg' AND bid.image != '' " . "");

		return $query->rows;
	}

	public function getPicture($picture_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "picture_image bi INNER JOIN " . DB_PREFIX . "picture_image_description bid ON (bi.picture_image_id  = bid.picture_image_id) WHERE bi.picture_id = '" . (int)$picture_id . "' AND bid.language_id = '" . (int)$this->config->get('config_language_id') . "' AND bid.image != 'no_image.jpg' AND bid.image != '' " . "");

		return $query->rows;
	}
	public function getVideos() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "video_image_description bid WHERE bid.language_id = '" . (int)$this->config->get('config_language_id') . "' AND bid.link != ''");

		return $query->rows;
	}

	public function getPictureCat($picture_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "picture p WHERE picture_id = '". $picture_id ."'");

		return $query->row;
	}

	public function getAllCat(){
		$query = $this->db->query("SELECT *, (SELECT image FROM " . DB_PREFIX . "picture_image_description WHERE language_id=2 AND picture_id=p.picture_id AND image <> '' LIMIT 1 ) as image FROM " . DB_PREFIX . "picture p WHERE p.status = 1 ORDER BY p.sort_order DESC, p.date_added DESC");

		return $query->rows;
	}
}
?>
