<?php echo $header; ?>
<div class="rowgutter breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
	<div class="container">
		<?php foreach ($breadcrumbs as $key=>$breadcrumb) { ?>
			<?php echo $breadcrumb['separator']; ?><span itemprop="itemListElement" itemscope
			itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo $breadcrumb['href']; ?>"><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
				<meta itemprop="position" content="<?php echo $key+1; ?>" />
			</a></span>
		<?php } ?>
	</div>
</div>
<div class="container">
	<div class="clear row searchpage">
		<div class="col-12 col-tablet-8 border-right rowgutter">
			<h1><?php echo $heading_title; ?></h1>
			<p>
				<?php if ($search) { ?>
					<input type="text" name="search" id="searchkeywordinput" onkeydown="search(event, 'searchkeywordinput');" value="<?php echo $search; ?>" />
					<?php } else { ?>
					<input type="text" name="search" id="searchkeywordinput" onkeydown="search(event, 'searchkeywordinput');" value="<?php echo $search; ?>" placeholder="<?php echo $text_keyword; ?>"  />
				<?php } ?>
			</p>
			<br />
			<span onclick="searchDesk('searchkeywordinput');" class="button"><?php echo $button_search; ?></span>
			<?php if (!empty($search_rs)) { ?>
                <?php foreach ($search_rs as $news_item) { ?>
                    <div class="clear">
						<h3 class="h6 strong"><a href="<?php echo $news_item['href']; ?>"><?php echo $news_item['name']; ?></a></h3>
						<span class=""><?php echo $news_item['type']; ?></span><br />
						<div class=""><?php echo $news_item['short_description']; ?></div>
					</div>
				<?php } ?>
                <div class="pagination">
                    <?php echo $pagination; ?>
				</div>
			<?php }else { ?>
				<?php echo $text_empty; ?>
			<?php } ?>
		</div>
		<div class="col-12 col-tablet-4">
			<?php echo $column_right; ?>
		</div>
	</div>
</div>
<?php echo $footer; ?>
