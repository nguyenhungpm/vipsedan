<?php echo $header; ?>
<div class="rowgutter breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
	<div class="container">
		    <?php foreach ($breadcrumbs as $key=>$breadcrumb) { ?>
		    	<?php echo $breadcrumb['separator']; ?><span itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo $breadcrumb['href']; ?>"><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
		    	    <meta itemprop="position" content="<?php echo $key+1; ?>" />
		    	</a></span>
		    <?php } ?>
	</div>
</div>
<div class="container">
	<div class="row clearfix">
		<div class="col-12 col-tablet-3 rowgutter">
				<?php echo $column_left; ?>
		</div>
		<div class="col-12 col-tablet-9">
			<h1 class="n-mt rowgutter color-black"><?php echo $heading_title; ?></h1>
			<?php if ($products) { ?>
			<div class="row">
			<?php foreach($products as $product){ ?>
			<article class="col-12 col-tablet-4">
				<div class="post-inner">
					<div class="entry-thumb">
						<a class="img" href="<?php echo $product['href'];?>" title="<?php echo $product['name'];?>"><img src="<?php echo $product['thumb'];?>" alt="<?php echo $product['name'];?>"/>

							<div class="caption scale-caption">
								<p class="caption-description"><?php echo $product['description'];?> </p>
							</div>
						</a></div>
					<div class="entry-content">
						<h3 class="entry-title"><a href="<?php echo $product['href'];?>"><?php echo $product['name'];?></a></h3>
						<div class="custom">
							<div class="price-valibar">Giá : <?php echo $product['price']; ?></div>
							<div class="date-time"><?php echo $product['note']; ?></div>
						</div>
					</div>
				</div>
			</article>
			<?php } ?>
			</div>
			<div class="pagination col-12"><?php echo $pagination; ?></div>
			<?php } ?>
		</div>
	</div>
</div>
<?php echo $footer; ?>
