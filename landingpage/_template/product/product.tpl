<?php echo $header; ?>
	<div id="main" class="wrapper">
        <div id="primary" class="site-content">
            <div id="content" role="main">

                <article id="post-126" class="post-126 post type-post status-publish format-standard sticky hentry category-xe">
                    <header class="entry-header">
						<h1 class="entry-title"><a href="<?php echo $url; ?>" rel="bookmark"><?php echo $heading_title; ?></a></h1>
                    </header>
                    <!-- .entry-header -->

                    <div class="entry-content">
                        <?php echo $description; ?>
                    </div>
                    <!-- .entry-content -->

                    <!-- .entry-meta -->
                </article>
                <!-- #post -->

            </div>
            <!-- #content -->
        </div>
        <!-- #primary -->

        <div id="secondary" class="widget-area" role="complementary">
			<?php echo $column_right_lp; ?>
            
        </div>
        <!-- #secondary -->
    </div>
<?php echo $footer; ?>
