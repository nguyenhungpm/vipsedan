<?php echo $header; ?>
  <div class="container">
    <div class="row clear">
      <div class="col-12 col-tablet-9">
    <div class="center">
      <h1><?php echo $heading_title; ?></h1>
    </div>

  <div class="middle">

    <?php if (true/*$testimonials*/) { ?>
    
      <?php foreach ($testimonials as $testimonial) { ?>
      <table class="col-12 col-tablet-12 " width="100%" border=0>
      <tr>
         <td valign="top" style="text-align:left;" colspan="2"><b><?php echo $testimonial['title']; ?></b></td>
      </tr>
      <tr>
      	<td colspan="2" style="text-align:left;">
                <?php echo $testimonial['description']; ?>
            </td>
      </tr>    

     <tr>
		<td style="font-size: 0.9em; text-align: right;">
                <?php if ($testimonial['rating']) { ?>
                <?php echo $text_average; ?><br>
                  <img src="static/stars-<?php echo $testimonial['rating'] . '.png'; ?>" style="margin-top: 2px;" />
                  <?php } ?><br>
-&nbsp;<i><?php echo $testimonial['name'].' - '.$testimonial['city'].' '.$testimonial['date_added']; ?></i>
             </td>
        </td>
      </tr>

	</table>
      <?php } ?>

    	<?php if ( isset($pagination)) { ?>
    		<div class="pagination"><?php echo $pagination;?></div>
    		
    	<?php }?>

    	<?php if (isset($showall_url)) { ?>
    		<div class="buttons" align="right"><a class="button" href="<?php echo $showall_url;?>" title="<?php echo $showall;?>"><span><?php echo $showall;?></span></a></div>
    	<?php }?>
    <?php } ?>
  </div>
</div>
<div class="col-12 col-tablet-3 rowgutter"> 
    <?php echo $column_right; ?>
  </div>
  </div>
  </div>
  
  </div>
<?php echo $footer; ?> 