<?php echo $header; ?>
<div class="rowgutter breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
	<div class="container">
		    <?php foreach ($breadcrumbs as $key=>$breadcrumb) { ?>
		    	<?php echo $breadcrumb['separator']; ?><span itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo $breadcrumb['href']; ?>"><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
		    	    <meta itemprop="position" content="<?php echo $key+1; ?>" />
		    	</a></span>
		    <?php } ?>
	</div>
</div>
<div class="clear row">
	<div class="col-12 col-tablet-8 border-right">
	<?php if ($news) { ?>
		<?php $k=0; foreach ($news as $news_item) { ?>
		<div class="clear news-item relative">
			<a class="col-4" href="<?php echo $news_item['href']; ?>"><img src="<?php echo $news_item['thumb']; ?>" alt="<?php echo $news_item['name']; ?>" /></a>
			<div class="col-8">
			  <h3 class="n--m"><a href="<?php echo $news_item['href']; ?>" class="color-black"><?php echo $news_item['name']; ?></a></h3>
			  <div class="h5 grey"><i class="icn i-clock" aria-hidden="true"></i> <?php echo $news_item['date_added']; ?> &nbsp; <i class="icn i-eye" aria-hidden="true"></i>  <?php echo $news_item['viewed']; ?> <?php echo $text_viewed; ?></div>
			  <div class=""><?php echo $news_item['short_description']; ?></div>
			  <div class="text-right"><a href="<?php echo $news_item['href']; ?>" class="color-green"><?php echo $text_readmore; ?></a></div>
			  </div>
		</div>
		<?php $k++; } ?>
		<div class="col-12"><div class="line"></div></div>
		<div class="tool col-12">
			<div class="pagination">
				<?php echo $pagination; ?>
			</div>
		</div>
	<?php } ?>
	<?php if (!$news) { ?>
		<div class="content"><?php echo $text_empty; ?></div>
	<?php } ?>
	</div>
	<div class="col-12 col-tablet-4">
	<?php echo $column_right; ?>
	</div>
</div>
<?php echo $footer; ?>
