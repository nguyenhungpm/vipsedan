<?php echo $header; ?>
<div class="rowgutter breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
	<div class="container">
		    <?php foreach ($breadcrumbs as $key=>$breadcrumb) { ?>
		    	<?php echo $breadcrumb['separator']; ?><span itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo $breadcrumb['href']; ?>"><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
		    	    <meta itemprop="position" content="<?php echo $key+1; ?>" />
		    	</a></span>
		    <?php } ?>
	</div>
</div>

<div class="container">
<div class="clear row">
	<div class="col-12 col-tablet-8 border-right">
	<h1 class="h1 color-black"><?php echo $heading_title; ?><?php echo $admin_logged ? ' (<a target="_blank" href="'.$edit.'">Sửa</a>)': ''?></h1>
    <div class="grey h5 date"><i class="icn i-clock"></i> <?php echo $date_added; ?> &nbsp; <i class="icn i-eye"></i> <?php echo $viewed; ?> <?php echo $text_viewed; ?></div>
<div class='movie_choice'>
<?php if($rate['total_rate']==0){
   $rate['total_rate'] = 1;
   $rate['total_point'] = 5;
}?>
    <div id="r1" class="rate_widget" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
        <div id="s1" onclick="rate('1','news','<?php echo $news_id; ?>')" class="star_1 ratings_stars <?php echo (round($average,0) >= 1) ? 'ratings_vote':''?>"></div>
        <div id="s2" onclick="rate('2','news','<?php echo $news_id; ?>')" class="star_2 ratings_stars <?php echo (round($average,0) >= 2) ? 'ratings_vote':''?>"></div>
        <div id="s3" onclick="rate('3','news','<?php echo $news_id; ?>')" class="star_3 ratings_stars <?php echo (round($average,0) >= 3) ? 'ratings_vote':''?>"></div>
        <div id="s4" onclick="rate('4','news','<?php echo $news_id; ?>')" class="star_4 ratings_stars <?php echo (round($average,0) >= 4) ? 'ratings_vote':''?>"></div>
        <div id="s5" onclick="rate('5','news','<?php echo $news_id; ?>')" class="star_5 ratings_stars <?php echo (round($average,0) >= 5) ? 'ratings_vote':''?>"></div>
        <span id="point_rs" class="small inline-block">Có <span itemprop="reviewCount"><?php echo $rate['total_rate']; ?></span> bầu chọn /
                điểm trung bình: <span itemprop="ratingValue"><?php echo round($average,1); ?></span></span>
    </div>
	<div id="total_vote"></div>
	<input type="hidden" id="point" value=""/>
</div>

	  <h2 class="short-desc strong h4" itemprop="description"><?php echo $short_description; ?></h2>
	  <div class="newscontent">
		  <?php echo $description; ?>
	  </div>
	  <div class="shareme">
	    	<a href="https://twitter.com/share?url=<?php echo $url; ?>" target="_blank" class="twitter-icon rowgutter inline-block" rel="nofollow"><i class="icn i-scl-twitter"></i> Twitter Share</a>
	    	<a href="https://plus.google.com/share?url=<?php echo $url; ?>" target="_blank" class="plus-icon rowgutter inline-block" rel="nofollow"><i class="icn i-plus"></i> Google+ Share</a>
	    	<a href="http://facebook.com/share.php?u=<?php echo $url; ?>" target="_blank" class="face-icon rowgutter inline-block" rel="nofollow"><i class="icn i-scl-facebook"></i> Facebook Share</a>
	  </div>
		<?php echo $content_bottom; ?>
	  <?php if ($other_news) { ?>
	   <div class="rowgutter">
		  <h3 class="h4 n--m em"><?php echo $tab_others; ?></h3>
	      	<ul class="n--m">
					<?php foreach ($other_news as $other_news_item) { ?>
						<li class=""><a class="color-black" href="<?php echo $other_news_item['href'];?>"><?php echo $other_news_item['name'];?></a> <em>(<?php echo $other_news_item['date_added'];?>)</em></li>
			    <?php } ?>
			</ul>
	   </div>
	   <?php } ?>
   </div>
	<div class="col-12 col-tablet-4">
		<?php if ($news) { ?>
	      <div class="othernews">
			  	<h3 class="title"><?php echo $tab_related; ?></h3>
		      <ul>
		      	<?php foreach ($news as $related_news) { ?>
		      		<li><a href="<?php echo $related_news['href']; ?>"><?php echo $related_news['name']; ?></a> <span class="small grey">(<?php echo $related_news['date_added']; ?>)</span></li>
		      	<?php } ?>
		     	</ul>
				</div>
	   <?php } ?>
	   <?php echo $column_right; ?>
	</div>
</div>
</div>
<?php echo $footer; ?>
