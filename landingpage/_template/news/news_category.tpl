<?php echo $header; ?>
<div class="small breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
	<div class="container">
		    <?php foreach ($breadcrumbs as $key=>$breadcrumb) { ?>
		    	<?php echo $breadcrumb['separator']; ?><span itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo $breadcrumb['href']; ?>"><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
		    	    <meta itemprop="position" content="<?php echo $key+1; ?>" />
		    	</a></span>
		    <?php } ?>
	</div>
</div>
<div class="container">
<div class="row clear">
<div class="content">
	<div class="col-12 col-tablet-8 border-right">
    <h1 class="childMainHead"><?php echo $heading_title; ?></h1>
    <?php if ($news) { ?>
		<div class="media-list">
		<?php foreach ($news as $news_item) { ?>
		<div class="media row clear">
			  <div class="col-4">
					<a href="<?php echo $news_item['href']; ?>"><img src="<?php echo $news_item['thumb']; ?>" alt="<?php echo $news_item['name']; ?>" class="block" /></a>
			  </div>
			  <div class="col-8">
				  <h3 class="n--m"><a href="<?php echo $news_item['href']; ?>" class="bluetext title-news"><?php echo $news_item['name']; ?></a></h3>
				  <div class="gutter grey h5"><i class="icn i-clock"></i> <?php echo $news_item['date_added']; ?> &nbsp; <i class="icn i-eye"></i> <?php echo $news_item['viewed']; ?> <?php echo $text_viewed; ?></div>
				  <div class="description hide-mobile show-tablet grey"><?php echo $news_item['short_description']; ?></div>
				  <a href="<?php echo $news_item['href']; ?>" class="bg-green color-white readmore"><?php echo $text_readmore; ?> &#x2192;</a>
			  </div>
		</div>
		<?php } ?>
		</div>
		<div class="tool">
			<div class="pagination">
				<?php echo $pagination; ?>
			</div>
		</div>
	<?php } ?>
	<?php if (!$news) { ?>
		<div class="content"><?php echo $text_empty; ?></div>
	<?php } ?>
	</div>
	<div class="col-12 col-tablet-4">
		<?php echo $column_right; ?>
	</div>

</div>
</div>
</div>
<?php echo $footer; ?>
