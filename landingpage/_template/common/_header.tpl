<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" prefix="og: http://ogp.me/ns#" class="no-js">
<head>
<meta charset="UTF-8" />
<title><?php echo $title; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<base href="<?php echo $base; ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<?php if ($icon) { ?>
	<link href="<?php echo $icon; ?>" rel="icon" />
<?php } ?>
<?php if (isset($google_verify)) { ?>
	<?php echo $google_verify; ?>
<?php } ?>
<?php if (isset($script_header)) { ?>
	<?php echo $script_header; ?>
<?php } ?>
<?php foreach ($metanames as $metaname) { ?>
	<meta name="<?php echo $metaname['name']; ?>" content="<?php echo $metaname['content']; ?>" />
<?php } ?>
<?php foreach ($metapros as $metapro) { ?>
	<meta property="<?php echo $metapro['property']; ?>" content="<?php echo $metapro['content']; ?>" />
<?php } ?>
<link href="<?php echo $opensearch; ?>" rel="search" type="application/opensearchdescription+xml" title="<?php echo $name; ?>" />
<?php foreach ($links as $link) { ?>
	<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<link rel="stylesheet" type="text/css" href="asset/style.css" media="screen" />
<link rel='stylesheet' id='twentytwelve-fonts-css' href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700&#038;subset=latin,latin-ext' type='text/css' media='all' />
<link rel="stylesheet" type="text/css" href="asset/ex_style.css" media="screen" />
<link rel="stylesheet" type="text/css" href="static/print.css" media="print" />
<link rel="stylesheet" type="text/css" href="static/icn.css" />
<?php foreach ($styles as $style) { ?>
	<link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<style type="text/css"><?php echo $custom_css; ?></style>
<style type="text/css" id="custom-background-css">
    body.custom-background {
        background-color: #128299;
    }
</style>
<?php if($copy_protect==1){ ?>
	<script language="javascript">
	function Disable_Control() {
		var keystroke = String.fromCharCode(event.keyCode).toLowerCase();
		if (event.ctrlKey && (keystroke == 'c' || keystroke == 'v' || keystroke == 'x' || keystroke == 'a')) {
		alert("No Copying Allowed!");
		event.returnValue = false;
		}
	}
	</script>
	<script language="javascript">
	var message="No Right Clicking Allowed!";
	function defeatIE() {if (document.all) {(message);return false;}}
	function defeatNS(e) {if
	(document.layers||(document.getElementById&&!document.all)) {
	if (e.which==2||e.which==3) {(message);return false;}}}
	if (document.layers)
	{document.captureEvents(Event.MOUSEDOWN);document.onmousedown=defeatNS;}
	else{document.onmouseup=defeatNS;document.oncontextmenu=defeatIE;}
	document.oncontextmenu=new Function("return false")
	</script>
<?php } ?>
</head>
<body itemscope="itemscope" itemtype="http://schema.org/WebPage"
	class="home blog custom-background custom-font-enabled single-author <?php echo $class; ?>"
	<?php if($copy_protect==1){ ?>
			ondragstart="return false" onselectstart="return false" oncopy="return false" oncut="return false" onpaste="return false" onkeydown="javascript:Disable_Control()"
  <?php } ?>
>
	<div id="page" class="hfeed site">
		<header id="masthead" class="site-header" role="banner">
            <hgroup>
                <h1 class="site-title"><a href="/" title="Thuê xe từ Hà Nội đi các tỉnh" rel="home">Thuê xe từ Hà Nội đi các tỉnh</a></h1>
                <h2 class="site-description">Hotline: Mr Cảnh  0982 35 8282</h2>
            </hgroup>

            <nav id="site-navigation" class="main-navigation" role="navigation">
                <button class="menu-toggle">Trình đơn</button>
                <a class="assistive-text" href="#content" title="Chuyển đến nội dung">Chuyển đến nội dung</a>
                <div class="menu-main-mennu-container">
                    <ul id="menu-main-mennu" class="nav-menu">
                        <li id="menu-item-8" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-8"><a href="/">Trang chủ</a></li>
                        <li id="menu-item-267" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-267"><a href="/thue-xe-di-tu-ha-noi-di-hai-phong-5/">thuê xe đi Hải Phòng</a></li>
                        <li id="menu-item-268" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-268"><a href="/thue-xe-du-lich-4-7-16-cho-tu-ha-noi-di-ha-long-0982-35-8282-11/">thuê xe đi Hạ Long</a></li>
                        <li id="menu-item-269" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-269"><a href="/cho-thue-xe-di-sapa-lao-cai-221/">Cho thuê xe đi Sapa</a></li>
                        <li id="menu-item-270" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-270"><a href="/cho-thue-xe-du-lich-tu-ha-noi-di-ninh-binh-139/">thuê xe đi Ninh Bình</a></li>
                        <li id="menu-item-117" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-117"><a href="/lien-he/">Liên hệ</a></li>
                    </ul>
                </div>
            </nav>
            <!-- #site-navigation -->

            <a href="/"><img src="wp-content/uploads/2017/09/thue-xe-du-lich-hanoi.jpg" class="header-image" width="960" height="250" alt="Thuê xe từ Hà Nội đi các tỉnh" /></a>
        </header>
        <!-- #masthead -->

