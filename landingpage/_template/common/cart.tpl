<?php echo $header; ?>
<div itemscope itemtype="http://schema.org/Product">
<div class="container">
<div class="row clear">
	<form action="common/cart" id="form_contact" method="post">
	<div class="col-12 col-tablet-9 zonegutter">
		<h3><?php echo $heading_title; ?></h3>
		<?php if($checkcart){ ?>
		<table class="n--m full-width cart">
			<thead>
			<tr>
				<th>Sản phẩm</th>
				<th class="text-center">Số lượng</th>
				<th class="text-right">Giá tiền</th>
				<th class="text-right">Thành tiền</th>
				<th></th>
			</tr>
			</thead>
			<tbody>
			<?php if($hosting){ ?>
			<?php
			$price = 0;
			$k=0;foreach($hosting as $ht){
				$price += $ht['price'];
				?>
			<tr>
				<td>
					<?php echo $ht['name']; ?><br />
					<input type="hidden" name="product[<?php echo $ht['product_id'];?>]" value="<?php echo $ht['name']; ?>"/>
				</td>
				<td class="text-center">
					<input type="text" name="quantity[<?php echo $ht['product_id'];?>]" value="1" size="2"/>
				</td>
				<td class="text-right">
					<?php echo number_format($ht['price'], 0 ,',','.').' vnđ'; ?>
				</td>
				<td class="text-right">

				</td>
				<td class="text-center">
					<a href="common/cart&type=model&remove=<?php echo $ht['product_id'];?>" class="remove"><i class="icn i-close"></i></a>
				</td>
			</tr>
			<?php $k++;} ?>
			<?php } ?>
			<tr>
				<td colspan="3" class="text-right strong">Tổng tiền:</td>
				<td class="text-right strong color-red" id="price"><?php echo number_format($price, 0, ',','.');?> vnđ</td>
				<td></td>
			</tr>
			</tbody>
		</table>
		<script>
			function calculate(price, register, duytri, id, type, e){
				var pr=0;
				if(type=='domain'){
					document.getElementById(id).value = parseInt(price) + parseInt(register) + parseInt(e.value * duytri);
				}else{
					document.getElementById(id).value = parseInt(price * e.value);
					//alert(e.value);
				}
				for(var k=0;k < <?php echo count($domains); ?>; k++){
					pr += parseInt(document.getElementById('domain'+k).value);
				}
				for(var a=0;a < <?php echo count($hosting); ?>; a++){
					pr += parseInt(document.getElementById('hosting'+a).value);
				}
				//document.getElementById('price').value = pr;
				pr = String(pr).replace(/(.)(?=(\d{3})+$)/g,'$1.')
				document.getElementById('price').innerHTML = pr + ' vnđ';

			}
		</script>
	</div>
	<div class="col-12 col-tablet-3 zonegutter contactpage">
		<h3 class="n--m skytext heading rowgutter">Thông tin của bạn</h3>
		<input type="text" placeholder="Tên của bạn" name="name" value="<?php echo $name; ?>"/>
		<?php echo $error_name ? '<span class="error">'.$error_name.'</span>' : ''; ?>
		<input type="text" placeholder="Số điện thoại" name="telephone" value="<?php echo $telephone; ?>"/>
		<?php echo $error_telephone ? '<span class="error">'.$error_telephone.'</span>' : ''; ?>
		<input type="text" placeholder="Email" name="email" value="<?php echo $email; ?>"/>
		<?php echo $error_email ? '<span class="error">'.$error_email.'</span>' : ''; ?>
		<div class="rowgutter"><a onclick="javascript: document.getElementById('form_contact').submit();" class="round button no-border bg-green color-white uppercase right">Đặt hàng</a></div>
	</div>
	</form>
	<?php }else{ ?>
		Giỏ hàng trống
	<?php } ?>
	<?php echo $content_bottom; ?>
</div>
</div>
</div>
<?php echo $footer; ?>
