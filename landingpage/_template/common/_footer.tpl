<div id="footer" class="bg-blue">
<div class="container">
<div class="clear row">
	<div class="col-6 col-tablet-4 info zonegutter">
		<h3 class="uppercase h4 n-mt color-green strong"><?php echo $store; ?></h3>
		<p class="h5 address"><i class="icn iconr i-location-pin"></i> <?php echo $address;?></p>
		<p class="h5 tel"><i class="icn iconr i-phone"></i> <?php echo $telephone;?></p>
		<p class="h5 email"><i class="icn iconr i-envelope"></i> <?php echo $email2;?></p>
	</div>
	<?php echo $footer_menu; ?>
	<div class="col-12 col-tablet-4 zonegutter">
		<?php echo $bottom; ?>
	</div>
</div>
</div>
</div>
<div id="copy">
	<div class="container h6 color-grey gutter">&copy; <?php echo date("Y");?>, <strong><?php echo $store; ?></strong>. All rights reversed.
	</div>
</div>
<a href="tel:<?php echo $hotline; ?>" class="call-now" rel="nofollow">
	<div class="mypage-alo-phone show-mobile hide-desktop hide-tablet">
		<div class="animated infinite zoomIn mypage-alo-ph-circle"></div>
		<div class="animated infinite pulse mypage-alo-ph-circle-fill"></div>
		<div class="animated infinite tada mypage-alo-ph-img-circle"></div>
	</div>
</a>
<script type="text/javascript" async src="static/js.js"></script>
<?php foreach ($scripts as $script) { ?>
<script type="text/javascript" src="<?php echo $script; ?>"></script>
<?php } ?>
<?php foreach ($exscripts as $exscript) { ?>
<script>
<?php echo $exscript; ?>
</script>
<?php } ?>
<?php if (isset($script_footer)) { ?>
	<?php echo $script_footer; ?>
<?php } ?>
<!--[if lte IE 9]>
	<script type="text/javascript" src="static/placeholder.js"></script>
<![endif]—>
<?php if(!empty($google_analytics)){ ?>
<script src="https://cdn.jsdelivr.net/ga-lite/latest/ga-lite.min.js" async></script> <script>
var galite = galite || {};
galite.UA = '<?php echo $google_analytics; ?>';
</script>
<?php } ?>
</body></html>
