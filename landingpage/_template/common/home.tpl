<?php echo $header; ?>
	<div id="main" class="wrapper">
        <div id="primary" class="site-content">
            <div id="content" role="main">
				<?php echo $content_top_lp; ?>
            </div>
            <!-- #content -->
        </div>
        <!-- #primary -->

        <div id="secondary" class="widget-area" role="complementary">
            <?php echo $column_right_lp; ?>
        </div>
        <!-- #secondary -->
    </div>
    <!-- #main .wrapper -->
<?php echo $footer; ?>