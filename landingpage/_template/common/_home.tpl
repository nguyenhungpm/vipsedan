<?php echo $header; ?>
<div class="body">
	<div class="">
		<?php echo $slideside; ?>
	</div>
	<div class="container">
		<div class="row clear">
			<div class="col-12 rowgutter">
				<div class="relative">
					<form action="common/cart" id="booking_form" method="post">
						<div class="absolute booking-form">
						    <h4>Đặt lịch xe</h4>
						    <div class="row box_row">
						        <div class="col-12 col-tablet-3">Điểm đi</div>
						        <div class="col-12 col-tablet-9"><input type="text" name="destinations" value="" size="30" placeholder="Tên xã / phường" /></div>
						    </div>
						    <div class="row box_row">
						        <div class="col-12 col-tablet-3">Điểm đến</div>
						        <div class="col-12 col-tablet-9"><input type="text" name="origins" value="" size="30" placeholder="Tên xã / phường" /></div>
						    </div>
						    <div class="row box_row">
						        <div class="col-12 col-tablet-3">Thời gian</div>
						        <div class="col-7 col-tablet-5"><input type="text" name="date" value="" size="17" placeholder="Ngày đi" /></div>
						        <div class="col-5 col-tablet-4"><input type="text" name="time" value="" size="13" placeholder="Thời gian đi" /></div>
						    </div>
							<div class="clear">
								<table class="col-12 text-center n--m">
									<tr class="bg-white">
										<th>Xe 4 chỗ</th>
										<th>Xe 7 chỗ</th>
										<th>Xe 16 chỗ</th>
										<th>Xe khác</th>
									</tr>
									<tr>
										<td><label><input type="radio" checked name="number_seats" value="4"/></label></td>
										<td><label><input type="radio" name="number_seats" value="7"/></label></td>
										<td><label><input type="radio" name="number_seats" value="16"/></label></td>
										<td><label><input type="radio" name="number_seats" value="other"/></label></td>
									</tr>
								</table>
							</div>
						    <div class="rowgutter clear n-pb"> 
								<div class="left">
									<span id="result" class="color-white"></span>
								</div>
								<div class="right">
									<a class="cal" onclick="calDistance('booking_form', event)">Tạm tính</a>
								</div>
							</div>
						    <div class="row box_row">
						        <div class="col-12 col-tablet-3">Họ tên</div>
						        <div class="col-12 col-tablet-9"><input type="text" name="name" value="" size="30" placeholder="Họ và tên" /></div>
						    </div>
						    <div class="row box_row">
						        <div class="col-12 col-tablet-3">Điện thoại</div>
						        <div class="col-12 col-tablet-9"><input type="text" name="telephone" value="" size="30" placeholder="Số điện thoại liên lạc" /></div>
						    </div>
					        <div class="row">
					            <div class="col-12">
					                <input type="submit" value="Booking" class="wpcf7-form-control wpcf7-submit"><span class="ajax-loader"></span></div>
					        </div>
						</div>
					</form>
				</div>
				<div class="rt-info">
					<div class="left-support-online">
				        <h4>Hỗ trợ trực tuyến</h4>
				        <p>Hãy gọi chúng tôi để có được tour du lịch yêu thích

				        </p>
				        <div class="icon">
				            <a href="#1"></a>
				            <a href="#2"></a>
				            <a href="#3"></a>
				            <a href="#4"></a>
				        </div>
				    </div>
				    <div class="right-item">

				        <div class="item">
				            <i class="fa fa-phone" aria-hidden="true"></i> TƯ VẤN TOUR :
				            <a href="tel:0964 474 121">0964 474 121</a>
				        </div>

				        <div class="item">
				            <i class="fa fa-phone" aria-hidden="true"></i> TƯ VẤN THUÊ XE :
				            <a href="tel:0964 474 121">0964 474 121</a>
				        </div>

				        <div class="item">
				            <i class="fa fa-phone" aria-hidden="true"></i> TƯ VẤN VÉ MÁY BAY :
				            <a href="tel:0964 474 121">0964 474 121</a>
				        </div>
				    </div>
				</div>
			</div>
			<div class="col-12">
				<?php echo $content_top; ?>
			</div>
			<div class="col-12 col-tablet-3 left">
				<?php echo $column_left; ?>
			</div>
		</div>
		<?php echo $content_bottom; ?>
	</div>
</div>
<?php echo $footer; ?>
