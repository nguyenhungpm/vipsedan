<?php if ($position == 'content_top'  or $position == 'content_bottom') { ?>
<div class="featured clear bg-white">
	<h3 class="col-12 no-margin rowgutter color-black"><?php echo $heading_title; ?></h3>
<?php foreach ($newss as $news) { ?>
	<div class="col-12 col-tablet-3">
		<a href="<?php echo $news['href']; ?>"><img class="col-12 no-padding block" alt="<?php echo $news['name']; ?>" src="<?php echo $news['thumb']; ?>"></a>
		<a class="strong color-black h6" href="<?php echo $news['href']; ?>"><?php echo $news['name']; ?></a>
		<div><?php echo $news['short_description'];?></div>
	</div>
<?php } ?>
</div>
<?php }else{ ?>
<div class="othernews">
	<h3 class="h4 strong no-margin rowgutter em"><?php echo $heading_title; ?></h3>
	<?php foreach ($newss as $news) { ?>
		<div class="clear item">
			<a class="col-3 no-padding" href="<?php echo $news['href'];?>"><img class="block" src="<?php echo $news['thumb'];?>" alt="<?php echo $news['name'];?>"/></a>
			<a class="col-9 color-black no-padding-right" href="<?php echo $news['href'];?>"><?php echo $news['name'];?></a>
		</div>
	<?php } ?>
</div> 
<?php } ?>