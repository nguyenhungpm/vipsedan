<div id="order" class="round clear">
<div class="thumb_order relative">
	<div class="waves absolute"></div>
</div>
<h2 class="h4 strong text-center bluetext2">Đặt hàng</h2>
<p class="medium">Yêu cầu đặt hàng</p>
	<form name="order_form" id="order_form" onsubmit="submitOrderHosting('order_form', event)">
		<div id="frm_order" class="relative">
			<input type="text" value="" name="name" id="name" placeholder="Tên của bạn"><br />
			<div class="required" id="order_formerror1">Bạn chưa điền tên của mình</div>
			<input type="text" value="" name="telephone" id="telephone" placeholder="Điện thoại"><br /> 
			<div class="required" id="order_formerror3">Bạn chưa nhập số điện thoại</div>
			<input type="text" value="" name="email" id="email" placeholder="Email"><br />
			<div class="required" id="order_formerror5">Bạn chưa nhập/ nhập sai địa chỉ email</div>
			<select name="product_id">
				<option>Chọn sản phẩm</option>
				<?php foreach($products as $product){ ?>
				<option value="<?php echo $product['product_id'];?>" <?php echo !empty($this->request->get['product_id']) && $this->request->get['product_id'] == $product['product_id'] ? 'selected':'';?>><?php echo $product['name'];?></option>
				<?php } ?>
			</select>
			<div class="clear"><textarea name="content" id="content" placeholder="Nội dung"></textarea><br />
			<input placeholder="Mã xác nhận" type="text" name="captcha" id="captcha" class="left" /> <img src="information/contact/captcha" alt="captcha" />
			</div> 
			<div class="required" id="order_formerror7">Chưa nhập mã captcha hoặc nhập sai</div>
			<input type="hidden" id="captcha_status" value="0" />
			<div class="clearfix">
				<a onclick="submitOrderHosting('order_form', event)" class="button right">Đăng ký</a>
			</div>
		</div>
	</form>
	<div id="order_result"></div>
</div>