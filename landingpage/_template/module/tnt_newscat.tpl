<?php if ($position == 'content_top'  or $position == 'content_bottom') { ?>
<div class="box box-news-left">
  <h3 class="box-heading"><span><?php echo $cat_name; ?></span></h3>
  <div class="box-content">
    <div class="box-product-left">
      <?php foreach ($newss as $news) { ?>
      <div>
        <?php if ($news['thumb']) { ?>
        <div class="image"><a href="<?php echo $news['href']; ?>"><img src="<?php echo $news['thumb']; ?>" alt="<?php echo $news['name']; ?>" <?php echo $news['width']; ?> <?php echo $news['height']; ?> /></a></div>
        <?php } ?>
        <div class="name"><a href="<?php echo $news['href']; ?>"><?php echo $news['name']; ?></a></div>

         <?php echo $news['short_description'];?>
         <div class="clrfix"></div>
      </div>
      <?php } ?>
    </div>
  </div>
</div>

<?php } else if ($position == 'column_left' or $position == 'column_right') { ?>                            
<div class="box box-news-left">
<h3 class="box-heading"><a href="<?php echo $cathref; ?>"><?php echo $cat_name; ?></a></h3>
<div class="box-content">
    <div class="box-product-left">
      <?php foreach ($newss as $key=>$news) { ?>
      <?php if ($key < $feature) { ?>
	      <div class="newsbycatrow">
	        <?php if ($news['thumb']) { ?>
	        <div class="image"><a href="<?php echo $news['href']; ?>"><img src="<?php echo $news['thumb']; ?>" alt="<?php echo $news['name']; ?>" <?php echo $news['width']; ?> <?php echo $news['height']; ?> /></a></div>
	        <?php } ?>
	        <div class="name"><a href="<?php echo $news['href']; ?>"><?php echo $news['name']; ?></a></div>
	      </div>
      <?php } else { ?>
	     <div class="newsbycatrowsmall">
	       <h3 class="name"><a href="<?php echo $news['href']; ?>"><?php echo $news['name']; ?></a></h3>
	     </div>
      <?php } ?>
      <?php } ?>
  </div>
</div>
</div>
<?php } else if ($position == 'before_footer') {?>
<div class="box-news-cat">
  <h3 class="box-heading"><?php echo $cat_name; ?></h3>
  <div class="box-content">
    <div class="box-product-left">
      <?php foreach ($newss as $news) { ?>
      <div>
        <?php if ($news['thumb']) { ?>
        <div class="image"><a href="<?php echo $news['href']; ?>"><img src="<?php echo $news['thumb']; ?>" alt="<?php echo $news['name']; ?>" <?php echo $news['width']; ?> <?php echo $news['height']; ?> /></a></div>
        <?php } ?>
        <div class="name"><a href="<?php echo $news['href']; ?>"><?php echo $news['name']; ?></a></div>

         <?php echo $news['short_description'];?>
         <div class="clrfix"></div>
      </div>
      <?php } ?>
    </div>
    <a class="orther" href="<?php echo $cathref; ?>">Xem toàn bộ</a>
  </div>
</div>
<?php } ?>