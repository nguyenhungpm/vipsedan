<div class="box">
  <h3 class="title_under"><span><?php echo $heading_title; ?></span></h3>
      <?php foreach ($images as $image) { ?>
    <div class="support-item clearfix">
      <img class="circle avatar left" src="<?php echo $image['avatar']; ?>" alt="<?php echo $image['name']; ?>" />
    	<h5 class="h4 n--m"><?php echo $image['name']; ?></h5>
		    <div class="sitem"><?php echo $text_phone; ?>: <a class="color-green" href="tel:<?php echo $image['phone']; ?>"><?php echo $image['phone']; ?></a></div>
		<?php if($image['facebook']){ ?>
			<a target="_blank" rel="nofollow" href="https://m.me/<?php echo $image['facebook']; ?>" class="facebookchat round inline-block"><i class="faceicon left"></i>Chat Facebook</a>
		<?php } ?>
		<?php if($image['skype']){ ?>
			<a href="skype:<?php echo $image['skype']; ?>?chat"><img class="right" src="static/skype.png"></a>
		<?php } ?>
		<div class="clearfix"></div>
	</div>
      <?php } ?>
</div>
