<?php foreach ($categories as $category) { ?>
<div class="category">
<h2 class="bg-dark color-white uppercase n--m n--p h3 box-heading">
  <i class="icn i-menu" aria-hidden="true"></i> <a class="color-white strong" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></h2>
    <ul class="list-unstyled">
    <?php foreach($category['children'] as $manufacturer){ ?>
      <li><a  href="<?php echo $manufacturer['href'];?>" class="block"><i class="font-normal iplay" aria-hidden="true">&#x25B8;</i><?php echo $manufacturer['name'];?></a>
      </li>
    <?php } ?>
    </ul>
</div>
<?php } ?>
