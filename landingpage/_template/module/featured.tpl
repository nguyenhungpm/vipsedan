<?php if($position=='bottom'){ ?>
	<div class="widget clearfix">
		<h4>Nổi bật</h4>
		<div id="post-list-footer">
			<?php foreach ($products as $product) { ?>
			<div class="spost clearfix">
				<div class="entry-image">
					<a href="<?php echo $product['href'];?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"></a>
				</div>
				<div class="entry-c">
					<div class="entry-title">
						<h4><a href="<?php echo $product['href'];?>"><?php echo $product['name']; ?></a></h4>
					</div>
					<ul class="entry-meta">
						<li><strong><?php echo $product['price']; ?></strong></li>
					</ul>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
<?php }elseif($position == 'content_top_lp'){ ?>
	<?php foreach ($products as $product) {$k=0; ?>
	<article id="post-126" class="post-126 post type-post status-publish format-standard sticky hentry category-xe">
		<?php if($k==0){ ?>
		<div class="featured-post">Bài viết nổi bật </div>
		<?php } ?>
		<header class="entry-header">
			<h1 class="entry-title"><a href="/bang-gia-thue-xe-chung-126/" rel="bookmark"><?php echo $product['name']; ?></a></h1>
		</header>
		<!-- .entry-header -->

		<div class="entry-content">
			<?php echo $product['full_description']; ?>
		</div>
		<!-- .entry-content -->
	</article>
	<!-- #post -->
	<?php $k++;} ?>
<?php } ?>
<?php /*if($position=='column_right'){ ?>
<div class="box">
	<h3 class="title_red n--m uppercase"><?php echo $heading_title; ?></h3>
		<?php foreach ($products as $product) { ?>
				<div class="rowgutter clearfix">
				<div class="text-center gutter border">
			      	<?php if ($product['thumb']) { ?>
			      	<a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" /></a>
			      	<?php } ?>
			      	<h3 class="name h4 n-mb"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h3>
							<span class="color-red strong block"><?php echo $product['price']; ?></span>
							<br />
		    </div>
				</div>
	    <?php } ?>
</div>
<?php }else{ ?>
<div class="box relative rowgutter product-home">
	<h2 class="uppercase h4 title_under"><span><?php echo $heading_title; ?></span></h2>
	<div class="swiper-container" id="featured">
		<div class="swiper-wrapper">
		<?php foreach($products as $product){ ?>
		<div class="swiper-slide text-center">
			<a class="block relative thumbc" href="<?php echo $product['href'];?>"><img src="<?php echo $product['thumb'];?>" alt="<?php echo $product['name'];?>" class="block"/>
			 	<span class="absolute view-a h6 text-center"><?php echo $text_readmore; ?></span>
			</a>
			<h3 class="h5 n-mb name"><a  href="<?php echo $product['href'];?>"><?php echo $product['name'];?></a></h3>
			<span class="color-red strong block"><?php echo $product['price']; ?></span>
		</div>
		<?php } ?>
		</div>
	</div>
	<div class="control <?php echo count($products)<4 ? 'none':''; ?>">
		<div class="swiper-next text-center next-featured"><i class="icn i-arrow-left" aria-hidden="true"></i></div>
		<div class="swiper-prev text-center prev-featured"><i class="icn i-arrow-right" aria-hidden="true"></i></div>
	</div>
</div>

<div class="list-product rowgutter">
	<h2 class="heading"><a href="http://khomaudepraothue.com/dulichtravel/?cat=58">Du lịch hè 2018</a></h2>
    <div class="clear row">
    	<?php foreach($products as $product){ ?>
        <article class="col-12 col-tablet-4">
            <div class="post-inner">
                <div class="entry-thumb">
                    <a class="img" href="<?php echo $product['href'];?>" title="<?php echo $product['name'];?>"><img src="<?php echo $product['thumb'];?>" alt="<?php echo $product['name'];?>"/>

                        <div class="caption scale-caption">
                            <h3 class="caption-title"><?php echo $product['name'];?></h3>
                            <p class="caption-description">Chúng ta vẫn biết rằng, làm việc với một đoạn văn bản dễ đọc và rõ nghĩa dễ gây rối trí và cản trở việc tập trung vào yếu tố trình bày văn bản. </p>
                        </div>
                    </a>
                    <p class="timestyle5" style="display:none;">06/01/2018</p><span>view</span></div>
                <div class="entry-content">
                    <h3 class="entry-title"><a href="<?php echo $product['href'];?>" title="PHÚ QUỐC TẠI RESORT SANG CHẢNH"><?php echo $product['name'];?></a></h3>
                    <div class="custom">
                        <div class="price-valibar">Giá : 3.799.000vnđ</div>
                        <div class="date-time">3 ngày 2 đêm ( chưa bao gồm vé máy bay )</div>
                    </div>
                    <div class="box-views"><a href="<?php echo $product['href'];?>" title="<?php echo $product['name'];?>" class="view-more">Lịch trình</a><a href="http://olatravel.vn/lien-he/" title="PHÚ QUỐC TẠI RESORT SANG CHẢNH" class="lienhe">Đặt tour</a></div>
                </div>
            </div>
        </article>
        <?php } ?>
    </div>
</div>
<?php }*/ ?>


