<h3 class="no-margin h6 uppercase strong font-avo">Đăng ký nhận tin</h3> 
<span class="block small">Đăng ký để nhận những thông tin mới nhất qua email</span> 
<div name="subscribe" id="subscribe"> 
	<div id="frm_subscribe" class="relative"> 
		<input type="text" value="" name="subscribe_email" onkeydown="subscribe(event)" placeholder="Email của bạn" id="subscribe_email"> 
		<a class="bg-blue color-white text-center absolute" onclick="email_subscribe()">Đăng ký</a> 
	</div> 
	<div class="text-left" id="subscribe_result"></div> 
</div>