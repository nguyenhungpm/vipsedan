<?php if ($position == 'content_top'  or $position == 'content_bottom') { ?>
<div class="box clear">
<h2 class="title_under text-center h4 uppercase"><span><?php echo $heading_title; ?></span></h2>
<div class="clear row">
<?php foreach ($newss as $news) { ?>
	<div class="col-6 col-tablet-2 rowgutter">
		<a href="<?php echo $news['href']; ?>"><img class="block" alt="<?php echo $news['name']; ?>" src="<?php echo $news['thumb']; ?>" /></a>
		<h3 class="newsname h4"><a href="<?php echo $news['href']; ?>"><?php echo $news['name']; ?></a></h3>
		<div class="h6 desc"><?php echo $news['short_description'];?></div>
	</div>
<?php } ?>
</div>
</div>
<?php }else{ ?>
<div class="box">
	<h3 class="n-mt title_under"><span><?php echo $heading_title; ?></span></h3>
	<?php foreach ($newss as $news) { ?>
		<div class="clear gutter">
			<a class="block col-4 n-pl" href="<?php echo $news['href'];?>"><img src="<?php echo $news['thumb'];?>" alt="<?php echo $news['name'];?>"/></a>
			<a class="col-8 n-pl" href="<?php echo $news['href'];?>"><?php echo $news['name'];?></a>
			<div class="h5"><i class="icn i-clock"></i> <?php echo $news['date_added'];?></div>
		</div>
	<?php } ?>
</div>
<?php } ?>
