<div class="box relative rowgutter">
	<h2 class="title_under uppercase t-d-n h4"><a href="<?php echo $link; ?>"><span><?php echo $title; ?></span></a></h2>
	<div class="product-home swiper-container clear" id="category-<?php echo $module; ?>">
	<div class="swiper-wrapper">
		<?php foreach ($products as $key=>$product) { ?>
			<div class="swiper-slide">
				<article class="">
					<div class="post-inner">
						<div class="entry-thumb">
							<a class="img" href="<?php echo $product['href'];?>" title="<?php echo $product['name'];?>"><img src="<?php echo $product['thumb'];?>" alt="<?php echo $product['name'];?>"/>

								<div class="caption scale-caption">
									<h3 class="caption-title"><?php echo $product['name'];?></h3>
									<p class="caption-description">Chúng ta vẫn biết rằng, làm việc với một đoạn văn bản dễ đọc và rõ nghĩa dễ gây rối trí và cản trở việc tập trung vào yếu tố trình bày văn bản. </p>
								</div>
							</a>
							<p class="timestyle5" style="display:none;">06/01/2018</p><span>view</span></div>
						<div class="entry-content">
							<h3 class="entry-title"><a href="<?php echo $product['href'];?>" title="PHÚ QUỐC TẠI RESORT SANG CHẢNH"><?php echo $product['name'];?></a></h3>
							<div class="custom">
								<div class="price-valibar">Giá : 3.799.000vnđ</div>
								<div class="date-time">3 ngày 2 đêm ( chưa bao gồm vé máy bay )</div>
							</div>
							<div class="box-views"><a href="<?php echo $product['href'];?>" title="<?php echo $product['name'];?>" class="view-more">Lịch trình</a><a href="http://olatravel.vn/lien-he/" title="PHÚ QUỐC TẠI RESORT SANG CHẢNH" class="lienhe">Đặt tour</a></div>
						</div>
					</div>
				</article>
				<!-- <a class="block relative thumbc" href="<?php echo $product['href'];?>"><img src="<?php echo $product['thumb'];?>" alt="<?php echo $product['name'];?>" class="block"/> -->
                <!-- <span class="absolute view-a h6 text-center">Xem chi tiết</span> -->
                <!-- </a> -->
                <!-- <h3 class="h5 text-center n-mb name"><a class="t-d-none" href="<?php echo $product['href'];?>"><?php echo $product['name'];?></a></h3> -->
				<!-- <span class="color-red strong text-center block"><?php echo $product['price']; ?></span> -->
			</div>
		<?php } ?>
    </div>
    </div>
		<div class="swiper-next next-category<?php echo $module; ?> text-center"><i class="icn i-arw-left" aria-hidden="true"></i> </div>
		<div class="swiper-prev prev-category<?php echo $module; ?> text-center"><i class="icn i-arw-right" aria-hidden="true"></i> </div>
</div>
