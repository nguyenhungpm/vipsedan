<div class="tab-content clearfix tabcontent" id="tab-flights"> 
	<h4 class="title">Đặt lịch xe</h4>
	<form action="module/booking/order" id="booking_form" method="post" class="nobottommargin">
		<div class="row">
			<div class="">
				<label for="">Điểm đi</label>
				<input name="origins" type="text" value="" class="sm-form-control" placeholder="VD: Nội Bài">
			</div>
			<div class="">
				<label for="">Điểm đến</label>
				<input name="destinations" type="text" value="" class="sm-form-control" placeholder="VD: Thanh Xuân">
			</div>
			<div class="col-md-6 col-12">
				<label for="">Thời gian</label>
				<input type="time" value="" class="sm-form-control tleft" name="time" placeholder="HH:MM">
			</div>
			<div class="">
				<label for="">Loại xe</label>
				<select style="height: 36px;" name="number_seats" onchange="calDistance('booking_form', event)" class="sm-form-control">
					<option value="">-- Chọn xe --</option>
					<option value="4">Xe 4 chỗ</option>
					<option value="7">Xe 7 chỗ</option>
					<option value="16">Xe 16 chỗ</option>
					<option value="other">Loại khác</option>
				</select>
			</div>
			<span id="result" class="color-white"></span><br><br><br>
			<div class="cal">
				<a onclick="calDistance('booking_form', event)" class="button">Tính tiền</a>
			</div>

			<hr>
			<div class="col-md-6 col-12">
				<label for="">Ngày đi</label>
				<input type="date" value="" class="sm-form-control tleft" name="date">
			</div>
			<div class="col-md-6 col-12">
				<label for="">Họ và tên</label>
				<input type="text" value="" class="sm-form-control tleft" name="name" placeholder="Họ và tên">
			</div>
			<div class="col-md-6 col-12">
				<label for="">Số điện thoại</label>
				<input type="text" value="" class="sm-form-control tleft" name="telephone" placeholder="Số điện thoại">
			</div>
			<div class="col-md-6 col-12">
				<label for="">Ghi chú</label>
				<textarea rows="4" name="note" placeholder="Ghi chú chuyến đi"></textarea>
			</div>
			<span id="rs" class="color-white"></span><br><br><br>
			<div class="cal">
				<a class="button" onclick="booking('booking_form', event)">Đặt chỗ</a>
			</div>
		</div>
	</form>
</div>
<style type="text/css">
	#booking_form label{
		display:block;
		width: 100%;
	}
	#booking_form input, #booking_form select, #booking_form textarea{
		width: 100%;
		margin-top:6px;
		margin-bottom: 15px;
		min-height: 36px;
		box-sizing:border-box;
	}
	.button:hover{ color: white }
	.button{
	    background: #108299;
	    color: white;
	    padding: 10px 20px;
	    margin-left: 5px;
	    cursor: pointer;
	    margin-top: 10px;
	}
	.cal{
		text-align: center;
		min-height: 30px;
	}
	
</style>
<script>

function rudrSwitchTab(rudr_tab_id, rudr_tab_content) {
	// first of all we get all tab content blocks (I think the best way to get them by class names)
	var x = document.getElementsByClassName("tabcontent");
	var i;
	for (i = 0; i < x.length; i++) {
		x[i].style.display = 'none'; // hide all tab content
	}
	document.getElementById(rudr_tab_content).style.display = 'block'; // display the content of the tab we need
 
	// now we get all tab menu items by class names (use the next code only if you need to highlight current tab)
	var x = document.getElementsByClassName("tabmenu");
	var i;
	for (i = 0; i < x.length; i++) {
		x[i].className = 'tabmenu'; 
	}
	document.getElementById(rudr_tab_id).className = 'tabmenu active';
}
function calDistance(id, event){
	event.preventDefault();
	var request = new XMLHttpRequest();
	// POST to httpbin which returns the POST data as JSON
	request.open('POST', 'index.php?route=module/booking/distance', /* async = */ false);
	var formData = new FormData(document.getElementById(id));
	request.send(formData);

	console.log(request);
	
	// var rs = request.responseText.replace('"', '');
	if (request.status == 200) {
		var json = JSON.parse(request.responseText);
		console.log(json);
		if(json[0].status == 'OK'){
			document.getElementById('result').innerHTML = 'Thành tiền: ' + json[0].price + ' ('+ json[0].distance +')';
		}else{
			document.getElementById('result').innerHTML = 'Bạn nhập địa điểm đến hoặc đi không đúng';
		}
	} else {
		document.getElementById('result').innerHTML = 'Bạn nhập địa điểm đến hoặc đi không đúng';
		console.log('Error: ' + request.statusText);
	}
}
function booking(id, event){
	event.preventDefault();
	var request = new XMLHttpRequest();
	// POST to httpbin which returns the POST data as JSON
	request.open('POST', 'index.php?route=module/booking/booking', /* async = */ false);
	var formData = new FormData(document.getElementById(id));
	request.send(formData);

	console.log(request);
	
	// var rs = request.responseText.replace('"', '');
	if (request.status == 200) {
		var json = JSON.parse(request.responseText);
		document.getElementById('rs').innerHTML = json['success'];
	} else {
		document.getElementById('rs').innerHTML = 'Đặt xe bị lỗi. Vui lòng gọi tới hotline để được hỗ trợ tốt nhất';
		console.log('Error: ' + request.statusText);
	}
}
</script>