<?php echo $header; ?>
<div class="container">
	<div class="clear row">
	<?php echo $content_top; ?>
    <div class="col-12 col-tablet-8 border-right">
			<h1 class="n--m rowgutter"><?php echo $heading_title;?></h1>

	<?php if($pictures){?>
	<div class="row clear">
	<?php foreach($pictures as $picture){ ?>
	<div class="col-12 col-tablet-6 gallery-item rowgutter">
		<div class="relative">
			<a href="<?php echo $picture['href'];?>" class="thumb block"><img class="block" alt="<?php echo $picture['name'];?>" src="<?php echo $picture['thumb'];?>"/></a>
			<h3 class="n--m gutter h4 font-normal text-center"><a href="<?php echo $picture['href'];?>"><?php echo $picture['name'];?></a>
			</h3>
		</div>
	</div>
	<?php } ?>
	</div>

	<?php }else{ ?>
	<?php echo $text_empty; ?>
	<?php } ?>
	<?php echo $content_bottom; ?>
    </div>
	<div class="col-12 col-tablet-4">
		<?php echo $column_right; ?>
	</div>
</div>
</div>
<?php echo $footer; ?>
