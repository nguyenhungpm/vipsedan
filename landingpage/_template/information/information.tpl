<?php echo $header; ?>
<div class="rowgutter breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
	<div class="container">
		    <?php foreach ($breadcrumbs as $key=>$breadcrumb) { ?>
		    	<?php echo $breadcrumb['separator']; ?><span itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo $breadcrumb['href']; ?>"><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
		    	    <meta itemprop="position" content="<?php echo $key+1; ?>" />
		    	</a></span>
		    <?php } ?>
	</div>
</div>
<div class="container">
<div class="clear row">
    <div class="col-12 col-tablet-8 rowgutter border-right">
	<h1><?php echo $heading_title; ?><?php echo $admin_logged ? ' (<a target="_blank" href="'.$edit.'">Sửa</a>)': ''?></h1>
	<div class="newscontent">
		  <?php echo $description; ?>
	</div>
		<?php echo $content_bottom; ?>
    </div>
	<div class="col-12 col-tablet-4">
		<?php echo $column_right; ?>
	</div>
</div>
</div>
<?php echo $footer; ?>
