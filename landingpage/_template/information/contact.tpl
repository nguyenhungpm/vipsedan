<?php echo $header; ?>
<div class="rowgutter breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
	<div class="container">
		    <?php foreach ($breadcrumbs as $key=>$breadcrumb) { ?>
		    	<?php echo $breadcrumb['separator']; ?><span itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo $breadcrumb['href']; ?>"><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
		    	    <meta itemprop="position" content="<?php echo $key+1; ?>" />
		    	</a></span>
		    <?php } ?>
	</div>
</div>
<div class="container">
<div class="clearfix row">
    <div class="col-12 col-tablet-8 contactleft border-right">
		<h1 class="h3 no-margin rowgutter"><?php echo $heading_title; ?></h1>
		<h2 class="uppercase strong h6"><?php echo $store; ?></h2>
		<p><strong><?php echo $text_address; ?>: </strong><?php echo $address; ?></p>
		<p><strong><?php echo $text_phone; ?>: </strong><a href="tel:<?php echo $telephone; ?>"><?php echo $telephone; ?></a><?php echo $fax ? '<strong>Fax:</strong> | '.$fax : ''; ?></p>
		<p><strong>E-mail: </strong><a href="mailto:<?php echo $email2; ?>"><?php echo $email2; ?></a></p>
		<br />
		<div class="map">
				<?php echo $content_top; ?>
	    </div>
    </div>
    <div class="col-12 col-tablet-4 contactpage rowgutter">
	<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
		<h2 class="uppercase strong h6"><?php echo $text_contact; ?></h2>
		<?php echo $entry_name; ?> *<br />
		<input type="text" name="name" value="<?php echo $name; ?>" placeholder="" /> <br />
		<?php if ($error_name) { ?>
		<span class="error"><?php echo $error_name; ?></span>
		<?php } ?>
		<?php echo $text_phone; ?><br />
		<input type="text" name="phone2" value="<?php echo $phone; ?>" placeholder="" /> <br />
		<?php echo $entry_email; ?> *<br />
		<input type="text" name="email" value="<?php echo $email; ?>" placeholder=""/>   <br />
		<?php if ($error_email) { ?>
		<span class="error"><?php echo $error_email; ?></span>
		<?php } ?>
		<?php echo $text_address; ?><br />
		<input type="text" name="address2" value="<?php echo $address2; ?>" placeholder="" /><br />
		<?php echo $entry_enquiry; ?><br />
		<textarea name="enquiry" cols="30" rows="5" placeholder=""><?php echo $enquiry; ?></textarea>
		<br />
		<?php if ($error_enquiry) { ?>
		<span class="error"><?php echo $error_enquiry; ?></span>
		<?php } ?>
		<div class="clear">
		<input placeholder="<?php echo $entry_captcha; ?>" type="text" name="captcha" value="<?php echo $captcha; ?>" />
		<img src="index.php?route=information/contact/captcha" alt="" class="right"/>
		</div>
		<?php if ($error_captcha) { ?>
		<span class="error clear"><?php echo $error_captcha; ?></span>
		<?php } ?>
		<input type="submit" value="<?php echo $entry_submit; ?>" class="button right" />
	</form>
	</div>

</div>
</div>
<?php echo $footer; ?>
