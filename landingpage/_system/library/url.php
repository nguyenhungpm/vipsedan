<?php
class Url {
	private $url;
	private $ssl;
	private $rewrite = array();

	public function __construct($url, $ssl = '') {
		$this->url = $url;
		$this->ssl = $ssl;
	}

	public function addRewrite($rewrite) {
		$this->rewrite[] = $rewrite;
	}

	public function link($route, $args = '', $connection = 'NONSSL') {
		if ($connection ==  'NONSSL') {
			$url = $this->url;
		} else {
			$url = $this->ssl;
		}

		if(!strpos($url,'/oadmin/') && $route == 'common/home'){

		}else{
			// LANGUAGE_CODE_IN_URL start
			if (isset($_SESSION['language']) && (!defined('HTTP_CATALOG'))) {
				$url .= strtolower($_SESSION['language']) . '/';
			}
			// LANGUAGE_CODE_IN_URL end
			$url .= 'index.php?route=' . $route;
		}

		if ($args) {
			$url .= str_replace('&', '&amp;', '&' . ltrim($args, '&'));
		}

		foreach ($this->rewrite as $rewrite) {
			$url = $rewrite->rewrite($url);
		}

		return $url;
	}
}
?>
