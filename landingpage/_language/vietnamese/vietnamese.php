<?php
// Locale
$_['code']                  = 'vi';
$_['locate']                = 'vi_VN';
$_['direction']             = 'ltr';
$_['date_format_short']     = 'd/m/Y';
$_['date_format_long']      = 'l dS F Y';
$_['time_format']           = 'h:i:s A';
$_['decimal_point']         = ',';
$_['thousand_point']        = '.';
$_['call_for_price']        = 'Liên hệ';

// Text
$_['text_home']             = 'Trang chủ';
$_['text_yes']              = 'Có';
$_['text_no']               = 'Không';
$_['text_none']             = ' --- Không --- ';
$_['text_select']           = ' --- Chọn --- ';
$_['text_all_zones']        = 'Tất cả các khu vực';
$_['text_pagination']       = 'Hiển thị {start} đến {end} trong {total} ({pages} trang)';
$_['text_separator']        = ' &raquo; ';
$_['text_page']             = ':: Trang ';

// Buttons
$_['button_add_address']    = 'Thêm địa chỉ';
$_['button_back']           = 'Quay lại';
$_['button_continue']       = 'Tiếp tục';
$_['button_cart']           = 'Thêm vào giỏ';
$_['button_checkout']       = 'Thanh toán';
$_['button_confirm']        = 'Xác nhận đơn hàng';
$_['button_coupon']         = 'Áp dụng giảm giá';
$_['button_delete']         = 'Xóa';
$_['button_download']       = 'Download';
$_['button_edit']           = 'Sửa';
$_['button_new_address']    = 'Địa chỉ mới';
$_['button_change_address'] = 'Đổi địa chỉ';
$_['button_add_product']    = 'Thêm sản phẩm';
$_['button_reviews']        = 'Đánh giá';
$_['button_write']          = 'Viết đánh giá';
$_['button_login']          = 'Đăng nhập';
$_['button_update']         = 'Cập nhật';
$_['button_remove']         = 'Loại bỏ';
$_['button_search']         = 'Tìm kiếm';
$_['button_view']           = 'Xem';
$_['button_quote']          = 'Lấy trích dẫn';

// Error
?>
