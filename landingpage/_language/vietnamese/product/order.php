<?php
$_['heading_title']  = 'Đặt hàng trực tuyến';

$_['text_message'] = 'Bạn đã đặt mua hàng thành công!';
$_['text_add_product'] = 'Thêm sản phẩm';
$_['text_select_product'] = 'Chọn sản phẩm';
$_['text_image'] = 'Hình ảnh'; 
$_['text_product_name'] = 'Tên sản phẩm';
$_['text_model'] = 'Mã hàng';
$_['text_delete'] = 'Xóa';
$_['text_address2'] = 'Địa chỉ:';
$_['text_checkout'] = 'Đặt hàng';
$_['text_information'] = 'Thông tin của bạn';
$_['text_form_order'] = 'Biểu mẫu đặt hàng';
$_['text_froduct'] = 'Các sản phẩm: \n';
?>