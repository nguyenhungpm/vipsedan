<?php
class ControllerInformationGallery extends Controller {
	public function index() {
		$this->language->load('information/gallery');

		$this->data['text_empty'] = $this->language->get('text_empty');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_gallery'] = $this->language->get('text_gallery');

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),
			'separator' => false
		);

		$this->load->model('design/banner');
		$this->load->model('tool/image');

		if(isset($this->request->get['picture_id'])){
			$picture_id = $this->request->get['picture_id'];
			$this->data['picture_id'] = $this->request->get['picture_id'];
		}else{
			$picture_id = 0;
			$this->data['picture_id'] = 0;
		}

		$picture_info = $this->model_design_banner->getPictureCat($picture_id);

		if($picture_info){
			$this->document->setTitle($picture_info['name']);

			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('heading_title'),
				'href'      => $this->url->link('information/gallery'),
				'separator' => $this->language->get('text_separator')
			);

			$this->data['pictures_list'] = array();
			foreach($this->model_design_banner->getAllCat() as $cat){
				if ($cat['image']) {
					$image = $this->model_tool_image->cropsize($cat['image'], $this->config->get('config_gallery_width'), $this->config->get('config_gallery_height'));
				} else {
					$image = false;
				}
				$this->data['pictures_list'][] = array(
					'picture_id' => $cat['picture_id'],
					'name' => $cat['name'],
					'thumb' => $image,
					'href' => $this->url->link('information/gallery', 'picture_id=' . $cat['picture_id'])
				);
			}


			$this->data['info'] = $picture_info;
			$this->data['pictures'] = array();
			foreach($this->model_design_banner->getPicture($picture_id) as $rs){
				if ($rs['image']) {
					$image = $this->model_tool_image->cropsize($rs['image'], $this->config->get('config_album_width'), $this->config->get('config_album_height'));
				} else {
					$image = false;
				}

				$this->data['pictures'][] = array(
					'picture_image_id' => $rs['picture_image_id'],
					'name' => $rs['title'],
					'popup' => 'media/' . $rs['image'],
					'thumb' => $image,
					'href' => $this->url->link('information/gallery', 'picture_id=' . $rs['picture_image_id'])
				);
			}
			$this->document->addScript('static/baguetteBox.js');
			$this->document->addStyle('static/baguetteBox.css');
			$script = "baguetteBox.run(
									'.gallery', {
									captions: true,
									buttons: 'auto',
									async: false,
									preload: 2,
									animation: 'slideIn' // fadeIn or slideIn
							});";

			$this->document->addExScript($script);
			$this->template = 'information/gallery_detail.tpl';
			$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/slideside',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'
			);

			$this->response->setOutput($this->render());
		}else{

			$this->document->setTitle($this->language->get('heading_title'));

			$this->data['pictures'] = array();
			foreach($this->model_design_banner->getAllCat() as $rs){
				if ($rs['image']) {
					$image = $this->model_tool_image->cropsize($rs['image'], $this->config->get('config_gallery_width'), $this->config->get('config_gallery_height'));
				} else {
					$image = false;
				}

				$this->data['pictures'][] = array(
					'picture_id' => $rs['picture_id'],
					'name' => $rs['name'],
					'thumb' => $image,
					'href' => $this->url->link('information/gallery', 'picture_id=' . $rs['picture_id'])
				);
			}

			$this->template = 'information/gallery.tpl';

			$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/slideside',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'
			);

			$this->response->setOutput($this->render());
		}


	}
}
?>
