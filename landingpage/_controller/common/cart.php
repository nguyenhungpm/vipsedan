<?php
class ControllerCommonCart extends Controller {
    private $error = array();

    public function index() {
		$this->load->model('catalog/product');
		$this->load->model('module/dathang');

		$this->data['heading_title'] = 'Giỏ hàng của bạn';

		$this->document->setTitle('Giỏ hàng');

		// echo $this->session->data['cart'];
		// print_r($this->request->post);

		if(isset($this->request->get['type'])){
			$arr = explode('-',$this->request->cookie[$this->request->get['type']]);
			if(($key = array_search($this->request->get['remove'], $arr)) !== false) {
				unset($arr[$key]);
			}

			$value = join('-',$arr);
			setcookie($this->request->get['type'], $value, time() + 60 * 60 * 24 * 30, '/', $this->request->server['HTTP_HOST']);
			$this->redirect($this->url->link('common/cart'));
		}
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

			$this->model_module_dathang->addOrderProduct($this->request->post);

            if($this->config->get('config_order_email')){
			
				$message .= "Khách hàng: ".$this->request->post['name']."<br />";
				$message .= "Email: ".$this->request->post['email']."<br />";
				$message .= "Điện thoại: ".$this->request->post['telephone']."<br />";
				
				$message .= 'Các sản phẩm đặt hàng:<br />';
				foreach($this->request->post['product'] as $product_id => $product){
					$message .= "\t-".$product." - Số lượng: ". $this->request->post['quantity'][$product_id] ."<br />";
				}
				
				$mail = new Mail();
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->hostname = $this->config->get('config_smtp_host');
				$mail->username = $this->config->get('config_smtp_username');
				$mail->password = $this->config->get('config_smtp_password');
				$mail->port = $this->config->get('config_smtp_port');
				$mail->timeout = $this->config->get('config_smtp_timeout');
				$mail->setTo($this->config->get('config_email'));
				$mail->setFrom($this->request->post['email']);
				$mail->setSender($this->request->post['name']);
				$mail->setSubject(html_entity_decode('Đơn đặt hàng', ENT_QUOTES, 'UTF-8'));
				$mail->setHtml(strip_tags(html_entity_decode($message, ENT_QUOTES, 'UTF-8')));
				$mail->send();
			}
            $this->redirect($this->url->link('information/contact/success'));
        }

		if (isset($this->request->post['name'])) {
            $this->data['name'] = $this->request->post['name'];
        } else {
            $this->data['name'] = '';
        }

        if (isset($this->request->post['email'])) {
            $this->data['email'] = $this->request->post['email'];
        } else {
            $this->data['email'] = '';
        }

        if (isset($this->request->post['telephone'])) {
            $this->data['telephone'] = $this->request->post['telephone'];
        } else {
            $this->data['telephone'] = '';
        }

		if (isset($this->error['name'])) {
            $this->data['error_name'] = $this->error['name'];
        } else {
            $this->data['error_name'] = '';
        }

        if (isset($this->error['email'])) {
            $this->data['error_email'] = $this->error['email'];
        } else {
            $this->data['error_email'] = '';
        }

        if (isset($this->error['telephone'])) {
            $this->data['error_telephone'] = $this->error['telephone'];
        } else {
            $this->data['error_telephone'] = '';
        }

		$this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home'),
            'separator' => false
        );

		$this->data['checkcart'] = 0;
		if(isset($this->request->cookie['domain']) || isset($this->request->cookie['model'])){
			$this->data['checkcart'] = 1;
		}

		$hosting = array();
		if(!empty($this->request->cookie['model'])){
			$hosting = explode('-',$this->request->cookie['model']);
		}
		$hosting = array_filter($hosting);
		$this->data['hosting'] = array();
		foreach($hosting as $model){
			$this->data['hosting'][] = $this->model_catalog_product->getProduct($model);
		}

		$this->template = 'common/cart.tpl';

		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'
		);

		$this->response->setOutput($this->render());
	}

	protected function validate() {
        if ((utf8_strlen($this->request->post['telephone']) < 1) || (utf8_strlen($this->request->post['telephone']) > 15)) {
            $this->error['telephone'] = 'Số điện thoại không đúng!';
        }

        if (empty($this->request->post['name'])) {
            $this->error['name'] = 'Vui lòng nhập tên.';
        }

        if (!preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['email'])) {
            $this->error['email'] = 'Vui lòng nhập đúng địa chỉ email';
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

	public function cart(){
		$json = 1;

		if(isset($this->request->post['model']) && !empty($this->request->post['model'])){
			if(isset($this->request->post['type']) && !empty($this->request->post['type'])){
				$type = $this->request->post['type'];
			}else{
				$type = 'model';
			}

			$arr = explode('-',$this->request->cookie[$this->request->post['type']]);
			if(!in_array($this->request->post['model'], $arr)){
				$value = $this->request->cookie[$this->request->post['type']] . '-' . $this->request->post['model'];
			}else{
				$value = $this->request->cookie[$this->request->post['type']];
			}

			setcookie($type, $value, time() + 60 * 60 * 24 * 30, '/', $this->request->server['HTTP_HOST']);
			$json = 2;
		}
		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
	}
}
?>
