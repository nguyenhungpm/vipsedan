<?php
class ControllerCommonDistance extends Controller {
	public function index() {

		$origins = str_replace(" ","+",$this->request->post['origins']);
		$destinations = str_replace(" ","+",$this->request->post['destinations']);
		// $origins = 'Hồng+Thuận';
		// $destinations = 'Giao+Lac';

		$url = "https://maps.googleapis.com/maps/api/distancematrix/json?key=AIzaSyCAsAB47nPnc92FCSYKoVfWFGM_ysAGKCI&origins=" .$origins. "&destinations=" .$destinations;
		// echo $url;
		ini_set("allow_url_fopen", 1);
		$json = file_get_contents($url);
		$obj = json_decode($json, true);
		$status = $obj['status'];
		// print_r($status);
		$json = array();
		if($status == 'OK'){
			$check = $obj['rows'][0]['elements'][0]['status'];
			$distance = ($check=='OK') ? $obj['rows'][0]['elements'][0]['distance']['text'] : 0; 
			$value = ($check=='OK') ? $obj['rows'][0]['elements'][0]['distance']['value'] : 0;
			$number_seats = $this->request->post['number_seats'];
			$price = (int)$this->config->get('config_seat_'.$number_seats) * round($value/1000, 2);
			
			$json[] = array(
				'destination' => $obj['destination_addresses'][0],
				'origin' => $obj['origin_addresses'][0],
				'distance' => $distance,
				'value' => $value,
				'price' => number_format($price,0,",",".") . ' vnđ',
				'status' => $check
			);
		}else{
			$json[] = array(
				'status' => $status
			);
		}
		
		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
        // $this->response->setOutput($json);
	}
}
?>
