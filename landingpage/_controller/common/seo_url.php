<?php
class ControllerCommonSeoUrl extends Controller {
	public function index() {
		// Add rewrite to url class
		$this->load->model('module/redirect');
		
		$this->model_module_redirect->detect301Status();
		if ($this->config->get('config_seo_url')) {
			$this->url->addRewrite($this);
		}
		
		// Decode URL
		if (isset($this->request->get['_route_'])) {
			$parts = explode('/', $this->request->get['_route_']);
			$parts = array_filter($parts);
			$amp = in_array('amp-page', $parts) ? true : false;
			$k=0;
			
			foreach ($parts as $part) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE keyword = '" . $this->db->escape($part) . "'");
				
				if ($query->num_rows) {
					$url = explode('=', $query->row['query']);
					$k++;
					
					if ($url[0] == 'product_id') {
						$this->request->get['product_id'] = $url[1];
					}
					
					if ($url[0] == 'category_id') {
						if (!isset($this->request->get['path'])) {
							$this->request->get['path'] = $url[1];
						} else {
							$this->request->get['path'] .= '_' . $url[1];
						}
					}	
					// --- START : OSD.VN CUSTOMIZATION [1]
					if ($url[0] == 'news_id') {
						$this->request->get['news_id'] = $url[1];
					}

					if ($url[0] == 'news_category_id') {
						if (!isset($this->request->get['cat_id'])) {
							$this->request->get['cat_id'] = $url[1];
						} else {
							$this->request->get['cat_id'] .= '_' . $url[1];
						}
					}
					// --- END : OSD.VN CUSTOMIZATION [1] */
					if ($url[0] == 'manufacturer_id') {
						$this->request->get['manufacturer_id'] = $url[1];
					}
					
					if ($url[0] == 'information_id') {
						$this->request->get['information_id'] = $url[1];
					}	
				} else {
					$this->request->get['route'] = 'error/not_found';	
				}
			}
			
			if (isset($this->request->get['product_id'])) {
				if($amp){
					$this->request->get['route'] = 'amp/product';
				}else{
					$this->request->get['route'] = 'product/product';
				}
			} elseif ($this->request->get['_route_'] ==  'contact') {
				$this->request->get['route'] =  'information/contact';
			} elseif ($this->request->get['_route_'] ==  'sitemap') {
				$this->request->get['route'] =  'information/sitemap';
			} elseif ($this->request->get['_route_'] ==  'search') {
				$this->request->get['route'] =  'product/search';
			} elseif (isset($this->request->get['path'])) {
				$this->request->get['route'] = 'product/category';
			} elseif (isset($this->request->get['news_id'])) { 
				if($amp){
					$this->request->get['route'] = 'amp/news';
				}else{
					$this->request->get['route'] = 'news/news';
				}
				
			} elseif (isset($this->request->get['cat_id'])) {
				$this->request->get['route'] = 'news/news_category';
			}elseif (isset($this->request->get['manufacturer_id'])) {
				$this->request->get['route'] = 'product/manufacturer/info';
			} elseif (isset($this->request->get['information_id'])) {
				if($amp){
					$this->request->get['route'] = 'amp/information';
				}else{
					$this->request->get['route'] = 'information/information';
				}
				
			}else {
				$this->request->get['route'] = $this->request->get['_route_'];
			}
			
			if($k != count($parts) && $k!=0 && !$amp){
				$this->request->get['route'] = 'error/not_found';
			}
			
			if (isset($this->request->get['route'])) {
				return $this->forward($this->request->get['route']);
			}
		}
	}
	
	public function rewrite($link) {
		$url_info = parse_url(str_replace('&amp;', '&', $link));
	
		$url = ''; 
		
		$data = array();
		
		if(isset($url_info['query'])){
			parse_str($url_info['query'], $data);
		}
		
		foreach ($data as $key => $value) {
			if (isset($data['route'])) {
				if (($data['route'] == 'product/product' && $key == 'product_id') || (($data['route'] == 'product/manufacturer/info' || $data['route'] == 'product/product') && $key == 'manufacturer_id') || ($data['route'] == 'information/information' && $key == 'information_id')/*osd.vn*/ || ($data['route'] == 'news/news' && $key == 'news_id') /* osd.vn*/) {
					$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = '" . $this->db->escape($key . '=' . (int)$value) . "'");
				
					if ($query->num_rows) {
						//$url .= '/' . $query->row['keyword'];
						// remove category from URL
						$url = '/' . $query->row['keyword'];
						
						unset($data[$key]);
					}					
					} elseif (isset($data['route']) && $data['route'] ==   'common/home') {
                $url .=  '/';
                } elseif (isset($data['route']) && $data['route'] ==   'information/contact') {
                $url .=  '/contact';
                } elseif (isset($data['route']) && $data['route'] ==   'information/sitemap') {
                $url .=  '/sitemap';
                } elseif (isset($data['route']) && $data['route'] ==   'product/search') { 
                $url .=  '/search';
				} elseif ($key == 'path') {
					$categories = explode('_', $value);
					
					foreach ($categories as $category) {
						$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = 'category_id=" . (int)$category . "'");
				
						if ($query->num_rows) {
							$url = '/' . $query->row['keyword'];
						}							
					}
					
					unset($data[$key]);
				}// --- START : OSD.VN CUSTOMIZATION [3]
				elseif ($key == 'cat_id') {
					$news_category = explode('_', $value);

					foreach ($news_category as $category) {
						$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = 'news_category_id=" . (int)$category . "'");

						if ($query->num_rows) {
							$url .= '/' . $query->row['keyword'];
						}
					}

					unset($data[$key]);
				} elseif (($data['route'] == 'product/amp_product' && $key == 'product_id') || ($data['route'] == 'news/news_amp' && $key == 'news_id') || ($data['route'] == 'information/info_amp' && $key == 'information_id')) {
					$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = '" . $this->db->escape($key . '=' . (int)$value) . "'");
					if(isset($query->row['keyword'])){
						$url .= '/amp-page/' . $query->row['keyword'];
					}else{
						$url .= '/amp-page&' . $key . '=' . $value;
					}

					unset($data[$key]);
				}
				// --- END : OSD.VN CUSTOMIZATION [3] /
			}
		}
	
		if ($url) {
			unset($data['route']);
		
			$query = '';
		
			if ($data) {
				foreach ($data as $key => $value) {
					$query .= '&' . $key . '=' . $value;
				}
				
				if ($query) {
					$query = '?' . trim($query, '&');
				}
			}

			return $url_info['scheme'] . '://' . $url_info['host'] . (isset($url_info['port']) ? ':' . $url_info['port'] : '') . str_replace('/index.php', '', $url_info['path']) . $url . $query;
		} else {
			
			if(strpos($link,'index.php?route=common/home')){
				$link = str_replace('index.php?route=common/home','',$link);
			}
				$link = str_replace('index.php?route=', '', $link);
			return $link;
		}
	}	
}
?>