<?php
class ControllerCommonHome extends Controller {
	public function index() {

		if(strlen($this->config->get('config_title')>1)){
			$this->document->setTitle($this->config->get('config_title'));
		}else{
			$this->document->setTitle($this->config->get('config_name'));
		}
		$meta_description = nl2br($this->config->get('config_meta_description')[$this->config->get('config_language_id')]);
		$meta_keyword = nl2br($this->config->get('config_meta_keyword')[$this->config->get('config_language_id')]);

		if($this->config->get('config_secure')==1){
			$this->document->addLink(HTTPS_SERVER, 'canonical');
		}else{
			$this->document->addLink(HTTP_SERVER, 'canonical');
		}

		$this->document->setMetapros('description',$meta_description);
		$this->document->setMetapros('keyword',$meta_keyword);

		$this->data['hotline'] = $this->config->get('config_hotline');

		$this->data['heading_title'] = $this->config->get('config_name');


		$this->template = 'common/home.tpl';

		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/adv_top',
			'common/content_bottom',
			'common/slideside',
			'common/footer',
			'common/header'
		);

		$this->response->setOutput($this->render());
	}
}
?>
