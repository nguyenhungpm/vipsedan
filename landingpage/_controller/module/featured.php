<?php
class ControllerModuleFeatured extends Controller {
	protected function index($setting) {
		$this->language->load('module/featured');

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['button_cart'] = $this->language->get('button_cart');
		$this->data['text_readmore'] = $this->language->get('text_readmore');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');
		$this->load->model('tool/t2vn');

		$this->data['products'] = array();

		$products = explode(',', $this->config->get('featured_product'));

		if (empty($setting['limit'])) {
			$setting['limit'] = 5;
		}

		$this->data['position'] = $setting['position'];

		$products = array_slice($products, 0, (int)$setting['limit']);

		foreach ($products as $product_id) {
			$result = $this->model_catalog_product->getProduct($product_id);

			if ($result) {
				if ($result['image']) {
					$image = $this->model_tool_image->cropsize($result['image'], $setting['image_width'], $setting['image_height']);
				} else {
					$image = false;
				}
				$limit_text = 120;
				if (empty($result['short_description'])) {
					$short_description = $this->model_tool_t2vn->cut_string(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'), $limit_text);
				} else {
					$short_description = $this->model_tool_t2vn->cut_string(html_entity_decode($result['short_description'], ENT_QUOTES, 'UTF-8'), $limit_text);
				}
				if ($result['price']>0){
						$price = number_format($result['price'],0,'',' ') . 'đ';
				} else {
						$price = $this->language->get('call_for_price');
				}
				$this->data['products'][] = array(
					'product_id' => $result['product_id'],
					'thumb'   	 => $image,
					'price'    	 => $price,
					'name'    	 => $result['name'],
					'full_description'    	 => html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'),
					'description'    	 => $short_description,
					'href'    	 => $this->url->link('product/product', 'product_id=' . $result['product_id'])
				);
			}
		}

		$this->document->addScript('static/swiper.min.js');
		$script = "
		var mySwiper = new Swiper ('#featured', {
		    pagination: false,
				slidesPerView: 3,
		    nextButton: '.prev-featured',
		    prevButton: '.next-featured',
				spaceBetween: 15,
				breakpoints: {
					768: {
							 slidesPerView: 3,
							 spaceBetween: 30
					 }
				}
		});";
		$this->document->addExScript($script);
		$this->template = 'module/featured.tpl';
		$this->render();
	}
}
?>
