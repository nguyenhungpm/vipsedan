<?php

class ControllerModuleMenu extends Controller {

	private $menu_id = 0;

    protected function index($setting) {

		$this->menu_id = $setting['menu_id'];

    static $module = 0;
    $this->load->model('menumanager/menu');
    $this->data['position'] = $setting['position'];

		$this->data['menuname'] = $this->model_menumanager_menu->getMenuname($setting['menu_id']);

		$link = urldecode(substr($_SERVER['REQUEST_URI'],13));

		$query = $this->db->query("SELECT * FROM ever_menu WHERE url = '". $link ."' AND group_id = '". $setting['menu_id'] ."'");
		$id_active = array();
		if($query->rows){
			foreach($query->rows as $rs){
				$id_active[] = $rs['id'];
			}
		} elseif(!empty($this->request->get['product_id'])){
			$qr_c = $this->db->query("SELECT category_id FROM " . DB_PREFIX . "product_to_category WHERE product_id='" . $this->request->get['product_id'] . "' limit 1");

			if($qr_c->row){
				$qr = $this->db->query("SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = CONCAT('category_id=',". $qr_c->row['category_id'] .")");
				if($qr->row){
					$link = $qr->row['keyword'];
				}else{
					$link = 'index.php?route=product/category&path='. $qr_c->row['category_id'];
				}
			}else{
				$link = 'product/all';
			}

			$query = $this->db->query("SELECT * FROM ever_menu WHERE url = '". $link ."' AND group_id = '". $setting['menu_id'] ."'");
			if($query->rows){
				foreach($query->rows as $rs){
					$id_active[] = $rs['id'];
				}
			}
		} elseif(!empty($this->request->get['news_id'])){
			$qr_c = $this->db->query("SELECT news_category_id FROM " . DB_PREFIX . "news_to_category WHERE news_id='" . $this->request->get['news_id'] . "' limit 1");

			if($qr_c->row){
				$qr = $this->db->query("SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = CONCAT('news_category_id=',". $qr_c->row['news_category_id'] .")");
				if($qr->row){
					$link = $qr->row['keyword'];
				}else{
					$link = 'index.php?route=news/news_category&cat_id='. $qr_c->row['news_category_id'];
				}
			}else{
				$link = 'news/all';
			}

			$query = $this->db->query("SELECT * FROM ever_menu WHERE url = '". $link ."' AND group_id = '". $setting['menu_id'] ."'");
			if($query->rows){
				foreach($query->rows as $rs){
					$id_active[] = $rs['id'];
				}
			}
		} elseif(!empty($this->request->get['path'])){

			$link = 'product/all';

			$query = $this->db->query("SELECT * FROM ever_menu WHERE url = '". $link ."' AND group_id = '". $setting['menu_id'] ."'");
			if($query->rows){
				foreach($query->rows as $rs){
					$id_active[] = $rs['id'];
				}
			}
		} elseif(!empty($this->request->get['cat_id'])){

			$link = 'news/all';

			$query = $this->db->query("SELECT * FROM ever_menu WHERE url = '". $link ."' AND group_id = '". $setting['menu_id'] ."'");
			if($query->rows){
				foreach($query->rows as $rs){
					$id_active[] = $rs['id'];
				}
			}
		} else/* if(strpos($link, 'index.php?route=') == false) */{
			$link = 'index.php?route=' . $link;
			$query = $this->db->query("SELECT * FROM ever_menu WHERE url = '". $link ."' AND group_id = '". $setting['menu_id'] ."'");
			if($query->rows){
				foreach($query->rows as $rs){
					$id_active[] = $rs['id'];
				}
			}
		}

		$menu = $this->model_menumanager_menu->getMenuByGroup($setting['menu_id']);
		$this->data['menu_ul'] = '<ul class="list-inline"></ul>';
		if ($menu) {

			$tree = new Tree;
			foreach ($menu as $row) {
				if(empty($row['title'])) continue;
				$row['id_active'] = $id_active;
				$tree->add_item($row);
			}

			$this->data['menu_ul'] = $tree->generate_list('class="list-inline"');
		}

        $this->template = 'module/menu.tpl';
		$this->render();
    }

}
class Tree {

	/**
	 * variable to store temporary data to be processed later
	 *
	 * @var array
	 */
	var $data;

	/**
	 * Add an item
	 *
	 * @param int $id 			ID of the item
	 * @param int $parent 		parent ID of the item
	 * @param string $li_attr 	attributes for <li>
	 * @param string $label		text inside <li></li>
	 */
	function add_row($id, $parent, $li_attr, $label) {
		$this->data[$parent][] = array(
			'id' => $id,
			'li_attr' => $li_attr,
			'label' => $label
		);
	}

	function add_item($data) {
		$this->data[$data['parent_id']][] = array(
			'id' => $data['id'],
			'class' => $data['class'],
			'id_active' => $data['id_active'],
			'title' => $data['title'],
			'url' => $data['url'],
		);
	}

	/**
	 * Generates nested lists
	 *
	 * @param string $ul_attr
	 * @return string
	 */
	function generate_list($ul_attr = '') {
		return $this->ul(0, $ul_attr);
	}

	/**
	 * Recursive method for generating nested lists
	 *
	 * @param int $parent
	 * @param string $attr
	 * @return string
	 */
	function ul($parent = 0, $attr = '') {
		static $i = 1;
		$indent = str_repeat("\t\t", $i);
		if (isset($this->data[$parent])) {
			if ($attr) {
				$attr = ' ' . $attr;
			}
			$html = "\n$indent";
			$html .= "<ul$attr>";
			$i++;
			foreach ($this->data[$parent] as $row) {
				$child = $this->ul($row['id']);

				$selected = in_array($row['id'], $row['id_active']) ? 'selected':'';
				$class = $row['class'];
				if($selected){
					$class .= ' '.$selected;
				}
				if($child){
					$class .= ' hassub';
				}

				$html .= "\n\t$indent";
				$html .= '<li class="'. $class .'">';
				$html .= '<a href="'. $row['url'] .'">'. $row['title'] .'</a>';
				if ($child) {
					$i--;
					$html .= $child;
					$html .= "\n\t$indent";
				}
				$html .= '</li>';
			}
			$html .= "\n$indent</ul>";
			return $html;
		} else {
			return false;
		}
	}

	/**
	 * Clear the temporary data
	 *
	 */
	function clear() {
		$this->data = array();
	}
}

?>
