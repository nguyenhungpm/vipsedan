<?php
class ControllerModuleProductCategory extends Controller {
	protected function index($setting) {
		static $module = 0;

		$this->language->load('module/product_category');
    $this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_readmore'] = $this->language->get('text_readmore');

		$this->load->model('catalog/product');
		$this->load->model('catalog/category');
		$this->load->model('tool/image');
		$this->data['position'] = $setting['position'];
		$this->data['products'] = array();

		$this->data['title'] = $setting[$this->config->get('config_language_id')]['title'];

		$info_category = $this->model_catalog_category->getCategory($setting['cat_p']);

		if(isset($info_category['name'])){
			$data = array(
				'filter_category_id' =>$info_category['category_id'],
				'sort'  => $setting['sort'],
				'order' => $setting['type'],
				'start' => 0,
				'limit' => $setting['limit']
			);

			$results = $this->model_catalog_product->getProducts($data);
			foreach ($results as $result) {
				if ($result['image']) {
					// $image = 'image/'.$result['image'];
					$image = $this->model_tool_image->cropsize($result['image'], $setting['image_width'], $setting['image_height']);
				}else {
					$image = false;
				}

				$this->data['link'] = $this->url->link('product/category', 'path=' . $info_category['category_id']);
				if ($result['price']>0){
						$price = number_format($result['price'],0,'',' ') . 'đ';
				} else {
						$price = $this->language->get('call_for_price');
				}
			$this->data['products'][] = array(
				'product_id' => $result['product_id'],
				'thumb'   	 => $image,
				'price'   	 => $price,
				'name'    	 => $result['name'],
				'href'    	 => $this->url->link('product/product', 'product_id=' . $result['product_id']),
			);
			}
		}
		$this->document->addScript('static/swiper.min.js');
		$script = "
		var mySwiper = new Swiper ('#category-".$module."', {
		    pagination: false,
				slidesPerView: 3,
		    nextButton: '.prev-category".$module."',
		    prevButton: '.next-category".$module."',
		    autoplay:false,
				spaceBetween: 15,
		    breakpoints: {
					768:{
						slidesPerView: 2
					}
			}
		});";
		$this->document->addExScript($script);
		$this->data['module'] = $module++;
		$this->template = 'module/product_category.tpl';
		$this->render();
	}
}
?>
