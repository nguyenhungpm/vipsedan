<?php
class ControllerModuleSlideshow extends Controller {
	protected function index($setting) {
		static $module = 0;
		$this->load->model('design/banner');
		$this->load->model('tool/image');
		$this->data['banners'] = array();

		if (isset($setting['banner_id'])) {
			$results = $this->model_design_banner->getBanner($setting['banner_id']);

			foreach ($results as $result) {
				if (file_exists(DIR_IMAGE . $result['image'])) {
					$this->data['banners'][] = array(
						'title' => $result['title'],
						'description' => $result['description'],
						'link'  => $result['link'],
						// 'image' => 'image/'.$result['image']
						'image' => $this->model_tool_image->cropsize($result['image'], $setting['width'], $setting['height'])
					);
				}
			}
		}
		$this->document->addScript('static/swiper.min.js');
		$script = "
		var mySwiper = new Swiper ('#slideshow', {
		    pagination: '.swiper-pagination',
		    centeredSlides: true,
		    effect: 'slide',
		    nextButton: '.arrow-n-s',
		    prevButton: '.arrow-p-s',
		    autoplay:5000,
		    paginationClickable: true,
		    breakpoints: {
				1024: {
					height:500
					}
			}
		});";
	$this->document->addExScript($script);

		$this->data['module'] = $module++;

		$this->template = 'module/slideshow.tpl';

		$this->render();
	}
}
?>
