<?php
class ControllerModuledathang extends Controller {
	protected function index($setting) {
		$this->language->load('module/dathang');
      	$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['position'] = !empty($setting['position']) ? $setting['position'] :'';
		
		$this->load->model('catalog/product'); 
		$data = array(
			'filter_category_id' => 0,
			'filter_sub_category' => true,
		);
		$this->data['products'] = $this->model_catalog_product->getProducts($data);
		// print_r($this->data['products']);
		
		$this->template = 'module/dathang.tpl';
		$this->render();
	}
	
	public function add() {
        $json = '';
        $this->load->model('module/dathang');
		$this->session->data['d'] = $this->request->post;
        if (empty($this->request->post['name'])) {
            $json .= '1,';
        }

        if (empty($this->request->post['telephone'])) {
            $json .= '3,';
        }

        if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['email']) || empty($this->request->post['email'])) {
            $json .= '5,';
        }
		
		if (empty($this->session->data['captcha']) || ($this->session->data['captcha'] != $this->request->post['captcha'])) {
			$json .= '7,';
		}
		
		if($json==''){
			$this->model_module_dathang->addOrder($this->request->post);
			$message = '';
			
			if($this->request->post['name']){
				$message .= '<span>Tên người đặt: '. $this->request->post['name'] .'</span><br />';
			}
			
			if($this->request->post['telephone']){
				$message .= '<span>Điện thoại: '. $this->request->post['telephone'] .'</span><br />';
			}
						
			if($this->request->post['email']){
				$message .= '<span>Email: '. $this->request->post['email'] .'</span><br />';
			}
			
			if($this->request->post['content']){
				$message .= '<span>Nội dung: '. $this->request->post['content'] .'</span><br />';
			}
			
			/* $mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->hostname = $this->config->get('config_smtp_host');
			$mail->username = $this->config->get('config_smtp_username');
			$mail->password = $this->config->get('config_smtp_password');
			$mail->port = $this->config->get('config_smtp_port');
			$mail->timeout = $this->config->get('config_smtp_timeout');
			$mail->setTo('dungdo@osd.vn'); 
			$mail->setFrom('no-reply@osd.vn');
			$mail->setSender($this->request->post['order_name']);
			$mail->setSubject('Yêu cầu tư vấn');
			$mail->setHtml($message);
			$mail->send(); */
		}

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
}
?>