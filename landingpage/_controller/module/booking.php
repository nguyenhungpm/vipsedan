<?php
class ControllerModuleBooking extends Controller {
	protected function index($setting) {
		$this->language->load('module/booking');
      	$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['position'] = !empty($setting['position']) ? $setting['position'] :'';
		
		$this->template = 'module/booking.tpl';
		$this->render();
	}
	
	public function distance(){
		$origins = str_replace(" ","+",$this->request->post['origins']);
		$destinations = str_replace(" ","+",$this->request->post['destinations']);
		// $origins = 'Hồng+Thuận';
		// $destinations = 'Giao+Lac';

		$url = "https://maps.googleapis.com/maps/api/distancematrix/json?key=AIzaSyCAsAB47nPnc92FCSYKoVfWFGM_ysAGKCI&origins=" .$origins. "&destinations=" .$destinations;
		// echo $url;
		ini_set("allow_url_fopen", 1);
		$json = file_get_contents($url);
		$obj = json_decode($json, true);
		$status = $obj['status'];
		// print_r($status);
		$json = array();
		if($status == 'OK'){
			$check = $obj['rows'][0]['elements'][0]['status'];
			$distance = ($check=='OK') ? $obj['rows'][0]['elements'][0]['distance']['text'] : 0; 
			$value = ($check=='OK') ? $obj['rows'][0]['elements'][0]['distance']['value'] : 0;
			$number_seats = $this->request->post['number_seats'];
			$price = (int)$this->config->get('config_seat_'.$number_seats) * round($value/1000, 2);
			
			$json[] = array(
				'destination' => $obj['destination_addresses'][0],
				'origin' => $obj['origin_addresses'][0],
				'distance' => $distance,
				'value' => $value,
				'price' => number_format($price,0,",",".") . ' vnđ',
				'status' => $check
			);
		}else{
			$json[] = array(
				'status' => $status
			);
		}
		
		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
        // $this->response->setOutput($json);
	}

	public function booking(){
		$json = array();

		$servername = "localhost";
		$username = "vipsedan_2018";
		$password = "Hung@1993";
		$dbname = "vipsedan_2018";

		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);

		// Check connection
		if ($conn->connect_error) {
		    $json['error'] = 'Lỗi kết nối dữ liệu';
		    die("Connection failed: " . $conn->connect_error);
		} 
		mysqli_set_charset($conn,"utf8");

		$sql = "INSERT INTO se_booking SET customer = '". $this->request->post['name'] ."', telephone = '". $this->request->post['telephone'] ."', origin = '". $this->request->post['origins'] ."', destination = '". $this->request->post['destinations'] ."', note = '". $this->request->post['note'] ."', type_car = '". $this->request->post['number_seats'] ."', date_execute = '". $this->request->post['date']. " ". $this->request->post['time'] ."'";

		if ($conn->query($sql) === TRUE) {
		    $json['success'] = "New record created successfully";
		} else {
		    $json['error'] = "Error: " . $sql . "<br>" . $conn->error;
		}

		$conn->close();

		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
	}
}
?>