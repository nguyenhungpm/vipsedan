<?php
class ModelCatalogSchedule extends Model {
	function checkBooking($booking_id){
		$sql = "SELECT user_id FROM " . DB_PREFIX . "booking WHERE booking_id = '". $booking_id ."'";

		$query = $this->db->query($sql);

		return $query->row['user_id'] == 0 ? false:true ;
	}

	function checkUserBooking($booking_id){
		$sql = "SELECT count(user_id) as total FROM " . DB_PREFIX . "booking WHERE booking_id = '". $booking_id ."' AND user_id = '". $this->user->getId() ."'";

		$query = $this->db->query($sql);

		return $query->row['total'] == 0 ? false:true ;
	}

	function assignBooking($booking_id){
		$query = $this->db->query("UPDATE " . DB_PREFIX . "booking SET user_id = '". $this->user->getId() ."', state='pending' WHERE booking_id = '". $booking_id ."'");
		$this->load->model('user/coin');
		$this->load->model('catalog/booking');
		$booking_info = $this->model_catalog_booking->getBooking($booking_id);
		$data['coin'] = $booking_info['money'];
		$data['user_id'] = $this->user->getId();
		$data['note'] = 'Nhận cuốc '. $booking_info['origin'] . ' --> '.$booking_info['destination'] . ' ('. $booking_info['date_execute'] .')';
		$id = $this->model_user_coin->addCoin($data);
	}

	function changeStatusBooking($booking_id, $state){
		$query_ck = $this->db->query("SELECT state, money FROM " . DB_PREFIX . "booking WHERE booking_id = '". $booking_id ."' AND user_id = '". $this->user->getId() ."'");
		if($state == 'draft' && $query_ck->row['state']!='done'){
			$query = $this->db->query("UPDATE " . DB_PREFIX . "booking SET user_id = '0', state = '". $state ."' WHERE booking_id = '". $booking_id ."' AND user_id = '". $this->user->getId() ."'");
			$this->db->query("UPDATE `" . DB_PREFIX . "user` SET coin = coin + ". (int)$query_ck->row['money'] ." WHERE user_id = '". $this->user->getId() ."'");
	
		}else{
			$query = $this->db->query("UPDATE " . DB_PREFIX . "booking SET state = '". $state ."' WHERE booking_id = '". $booking_id ."' AND user_id = '". $this->user->getId() ."'");
		}
	}
}