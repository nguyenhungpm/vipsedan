<?php
class ModelUserCoin extends Model {
	public function addCoin($data) {
		$this->db->query("INSERT INTO `" . DB_PREFIX . "coin` SET note = '" . $this->db->escape($data['note']) . "', user_id = '" . (int)$data['user_id'] . "', coin = '" . (int)$data['coin'] . "', date_added = NOW()");
		$this->db->query("UPDATE `" . DB_PREFIX . "user` SET coin = coin - ". $data['coin'] ." WHERE user_id = '". $data['user_id'] ."'");
	
		return $this->db->getLastId();
	}

	public function editCoin($coin_id, $data) {
		$this->db->query("UPDATE `" . DB_PREFIX . "coin` SET username = '" . $this->db->escape($data['username']) . "', coin_group_id = '" . (int)$data['coin_group_id'] . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', image = '" . $this->db->escape($data['image']) . "', status = '" . (int)$data['status'] . "' WHERE coin_id = '" . (int)$coin_id . "'");

		if ($data['password']) {
			$this->db->query("UPDATE `" . DB_PREFIX . "coin` SET salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "' WHERE coin_id = '" . (int)$coin_id . "'");
		}
	}

	public function editPassword($coin_id, $password) {
		$this->db->query("UPDATE `" . DB_PREFIX . "coin` SET salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($password)))) . "', code = '' WHERE coin_id = '" . (int)$coin_id . "'");
	}

	public function editCode($email, $code) {
		$this->db->query("UPDATE `" . DB_PREFIX . "coin` SET code = '" . $this->db->escape($code) . "' WHERE LCASE(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");
	}

	public function deleteCoin($coin_id) {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "coin` WHERE coin_id = '" . (int)$coin_id . "'");
	}

	public function getCoin($coin_id) {
		$query = $this->db->query("SELECT *, (SELECT ug.name FROM `" . DB_PREFIX . "coin_group` ug WHERE ug.coin_group_id = u.coin_group_id) AS coin_group FROM `" . DB_PREFIX . "coin` u WHERE u.coin_id = '" . (int)$coin_id . "'");

		return $query->row;
	}

	public function getCoinByCoinname($username) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coin` WHERE username = '" . $this->db->escape($username) . "'");

		return $query->row;
	}

	public function getCoinByEmail($email) {
		$query = $this->db->query("SELECT DISTINCT * FROM `" . DB_PREFIX . "coin` WHERE LCASE(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		return $query->row;
	}

	public function getCoinByCode($code) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coin` WHERE code = '" . $this->db->escape($code) . "' AND code != ''");

		return $query->row;
	}

	public function getCoins($data = array()) {
		$sql = "SELECT * FROM `" . DB_PREFIX . "coin`";

		if (!empty($data['filter_name'])) {
			$sql .= " WHERE username LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		$sort_data = array(
			'username',
			'status',
			'date_added'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY username";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalCoins() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "coin`");

		return $query->row['total'];
	}

	public function getTotalCoinsByGroupId($coin_group_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "coin` WHERE coin_group_id = '" . (int)$coin_group_id . "'");

		return $query->row['total'];
	}

	public function getTotalCoinsByEmail($email) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "coin` WHERE LCASE(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		return $query->row['total'];
	}
}