<?php
class ControllerCatalogSchedule extends Controller {
	private $error = array();

	public function index() {
		$this->document->setTitle('Lịch đặt xe');
		$this->load->model('catalog/booking');
		$this->load->language('catalog/schedule');
		
		$this->getList();
	}
	
	protected function getList() {

		$data['user_token'] = $this->session->data['user_token'];
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'b.date_execute';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}
		
		$data['detail'] = $this->url->link('catalog/schedule/detail', 'user_token=' . $this->session->data['user_token'], true);
		
		$data['bookings'] = array();

		$filter_data = array(
			'sort'            => $sort,
			'order'           => $order,
			'start'           => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'           => $this->config->get('config_limit_admin')
		);

		$this->load->model('tool/image');

		$booking_total = $this->model_catalog_booking->getTotalBookings($filter_data);

		$results = $this->model_catalog_booking->getBookings($filter_data);

		foreach ($results as $result) {
			$data['bookings'][] = array(
				'booking_id' => $result['booking_id'],
				'date_execute'       => $result['date_execute'],
				'distance'       => $result['distance'],
				'type_car'       => $result['type_car'],
				'origin'       => $result['origin'],
				'note'       => $result['note'],
				'state'       => $result['state'],
				'user_id'       => $result['user_id'],
				'destination'       => $result['destination'],
				'money'       => number_format($result['money'],0,",","."),
				'view'       => $this->url->link('catalog/schedule/detail', 'user_token=' . $this->session->data['user_token'] . '&booking_id=' . $result['booking_id'], true)
			);
		}

		// print_r($data['bookings']);
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/schedule_list', $data));
	}
	
	public function detail() {
		$data['user_token'] = $this->session->data['user_token'];
		$this->load->language('catalog/schedule');
		$this->document->setTitle('Chi tiết lịch đặt xe');
		
		$this->load->model('catalog/booking');
		$this->load->model('catalog/manufacturer');
		if(!empty($this->request->get['booking_id'])){
			$data['detail'] = $this->model_catalog_booking->getBooking($this->request->get['booking_id']);
			
			if (!empty($data['detail'])) {
				$manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($data['detail']['manufacturer_id']);

				if ($manufacturer_info) {
					$data['detail']['manufacturer'] = $manufacturer_info['name'];
				} else {
					$data['detail']['manufacturer'] = '';
				}
			}
		}
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/schedule_form', $data));
	}

	public function booking(){
		$json = array();
		$this->load->model('catalog/schedule');

		if(isset($this->request->get['booking_id'])){
			$ck = $this->model_catalog_schedule->checkBooking($this->request->get['booking_id']);
			if($ck){
				$json['error'] = 'Chuyến xe đã được nhận bởi tài xế khác.';
			}else{
				$this->model_catalog_schedule->assignBooking($this->request->get['booking_id']);
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function changeStatus(){
		$json = array();
		$this->load->model('catalog/schedule');

		if(isset($this->request->post['booking_id'])){
			$ck = $this->model_catalog_schedule->checkUserBooking($this->request->post['booking_id']);
			if(!$ck){
				$json['error'] = 'Bạn không có quyền thực thi trên cuốc xe này.';
			}else{
				$this->model_catalog_schedule->changeStatusBooking($this->request->post['booking_id'], $this->request->post['state']);
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}