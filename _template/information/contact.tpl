<?php echo $header; ?>
<div class="map">
		<?php echo $content_top; ?>
</div>
<div class="bg-silver">
<div class="container">
<div class="clear row">
    <div class="col-xs-12 col-sm-4 contactleft gutter">
    	<div class="gutter small uppercase breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
				    <?php foreach ($breadcrumbs as $key=>$breadcrumb) { ?>
				    	<?php echo $breadcrumb['separator']; ?><span itemprop="itemListElement" itemscope
		      itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo $breadcrumb['href']; ?>"><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
				    	    <meta itemprop="position" content="<?php echo $key+1; ?>" />
				    	</a></span>
				    <?php } ?>
		</div>
		<h1 class="h3 hide"><?php echo $heading_title; ?></h1>
		<h2 class="strong uppercase h4 n-mt blue"><?php echo $store; ?></h2>
		<p><i class="icon-location"></i> <?php echo $address; ?></p>
		<hr />
		<p><i class="icon-phone"></i> <a href="tel:<?php echo $telephone; ?>"><?php echo $telephone; ?></a> <?php echo $fax ? '|  <strong>Fax: </strong> '.$fax : ''; ?></p>
		<hr />
		<p><i class="icon-mail"></i> <a href="mailto:<?php echo $email2; ?>"><?php echo $email2; ?></a></p>
    </div>
    <div class="col-xs-12 col-sm-8 contactpage zonegutter">
    	<div class="card gutter">
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
				<h2 class="h4 uppercase mid-color strong text-center"><?php echo $text_contact; ?></h2>
				<div class="row clear">
					<div class="col-xs-12 col-sm-3">
						<?php echo $entry_name; ?> *
					</div>
					<div class="col-xs-12 col-sm-9">
						<input type="text" name="name" value="<?php echo $name; ?>" placeholder="" />
						<?php if ($error_name) { ?>
							<div class="error"><?php echo $error_name; ?></div>
						<?php } ?>
					</div>
				</div>
				<div class="row clear">
					<div class="col-xs-12 col-sm-3">
						<?php echo $text_phone; ?>
					</div>
					<div class="col-xs-12 col-sm-9">
						<input type="text" name="phone" value="<?php echo $phone; ?>" placeholder="" />
					</div>
				</div>
				<div class="row clear">
					<div class="col-xs-12 col-sm-3">
						<?php echo $entry_email; ?> *
					</div>
					<div class="col-xs-12 col-sm-9">
						<input type="text" name="phone" value="<?php echo $phone; ?>" placeholder="" />
						<?php if ($error_email) { ?>
							<div class="error"><?php echo $error_email; ?></div>
						<?php } ?>
					</div>
				</div>
				<div class="row clear">
					<div class="col-xs-12 col-sm-3">
						<?php echo $entry_enquiry; ?> *
					</div>
					<div class="col-xs-12 col-sm-9">
						<textarea name="enquiry" cols="30" rows="3" placeholder=""><?php echo $enquiry; ?></textarea>
						<?php if ($error_enquiry) { ?>
							<div class="error"><?php echo $error_enquiry; ?></div>
						<?php } ?>
					</div>
				</div>
				<div class="clear row">
					<div class="col-xs-12 col-sm-3">
						<?php echo $entry_captcha; ?> *
					</div>
					<div class="col-xs-12 col-sm-9">
						<button type="submit" class="button right uppercase silver h6">Gửi liên hệ <i class="icon-paper-plane"></i></button>
						<input type="text" name="captcha" value="<?php echo $captcha; ?>" />
						<img src="index.php?route=information/contact/captcha" alt="" class="left"/>
						<?php if ($error_captcha) { ?>
							<div class="error clear"><?php echo $error_captcha; ?></div>
						<?php } ?>
					</div>
				</div>
			</form>
		</div>
	</div>

</div>
</div>
</div>
<?php echo $footer; ?>