<?php echo $header; ?>
<div class="container">
<div class="row clear">
<div class="col-xs-12 col-sm-8 col-wide rowgutter">
  	<div class="small uppercase grey breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
			    <i class="icon-icons-47"></i> <?php foreach ($breadcrumbs as $key=>$breadcrumb) { ?>
			    	<?php echo $breadcrumb['separator']; ?><span itemprop="itemListElement" itemscope
	      itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo $breadcrumb['href']; ?>"><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
			    	    <meta itemprop="position" content="<?php echo $key+1; ?>" />
			    	</a></span>
			    <?php } ?>
		</div>
  		<h1 class="n--m darkblue" itemprop="name"><?php echo $heading_title; ?></h1>
  		<div class="grey">Người hỏi: <strong><?php echo $name; ?></strong> <span class="silver">|</span> <?php echo $create_time; ?></div>
	  	<h2 class="short-desc h4 roboto" itemprop="description"><?php echo $short_description; ?></h2>
		<hr />
		<div class="newscontent">
			  <?php echo $description; ?>
		</div>
	  	<div class="shareme small gutter">
	    	<a href="https://twitter.com/share?url=<?php echo $url; ?>" target="_blank" class="twitter-icon inline-block" rel="nofollow">Twitter Share</a>
	    	<a href="https://plus.google.com/share?url=<?php echo $url; ?>" target="_blank" class="plus-icon inline-block" rel="nofollow">Google+ Share</a>
	    	<a href="http://facebook.com/share.php?u=<?php echo $url; ?>" target="_blank" class="face-icon inline-block" rel="nofollow">Facebook Share</a>
	  	</div>
	   	<?php if ($other_faqs) { ?>
	   	<div class="othernews gutter">
	      <h3 class="title_under red"><span>Câu hỏi khác</span></h3>
	      <ul>
	      	<?php foreach ($other_faqs as $other_faq) { ?>
	      		<li style="padding: 2px;"><a href="<?php echo $other_faq['href']; ?>"><?php echo $other_faq['name']; ?></a> <span class="grey">(<?php echo $other_faq['create_time']; ?>)</span></li>
	      	<?php } ?>
	     	</ul>
	    </div>
	   <?php } ?>
	   <?php echo $content_bottom; ?>	   
   </div>
	<div class="col-xs-12 col-sm-4 rowgutter">
		<?php echo $column_right; ?>
	</div>

</div>
<?php echo $footer; ?>