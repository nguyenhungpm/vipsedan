<?php echo $header; ?>
<div class="rowgutter grey small uppercase breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
	<div class="container">
		    <i class="icon-icons-47"></i> <?php foreach ($breadcrumbs as $key=>$breadcrumb) { ?>
		    	<?php echo $breadcrumb['separator']; ?><span itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo $breadcrumb['href']; ?>"><span itemprop="name" class="grey"><?php echo $breadcrumb['text']; ?></span>
		    	    <meta itemprop="position" content="<?php echo $key+1; ?>" />
		    	</a></span>
		    <?php } ?>
	</div>
</div>
<div class="container">
<div class="clear row">
    <div class="col-xs-12 col-sm-8 border-right">
		<h1 class="darkblue n-mt"><?php echo $heading_title; ?><?php echo $admin_logged ? ' (<a target="_blank" href="'.$edit.'">Sửa</a>)': ''?></h1>
		<div class="newscontent">
			  <?php echo $description; ?>
		</div>
		<?php echo $column_left; ?>
	</div>
	<div class="col-xs-12 col-sm-4 rowgutter">
		<?php echo $column_right; ?>
	</div>
</div>
</div>
<?php echo $footer; ?>
