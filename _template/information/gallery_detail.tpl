<?php echo $header; ?>
<div class="rowgutter breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
	<div class="container">
		    <?php foreach ($breadcrumbs as $key=>$breadcrumb) { ?>
		    	<?php echo $breadcrumb['separator']; ?><span itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo $breadcrumb['href']; ?>"><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
		    	    <meta itemprop="position" content="<?php echo $key+1; ?>" />
		    	</a></span>
		    <?php } ?>
	</div>
</div>
<div class="container">
<div class="clear row">
	<?php echo $content_top; ?>
  <div class="col-xs-12 col-sm-10">
		<?php if($pictures) { ?>
		<h1 class="n-m-t rowgutter"><?php echo $info['name'];?></h1>
		<div class="gallery row clear">
			<?php foreach($pictures as $picture){ ?>
				<div class="col-6 col-sm-4 col-desktop-3">
				<a class="albumcover relative block" href="<?php echo $picture['popup']; ?>">
						<img data-href="<?php echo $picture['popup']; ?>" class="block picture" alt="<?php echo $picture['name'];?>" src="<?php echo $picture['thumb'];?>" />
						<span class="absolute overlay"></span>
						<i class="icn i-size-fullscreen absolute color-white"></i>
				</a>
				</div>
			<?php } ?>
		</div>
		<?php }else{ ?>
				<?php echo $text_empty; ?>
		<?php } ?>
	</div>
	<div class="col-xs-12 col-sm-2">
			<?php foreach ($pictures_list as $information) { ?>
			<div class="rowgutter text-center">
				<a href="<?php echo $information['href']; ?>" class="thumb <?php echo $information['picture_id'] == $picture_id ? 'active':''; ?>">
					<img src="<?php echo $information['thumb']; ?>" alt="<?php echo $information['name']; ?>" class="block" /></a>
					<h3 class="h5 font-normal n-mb"><a href="<?php echo $information['href']; ?>"><?php echo $information['name']; ?></a></h3>
			</div>
			<?php } ?>
		</div>
	</div>
</div>
</div>
<?php echo $footer; ?>
