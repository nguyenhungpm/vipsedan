<?php if($position=='content_bottom') { ?>
<div class="container text-center">
		<a href="#timdaily" class="timdaily"><img src="<?php echo $timdaily_image; ?>" alt="tim dai ly" class="block"/></a>
</div>
<div class="hide">
    <div id="timdaily" class="bg-white white-popup">
      <h2 class="red n-mt text-center">Đăng ký trở thành đại lý</h2>
      <div id="frm_daily" class="relative">
        <div class="mingutter">
            <input type="text" value="" class="full-width" name="daily_name" placeholder="Tên của bạn" id="daily_name" />
        </div>
        <div class="mingutter">
            <input type="text" value="" class="full-width" name="daily_email" placeholder="Địa chỉ email" id="daily_email" />
        </div>
        <div class="mingutter">
            <input type="text" value="" class="full-width" name="daily_phone" placeholder="Điện thoại" id="daily_phone" />
        </div>
        <div class="mingutter text-center"><span class="bg-red white text-center button" onclick="email_daily();">Đăng ký</span></div>
      </div>
      <div class="text-left" id="daily_result"></div>
    </div>
</div>
<?php } else { ?>
	<div class="text-center">
			<a href="#timdaily" class="timdaily"><img src="<?php echo $timdaily_image; ?>" alt="tim dai ly" class="block"/></a>
	</div>
	<div class="hide">
	    <div id="timdaily" class="bg-white white-popup">
	      <h2 class="red n-mt text-center">Đăng ký trở thành đại lý</h2>
	      <div id="frm_daily" class="relative">
	        <div class="mingutter">
	            <input type="text" value="" class="full-width" name="daily_name" placeholder="Tên của bạn" id="daily_name" />
	        </div>
	        <div class="mingutter">
	            <input type="text" value="" class="full-width" name="daily_email" placeholder="Địa chỉ email" id="daily_email" />
	        </div>
	        <div class="mingutter">
	            <input type="text" value="" class="full-width" name="daily_phone" placeholder="Điện thoại" id="daily_phone" />
	        </div>
	        <div class="mingutter text-center"><span class="bg-red white text-center button" onclick="email_daily();">Đăng ký</span></div>
	      </div>
	      <div class="text-left" id="daily_result"></div>
	    </div>
	</div>
<?php } ?>
