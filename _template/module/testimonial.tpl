<div class="testimonial_box relative zonegutter">
<div class="container relative">
    <div class="title_under_blue text-center">
    <h3 class="uppercase n-mb"><a class="black" href="<?php echo $showall_url;?>"><?php if ($testimonial_title=="") echo "<br>"; else echo $testimonial_title; ?></a></h3>
      <span class="decor"><span class="inner"></span></span>
    </h3>
    </div>
    <div class="testimonial_body gutter">
    <ul id="Carousel" class="list-unstyled">
        <?php 
        foreach ($testimonials as $testimonial) {?>
          <li class="relative text-center">
              <div class="arrow-testi circle absolute text-center bg-white blue h1">“</div>
              <div class="testimonial_desc center-e bg-white">
                <div class="h4">
                  <?php echo $testimonial['description']; ?>
                </div>
                <?php if(strlen($testimonial['name1'])>1){ ?>
                  <h4 class="strong n-mb text-center"><?php echo $testimonial['name1']; ?> </h4>
                  <div class="grey em"><?php echo $testimonial['city']; ?></div>
                <?php } ?>
              </div>
          </li>
        <?php } ?>
      </ul>
    </div>
</div>
</div>