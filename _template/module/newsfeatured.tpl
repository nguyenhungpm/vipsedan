<?php if ($position == 'content_top'  or $position == 'content_bottom') { ?>
<div class="container featured zonegutter">
<div class="title_under_center text-center">
<h3 class="h4 uppercase n-mb mid-color"><?php echo $heading_title; ?></h3>
	<span class="decor"><span class="inner"></span></span>
</h3>
</div>
<div class="row clear">
		<?php foreach ($newss as $news) { ?>
			<div class="col-6 col-sm-3 rowgutter n--p">
				<a href="<?php echo $news['href']; ?>" class="block relative news-f">
					<img class="block full-width" alt="<?php echo $news['name']; ?>" src="<?php echo $news['thumb']; ?>">
					<div class="absolute overlay_news">
						<span class="block yellow"><i class="icon-icons-10"></i></span>
						<h3 class="h4 white n-mt"><?php echo $news['name']; ?></h3>
					</div>
				</a>
			</div>
		<?php } ?>
</div>
</div>
<?php }elseif($position=='column_right'){ ?>
	<div class="col-xs-12 col-md-4">
		<div class="col-xs-12 rowgutter">
			<h2 class="heading_title"><?php echo $this->language->get('text_tripwithvipsedan'); ?></h2>
			<ul>
				<?php foreach ($newss as $key=> $news) { ?>
					<?php if($key==0){ ?>
						<li class="clearfix">
							<a href="<?php echo $news['href']; ?>"><img src="<?php echo $news['thumb']; ?>" height="50" width="55" alt="<?php echo $news['name']; ?>"/></a>
							<a href="<?php echo $news['href']; ?>"><?php echo $news['name']; ?></a>
						</li>
					<?php } else{ ?>
						<li><a href="<?php echo $news['href']; ?>"><?php echo $news['name']; ?></a></li>
					<?php } ?>
				<?php } ?>
			</ul>
		</div>
	</div>
<?php }else{ ?>
<div class="othernews">
	<h3 class="h4 strong no-margin rowgutter"><?php echo $heading_title; ?></h3>
	<?php foreach ($newss as $news) { ?>
		<div class="clear item">
			<a class="col-3 no-padding" href="<?php echo $news['href'];?>"><img class="block" src="<?php echo $news['thumb'];?>" alt="<?php echo $news['name'];?>"/></a>
			<a class="col-9 color-black no-padding-right" href="<?php echo $news['href'];?>"><?php echo $news['name'];?></a>
		</div>
	<?php } ?>
</div>
<?php } ?>
