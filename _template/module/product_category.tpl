<div class="box relative rowgutter">
	<h2 class="title_under uppercase t-d-n h4"><?php echo $title; ?></h2>
	<div class="swiper-wrapper">
		<?php foreach ($loiichs as $key=>$loiich) { ?>
			<div class="swiper-slide">
				<img src="<?php echo $loiich['thumb'];?>" alt="<?php echo $loiich['name'];?>" class="block"/>
                
                <h3 class=""><?php echo $loiich['name'];?></h3>
                <div class="h5 text-center n-mb name"><?php echo $loiich['description']; ?></div>
			</div>
		<?php } ?>
    </div>
</div>
