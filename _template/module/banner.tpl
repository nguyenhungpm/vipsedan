<div id="banner<?php echo $module; ?>" class="banner__zone rowgutter">
  <?php foreach ($banners as $banner) { ?>
	<div class="relative banner">
	  <?php if ($banner['link']) { ?>
	      <a target="_blank" href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>"/></a>
	  <?php } else { ?>
	      <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" />
	  <?php } ?>
		<span class="absolute badgeprice">
				<sup><?php echo $this->language->get('text_only'); ?></sup><br/>
				<span class="h3 strong"><?php echo $banner['description']; ?></span> k
			</span>
	</div>
  <?php } ?>
</div>
