<?php if ($position == 'adv_top'  or $position == 'content_bottom') { ?>
<div class="rowgutter bg-white">
<div class="container">
	<h2 class="h1 uppercase text-center darkred title_under_center"><?php echo $heading_title; ?></h2>
	<div class="clear row">
		<?php foreach ($newss as $news) { ?>
			<div class="col-6 col-sm-2">
				<a href="<?php echo $news['href']; ?>"><img class="block circle" alt="<?php echo $news['name']; ?>" src="<?php echo $news['thumb']; ?>" /></a>
				<div class="news_content_box">
					<h3 class="n-mb h4 strong text-center"><a class="red" href="<?php echo $news['href']; ?>"><?php echo $news['name']; ?></a></h3>
					<p class="grey hide em n--m"><?php echo $news['date_added'];?></p>
					<p class="desc h5 text-center"><?php echo $news['short_description'];?></p>
				</div>
			</div>
		<?php } ?>
	</div>
</div>
</div>
<?php } elseif ($position == 'column_left') { ?>
<div class="col-xs-12 col-sm-4">
<div class="newslatest">
	<h3 class="h4 n-mt uppercase"><a href="news/blog"><?php echo $heading_title; ?></a></h3>
	<ul>
	<?php foreach ($newss as $news) { ?>
		<li>
				<h3 class="n--m h5 lh11"><a class="darkblue" href="<?php echo $news['href'];?>"><?php echo $news['name'];?></a></h3>
				<span class="grey h6"><?php echo $news['date_added']; ?></span>

		</li>
	<?php } ?>
	</ul>
</div>
</div>
<?php }else{ ?>
<div class="news_box">
	<h2 class="box_title h4 grey uppercase strong"><?php echo $heading_title; ?></h2>
	<?php foreach ($newss as $news) { ?>
		<div class="clear row mingutter n-pb">
			<div class="col-xs-3">
				<div class="thumb_overlay relative">
					<a class="block" href="<?php echo $news['href'];?>"><img class="block" src="<?php echo $news['thumb'];?>" alt="<?php echo $news['name'];?>"/></a>
				</div>
			</div>
			<div class="col-xs-9 n-pl">
				<h3 class="n--m h5 lh11"><a class="darkblue" href="<?php echo $news['href'];?>"><?php echo $news['name'];?></a></h3>
				<span class="grey small"><?php echo $news['date_added']; ?></span>
			</div>
		</div>
	<?php } ?>
</div>
<?php } ?>
<style>
.mingutter {
	padding-top: 5px;
	padding-bottom: 5px;
}
</style>
