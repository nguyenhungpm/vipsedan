<div class="container">
<div class="row clear">
  	<div class="col-xs-12 col-sm-3 zonegutter show-tablet hide-mobile">
		<img src="<?php echo $intro_image; ?>" class="intro_image" alt="<?php echo $heading_title; ?>" />
	</div>
	<div class="col-xs-12 col-sm-9 zonegutter">
		<div class="intro_text">
			<h2 class="n--m h2 lh11 uppercase"><?php echo $heading_title; ?></h2>
			<div class="intro_description h3"><?php echo $content; ?></div>
			<a class="yellow uppercase h6 intro_link" href="<?php echo $intro_link; ?>"><?php echo $button; ?><i class="icon-icons-30"></i></a>
		</div>
	</div>

</div>
</div>
