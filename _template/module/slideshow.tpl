<div class="box">
<div class="swiper-container greybborder" id="slideshow">
    <div class="swiper-wrapper">
    <?php foreach ($banners as $banner) { ?>
	<div class="swiper-slide">
    <?php if ($banner['link']) { ?>
	    <a href="<?php echo $banner['link']; ?>"><img class="block" src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" /></a>
    <?php } else { ?>
    	<img class="block" src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" />
    <?php } ?>
	<?php /*if(!empty($banner['description'])){ ?>
	<div class="absolute description-slide color-white text-left show-desktop show-tablet hide-mobile">
		<h3 class="inline-block n--m"><?php echo $banner['title']; ?></h3>
		<h6 class="n--m"><?php echo $banner['description']; ?></h6>
	</div>
    <?php }*/ ?>
	</div>
    <?php } ?>
</div>
<div class="swiper-pagination"></div>
</div>
</div>
