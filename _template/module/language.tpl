<?php if (count($languages) > 1) { ?>
<form action="<?php echo $action; ?>" method="post" id="formlang" enctype="multipart/form-data" style="display:inline-block;">
  <div id="language" class="inline-block">
    <?php 
    foreach ($languages as $language) { ?>
			<img style="cursor: pointer;" src="media/flags/<?php echo $language['image']; ?>" alt="<?php echo $language['name']; ?>" title="<?php echo $language['name']; ?>" onclick="$('input[name=\'language_code\']').attr('value', '<?php echo $language['code']; ?>'); $(this).parent().parent().submit();" class=""/>
    <?php } ?>
    <input type="hidden" name="language_code" id="idlang"  value="" />
    <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
  </div>
</form>
<?php } ?>
