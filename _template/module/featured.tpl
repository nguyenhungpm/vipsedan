<?php if($position=='column_right'){ ?>
<div class="box">
	<h3 class="title_red n--m uppercase"><?php echo $heading_title; ?></h3>
		<?php foreach ($products as $product) { ?>
				<div class="rowgutter clearfix">
				<div class="text-center gutter border">
			      	<?php if ($product['thumb']) { ?>
			      	<a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" /></a>
			      	<?php } ?>
			      	<h3 class="name h4 n-mb"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h3>
							<span class="color-red strong block"><?php echo $product['price']; ?></span>
							<br />
		    </div>
				</div>
	    <?php } ?>
</div>
<?php } elseif($position=='column_left'){ ?>
	<div class="col-xs-12 col-md-8">
		<?php foreach ($products as $product) { ?>
		<div class="col-xs-12 col-md-6 rowgutter">
			<a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>"/></a>
			<h3>
				<a href="<?php echo $product['href']; ?>" class="left"><?php echo $product['title']; ?></a>
				<?php 
					$note = explode("|", $product['note'][0]);
				?>
				<span class="right"><small><?php echo $this->config->get('config_language_id') == 2 ? $note[0] : $note[1]; ?></small> <b><?php echo $product['note'][1]; ?></b></span>
			</h3>
		</div>
		<?php } ?>
	</div>
<?php }else{ ?>
<div id="featured" class="gray_bg box_lg">
	<style type="text/css">
		.gray_bg{
			background-color: #f5f5f5 /* layer fill content */;
		}
		.box_lg{
			padding-top: 50px;
			padding-bottom: 50px;
		}
		.tuyenphobien {
			border-top: 3px solid #e95e77;
			/* position: relative; */
			background-color: #ffffff;
			padding-bottom: 15px;
			margin-bottom: 15px;
	}
		.tuyenphobien .item > img{
			width: 100%;
		}
		.tuyenphobien .item {
			/*height: 310px;*/
	}
	.tuyenphobien .carousel-caption {
			text-align: left;
			padding: 0;
	}
	.carousel-caption {
			position: relative;
			z-index: 1;
			left: 0;
			top: 10px;
			color: #929292;
			text-shadow: none;
	}
	.carousel-caption h3 {
			color: #fff;
			position: absolute;
			top: -60px;
			left: 10px;
			padding: 2px 10px;
			border-radius: 3px;
			font-size: 12px;
			text-transform: uppercase;
			font-weight: 200;
			background-image: -moz-linear-gradient(left, #00C8FE, #4B57EB);
			background-image: -webkit-gradient(linear, 0 0, 100% 100%, from(#00C8FE), to(#4B57EB));
			background-image: -webkit-linear-gradient(left, #00C8FE, #4B57EB);
			background-image: -o-linear-gradient(left, #00C8FE, #4B57EB);
			background-image: linear-gradient(to right, #00C8FE, #4B57EB);
	}
	.popular_place {
			overflow: hidden;
	}
	.tuyenphobien .popular_place {
			padding: 5px 0;
			font-weight: bold;
			cursor: pointer;
	}
	.carousel-caption .item_price {
			color: #ff9800;
	}
	.box_sm {
			padding-top: 20px;
			padding-bottom: 20px;
	}
	.tuyenphobien h4 {
			text-align: center;
			color: #1ba0e2;
			margin: 0;
			cursor: pointer;
	}
	</style>
	<div class="container">
		<div class="row"> <div class="col-md-12"> <h3 style="text-align: center; margin-top: 0; margin-bottom: 25px;">Một số dịch vụ nổi bật</h3> </div> </div>
		<div class="row">
		<?php foreach ($products as $product) { ?>
			<div class="col-md-4 col-sm-4 box_md box_place">
				<div class="tuyenphobien">
					<div class="item active"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['title']; ?>"></a>
						<div class="carousel-caption">
								<h3 class="title" data-type-id="1" data-place-id="383">Vip Sedan</h3>
								<div class="popular_place col-md-12" data-type-id="1" data-booking-link="<?php echo $product['href']; ?>">
										<div class="col-md-9 item_title"><?php echo $product['title']; ?></div>
										<div class="item_price"><?php echo $product['price']; ?></div>
								</div>
								<table width="100%"> </table>
								<div class="row box_sm">
										<div class="col-md-offset-3 col-md-6 title" data-type-id="1" data-place-id="383">
												<h4><a href="<?php echo $product['href']; ?>">Xem chi tiết</a></h4> </div>
								</div>
						</div>
				</div>
				</div>
			</div>
		<?php } ?>
		</div>
	</div>
</div>
<?php } ?>
