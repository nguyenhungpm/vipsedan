<h3 class="h4 uppercase strong n-mt"><?php echo $heading_title; ?></h3>
<div class="row clear">
<div class="col-xs-12 col-sm-5 n-pr">
	<div class="h6"><?php echo $text_subscribe; ?></div> 
</div>
<div class="col-xs-12 col-sm-7">
	<div id="frm_subscribe" class="relative">
		<input type="text" value="" name="subscribe_email" placeholder="<?php echo $text_email; ?>" id="subscribe_email" class="full-width black round" />
		<a class="darkblue right absolute h3 newsletter-button" href="javascript:;" onclick="email_subscribe()"><i class="icon-icons-32"></i></a>
	</div>
</div>
</div>
<div class="text-left" id="subscribe_result"></div>
<hr />