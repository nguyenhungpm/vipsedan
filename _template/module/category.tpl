<h3 class="heading n--m uppercase mid-color h4">Danh mục sản phẩm</h3>
<ul class="box-category list-unstyled">
<?php foreach ($categories as $category) { ?>
	<li>
      <a href="<?php echo $category['href']; ?>" class="block">
      	<i class="icon-icons-30 right"></i>
      	<?php echo $category['name']; ?></a>
	</li>
<?php } ?>
</ul>