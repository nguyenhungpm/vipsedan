<?php if ($position == 'content_top'  or $position == 'content_bottom') { ?>
<div class="container">
  <h3 class="box-heading text-center uppercase mid-color"><span><?php echo $cat_name; ?></span></h3>
  <div class="rowgutter grey text-center n-pt">
    <?php echo $description; ?>
  </div>
  <div class="box-content">
    <div class="row clear">
      <?php foreach ($newss as $news) { ?>
      <div class="col-6 col-sm-2">
        <?php if ($news['thumb']) { ?>
        <a href="<?php echo $news['href']; ?>"><img class="featured_img block" src="<?php echo $news['thumb']; ?>" alt="<?php echo $news['name']; ?>" /></a>
        <?php } ?>
          <h3 class="blue h4 n-mt lh11 strong"><a class="blue" href="<?php echo $news['href']; ?>"><?php echo $news['name']; ?></a></h3>
         <div class="grey h6"><?php echo $news['short_description'];?></div>
      </div>
      <?php } ?>
    </div>
  </div>
</div>

<?php } else if ($position == 'column_left' or $position == 'column_right') { ?>                            
<div class="box box-news-left">
<h3 class="box-heading"><a href="<?php echo $cathref; ?>"><?php echo $cat_name; ?></a></h3>
<div class="box-content">
    <div class="box-product-left">
      <?php foreach ($newss as $key=>$news) { ?>
      <?php if ($key < $feature) { ?>
	      <div class="newsbycatrow">
	        <?php if ($news['thumb']) { ?>
	        <div class="image"><a href="<?php echo $news['href']; ?>"><img src="<?php echo $news['thumb']; ?>" alt="<?php echo $news['name']; ?>" <?php echo $news['width']; ?> <?php echo $news['height']; ?> /></a></div>
	        <?php } ?>
	        <div class="name"><a href="<?php echo $news['href']; ?>"><?php echo $news['name']; ?></a></div>
	      </div>
      <?php } else { ?>
	     <div class="newsbycatrowsmall">
	       <h3 class="name"><a href="<?php echo $news['href']; ?>"><?php echo $news['name']; ?></a></h3>
	     </div>
      <?php } ?>
      <?php } ?>
  </div>
</div>
</div>
<?php } else if ($position == 'before_footer') {?>
<div class="box-news-cat">
  <h3 class="box-heading"><?php echo $cat_name; ?></h3>
  <div class="box-content">
    <div class="box-product-left">
      <?php foreach ($newss as $news) { ?>
      <div>
        <?php if ($news['thumb']) { ?>
        <div class="image"><a href="<?php echo $news['href']; ?>"><img src="<?php echo $news['thumb']; ?>" alt="<?php echo $news['name']; ?>" <?php echo $news['width']; ?> <?php echo $news['height']; ?> /></a></div>
        <?php } ?>
        <div class="name"><a href="<?php echo $news['href']; ?>"><?php echo $news['name']; ?></a></div>

         <?php echo $news['short_description'];?>
         <div class="clrfix"></div>
      </div>
      <?php } ?>
    </div>
    <a class="orther" href="<?php echo $cathref; ?>">Xem toàn bộ</a>
  </div>
</div>
<?php } ?>