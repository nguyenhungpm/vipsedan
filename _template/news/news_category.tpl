<?php echo $header; ?>
<div class="grey small uppercase rowgutter breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
	<div class="container">
		    <i class="icon-icons-47"></i> <?php foreach ($breadcrumbs as $key=>$breadcrumb) { ?>
		    	<?php echo $breadcrumb['separator']; ?><span itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo $breadcrumb['href']; ?>"><span itemprop="name" class="grey"><?php echo $breadcrumb['text']; ?></span>
		    	    <meta itemprop="position" content="<?php echo $key+1; ?>" />
		    	</a></span>
		    <?php } ?>
	</div>
</div>
<div class="container">
<div class="row clear">
	<div class="col-xs-12 col-sm-8 border-right">
    <h1 class="blue n-mt"><?php echo $heading_title; ?></h1>
    <?php if ($news) { ?>
		<div class="topnews">
		<?php foreach ($news as $key=>$news_item) { ?>
			  <?php if($key==0) { ?>
			  	<div class="first-news">
					<div class="thumb_overlay relative">
						<a class="block" href="<?php echo $news_item['href']; ?>"><img src="<?php echo $news_item['thumb']; ?>" alt="<?php echo $news_item['name']; ?>" class="block" /></a>
					</div>
					<h3 class="n-mb h2"><a href="<?php echo $news_item['href']; ?>" class="color-black"><?php echo $news_item['name']; ?></a></h3>
						<div class="small grey"><i class="icon-icons-59" aria-hidden="true"></i> <?php echo $news_item['date_added']; ?> </div>					  
					 	<div class=""><?php echo $news_item['short_description']; ?></div>
				</div>
				<div class="line"></div>
			  <?php } else { ?>
				  <div class="row clear news-item rowgutter">
					<div class="col-xs-12 col-sm-4">
						<div class="thumb_overlay relative">
							<a class="block" href="<?php echo $news_item['href']; ?>"><img src="<?php echo $news_item['thumb']; ?>" class="block" alt="<?php echo $news_item['name']; ?>" /></a>
						</div>
					</div>
					<div class="col-xs-12 col-sm-8">
					  <h3 class="n--m"><a href="<?php echo $news_item['href']; ?>" class="color-black"><?php echo $news_item['name']; ?></a></h3>
					  <div class="small grey"><i class="icon-icons-59" aria-hidden="true"></i> <?php echo $news_item['date_added']; ?> </div>
					  <div class="grey"><?php echo $news_item['short_description']; ?></div>
					  </div>
				</div>
			  <?php } ?>
		<?php } ?>
		</div>
   	<?php } ?>
    <?php if ($news) { ?>
		<div class="pagination text-center pull-right">
			<?php echo $pagination; ?>
		</div>
	<?php } ?>
	<?php if (!$news) { ?>
		
	<?php } ?>
	</div>
	<div class="col-xs-12 col-sm-4 rowgutter">
		<?php echo $column_right; ?>
	</div>
</div>
</div>
<?php echo $footer; ?>
