<?php echo $header; ?>
<div itemscope itemtype="https://schema.org/CreativeWorkSeries">
<div class="rowgutter small uppercase grey breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
	<div class="container">
			<i class="icon-icons-47"></i>
		    <?php foreach ($breadcrumbs as $key=>$breadcrumb) { ?>
		    	<?php echo $breadcrumb['separator']; ?><span itemprop="itemListElement" itemscope
      itemtype="https://schema.org/ListItem"><a itemprop="item" href="<?php echo $breadcrumb['href']; ?>"><span itemprop="name" class="grey"><?php echo $breadcrumb['text']; ?></span>
		    	    <meta itemprop="position" content="<?php echo $key+1; ?>" />
		    	</a></span>
		    <?php } ?>
	</div>
</div>

<div class="container bg-white">
<div class="clear row">
	<div class="col-xs-12 col-sm-8 border-right">
	<h1 itemprop="name" class="h1 darkblue n-mb"><?php echo $heading_title; ?><?php echo $admin_logged ? ' (<a target="_blank" href="'.$edit.'">Sửa</a>)': ''?></h1>
	<div class='movie_choice'  itemprop="aggregateRating" itemscope itemtype="https://schema.org/AggregateRating">
		<?php
		if($rate['total_rate']==0){
		   $rate['total_rate'] = 1;
		   $rate['total_point'] = 5;
		}
		?>
	    <div id="r1" class="rate_widget">
	        <div id="s1" onclick="rate('1','news','<?php echo $news_id; ?>')" class="star_1 ratings_stars <?php echo (round($average,0) >= 1) ? 'ratings_vote':''?>"></div>
	        <div id="s2" onclick="rate('2','news','<?php echo $news_id; ?>')" class="star_2 ratings_stars <?php echo (round($average,0) >= 2) ? 'ratings_vote':''?>"></div>
	        <div id="s3" onclick="rate('3','news','<?php echo $news_id; ?>')" class="star_3 ratings_stars <?php echo (round($average,0) >= 3) ? 'ratings_vote':''?>"></div>
	        <div id="s4" onclick="rate('4','news','<?php echo $news_id; ?>')" class="star_4 ratings_stars <?php echo (round($average,0) >= 4) ? 'ratings_vote':''?>"></div>
	        <div id="s5" onclick="rate('5','news','<?php echo $news_id; ?>')" class="star_5 ratings_stars <?php echo (round($average,0) >= 5) ? 'ratings_vote':''?>"></div>
	        <span id="point_rs" class="small inline-block">Có <span itemprop="reviewCount"><?php echo $rate['total_rate']; ?></span> bầu chọn /
	                điểm trung bình: <span itemprop="ratingValue"><?php echo round($average,1); ?></span></span>
	    </div>
		<div id="total_vote"></div>
		<input type="hidden" id="point" value=""/>
	</div>

	  <div class="short-desc h4 roboto" itemprop="description"><?php echo $short_description; ?></div>
	  <div class="newscontent">
		  <?php echo $description; ?>
	  </div>
	  <div class="shareme small">
	    	<button onclick="window.open(this.getAttribute('data-href'),this.title,'width=500,height=500'); return false;" data-href="https://twitter.com/share?url=<?php echo $url; ?>" class="twitter-icon rowgutter inline-block" rel="noopener noreferrer"><i class="icn i-scl-twitter"></i> Tweet</button>
	    	<button onclick="window.open(this.getAttribute('data-href'),this.title,'width=500,height=500'); return false;" data-href="https://facebook.com/share.php?u=<?php echo $url; ?>" class="face-icon rowgutter inline-block" rel="noopener noreferrer"><i class="icn i-scl-facebook"></i> Chia sẻ</button>
	  </div>
	  <?php if ($news) { ?>
	      <div class="othernews">
			  	<h2 class="red title"><?php echo $tab_related; ?></h3>
		      <ul>
		      	<?php foreach ($news as $related_news) { ?>
		      		<li><a href="<?php echo $related_news['href']; ?>"><?php echo $related_news['name']; ?></a> <span class="small grey">(<?php echo $related_news['date_added']; ?>)</span></li>
		      	<?php } ?>
		     	</ul>
				</div>
	  <?php } ?>
	  <?php echo $content_bottom; ?>
	  <?php if ($other_news) { ?>
	   <div class="rowgutter">
		  <h3 class="h2 red n--m em"><?php echo $tab_others; ?></h3>
	      	<ul class="n--m">
					<?php foreach ($other_news as $other_news_item) { ?>
						<li class=""><a class="color-black" href="<?php echo $other_news_item['href'];?>"><?php echo $other_news_item['name'];?></a> <em>(<?php echo $other_news_item['date_added'];?>)</em></li>
			    <?php } ?>
			</ul>
	   </div>
	   <?php } ?>
   </div>
	<div class="col-xs-12 col-sm-4 rowgutter">
	   <?php echo $column_right; ?>
	</div>
</div>
</div>
</div>
<?php if($embed) {
	echo $embed;
} ?>
<?php echo $footer; ?>
