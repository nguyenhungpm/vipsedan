<?php echo $header; ?>
    <div class="content">
      	<h1 class="heading"><?php echo $heading_title;?></h1>
        <div class="sapo"><?php echo $short_description;?></div>
        <?php if(!empty($thumb)){ ?>
            <amp-img src="<?php echo $thumb; ?>" width="480" height="<?php echo $target_h; ?>" 
            alt="<?php echo $heading_title; ?>"
            layout="responsive"></amp-img>
        <?php } ?>
        <div class="content_text">
			       <?php echo $description; ?>
        </div>
    </div>
	<div class="relate_box">
		<h2>Các bài khác</h2>
		<ul>
		<?php foreach($relates as $item){ ?>
			<li><a href="<?php echo $item['href'];?>"><?php echo $item['name'];?></a></li>
		<?php } ?>
		</ul>
	</div>
<?php echo $footer;?>
