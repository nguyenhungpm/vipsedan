<!doctype html>
<html amp lang="<?php echo $lang; ?>">
<head>
<meta charset="utf-8">
<title><?php echo $title; ?></title>
<?php foreach ($links as $link) { ?>
	<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1, user-scalable=no" />
    <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
	<script async src="https://cdn.ampproject.org/v0.js"></script>
	<?php if (!empty($google_analytics)) { ?>
	<script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
	<?php } ?>
    <style amp-custom>
    amp-img {margin:.7em 0}
			table.table-scroll {
					display: block;
					overflow-x: auto;
					border-color:#e85e77;
					padding-bottom: .75rem;
					text-align: center;
					white-space: nowrap;
				}
				table.table-scroll tr td:first-child{
					font-weight: bold;
					background: #f9f9f9;
					text-align:left;
				}
				table b, .highlight{
					color: #e85e77;
				}
				.newscontent table tr:nth-child(odd) td{
					background: #f9f9f9
				}
        body,
        html {
        	height: 100%;
        	margin:0;
        	padding:0;
        	line-height:1.46em;
            font-family: Arial, sans-serif;
			animation: none;
        }
				th,td,
				table {
					border-collapse: collapse;
					border: 1px solid #e85e77;
				}
				th,td {
					padding: .25rem
				}
        .ft-cp{background:#eee;text-align:center;padding:5px}
        a{color:#0073aa}
        a:hover{text-decoration:underline}
        #wrap{padding:0 10px}
        .sapo {
            font-weight: 700;
        }
        .related a{
            text-decoration: none
        }
        .relate_box {
            padding-bottom: 15px
        }

        .fixed{position:fixed}
        .related {
            background-color: #f5f5f5;
            margin: 16px 2%;
            display: block;
            color: #111;
            height: 75px;
            padding: 0;
            width: 96%
        }


        .related>span {
            font-size: 16px;
            font-weight: 400;
            display: block;
            vertical-align: top;
            margin: 8px
        }

        .related:hover {
            background-color: #ccc
        }
		.relate_box p, .menu-ft p{padding: 0 15px; margin: 0;}

        .relate_box a{text-decoration:none}
        .relate_box ul{
			padding-left: 35px;
		}

.support__zalo {
    background: url(https://vipsedan.vn/static/icon_zalo.png) center center no-repeat;
    background-size: 100%;
    width: 64px;
    height: 64px;
    right: 18px;
    bottom: 160px;
    text-indent: -999px;
    overflow: hidden;
    display: block;
    z-index: 9999;
}
.support__call {
    background: url(https://vipsedan.vn/static/icon_call.svg) center center no-repeat;
    background-size: 50px;
    width: 52px;
    height: 52px;
    right: 20px;
    bottom: 105px;
    text-indent: -999px;
    overflow: hidden;
    display: block;
    z-index: 9999;
}
.support__messenger {
    background: url(https://vipsedan.vn/static/icon_messenger.svg) center center no-repeat;
    background-size: 52px;
    width: 52px;
    height: 52px;
    right: 20px;
    bottom: 40px;
    text-indent: -999px;
    overflow: hidden;
    display: block;
    z-index: 9999;
}
			.relate_box li{padding-right: 15px;}
        h1.heading {
            font-size: 24px
        }
        .hotline{color:red;font-weight:bold;font-size:16px;text-align:center;display:block;padding:5px}
        .content_text p {
            font-size: 14px
        }
				.header {text-align:center;padding: 10px;border-bottom:1px solid #ea5c76}
				.header img {width:120px;}
        .img_box {
            width: 100%
        }
    </style>
</head>
<body>
<div id="wrap">
	<div class="header">
			<a href="https://vipsedan.vn">
				<amp-img width="186" height="163" src="https://vipsedan.vn/media/data/logo/vipsedan_logo.png" alt="logo"></amp-img>
			</a>
	</div>
