</div>
<a href="tel:<?php  echo $hotline; ?>" class="hotline"><?php echo $hotline; ?></a>
<div class="ft-cp">
	<p>© <?php echo $store; ?>. All rights reserved.</p>
</div>
<?php if (!empty($google_analytics)) { ?>
	<amp-analytics type="googleanalytics">
		<script type="application/json">
		{
		  "vars": {
			"account": "<?php echo $google_analytics; ?>"
		  },
		  "triggers": {
			"trackPageview": {
			  "on": "visible",
			  "request": "pageview"
			}
		  }
		}
		</script>
	</amp-analytics>
<?php } ?>
<a target="_blank" rel="noopener noreferrer" class="support__zalo fixed" target="_blank" href="https://zalo.me/0388155155">Zalo</a>
<a target="_blank" rel="noopener noreferrer" class="support__call fixed" target="_blank" href="tel:0388155155">phone</a>
<a target="_blank" rel="noopener noreferrer" class="support__messenger fixed" target="_blank" href="https://m.me/xesanbaynoibaitienchuyen">messenger</a>
</body>
</html>
