<?php echo $header; ?>
<div class="rowgutter small uppercase grey breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
	<div class="container">
		    <i class="icon-icons-47"></i> <?php foreach ($breadcrumbs as $key=>$breadcrumb) { ?>
		    	<?php echo $breadcrumb['separator']; ?><span itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo $breadcrumb['href']; ?>"><span itemprop="name" class="grey"><?php echo $breadcrumb['text']; ?></span>
		    	    <meta itemprop="position" content="<?php echo $key+1; ?>" />
		    	</a></span>
		    <?php } ?>
	</div>
</div>
<div class="container">
	<div class="clear row searchpage">
		<div class="col-xs-12 col-sm-8 border-right ">
			<h1 class="n--m h2 blue"><?php echo $heading_title; ?></h1>
			<p>
				<?php if ($search) { ?>
					<input type="text" name="search" id="searchkeywordinput" onkeydown="search(event, 'searchkeywordinput');" value="<?php echo $search; ?>" />
					<?php } else { ?>
					<input type="text" name="search" id="searchkeywordinput" onkeydown="search(event, 'searchkeywordinput');" value="<?php echo $search; ?>" placeholder="<?php echo $text_keyword; ?>"  />
				<?php } ?>
			</p>
			<span onclick="searchDesk('searchkeywordinput');" class="button bg-blue h6 white"><i class="icon-icons-49"></i> <?php echo $button_search; ?></span>
			<hr />
			<?php if (!empty($search_rs)) { ?>
                <?php foreach ($search_rs as $news_item) { ?>
                    <div class="clear">
						<h3 class="h4 m-mt strong"><a class="blue" href="<?php echo $news_item['href']; ?>"><?php echo $news_item['name']; ?></a></h3>
						<div class="grey"><?php echo $news_item['short_description']; ?></div>
						<hr />
					</div>
				<?php } ?>
                <div class="pagination small">
                    <?php echo $pagination; ?>
				</div>
			<?php }else { ?>
				<?php echo $text_empty; ?>
			<?php } ?>
		</div>
		<div class="col-xs-12 col-sm-4">
			<?php echo $column_right; ?>
		</div>
	</div>
</div>
<?php echo $footer; ?>
