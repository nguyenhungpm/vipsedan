	<footer itemscope itemtype="http://schema.org/LocalBusiness">
        <div class="footer-black zonegutter">
            <div class="container">
								<div class="row clear">
                <div class="col-md-5">
	                <div class="col-md-4">
	                    <a href="."><img class="logo-desc" itemprop="image" src="https://vipsedan.vn/media/data/logo/vipsedan_logo.png" alt="Công ty TNHH Phát Triển Thương Mại và Du Lịch Ngọc Minh"></a>
	                </div>
	                <div class="col-md-8">
	                    <h4 itemprop="name"><?php echo $config_name; ?></h4>
											<span>Hotline: <span itemprop="telephone"><?php echo $telephone; ?></span></span><br />
											<span>Email: <?php echo $email; ?></span><br />
											<span><?php echo $this->language->get('text_head_office'); ?>: Sảnh A - nhà ga T1 sân bay Nội Bài</span><br />
											<span itemprop="address"><?php echo $this->language->get('text_headquarters'); ?>: <?php echo $address; ?></span><br />
											<span><?php echo $this->language->get('text_taxcode'); ?>: 0109548725</span><br />
	                </div>
                </div>
                <div class="col-md-3">
                    <h4><?php echo $this->language->get('text_representative_office'); ?>:</h4>
                    <?php echo $this->language->get('text_representative_office_content'); ?>
                </div>
                <div class="col-md-4">
                    <h4><?php echo $this->language->get('text_representative_office_other'); ?>:</h4>
                    <?php echo $this->language->get('text_representative_office_other_content'); ?>
                </div>
								<div class="col-md-6">
                    <?php // echo $footer_menu; ?>
                </div>
								</div>
						</div>
        </div>
    </footer>
		<a target="_blank" rel="noopener noreferrer" class="support__zalo fixed" href="https://zalo.me/<?php echo $hotline; ?>">Zalo</a>
		<a target="_blank" rel="noopener noreferrer" class="support__facebook fixed" href="https://m.me/VIPSedanHn">Facebook</a>

		<div id="phonering-alo-phoneIcon" class="fixed phonering-alo-phone phonering-alo-green phonering-alo-show">
				<div class="phonering-alo-ph-circle"></div>
				<div class="phonering-alo-ph-circle-fill"></div>
				<div class="phonering-alo-ph-img-circle">
						<a class="pps-btn-img " title="Liên hệ" href="tel:<?php echo $hotline; ?>"> <img src="https://3.bp.blogspot.com/-jipOkVbgvtk/WPd_CdNwOoI/AAAAAAAAEn0/iYoBqhrSHWgSGDOiEvvEzTYa-khhJt9NACLcB/s1600/v8TniL3.png" alt="Liên hệ" width="32" class="img-responsive"> </a>
				</div>
		</div>
<script type="text/javascript" src="static/js.js"></script>
<style type="text/css">
.fixed {position:fixed}
#phonering-alo-phoneIcon {
	right: 38px;
	bottom: 160px;
}
.support__facebook{
	right: 30px;
	bottom: 40px;
	background: url(static/icon_facebook.png) center center no-repeat;
	background-size:72px;
	width: 72px;
	height: 72px;
	text-indent: -999px;
	overflow: hidden;
	display: block;
}
.support__zalo:before {
    content:'';
    width: 9px;
    height: 9px;
    display: block;
    border-radius: 50%;
    border:1px solid #fff;
    position: absolute;
    background:red;
    top: 9px;
    right: 5px;
    -webkit-animation: argh-my-eyes 3s infinite;
    -moz-animation:    argh-my-eyes 3s infinite;
    animation:         argh-my-eyes 3s infinite;
}
.support__zalo {
    background: url(static/icon_zalo.png) center center no-repeat;
    background-size:52px;
    width: 52px;
    height: 52px;
		right: 38px;
		bottom: 180px;
    text-indent: -999px;
    overflow: hidden;
    display: block;
		z-index:9999999999
}
@-webkit-keyframes argh-my-eyes {
  0%   { background:orange }
  33% { background: #c43550; }
  66% { background:#c42342; }
  100% { background: #c41435; }
}
@-moz-keyframes argh-my-eyes {
  0%   { background:orange; }
  33% { background: #c43550; }
  66% { background:#c42342; }
  100% { background: #c41435; }
}
@keyframes argh-my-eyes {
  0%   { background:orange; }
  33% { background: #c43550; }
  66% { background:#c42342; }
  100% { background: #c41435; }
}

	footer h4{
		font-size: 16px;
		text-transform: uppercase;
	}
	footer li a{
		color: white;
	}
	footer{
		color: white;
	}

.phonering-alo-phone {position:fixed;visibility:hidden;background-color:transparent;width:120px;height:120px;
 cursor:pointer;z-index:9;bottom:-15px;right:-15px;display:block;
 -webkit-backface-visibility:hidden;
 -webkit-transform:translateZ(0);
 transition:visibility .5s;
}
.phonering-alo-phone.phonering-alo-show {visibility:visible}
.phonering-alo-phone.phonering-alo-static {opacity:.6}
.phonering-alo-phone.phonering-alo-hover,.phonering-alo-phone:hover {opacity:1}
.phonering-alo-ph-circle {width:160px;height:160px;top:52px;left:25px;position:absolute;
 background-color:transparent;border-radius:100%;border:2px solid rgba(30,30,30,0.4);
 opacity:.1;
 -webkit-animation:phonering-alo-circle-anim 1.2s infinite ease-in-out;
 animation:phonering-alo-circle-anim 1.2s infinite ease-in-out;
 transition:all .5s;
 -webkit-transform-origin:50% 50%;
 -ms-transform-origin:50% 50%;
 transform-origin:50% 50%
}
.phonering-alo-phone.phonering-alo-active .phonering-alo-ph-circle {
    -webkit-animation:phonering-alo-circle-anim 1.1s infinite ease-in-out!important;
    animation:phonering-alo-circle-anim 1.1s infinite ease-in-out!important
}
.phonering-alo-phone.phonering-alo-static .phonering-alo-ph-circle {
    -webkit-animation:phonering-alo-circle-anim 2.2s infinite ease-in-out!important;
    animation:phonering-alo-circle-anim 2.2s infinite ease-in-out!important
}
.phonering-alo-phone.phonering-alo-hover .phonering-alo-ph-circle,.phonering-alo-phone:hover .phonering-alo-ph-circle {
    border-color:#00aff2;
    opacity:.5
}
.phonering-alo-phone.phonering-alo-green.phonering-alo-hover .phonering-alo-ph-circle,.phonering-alo-phone.phonering-alo-green:hover .phonering-alo-ph-circle {
    border-color:#75eb50;
    opacity:.5
}
.phonering-alo-phone.phonering-alo-green .phonering-alo-ph-circle {
    border-color:#00aff2;
    opacity:.5
}
.phonering-alo-phone.phonering-alo-gray.phonering-alo-hover .phonering-alo-ph-circle,.phonering-alo-phone.phonering-alo-gray:hover .phonering-alo-ph-circle {
    border-color:#ccc;
    opacity:.5
}
.phonering-alo-phone.phonering-alo-gray .phonering-alo-ph-circle {
    border-color:#75eb50;
    opacity:.5
}
.phonering-alo-ph-circle-fill {width:100px;height:100px;top:87px;left:44px;position:absolute;background-color:#000;
 border-radius:100%;border:2px solid transparent;
 -webkit-animation:phonering-alo-circle-fill-anim 2.3s infinite ease-in-out;
 animation:phonering-alo-circle-fill-anim 2.3s infinite ease-in-out;
 transition:all .5s;
 -webkit-transform-origin:50% 50%;
 -ms-transform-origin:50% 50%;
 transform-origin:50% 50%
}
.phonering-alo-phone.phonering-alo-active .phonering-alo-ph-circle-fill {
    -webkit-animation:phonering-alo-circle-fill-anim 1.7s infinite ease-in-out!important;
    animation:phonering-alo-circle-fill-anim 1.7s infinite ease-in-out!important
}
.phonering-alo-phone.phonering-alo-static .phonering-alo-ph-circle-fill {
    -webkit-animation:phonering-alo-circle-fill-anim 2.3s infinite ease-in-out!important;
    animation:phonering-alo-circle-fill-anim 2.3s infinite ease-in-out!important;
    opacity:0!important
}
.phonering-alo-phone.phonering-alo-hover .phonering-alo-ph-circle-fill,.phonering-alo-phone:hover .phonering-alo-ph-circle-fill {
    background-color:rgba(39,45,107,0.5);
    opacity:.75!important
}
.phonering-alo-phone.phonering-alo-green.phonering-alo-hover .phonering-alo-ph-circle-fill,.phonering-alo-phone.phonering-alo-green:hover .phonering-alo-ph-circle-fill {
    background-color:rgba(39,45,107,0.5);
    opacity:.75!important
}
.phonering-alo-phone.phonering-alo-green .phonering-alo-ph-circle-fill {
    background-color:rgba(0,175,242,0.5);
}
.phonering-alo-phone.phonering-alo-gray.phonering-alo-hover .phonering-alo-ph-circle-fill,.phonering-alo-phone.phonering-alo-gray:hover .phonering-alo-ph-circle-fill {
    background-color:rgba(204,204,204,0.5);
    opacity:.75!important
}
.phonering-alo-phone.phonering-alo-gray .phonering-alo-ph-circle-fill {
    background-color:rgba(117,235,80,0.5);
    opacity:.75!important
}
.phonering-alo-ph-img-circle {
    width:48px;
    height:48px;
    top:110px;
    left:70px;
    position:absolute;
    background:rgba(30,30,30,0.1) url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAACXBIWXMAAAsTAAALEwEAmpwYAAABNmlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjarY6xSsNQFEDPi6LiUCsEcXB4kygotupgxqQtRRCs1SHJ1qShSmkSXl7VfoSjWwcXd7/AyVFwUPwC/0Bx6uAQIYODCJ7p3MPlcsGo2HWnYZRhEGvVbjrS9Xw5+8QMUwDQCbPUbrUOAOIkjvjB5ysC4HnTrjsN/sZ8mCoNTIDtbpSFICpA/0KnGsQYMIN+qkHcAaY6addAPAClXu4vQCnI/Q0oKdfzQXwAZs/1fDDmADPIfQUwdXSpAWpJOlJnvVMtq5ZlSbubBJE8HmU6GmRyPw4TlSaqo6MukP8HwGK+2G46cq1qWXvr/DOu58vc3o8QgFh6LFpBOFTn3yqMnd/n4sZ4GQ5vYXpStN0ruNmAheuirVahvAX34y/Axk/96FpPYgAAACBjSFJNAAB6JQAAgIMAAPn/AACA6AAAUggAARVYAAA6lwAAF2/XWh+QAAAB/ElEQVR42uya7W3CMBCG31QM4A1aNggTlG6QbpBMkHYC1AloJ4BOABuEDcgGtBOETnD9c1ERCH/lwxeaV8oPFGP86Hy+DxMREW5Bd7gRjSDSNGn4/RiAOvm8C0ZCRD5PSkQVXSr1nK/xE3mcWimA1ZV3JYBZCIO4giQANoYxMwYS6+xKY4lT5dJPreWZY+uspqSCKPYN27GJVBDXheVSQe494ksiEWTuMXcu1dld9SARxDX1OAJ4lgjy4zDnFsC076A4adEiRwAZg4hOUSpNoCsBPDGM+HqkNGynYBCuILuWj+dgWysGsNe8nwL4GsrW0m2fxZBq9rW0rNcX5MOQ9eZD8JFahcG5g/iKT671alGAYQggpYWvpEPYWrU/HDTOfeRIX0q2SL3QN4tGhZJukVobQyXYWw7WtLDKDIuM+ZSzscyCE9PCy5IttCvnZNaeiGLNHKuz8ZVh/MXTVu/1xQKmIqLEAuJ0fNo3iG5B51oSkeKnsBi/4bG9gYB/lCytU5G9DryFW+3Gm+JLwU7ehbJrwTjq4DJU8bHcVbEV9dXXqqP6uqO5e2/QZRYJpqu2IUAA4B3tXvx8hgKp05QZW6dJqrLTNkB6vrRURLRwPHqtYgkC3cLWQAcDQGGKH13FER/NATzi786+BPDNjm1dMkfjn2pGkBHkf4D8DgBJDuDHx9BN+gAAAABJRU5ErkJggg==) no-repeat center center;
    border-radius:100%;
		background-size: 32px;
    border:2px solid transparent;
    -webkit-animation:phonering-alo-circle-img-anim 1s infinite ease-in-out;
    animation:phonering-alo-circle-img-anim 1s infinite ease-in-out;
    -webkit-transform-origin:50% 50%;
    -ms-transform-origin:50% 50%;
    transform-origin:50% 50%
}

.phonering-alo-phone.phonering-alo-active .phonering-alo-ph-img-circle {
    -webkit-animation:phonering-alo-circle-img-anim 1s infinite ease-in-out!important;
    animation:phonering-alo-circle-img-anim 1s infinite ease-in-out!important
}

.phonering-alo-phone.phonering-alo-static .phonering-alo-ph-img-circle {
    -webkit-animation:phonering-alo-circle-img-anim 0 infinite ease-in-out!important;
    animation:phonering-alo-circle-img-anim 0 infinite ease-in-out!important
}

.phonering-alo-phone.phonering-alo-hover .phonering-alo-ph-img-circle,.phonering-alo-phone:hover .phonering-alo-ph-img-circle {
    background-color:#00aff2;
}

.phonering-alo-phone.phonering-alo-green.phonering-alo-hover .phonering-alo-ph-img-circle,.phonering-alo-phone.phonering-alo-green:hover .phonering-alo-ph-img-circle {
    background-color:#75eb50;
}

.phonering-alo-phone.phonering-alo-green .phonering-alo-ph-img-circle {
    background-color:#00aff2;
}

.phonering-alo-phone.phonering-alo-gray.phonering-alo-hover .phonering-alo-ph-img-circle,.phonering-alo-phone.phonering-alo-gray:hover .phonering-alo-ph-img-circle {
    background-color:#ccc;
}

.phonering-alo-phone.phonering-alo-gray .phonering-alo-ph-img-circle {
    background-color:#75eb50
}

@-webkit-keyframes phonering-alo-circle-anim {
    0% {
        -webkit-transform:rotate(0) scale(.5) skew(1deg);
        -webkit-opacity:.1
    }

    30% {
        -webkit-transform:rotate(0) scale(.7) skew(1deg);
        -webkit-opacity:.5
    }

    100% {
        -webkit-transform:rotate(0) scale(1) skew(1deg);
        -webkit-opacity:.1
    }
}

@-webkit-keyframes phonering-alo-circle-fill-anim {
    0% {
        -webkit-transform:rotate(0) scale(.7) skew(1deg);
        opacity:.2
    }

    50% {
        -webkit-transform:rotate(0) scale(1) skew(1deg);
        opacity:.2
    }

    100% {
        -webkit-transform:rotate(0) scale(.7) skew(1deg);
        opacity:.2
    }
}

@-webkit-keyframes phonering-alo-circle-img-anim {
    0% {
        -webkit-transform:rotate(0) scale(1) skew(1deg)
    }

    10% {
        -webkit-transform:rotate(-25deg) scale(1) skew(1deg)
    }

    20% {
        -webkit-transform:rotate(25deg) scale(1) skew(1deg)
    }

    30% {
        -webkit-transform:rotate(-25deg) scale(1) skew(1deg)
    }

    40% {
        -webkit-transform:rotate(25deg) scale(1) skew(1deg)
    }

    50% {
        -webkit-transform:rotate(0) scale(1) skew(1deg)
    }

    100% {
        -webkit-transform:rotate(0) scale(1) skew(1deg)
    }
}

@-webkit-keyframes phonering-alo-circle-anim {
    0% {
        -webkit-transform:rotate(0) scale(.5) skew(1deg);
                transform:rotate(0) scale(.5) skew(1deg);
        opacity:.1
    }

    30% {
        -webkit-transform:rotate(0) scale(.7) skew(1deg);
                transform:rotate(0) scale(.7) skew(1deg);
        opacity:.5
    }

    100% {
        -webkit-transform:rotate(0) scale(1) skew(1deg);
                transform:rotate(0) scale(1) skew(1deg);
        opacity:.1
    }
}

@keyframes phonering-alo-circle-anim {
    0% {
        -webkit-transform:rotate(0) scale(.5) skew(1deg);
                transform:rotate(0) scale(.5) skew(1deg);
        opacity:.1
    }

    30% {
        -webkit-transform:rotate(0) scale(.7) skew(1deg);
                transform:rotate(0) scale(.7) skew(1deg);
        opacity:.5
    }

    100% {
        -webkit-transform:rotate(0) scale(1) skew(1deg);
                transform:rotate(0) scale(1) skew(1deg);
        opacity:.1
    }
}

@-webkit-keyframes phonering-alo-circle-fill-anim {
    0% {
        -webkit-transform:rotate(0) scale(.7) skew(1deg);
                transform:rotate(0) scale(.7) skew(1deg);
        opacity:.2
    }

    50% {
        -webkit-transform:rotate(0) scale(1) skew(1deg);
                transform:rotate(0) scale(1) skew(1deg);
        opacity:.2
    }

    100% {
        -webkit-transform:rotate(0) scale(.7) skew(1deg);
                transform:rotate(0) scale(.7) skew(1deg);
        opacity:.2
    }
}

@keyframes phonering-alo-circle-fill-anim {
    0% {
        -webkit-transform:rotate(0) scale(.7) skew(1deg);
                transform:rotate(0) scale(.7) skew(1deg);
        opacity:.2
    }

    50% {
        -webkit-transform:rotate(0) scale(1) skew(1deg);
                transform:rotate(0) scale(1) skew(1deg);
        opacity:.2
    }

    100% {
        -webkit-transform:rotate(0) scale(.7) skew(1deg);
                transform:rotate(0) scale(.7) skew(1deg);
        opacity:.2
    }
}

@-webkit-keyframes phonering-alo-circle-img-anim {
    0% {
        -webkit-transform:rotate(0) scale(1) skew(1deg);
                transform:rotate(0) scale(1) skew(1deg)
    }

    10% {
        -webkit-transform:rotate(-25deg) scale(1) skew(1deg);
                transform:rotate(-25deg) scale(1) skew(1deg)
    }

    20% {
        -webkit-transform:rotate(25deg) scale(1) skew(1deg);
                transform:rotate(25deg) scale(1) skew(1deg)
    }

    30% {
        -webkit-transform:rotate(-25deg) scale(1) skew(1deg);
                transform:rotate(-25deg) scale(1) skew(1deg)
    }

    40% {
        -webkit-transform:rotate(25deg) scale(1) skew(1deg);
                transform:rotate(25deg) scale(1) skew(1deg)
    }

    50% {
        -webkit-transform:rotate(0) scale(1) skew(1deg);
                transform:rotate(0) scale(1) skew(1deg)
    }

    100% {
        -webkit-transform:rotate(0) scale(1) skew(1deg);
                transform:rotate(0) scale(1) skew(1deg)
    }
}

@keyframes phonering-alo-circle-img-anim {
    0% {
        -webkit-transform:rotate(0) scale(1) skew(1deg);
                transform:rotate(0) scale(1) skew(1deg)
    }

    10% {
        -webkit-transform:rotate(-25deg) scale(1) skew(1deg);
                transform:rotate(-25deg) scale(1) skew(1deg)
    }

    20% {
        -webkit-transform:rotate(25deg) scale(1) skew(1deg);
                transform:rotate(25deg) scale(1) skew(1deg)
    }

    30% {
        -webkit-transform:rotate(-25deg) scale(1) skew(1deg);
                transform:rotate(-25deg) scale(1) skew(1deg)
    }

    40% {
        -webkit-transform:rotate(25deg) scale(1) skew(1deg);
                transform:rotate(25deg) scale(1) skew(1deg)
    }

    50% {
        -webkit-transform:rotate(0) scale(1) skew(1deg);
                transform:rotate(0) scale(1) skew(1deg)
    }

    100% {
        -webkit-transform:rotate(0) scale(1) skew(1deg);
                transform:rotate(0) scale(1) skew(1deg)
    }
}
</style>
<link rel="stylesheet" type="text/css" href="https://pinceladasdaweb.github.io/mediabox/dist/mediabox.min.css" media="screen" />
<script type="text/javascript" src="https://pinceladasdaweb.github.io/mediabox/dist/mediabox.min.js"></script>
<script type="text/javascript">
		MediaBox('.mediabox');
</script>
<script type="text/javascript">
        /**
         * Script execution order manager
         */

        window.queue = {
            // managing scripts that executes after libs JS loaded
            isLibsLoaded: false,
            afterLibsList: [],
            afterLibsLoaded: function(fn) {
                if (this.isLibsLoaded) {
                    fn()
                } else {
                    this.afterLibsList.push(fn)
                }
            },
            libsLoaded: function() {
                this.isLibsLoaded = true
                this.afterLibsList.forEach(function(fn) {
                    fn()
                })
            },

            // managing scripts that executes after main app JS loaded
            isAppLoaded: false,
            afterAppList: [],
            afterAppLoaded: function(fn) {
                if (this.isAppLoaded) {
                    fn()
                } else {
                    this.afterAppList.push(fn)
                }
            },
            appLoaded: function() {
                this.isAppLoaded = true
                this.afterAppList.forEach(function(fn) {
                    fn()
                })
            }
        }
    </script>

    <script async type="text/javascript" src="static/js/app/pageLoadReporter.js"></script>
    <script defer type="text/javascript" src="static/js/libs/bundle.libs.main.js"></script>
    <script type="text/javascript">
        window.queue.afterLibsLoaded(function() {

            Raven.config('https://a94fab78a5ca46de828224eb6f191f4b@sentry.key-g.com/3', {
                environment: 'production',
                release: '87cf119afc96c6f81e0eb5c5e4ae5ca9bd847d25'
            }).install();

        })
    </script>

    <script defer type="text/javascript" src="static/js/app/bundle.main.js"></script>

    <!-- <script defer type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC-SuFwGnt_OB8ZkNY1q2dRoOonWTDHdbQ&libraries=drawing%2Cplaces%2Cgeometry&callback=google_init&language=en"></script> -->
		<?php /*
    <script type="text/javascript">
        (window.Image ? (new Image()) : document.createElement('img')).src = 'https://vk.com/rtrg?p=VK-RTRG-211706-dYb4k';
    </script>

    <script type='application/ld+json'>{"@context":"https://www.schema.org","@type":"Organization","name":"GetTransfer","url":"/en","logo":"https://vipsedan.vn/media/data/dm/img/logo1.png","image":"https://vipsedan.vn/media/data/dm/img/logo1.png","email":"info@gettransfer.com","description":"International transfer booking service around the world","address":[{"@type":"PostalAddress","streetAddress":"15/F., BOC GROUP LIFE ASSURANCE TOWER, 136 DES VOEUX ROAD CENTRAL","addressLocality":"CENTRAL, HONG KONG","addressRegion":"HONG KONG","postalCode":null,"addressCountry":"HK"},{"@type":"PostalAddress","streetAddress":"PATRON 10","addressLocality":"LARNACA","addressRegion":"LARNACA","postalCode":"6051","addressCountry":"CY"}],"contactPoint":[{"@type":"ContactPoint","telephone":"+1 415 890 08 45","contactOption":null,"areaServed":"US","availableLanguage":"en","contactType":"customer support"},{"@type":"ContactPoint","telephone":"+1 305 709 05 32","contactOption":null,"areaServed":"US","availableLanguage":"en","contactType":"customer support"},{"@type":"ContactPoint","telephone":"+41 22 518 31 38","contactOption":null,"areaServed":"EU","availableLanguage":"en","contactType":"customer support"},{"@type":"ContactPoint","telephone":"+7 499 404 05 05","contactOption":null,"areaServed":"RU","availableLanguage":"ru","contactType":"customer support"},{"@type":"ContactPoint","telephone":"+852 800 90 66 77","contactOption":null,"areaServed":"CN","availableLanguage":"en","contactType":"customer support"},{"@type":"ContactPoint","telephone":"+357 24 202511","contactOption":null,"areaServed":"CY","availableLanguage":"en","contactType":"customer support"}]}</script>
		*/ ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>"></script>
<?php } ?>
<?php foreach ($exscripts as $exscript) { ?>
<script>
<?php echo $exscript; ?>
</script>
<?php } ?>
<?php if (isset($script_footer)) { ?>
	<?php echo $script_footer; ?>
<?php } ?>
<?php /*?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-124193089-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-124193089-1');
</script>
<?php */?>
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      xfbml            : true,
      version          : 'v3.3'
    });
  };

  (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Your customer chat code -->
<div class="fb-customerchat"
  attribution=setup_tool
  page_id="1118881244973338"
  theme_color="#ea5c76"
  logged_in_greeting="Chúng tôi có thể giúp gì cho bạn?"
  logged_out_greeting="Chúng tôi có thể giúp gì cho bạn?">
</div>
</body>
</html>
