<?php echo $header; ?>
<h1 class="hide"><?php echo $heading_title; ?></h1>

<?php echo $slideside; ?>

	<div class="gray_bg">
		<div class="container">
			<div class="row" style="padding-bottom:0; background: white">
				<div class="priceList col-md-7">
          
        <?php echo $adv_top; ?>


				</div>
				<div class=" col-md-5">
          <div class="row">
            <div class="col-md-1">
            </div>
            <div style="padding-left: 0; padding-top: 70px; display: none" class="col-md-5">
              <b style="color: #08a8f3">Khuyến mại:</b>
              <i style="padding-top: 5px; display: inline-block; font-size: 12px">
      				<p>- <u>Giá chỉ từ <b class="highlight" style="font-size: 16px">130.000đ</b> trong khung giờ <b class="highlight" style="font-size: 16px">20h-00h</b> (Hà nội - Nội Bài)</u></p>
      				<p>- <u>Giá chỉ từ <b class="highlight" style="font-size: 16px">230.000đ</b> trong khung giờ <b class="highlight" style="font-size: 16px">6h-11h</b> (Nội Bài - Hà Nội)</u></p>
      				<p>Bảng giá xe sân bay có thể thay đổi tùy thuộc vào vị trí đưa đón ở Hà Nội</p>
      				<p>Bảng giá chưa bao gồm phí cầu đường, vé ra vào sân bay <span class="highlight">15.000 đ</span></p>
      				</i>
            </div>
            <div class="col-md-11" style="height: 312px; padding-left: 0;">
              <div class="booking">
  		          <?php echo $content_top; ?>
              </div>
            </div>
          </div>
				</div>
			</div>
		</div>
	</div>
  <div class="introduce">
    <style>
      .introduce img{
        max-height: 336px;
      }
      .introduce{
        background: url('./static/img/vipsedan-home-bg.jpg');
        padding-top: 30px;
        padding-bottom: 30px;
      }
      .item-intro p{
        font-size: 14px;
      }
      .item-intro h4{
        font-size: 20px;
        text-transform: uppercase;
        margin-bottom: 5px;
        font-weight: 400;
      }
      .item-intro {
        color: white;
        padding-left: 85px;
        margin: 50px 0;
      }
      .b-price{
        background: url('static/img/price-icon.png') no-repeat left center;
        margin-top: 15px;
      }
      .b-service{
        background: url('static/img/service-icon.png') no-repeat left center;
      }
      .b-safe{
        background: url('static/img/safe-icon.png') no-repeat left center;
        margin-bottom: 15px;
      }
    </style>
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-md-7">
        <div class="col-xs-12">
          <img style="width: 100%" alt="Ra mắt vipsedan group" src="/static/img/rm.jpg">
          <a class="mediabox" href="https://www.youtube.com/watch?v=9-n4xxqMs18"> <span class="play-i absolute"></span> </a>
        </div>
        </div>
        <div class="col-xs-12 col-md-5">
        <div class="col-xs-12">
          <div class="item-intro b-price">
            <h4><?php echo $this->language->get('text_bestprice'); ?></h4>
            <p><?php echo $this->language->get('text_bestprice_content'); ?></p>
          </div>
          <div class="item-intro b-service">
            <h4><?php echo $this->language->get('text_bestservice'); ?></h4>
            <p><?php echo $this->language->get('text_bestservice_content'); ?></p>
          </div>
          <div class="item-intro b-safe">
            <h4><?php echo $this->language->get('text_bestsuport'); ?></h4>
            <p><?php echo $this->language->get('text_bestsuport_content'); ?></p>
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>
  <div class="news-features rowgutter">
    <style>
      .news-features{
        background: white;
      }
      .news-features h3{
        margin-top: 10px;
      }
      .news-features h3 span b{
        color: #e75d77;
      }
      .news-features h3 a,
      .news-features h3 span
      {
        color: #090909;
        text-transform: uppercase;
        font-size: 16px;
      }
      .heading_title:after{
        position: absolute;
        content: "";
        display: inline-block;
        width: 30px;
        height: 3px;
        background: #e75d77;
        left: 0;
        bottom: -7px;
      }
      .heading_title{
        font-size: 16px;
        text-transform: uppercase;
        margin-top: 0;
        margin-bottom: 20px;
        position: relative;
      }
      .news-features ul li a{
        color: #546c7c;
      }
      .news-features ul li{
        margin-bottom: 5px;
      }
      .news-features ul{
        list-style: none;
        padding-left: 0;
      }
      .news-features ul li:first-child a:first-child{
        float: left;
        margin-right: 10px;
      }
    </style>
    <div class="container">
      <div class="row">
        <?php echo $column_left; ?>
        <?php echo $column_right; ?>
      </div>
    </div>
  </div>

   	<?php //echo $content_top; ?>

    <?php /* ?>
   	<div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h3>Chúng tôi phục vụ<i class="line-y"></i></h3>

                <div id="transport_type_slider">

                    <!-- Add Pagination -->
                    <div class="slick-dots-wrapper"></div>
                    <?php foreach($listCars as $car){ ?>
                    <div class="slick-slide-wrapper">
                        <a href="<?php echo $car['link']; ?>" onclick="try { gtag('event', 'calculate1', { 'event_action': 'calculate' }); } catch(e) {}">
                            <figure><img src="media/<?php echo $car['image']; ?>" alt="<?php echo $car['name']; ?>" /></figure>
                            <p data-name="<?php echo $car['name']; ?>">
                                <span class="icon-male"></span>×<?php echo $car['seat']; ?>
                                <span class="icon-suitcase"></span>×<?php echo $car['luggage']; ?>

                            </p>
                        </a>
                    </div>
                    <?php } ?>
                </div>

            </div>
        </div>
    </div>
    <?php */ ?>
<?php echo $footer; ?>
