<?php echo $header; ?>
<div itemscope itemtype="http://schema.org/Product">
<div class="container">
<div class="row clear">
	<form action="common/cart" id="form_contact" method="post">
	<div class="col-xs-12 col-sm-8 zonegutter">
		<h1 class="blue h3 n-mt title_module_heading n-mt"><?php echo $heading_title; ?></h1>
		<span>Thêm sản phẩm khác: </span>&nbsp;
		<select name="other_product" onchange="addOther(this)">
			<option>---Chọn sản phẩm---</option>
			<?php foreach($products as $product){ ?>
			<option value="<?php echo $product['product_id'];?>"><?php echo $product['name'];?></option>
			<?php } ?>
		</select>
		<br />
		<br />
		<?php if($checkcart){ ?>
		<table class="n--m full-width cart">
			<thead class="bg-silver strong">
			<tr>
				<th>Sản phẩm</th>
				<th class="text-center">Số lượng</th>
				<th class="text-right">Giá tiền</th>
				<th class="text-right">Thành tiền</th>
				<th></th>
			</tr>
			</thead>
			<tbody>
			<?php if($hosting){ ?>
			<?php
			$price = 0;
			$k=0;foreach($hosting as $ht){
				// $price += $ht['price'];
				?>
			<tr>
				<?php
				$quantity = isset($detail[$ht['product_id']]['quantity']) ? $detail[$ht['product_id']]['quantity'] : 1;
				$price_item = $quantity*$ht['price'];
				$price += $price_item;
				?>
				<td>
					<?php echo $ht['name']; ?><br />
					<input type="hidden" name="product[<?php echo $ht['product_id'];?>]" value="<?php echo $ht['name']; ?>"/>
					<input type="hidden" name="detail[<?php echo $ht['product_id'];?>][name]" value="<?php echo $ht['name']; ?>"/>
					<input type="hidden" name="detail[<?php echo $ht['product_id'];?>][price]" value="<?php echo $ht['price']; ?>"/>
				</td>
				<td class="text-center">
					<a onclick="decrease('<?php echo $ht['product_id']; ?>', <?php echo $ht['price']; ?>)">-</a> &nbsp;&nbsp;
					<input id="detail<?php echo $ht['product_id']; ?>" onkeyup="change(this, <?php echo $ht['price']; ?>, '<?php echo $ht['product_id']; ?>')" type="text" name="detail[<?php echo $ht['product_id'];?>][quantity]" value="<?php echo $quantity ;?>" size="2"/>
					&nbsp;&nbsp;<a onclick="increase('<?php echo $ht['product_id']; ?>', <?php echo $ht['price']; ?>)">+</a>
				</td>
				<td class="text-right">
					<?php echo number_format($ht['price'], 0 ,',','.').' vnđ'; ?>
				</td>
				<td class="text-right">
					<span id="price<?php echo $ht['product_id']; ?>"><?php echo number_format($price_item, 0 ,',','.').' vnđ'; ?></span>
				</td>
				<td class="text-center">
					<a onclick="remove('<?php echo $ht['product_id']; ?>')" class="remove">x</a>
				</td>
			</tr>
			<?php $k++;} ?>
			<?php } ?>
			<tr>
				<td colspan="3" class="text-right strong">Tổng tiền:</td>
				<td class="text-right strong red" id="price"><?php echo number_format($price, 0, ',','.');?> vnđ</td>
				<td></td>
			</tr>
			</tbody>
		</table>
		<script>
			function calculate(price, register, duytri, id, type, e){
				var pr=0;
				if(type=='domain'){
					document.getElementById(id).value = parseInt(price) + parseInt(register) + parseInt(e.value * duytri);
				}else{
					document.getElementById(id).value = parseInt(price * e.value);
					//alert(e.value);
				}
				for(var k=0;k < <?php echo count($domains); ?>; k++){
					pr += parseInt(document.getElementById('domain'+k).value);
				}
				for(var a=0;a < <?php echo count($hosting); ?>; a++){
					pr += parseInt(document.getElementById('hosting'+a).value);
				}
				//document.getElementById('price').value = pr;
				pr = String(pr).replace(/(.)(?=(\d{3})+$)/g,'$1.');
				document.getElementById('price').innerHTML = pr + ' vnđ';

			}
		</script>
	</div>
	<div class="col-xs-12 col-sm-4 zonegutter contactpage">
		<h3 class="n--m blue heading rowgutter">Thông tin của bạn</h3>
		<input type="text" placeholder="Tên của bạn" name="name" value="<?php echo $name; ?>"/>
		<?php echo $error_name ? '<span class="error">'.$error_name.'</span>' : ''; ?>
		<input type="text" placeholder="Số điện thoại" name="telephone" value="<?php echo $telephone; ?>"/>
		<?php echo $error_telephone ? '<span class="error">'.$error_telephone.'</span>' : ''; ?>
		<input type="text" placeholder="Email" name="email" value="<?php echo $email; ?>"/>
		<?php echo $error_email ? '<span class="error">'.$error_email.'</span>' : ''; ?>
		<textarea name="note" placeholder="Ghi chú" rows="5"></textarea>
		<div class="rowgutter"><a onclick="javascript: document.getElementById('form_contact').submit();" class="round button white bg-blue uppercase right">Đặt hàng</a></div>
	</div>
	</form>
	<?php }else{ ?>
		Giỏ hàng trống
	<?php } ?>
	<?php echo $content_bottom; ?>
</div>
</div>
</div>
<?php echo $footer; ?>
