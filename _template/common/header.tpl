<!DOCTYPE html>
<html lang="vi" data-distance-unit="km" data-currency="VND" data-country="VN" data-google-proxy-url="/api">

<head>
    <title><?php echo $title; ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <base href="https://vipsedan.vn/" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#fedb06">
    <?php if ($icon) { ?>
		<link href="<?php echo $icon; ?>" rel="icon" />
	<?php } ?>
	<?php if (isset($google_verify)) { ?>
		<?php echo $google_verify; ?>
	<?php } ?>
	<?php if (isset($script_header)) { ?>
		<?php echo $script_header; ?>
	<?php } ?>
	<?php foreach ($metanames as $metaname) { ?>
		<meta name="<?php echo $metaname['name']; ?>" content="<?php echo $metaname['content']; ?>" />
	<?php } ?>
	<?php foreach ($metapros as $metapro) { ?>
		<meta property="<?php echo $metapro['property']; ?>" content="<?php echo $metapro['content']; ?>" />
	<?php } ?>
	<?php foreach ($links as $link) { ?>
		<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
	<?php } ?>
	<link href="<?php echo $opensearch; ?>" rel="search" type="application/opensearchdescription+xml" title="<?php echo $name; ?>" />
	<?php foreach ($styles as $style) { ?>
		<link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
	<?php } ?>
	<style type="text/css">
  .col-left .tns-outer {clear:both;padding-top:20px}
  <?php echo $custom_css; ?></style>

  <link rel="alternate" hreflang="x-default" href="/">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&subset=latin,cyrillic-ext" rel="stylesheet">
    <link rel="stylesheet" href="static/fonts/fontello/css/fontello.css" />
    <link rel="stylesheet" href="static/ex_style.css?v=11" />
    <link rel="stylesheet" href="static/mmenu-light.css?v=9" />

</head>
<body class="is-page-loading show-cookie-notice" itemscope="itemscope" itemtype="http://schema.org/WebPage" class="<?php echo $class; ?>" >
    <div class="page-preloader"></div>

    <!-- Навигация -->
    <nav class="navbar navbar-default" id="menu_home_page">

        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <nav class="top-nav noselect">
												<div class="header hidden-desktop">
													<a href="#menu"><span></span></a>
													<nav id="menu">
														<?php echo $top_menu; ?>
													</nav>
												</div>
												
												
                        <figure class="top-nav__logo">
                            <a href="."><img class="logo-desc" src="<?php echo $logo; ?>" alt="<?php echo $name; ?>" /></a>
                            <a href="."><img class="logo-mobi" src="<?php echo $logo; ?>" alt="<?php echo $name; ?>" /></a>
                        </figure>
                        <div id="menu" class="top-nav__nav nowrap">
                          <ul class="top-nav__second-list list-unstyled">
                              <li class="hamburger-menu drop-menu">
                              	<?php echo $top_menu; ?>
                              </li>
                          </ul>
                        </div>
												
												<?php echo $language; ?>
												
												<a href="tel:<?php echo $hotline; ?>" class="text-right hotline hide-mobile">
													<?php echo $hotline; ?>
												</a>
                    </nav>

                </div>
            </div>
        </div>
    </nav>
		<script src="static/mmenu-light.js"></script>
		<script>
			var menu = new MmenuLight(
				document.querySelector( '#menu' ),
				'all'
			);

			var navigator = menu.navigation({
				// selectedClass: 'Selected',
				// slidingSubmenus: true,
				// theme: 'dark',
				// title: 'Menu'
			});

			var drawer = menu.offcanvas({
				// position: 'left'
			});

			//	Open the menu.
			document.querySelector( 'a[href="#menu"]' )
				.addEventListener( 'click', evnt => {
					evnt.preventDefault();
					drawer.open();
				});

		</script>