<?php echo $header; ?>
<div class="container m-container">
	<div class="small breadcrumbs">
		<i class="icon-icons-47"></i> <a href="."><?php echo $text_home?></a> » <a href="common/showroom"><?php echo $heading_title?></a> » <a href="<?php echo $province_url; ?>"><?php echo $province_name; ?></a> » <a href="<?php echo $district_url; ?>"><?php echo $district_name; ?></a>
	</div>
	<div class="row clear">
	<div class="col-xs-12 col-sm-8 left-col rowgutter">
		<div class="row clear">
			<div class="col-xs-12 col-desktop-12 col-sm-12">
				<div class="heading1 uppercase strong"><span class="left block greenbg h3 strong">1</span><?php echo $text_intro?></div>  
			</div>
		</div>
		<div class="row clear">
			<div class="col-6">
					<h1 class="uppercase h2 blue"><?php echo $district_name; ?></h1>		
			</div>
			<div class="col-6">
				<div id="showroomdrop">
				<select id="showroom" onchange="optionCheck()">
					<?php foreach($provincies as $province){ ?>
					<?php if($province['province_id']==$province_id){ ?>
					<option value="<?php echo $province['province_id']?>" selected><?php echo $province['name']?></option>
					<?php }else{ ?>
					<option value="<?php echo $province['province_id']?>"><?php echo $province['name']?></option>
						<?php } ?>
					<?php } ?>
				</select>
				</div>
				<script>
				function optionCheck(){
					var option = document.getElementById("showroom").value;
					<?php foreach($provincies as $province){ ?>
					if(option == "<?php echo $province['province_id'];?>"){
						window.location = "<?php echo $province['name_url']."&province_id=".$province['province_id'];?>";
					}
					<?php } ?>
				}
				</script>
			</div>
		</div>

		<?php if(isset($dealers)){ ?>
		<div class="table">
			<div class="no-margin thead row clear show-desktop hide-tablet hide-mobile">
				<div class="col-xs-12 col-desktop-3 col-sm-4"><?php echo $text_name; ?></div>
				<div class="col-xs-12 col-desktop-4 col-sm-4"><?php echo $text_address; ?></div>
				<div class="col-xs-12 col-desktop-2 col-sm-2"><?php echo $text_phone; ?></div>
				<div class="col-xs-12 col-desktop-3 col-sm-2"><?php echo $text_email; ?></div>
			</div>
			<?php foreach($dealers as $dealer){ ?>
			<div class="no-margin innerd row clear">
				<div class="col-xs-12 no-padding col-desktop-3 col-sm-6"><span class="uppercase strong"><?php echo $dealer['name']; ?></span></div>
				<div class="col-xs-12 col-desktop-4 col-sm-6"><?php echo $dealer['address']; ?></div>
				<div class="col-xs-12 no-padding col-desktop-2 col-sm-6"><?php echo $dealer['telephone']; ?></div>
				<div class="col-xs-12 col-desktop-3 col-sm-6"><?php echo $dealer['email']; ?></div>
			</div>
			<?php } ?>
		</div>
		<?php }else{ ?>
			<?php echo $no_result; ?>
		<?php } ?>
		</div>
		<div class="col-xs-12 col-sm-4 rowgutter">
	  		<?php echo $column_left; ?> 
		</div>
		</div>
			<?php echo $content_bottom; ?>
		</div>
<?php echo $footer; ?>