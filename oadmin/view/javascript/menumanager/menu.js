jQuery(function($) {

	/* highlight current menu group
	------------------------------------------------------------------------- */
	$('#menu-group li[id="group-' + current_group_id + '"]').addClass('current');
// alert(current_group_id);
	/* global ajax setup
	------------------------------------------------------------------------- */
	$.ajaxSetup({
		type: 'GET',
		datatype: 'json',
		timeout: 20000
	});
	$('#loading').ajaxStart(function() {
		$(this).show();
	});
	$('#loading').ajaxStop(function() {
		$(this).hide();
	});

	/* modal box
	------------------------------------------------------------------------- */
	gbox = {
		defaults: {
			autohide: false,
			buttons: {
				'Close': function() {
					gbox.hide();
				}
			}
		},
		init: function() {
			var winHeight = $(window).height();
			var winWidth = $(window).width();
			var box =
				'<div id="gbox">' +
					'<div id="gbox_content"></div>' +
				'</div>' +
				'<div id="gbox_bg"></div>';

			$('body').append(box);

			$('#gbox').css({
				top: '15%',
				left: winWidth / 2 - $('#gbox').width() / 2
			});

			$('#gbox_close, #gbox_bg').click(gbox.hide);
		},
		show: function(options) {
			var options = $.extend({}, this.defaults, options);
			switch (options.type) {
				case 'ajax':
					$.ajax({
						type: 'GET',
						datatype: 'html',
						url: options.url,
						success: function(data) {
							options.content = data;
							gbox._show(options);
						}
					});
					break;
				default:
					this._show(options);
					break;
			}
		},
		_show: function(options) {
			$('#gbox_footer').remove();
			if (options.buttons) {
				$('#gbox').append('<div id="gbox_footer"></div>');
				$.each(options.buttons, function(k, v) {
					buttonclass = '';
					if (k == 'Save' || k == 'Yes' || k == 'OK') {
						buttonclass = 'primary';
					}
					$('<button></button>').addClass(buttonclass).text(k).click(v).appendTo('#gbox_footer');
				});
			}

			$('#gbox, #gbox_bg').fadeIn();
			$('#gbox_content').html(options.content);
			$('#gbox_content input:first').focus();
			if (options.autohide) {
				setTimeout(function() {
					gbox.hide();
				}, options.autohide);
			}
		},
		hide: function() {
			$('#gbox').fadeOut(function() {
				$('#gbox_content').html('');
				$('#gbox_footer').remove();
			});
			$('#gbox_bg').fadeOut();
		}
	};
	gbox.init();

	/* same as site_url() in php
	------------------------------------------------------------------------- */
	function site_url(url) {
		return _BASE_URL;
	}

	/* nested sortables
	------------------------------------------------------------------------- */
	var menu_serialized;
	var fixSortable = function() {
		if (!$.browser.msie) return;
		//this is fix for ie
		$('#easymm').NestedSortableDestroy();
		$('#easymm').NestedSortable({
			accept: 'sortable',
			helperclass: 'ns-helper',
			opacity: .8,
			handle: '.ns-row',
			onStop: function() {
				fixSortable();
			},
			onChange: function(serialized) {
				menu_serialized = serialized[0].hash;
				$('#btn-save-menu').attr('disabled', false);
			}
		});
	};
	$('#easymm').NestedSortable({
		accept: 'sortable',
		helperclass: 'ns-helper',
		opacity: .8,
		handle: '.ns-row',
		onStop: function() {
			fixSortable();
		},
		onChange: function(serialized) {
			menu_serialized = serialized[0].hash;
			$('#btn-save-menu').attr('disabled', false);
		}
	});

	/* edit menu item
	------------------------------------------------------------------------- */
	$('.edit-menu').live('click', function() {
	//$(document).on('click', '.ns-blank input[type="checkbox"]', function() {
		$('#btn-save-menu').attr('disabled', false);
	});
	/*
	$(document).on('click', '.ns-class input[type="text"]', function() {
		$('#btn-save-menu').attr('disabled', false);
	});
	
	$(document).on('click', '.ns-nofollow input[type="checkbox"]', function() {
		$('#btn-save-menu').attr('disabled', false);
	});
	*/
	//$(document).on('click', '.edit-menu', function() {
	$('.edit-menu').live('click', function() {	
		var menu_id = $(this).next().next().val();
		var menu_div = $(this).parent().parent();
		var li = $(this).closest('li');
		gbox.show({
			type: 'ajax',
			url: $(this).attr('href'),
			buttons: {
				'Save': function() {
					$.ajax({
						type: 'POST',
						url: $('#gbox form').attr('action'),
						data: $('#gbox form').serialize(),
						success: function(data) {
							switch (data.status) {
								case 1:
									gbox.hide();
									menu_div.find('.ns-title').html(data.menu.title);
									menu_div.find('.ns-url').html(data.menu.url);
									menu_div.find('.ns-class').html(data.menu.klass);
									break;
								case 2:
									gbox.hide();
									break;
								case 4:
									gbox.hide();
									li.remove();
									break;
							}
						}
					});
				},
				'Cancel': gbox.hide
			}
		});
		return false;
	});

	/* delete menu item
	------------------------------------------------------------------------- */
	 $('.delete-menu').live('click', function() {
	//$(document).on('click', '.delete-menu', function() {
		var li = $(this).closest('li');
		var param = { action:'delete_menu_item', id : $(this).next().val() };
		var menu_title = $(this).parent().parent().children('.ns-title').text();
		gbox.show({
			content: '<h2>Delete Menu Item</h2>Are you sure you want to delete this menu item?<br><b>'
				+ menu_title +
				'</b><br><br>This will also delete all sub items.',
			buttons: {
				'Yes': function() {
					$.post(site_url(), param, function(data) {
						if (data.success) {
							gbox.hide();
							li.remove();
						} else {
							gbox.show({
								content: 'Failed to delete this menu item.'
							});
						}
					});
				},
				'No': gbox.hide
			}
		});
		return false;
	});

	/* add menu item
	------------------------------------------------------------------------- */
	$('#form-add-menu').submit(function() {
		if ($('input[name="name[2]"]').val() == '') {
			$('input[name="name[2]"]').focus();
		} else {
			$.ajax({
				type: 'POST',
				url: $(this).attr('action'),
				data: $(this).serialize(),
				error: function() {
					gbox.show({
						content: 'Add menu item error. Please try again.',
						autohide: 1000
					});
				},
				success: function(data) {
					switch (data.status) {
						case 1:
							$('#form-add-menu')[0].reset();
							$('#easymm')
								.append(data.li)
								.SortableAddItem($('#'+data.li_id)[0]);
							break;
						case 2:
							gbox.show({
								content: data.msg,
								autohide: 1000
							});
							break;
						case 3:
							$('input[name="name[2]"]').val('').focus();
							break;
					}
				}
			});
		}
		return false;
	});

	 $('#gbox form').live('submit', function() {
	//$(document).on('click', '#gbox form', function() {
		return false;
	});

	/* add group
	------------------------------------------------------------------------- */
	$('#add-group a').click(function() {
		gbox.show({
			type: 'ajax',
			url: $(this).attr('href'),
			buttons: {
				'Save': function() {
					var languages = $('#add-group a').attr('data-language_id').split('_');
					var _url = '';
					var k = 0;
					for (i = 0; i < languages.length; i++) { 
						if($('#menu-group-title_'+ languages[i]).val() != ''){
							k = languages[i];
						}
						_url += '&title_group_' + languages[i] + '=' + $('#menu-group-title_'+ languages[i]).val();
					}
					if (k == 0) {
						$('#menu-group-title_'+ k).focus();
					} else {
						//$('#gbox_ok').attr('disabled', true);
						$.ajax({
							type: 'POST',
							url: site_url(),
							data: 'action=save_group'+ _url,
							error: function() {
								//$('#gbox_ok').attr('disabled', false);
							},
							success: function(data) {
								//$('#gbox_ok').attr('disabled', false);
								switch (data.status) {
									case 1:
										gbox.hide();
										$('#menu-group').append('<li><a href="' + site_url('menu&group_id=' + data.id) + '">' + data.name + '</a></li>');
										break;
									case 2:
										$('<span class="error"></span>')
											.text(data.msg)
											.prependTo('#gbox_footer')
											.delay(1000)
											.fadeOut(500, function() {
												$(this).remove();
											});
										break;
									case 3:
										$('#menu-group-title').val('').focus();
										break;
								}
							}
						});
					}
				},
				'Cancel': gbox.hide
			}
		});
		return false;
	});

	/* update menu / save order
	------------------------------------------------------------------------- */
	$('#btn-save-menu').attr('disabled', true);
	$('#form-menu').submit(function() {
		$('#btn-save-menu').attr('disabled', true);
		$.ajax({
			type: 'POST',
			url: $(this).attr('action'),
			data: 'action=update_menu_item&'+menu_serialized,
			error: function() {
				$('#btn-save-menu').attr('disabled', false);
				gbox.show({
					content: '<h2>Error</h2>Save menu error. Please try again.',
					autohide: 1000
				});
			},
			success: function(data) {
				gbox.show({
					content: '<h2>Thành công</h2>Menu đã được lưu',
					autohide: 1000
				});
			}
		});
		return false;
	});

	/* edit group
	------------------------------------------------------------------------- */
	$('#edit-group a#change-group').click(function() {
		var languages = $('#edit-group a#change-group').attr('data-language_id').split('_');
		for (i = 0; i < languages.length; i++) { 
			$('#menu-group-title_edit_'+ languages[i]).prop("readonly",false);
		}
		$('#edit-group a.#change-group').hide();
		$('#edit-group a.#save-group').show();
	});
	$('#edit-group a#save-group').click(function() {
		// var sgroup = $('#edit-group-input');
		// var group_title = sgroup.text();
		// var group_title_en = $('input[name="title_en"]').val();
		// sgroup.html('<input name="group_title" value="' + group_title + '"><input name="group_title_en" value="' + group_title_en + '">');
		// var inputgroup = sgroup.find('input[name="group_title"]');
		// inputgroup.focus().select().keydown(function(e) {
		// 	if (e.which == 13) {
				// var title_en = $('input[name="group_title_en"]').val();
				// var title = $('input[name="group_title"]').val();
				// if (title == '') {
				// 	return false;
				// }

				var current_group_id = $('#edit-group a.#save-group').attr('data-group_id');
				var languages = $('#edit-group a#save-group').attr('data-language_id').split('_');
				var _url = '';
				var k = 0;
				for (i = 0; i < languages.length; i++) { 
					if($('#menu-group-title_edit_'+ languages[i]).val() == ''){
						k = languages[i];
					}
					_url += '&title_group_' + languages[i] + '=' + $('#menu-group-title_edit_'+ languages[i]).val();
				}
				// alert(k);
				if (k != 0) {
					alert('Vui lòng nhập đủ tên nhóm menu!');
					$('#menu-group-title_edit_'+ k).focus();
				} else {
					$.ajax({
						type: 'POST',
						url: site_url(),
						data: 'action=update_menu_group&id=' + current_group_id + _url,
						success: function(data) {
							console.log(data);
							if (data.success) {
								// sgroup.html(title);
								// $('input[name="title_en"]').val(title_en);
								// $('#group-' + current_group_id + ' a').text(title);
								for (i = 0; i < languages.length; i++) { 
									$('#menu-group-title_edit_'+ languages[i]).prop("readonly",true);
								}
								$('#edit-group a.#change-group').show();
								$('#edit-group a.#save-group').hide();
								alert('Thành công');
							}
						}
					});
				}
			// }
		// 	if (e.which == 27) {
		// 		sgroup.html(group_title);
		// 	}
		// });
		// return false;
	});

	/* delete group
	------------------------------------------------------------------------- */
	$('#delete-group').click(function() {
		var group_title = $('#menu-group li.current a').text();
		var param = { action:'delete_menu_group', id : current_group_id };
		gbox.show({
			content: '<h2>Delete Group</h2>Are you sure you want to delete this group?<br><b>'
				+ group_title +
				'</b><br><br>This will also delete all items under this group.',
			buttons: {
				'Yes': function() {
					$.post(site_url('menu_group.delete'), param, function(data) {
						if (data.success) {
							window.location = site_url('menu');
						} else {
							gbox.show({
								content: 'Failed to delete this group.'
							});
						}
					});
				},
				'No': gbox.hide
			}
		});
		return false;
	});

});