<?php echo $header; ?>
<script src="//cdn.jsdelivr.net/gh/osdvn/mkmate/ckeditor-full.js"></script>
<link rel="stylesheet" href="//cdn.jsdelivr.net/gh/osdvn/mkmate/finder/css/elfinder.min.css" />
<script src="//cdn.jsdelivr.net/gh/osdvn/mkmate/finder/js/elfinder.min.js"></script>
<script src="asset/initck5.js"></script>
<style type="text/css">
  #intro_module .row{padding:10px 0;}
  #intro_module .row:hover{background:#eee}
</style>
<div class="content-wrapper" id="content">
  <section class="content-header">
    <h1><?php echo $heading_title; ?><small>(<?php echo $text_configuration; ?>)</small></h1>
    <ol class="breadcrumb">
      <?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
        <li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
      <?php $k++; } ?>
    </ol>
  </section>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="content">
    <div class="box box-default">
    <div class="box-header">
         <div class="pull-right">
             <a onclick="location = '<?php echo $cancel; ?>';" class="btn btn-default"><i class="fa fa-reply"></i> <?php echo $button_cancel; ?></a>
               <a onclick="$('#form').submit();" class="btn btn-primary"><i class="fa fa-check" aria-hidden="true"></i> <?php echo $button_save; ?></a>
         </div>
    </div>
    <div class="box-body" id="intro_module">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <?php $module_row = 1; ?>
          <?php foreach ($modules as $module) { ?>
          <div class="row">
              <div class="col-sm-2">Tiêu đề</div>
              <div class="col-sm-10">
                <input class="form-control" name="intro_module[<?php echo $module_row; ?>][heading_title]" value="<?php echo $module['heading_title']; ?>" />
              </div>
          </div>
          <div class="row">
              <div class="col-sm-2">Nội dung</div>
              <div class="col-sm-10">
                <textarea class="ckeditor5" id="content-<?php echo $module_row; ?>"" name="intro_module[<?php echo $module_row; ?>][content]"><?php echo $module['content']; ?></textarea>
              </div>
          </div>
          <div class="row">
              <div class="col-sm-2">Button</div>
              <div class="col-sm-3">
                  <input class="form-control" placeholder="..." name="intro_module[<?php echo $module_row; ?>][button]" value="<?php echo $module['button']; ?>" />
              </div>
              <div class="col-sm-7">
                  <input class="form-control" placholder="http://..." name="intro_module[<?php echo $module_row; ?>][link]" value="<?php echo $module['link']; ?>" />
              </div>
          </div>
          <div class="row">
              <div class="col-sm-2">
                  <div class="form-group text-center">
                  <strong>Ảnh đại diện</strong><br />
                  <div class="image">
                      <a onclick="filebrowser('image', 'thumb');">
                        <img src="<?php echo $intro_thumb; ?>" alt="" id="thumb" />
                      </a>
                      <a class="pull-left"  onclick="filebrowser('image', 'thumb');">Chọn</a>
                      <a class="pull-right" onclick="$('#thumb').attr('src', '<?php echo $no_image; ?>'); $('#image').attr('value', '');">Xóa</a>
                      <input type="hidden" name="intro_module[<?php echo $module_row; ?>][intro_image]" value="<?php echo $module['intro_image']; ?>" id="image" />
                  </div>
                  </div>
              </div>
          </div>
          <table class="list">
            <tbody id="module-row<?php echo $module_row; ?>">
            <tr>
              <td><?php echo $entry_layout; ?></td>
              <td><select name="intro_module[<?php echo $module_row; ?>][layout_id]">
                  <?php foreach ($layouts as $layout) { ?>
                  <?php if ($layout['layout_id'] == $module['layout_id']) { ?>
                  <option value="<?php echo $layout['layout_id']; ?>" selected="selected"><?php echo $layout['name']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select></td>
              <td><?php echo $entry_position; ?></td>
              <td><select name="intro_module[<?php echo $module_row; ?>][position]">
                  <?php foreach($positions as $position){ ?>
          <option value="<?php echo $position['value'];?>" <?php echo $position['value']==$module['position'] ? 'selected':'';?>><?php echo $position['text'];?></option>
          <?php } ?>
                </select></td>
              <td><?php echo $entry_status; ?></td>
              <td><select name="intro_module[<?php echo $module_row; ?>][status]">
                  <?php if ($module['status']) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
              <td><?php echo $entry_sort_order; ?></td>
              <td><input type="text" name="intro_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $module['sort_order']; ?>" size="3" /></td>
              <td class="left"><a onclick="$('#module-row<?php echo $module_row; ?>').remove();" class="btn btn-sm btn-default" title="<?php echo $button_remove; ?>"><i class="fa fa-minus"></i></a></td>
            </tr>
          </table>
          <?php $module_row++; ?>
          <?php } ?>
          <span onclick="addModule();" class="pull-right btn btn-primary btn-sm" id="module-add"><?php echo $button_add_module; ?>&nbsp;<i class="fa fa-plus"></i></span>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript" src="view/javascript/ckeditor/ckeditor.js"></script>
<script type="text/javascript"><!--
<?php $module_row = 1; ?>
<?php foreach ($modules as $module) { ?>
  CKEDITOR.replace('content-<?php echo $module_row; ?>', {
      filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
      filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
      filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
      filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
      filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
      filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
  });
<?php } ?>
var module_row = <?php echo $module_row; ?>;
function addModule() {
  html ='<div class="row">';
  html +='<div class="col-sm-2">Tiêu đề</div>';
  html +='<div class="col-sm-10">';
  html +='    <input class="form-control" name="intro_module[<?php echo $module_row; ?>][heading_title]" value="" />';
  html +=' </div>';
  html +='</div>';
  html +='<div class="row">';
  html +='    <div class="col-sm-2">Nội dung</div>';
  html +='    <div class="col-sm-10">';
  html +='      <textarea class="ckeditor5" id="content-<?php echo $module_row; ?>" name="intro_module[<?php echo $module_row; ?>][content]"></textarea>';
  html +='    </div>';
  html +='</div>';
  html +='<div class="row">';
  html +='    <div class="col-sm-2">Button</div>';
  html +='    <div class="col-sm-3">';
  html +='      <input class="form-control" placeholder="Quy trình sản xuất" name="intro_module[<?php echo $module_row; ?>][button]" value="" />';
  html +='    </div>';
  html +='    <div class="col-sm-7">';
  html +='      <input class="form-control" placeholder="http://..." name="intro_module[<?php echo $module_row; ?>][link]" value="" />';
  html +='    </div>';
  html +='</div>';
  html +='<div class="row">';
  html +='    <div class="col-sm-2"><strong>Ảnh đại diện</strong></div>';
  html +='            <div class="col-sm-2">';
  html +='                <div class="form-group text-center">';
  html +='                <div class="image">';
  html +='                    <a class="pull-right" onclick="$(\'#thumb\').attr(\'src\', \'<?php echo $no_image; ?>\'); $(\'#image\').attr(\'value\', \'\');"><?php echo $text_clear; ?></a>';
  html +='                    <a onclick="filebrowser(\'image\', \'thumb\');">';
  html +='                      <img src="<?php echo $no_image; ?>" alt="" id="thumb" />';
  html +='                    </a>';
  html +='                    <input type="hidden" name="intro_module[<?php echo $module_row; ?>][image]" id="image" />';
  html +='                </div>';
  html +='                  </div>';
  html +='            </div>';
  html +='        </div>';
  html += '<div id="tab-module-' + module_row + '" class="contenttab">';
  html += '  <table class="form">';
  html += '    <tr>';
  html += '      <td>layout <select name="intro_module[' + module_row + '][layout_id]">';
  <?php foreach ($layouts as $layout) { ?>
  html += '           <option value="<?php echo $layout['layout_id']; ?>"><?php echo addslashes($layout['name']); ?></option>';
  <?php } ?>
  html += '      </select></td>';
  html += '      <?php echo $entry_position; ?>';
  html += '      <td><select name="intro_module[' + module_row + '][position]">';
  <?php foreach($positions as $position){ ?>
  html += '      <option value="<?php echo $position['value'];?>"><?php echo $position['text'];?></option>';
  <?php } ?>
  html += '      </select></td>';
  html += '      <td><?php echo $entry_status; ?> ';
  html += '      <select name="intro_module[' + module_row + '][status]">';
  html += '        <option value="1"><?php echo $text_enabled; ?></option>';
  html += '        <option value="0"><?php echo $text_disabled; ?></option>';
  html += '      </select></td>';
  html += '      <td><?php echo $entry_sort_order; ?> ';
  html += '      <input type="text" name="intro_module[' + module_row + '][sort_order]" value="" size="3" /></td>';
  html += '    <td class="left"><a onclick="$(\'#module-row' + module_row + '\').remove();" class="btn btn-sm btn-default" title="<?php echo $button_remove; ?>"><i class="fa fa-minus"></i></a><?php echo $button_remove; ?></a></td>';
  html += '    </tr>';
  html += '  </table>';
  html += '</div>';

  
<?php echo $footer; ?>
