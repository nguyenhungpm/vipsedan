<?php echo $header; ?>
<style type="text/css">
  #whyus_module .row{padding:10px 0;}
  #whyus_module .row:hover{background:#eee}
</style>
<div class="content-wrapper" id="content">
  <section class="content-header">
    <h1><?php echo $heading_title; ?><small>(<?php echo $text_configuration; ?>)</small></h1>
    <ol class="breadcrumb">
      <?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
        <li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
      <?php $k++; } ?>
    </ol>
  </section>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="content">
    <div class="box box-default">
    <div class="box-header">
         <div class="pull-right">
             <a onclick="location = '<?php echo $cancel; ?>';" class="btn btn-default"><i class="fa fa-reply"></i> <?php echo $button_cancel; ?></a>
               <a onclick="$('#form').submit();" class="btn btn-primary"><i class="fa fa-check" aria-hidden="true"></i> <?php echo $button_save; ?></a>
         </div>
    </div>
    <div class="box-body" id="whyus_module">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
          <div class="row">
              <div class="col-sm-2"><strong>Tiêu đề</strong></div>
              <div class="col-sm-7">
                <input class="form-control" name="whyus_heading_title" value="<?php echo $whyus_heading_title; ?>" />
              </div>
              <div class="col-sm-5">
                <strong>Banner</strong>
                <div class="image">
                      <a onclick="image_upload('whyus_image', 'whyus_thumb');">
                        <img src="<?php echo $whyus_thumb; ?>" alt="" id="whyus_thumb" />
                      </a>
                      <a class="pull-left"  onclick="image_upload('whyus_image', 'whyus_thumb');">Chọn</a>
                      <a class="pull-right" onclick="$('#whyus_thumb').attr('src', '<?php echo $no_image; ?>'); $('#whyus_image').attr('value', '');">Xóa</a>
                      <input type="hidden" name="whyus_image" value="<?php echo $whyus_image; ?>" id="whyus_image" />
                  </div>
              </div>
          </div>
          <div class="row">
              <div class="col-sm-2"><strong>Box 1</strong></div>
              <div class="col-sm-2">
                <strong>icon</strong>
                  <div class="form-group text-center">
                  <div class="image">
                      <a onclick="image_upload('whyus_image1', 'whyus_thumb1');">
                        <img src="<?php echo $whyus_thumb1; ?>" alt="" id="whyus_thumb1" />
                      </a>
                      <a class="pull-left"  onclick="image_upload('whyus_image1', 'whyus_thumb1');">Chọn</a>
                      <a class="pull-right" onclick="$('#whyus_thumb1').attr('src', '<?php echo $no_image; ?>'); $('#whyus_image1').attr('value', '');">Xóa</a>
                      <input type="hidden" name="whyus_image1" value="<?php echo $whyus_image1; ?>" id="whyus_image1" />
                  </div>
                  </div>
              </div>
              <div class="col-sm-2">
                <strong>Số</strong>
                <input class="form-control" name="whyus_number1" value="<?php echo $whyus_number1; ?>" />
              </div>
              <div class="col-sm-6">
                <strong>Nội dung</strong>
                <input class="form-control" name="whyus_content1" value="<?php echo $whyus_content1; ?>" />
              </div>
          </div>
          <div class="row">
              <div class="col-sm-2"><strong>Box 2</strong></div>
              <div class="col-sm-2">
                <strong>Icon</strong>
                  <div class="form-group text-center">
                  <div class="image">
                      <a onclick="image_upload('whyus_image2', 'whyus_thumb2');">
                        <img src="<?php echo $whyus_thumb2; ?>" alt="" id="whyus_thumb2" />
                      </a>
                      <a class="pull-left"  onclick="image_upload('whyus_image2', 'whyus_thumb2');">Chọn</a>
                      <a class="pull-right" onclick="$('#whyus_thumb2').attr('src', '<?php echo $no_image; ?>'); $('#whyus_image2').attr('value', '');">Xóa</a>
                      <input type="hidden" name="whyus_image2" value="<?php echo $whyus_image2; ?>" id="whyus_image2" />
                  </div>
                  </div>
              </div>
              <div class="col-sm-2">
                <strong>Số</strong>
                <input class="form-control" name="whyus_number2" value="<?php echo $whyus_number2; ?>" />
              </div>
              <div class="col-sm-6">
                <strong>Nội dung</strong>
                <input class="form-control" name="whyus_content2" value="<?php echo $whyus_content2; ?>" />
              </div>
          </div>
          <div class="row">
              <div class="col-sm-2"><strong>Box 3</strong></div>
              <div class="col-sm-2">
                <strong>icon</strong>
                  <div class="form-group text-center">
                  <div class="image">
                      <a onclick="image_upload('whyus_image3', 'whyus_thumb3');">
                        <img src="<?php echo $whyus_thumb3; ?>" alt="" id="whyus_thumb3" />
                      </a>
                      <a class="pull-left"  onclick="image_upload('whyus_image3', 'whyus_thumb3');">Chọn</a>
                      <a class="pull-right" onclick="$('#whyus_thumb3').attr('src', '<?php echo $no_image; ?>'); $('#whyus_image3').attr('value', '');">Xóa</a>
                      <input type="hidden" name="whyus_image3" value="<?php echo $whyus_image3; ?>" id="whyus_image3" />
                  </div>
                  </div>
              </div>
              <div class="col-sm-2">
                <strong>Số</strong>
                <input class="form-control" name="whyus_number3" value="<?php echo $whyus_number3; ?>" />
              </div>
              <div class="col-sm-6">
                <strong>Nội dung</strong>
                <input class="form-control" name="whyus_content3" value="<?php echo $whyus_content3; ?>" />
              </div>
          </div>
          <div class="row">
              <div class="col-sm-2"><strong>Box 4</strong></div>
              <div class="col-sm-2">
                <strong>icon</strong>
                  <div class="form-group text-center">
                  <div class="image">
                      <a onclick="image_upload('whyus_image4', 'whyus_thumb4');">
                        <img src="<?php echo $whyus_thumb4; ?>" alt="" id="whyus_thumb4" />
                      </a>
                      <a class="pull-left"  onclick="image_upload('whyus_image4', 'whyus_thumb4');">Chọn</a>
                      <a class="pull-right" onclick="$('#whyus_thumb4').attr('src', '<?php echo $no_image; ?>'); $('#whyus_image4').attr('value', '');">Xóa</a>
                      <input type="hidden" name="whyus_image4" value="<?php echo $whyus_image4; ?>" id="whyus_image4" />
                  </div>
                  </div>
              </div>
              <div class="col-sm-2">
                <strong>Số</strong>
                <input class="form-control" name="whyus_number4" value="<?php echo $whyus_number4; ?>" />
              </div>
              <div class="col-sm-6">
                <strong>Nội dung</strong>
                <input class="form-control" name="whyus_content4" value="<?php echo $whyus_content4; ?>" />
              </div>
          </div>
          <div class="row hide">
              <div class="col-sm-2"><strong>Box 5</strong></div>
              <div class="col-sm-2">
                <strong>icon</strong>
                  <div class="form-group text-center">
                  <div class="image">
                      <a onclick="image_upload('whyus_image5', 'whyus_thumb5');">
                        <img src="<?php echo $whyus_thumb5; ?>" alt="" id="whyus_thumb5" />
                      </a>
                      <a class="pull-left"  onclick="image_upload('whyus_image5', 'whyus_thumb5');">Chọn</a>
                      <a class="pull-right" onclick="$('#whyus_thumb5').attr('src', '<?php echo $no_image; ?>'); $('#whyus_image5').attr('value', '');">Xóa</a>
                      <input type="hidden" name="whyus_image5" value="<?php echo $whyus_image5; ?>" id="whyus_image5" />
                  </div>
                  </div>
              </div>
              <div class="col-sm-2">
                <strong>Số</strong>
                <input class="form-control" name="whyus_number5" value="<?php echo $whyus_number5; ?>" />
              </div>
              <div class="col-sm-6">
                <strong>Nội dung</strong>
                <input class="form-control" name="whyus_content5" value="<?php echo $whyus_content5; ?>" />
              </div>
          </div>
        <?php $module_row = 1; ?>
          <table id="module" class="list">
          <thead>
            <tr>
              <td class="left"><?php echo $entry_layout; ?></td>
              <td class="left"><?php echo $entry_position; ?></td>
              <td class="left"><?php echo $entry_status; ?></td>
              <td class=""><?php echo $entry_sort_order; ?></td>
              <td></td>
            </tr>
          </thead>
          <?php foreach ($modules as $module) { ?>
            <tbody id="module-row<?php echo $module_row; ?>">
            <tr>
              <td><select name="whyus_module[<?php echo $module_row; ?>][layout_id]">
                  <?php foreach ($layouts as $layout) { ?>
                  <?php if ($layout['layout_id'] == $module['layout_id']) { ?>
                  <option value="<?php echo $layout['layout_id']; ?>" selected="selected"><?php echo $layout['name']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select></td>
              <td><select name="whyus_module[<?php echo $module_row; ?>][position]">
                  <?php foreach($positions as $position){ ?>
          <option value="<?php echo $position['value'];?>" <?php echo $position['value']==$module['position'] ? 'selected':'';?>><?php echo $position['text'];?></option>
          <?php } ?>
                </select></td>
              <td><select name="whyus_module[<?php echo $module_row; ?>][status]">
                  <?php if ($module['status']) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
              <td><input type="text" name="whyus_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $module['sort_order']; ?>" size="3" /></td>
              <td class="left"><a onclick="$('#module-row<?php echo $module_row; ?>').remove();" class="btn btn-sm btn-default" title="<?php echo $button_remove; ?>"><i class="fa fa-minus"></i></a></td>
            </tr>
            </tbody>
          <?php $module_row++; ?>
          <?php } ?>
          </table>
          <span onclick="addModule();" class="pull-right btn btn-primary btn-sm" id="module-add"><?php echo $button_add_module; ?>&nbsp;<i class="fa fa-plus"></i></span>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
var module_row = <?php echo $module_row; ?>;

function addModule() {
  html  = '<div id="tab-module-' + module_row + '" class="content">';
  html += '  <table class="form">';
  html += '    <tr>';
  html += '      <td><select name="whyus_module[' + module_row + '][layout_id]">';
  <?php foreach ($layouts as $layout) { ?>
  html += '           <option value="<?php echo $layout['layout_id']; ?>"><?php echo addslashes($layout['name']); ?></option>';
  <?php } ?>
  html += '      </select></td>';
  html += '      <td><select name="whyus_module[' + module_row + '][position]">';
  <?php foreach($positions as $position){ ?>
  html += '      <option value="<?php echo $position['value'];?>"><?php echo $position['text'];?></option>';
  <?php } ?>
  html += '      </select></td>';
  html += '      <select name="whyus_module[' + module_row + '][status]">';
  html += '        <option value="1"><?php echo $text_enabled; ?></option>';
  html += '        <option value="0"><?php echo $text_disabled; ?></option>';
  html += '      </select></td>';
  html += '      <td><?php echo $entry_sort_order; ?> ';
  html += '      <input type="text" name="whyus_module[' + module_row + '][sort_order]" value="" size="3" /></td>';
  html += '    </tr>';
  html += '  </table>';
  html += '</div>';

  $('#form').append(html);

  module_row++;
}
function image_upload(field, thumb) {
  $('#dialog').remove();
  $('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token; ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');

  $('#dialog').dialog({
    title: '<?php echo $text_image_manager; ?>',
    close: function (event, ui) {
      if ($('#' + field).attr('value')) {
        $.ajax({
          url: 'index.php?route=common/filemanager/image&token=<?php echo $token; ?>&image=' + encodeURIComponent($('#' + field).attr('value')),
          dataType: 'text',
          success: function(text) {
            $('#' + thumb).replaceWith('<img src="' + text + '" alt="" id="' + thumb + '" />');
          }
        });
      }
    },
    bgiframe: false,
    width: 1002,
    height: 600,
    resizable: false,
    modal: false
  });
};
//--></script>
<?php echo $footer; ?>