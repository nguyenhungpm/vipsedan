<?php echo $header; ?>
<div class="content-wrapper">
  <section class="content-header">
		<h1><?php echo $heading_title; ?><small>(<?php echo $text_configuration; ?>)</small></h1>
		<ol class="breadcrumb">
			<?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
				<li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
			<?php $k++; } ?>
		</ol>
	</section>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<div class="content">
  <div class="box box-default">
    <div class="box-header">
    <div class="pull-right">
  		<a onclick="location = '<?php echo $cancel; ?>';" class="btn btn-default"><i class="fa fa-reply"></i>  <?php echo $button_cancel; ?></a>
  		<a onclick="$('#form').submit();" class="btn btn-primary"><i class="fa fa-check" aria-hidden="true"></i>  <?php echo $button_save; ?></a>
  	</div>
    </div>
    <div class="box-body">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
      <table class="form">
        <tr>
          <td><?php echo $entry_news; ?></td>
          <td><input type="text" name="news" value="" /></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><div id="newsfeatured-news" class="scrollbox">
              <?php $class = 'odd'; ?>
              <?php foreach ($newss as $news) { ?>
              <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
              <div id="newsfeatured-news<?php echo $news['news_id']; ?>" class="<?php echo $class; ?>"><?php echo $news['name']; ?> <img src="view/image/delete.png" />
                <input type="hidden" value="<?php echo $news['news_id']; ?>" />
              </div>
              <?php } ?>
            </div>
			<a href="#" class="m_mover" onclick="moveMarkedItemsUp(); return false;"><span>Move Up</span></a>&nbsp;
			<a href="#" class="m_mover" onclick="moveMarkedItemsDown(); return false;"><span>Move Down</span></a>
            <input type="hidden" name="newsfeatured_news" value="<?php echo $newsfeatured_news; ?>" /></td>
        </tr>
      </table>
      <table id="module" class="list">
        <thead>
          <tr>
            <td class="left"><?php echo $entry_limit; ?></td>
			<td class="left"><?php echo $entry_limitdescription; ?></td>
            <td class="left"><?php echo $entry_image; ?></td>
            <td class="left"><?php echo $entry_layout; ?></td>
            <td class="left"><?php echo $entry_position; ?></td>
            <td class="left"><?php echo $entry_description; ?></td>
			<td class="left"><?php echo $entry_imagestatus; ?></td>
            <td class="left"><?php echo $entry_status; ?></td>
            <td class="right"><?php echo $entry_sort_order; ?></td>
            <td></td>
          </tr>
        </thead>
        <?php $module_row = 0; ?>
        <?php foreach ($modules as $module) { ?>
        <tbody id="module-row<?php echo $module_row; ?>">
          <tr>
            <td class="left"><input type="text" name="newsfeatured_module[<?php echo $module_row; ?>][limit]" value="<?php echo $module['limit']; ?>" size="3" /></td>
			<td class="left"><input type="text" name="newsfeatured_module[<?php echo $module_row; ?>][limitdescription]" value="<?php echo $module['limitdescription']; ?>" size="3" /></td>
            <td class="left"><input type="text" name="newsfeatured_module[<?php echo $module_row; ?>][image_width]" value="<?php echo $module['image_width']; ?>" size="3" />
              <input type="text" name="newsfeatured_module[<?php echo $module_row; ?>][image_height]" value="<?php echo $module['image_height']; ?>" size="3" />
              <?php if (isset($error_image[$module_row])) { ?>
              <span class="error"><?php echo $error_image[$module_row]; ?></span>
              <?php } ?></td>
            <td class="left"><select name="newsfeatured_module[<?php echo $module_row; ?>][layout_id]">
                <?php foreach ($layouts as $layout) { ?>
                <?php if ($layout['layout_id'] == $module['layout_id']) { ?>
                <option value="<?php echo $layout['layout_id']; ?>" selected="selected"><?php echo $layout['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select></td>
            <td class="left"><select name="newsfeatured_module[<?php echo $module_row; ?>][position]">
                <?php foreach($positions as $position){ ?>
					<option value="<?php echo $position['value'];?>" <?php echo $position['value']==$module['position'] ? 'selected':'';?>><?php echo $position['text'];?></option>
				  <?php } ?>
              </select></td>
            <td class="left"><select name="newsfeatured_module[<?php echo $module_row; ?>][description]">
                <?php if ($module['description']) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
			 <td class="left"><select name="newsfeatured_module[<?php echo $module_row; ?>][imagestatus]">
                <?php if ($module['imagestatus']) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
            <td class="left"><select name="newsfeatured_module[<?php echo $module_row; ?>][status]">
                <?php if ($module['status']) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
            <td class="right"><input type="text" name="newsfeatured_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $module['sort_order']; ?>" size="3" /></td>
            <td class="left"><a onclick="$('#module-row<?php echo $module_row; ?>').remove();" class="btn btn-sm btn-defalt" title="<?php echo $button_remove; ?>"><i class="fa fa-minus"></i></a></td>
          </tr>
        </tbody>
        <?php $module_row++; ?>
        <?php } ?>
        <tfoot>
          <tr>
            <td colspan="7"></td>
            <td class="left"><a onclick="addModule();" class="btn btn-primary"><i class="fa fa-plus"></i> <?php echo $button_add_module; ?></a></td>
          </tr>
        </tfoot>
      </table>
    </form>
  </div>
</div>
</div>
<script type="text/javascript"><!--
$('input[name=\'news\']').autocomplete({
	delay: 0,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/news/autocomplete&token=<?php echo $token; ?>',
			type: 'POST',
			dataType: 'json',
			data: 'filter_name=' +  encodeURIComponent(request.term),
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item.name,
						value: item.news_id
					}
				}));
			}
		});

	},
	select: function(event, ui) {
		$('#newsfeatured-news' + ui.item.value).remove();

		$('#newsfeatured-news').append('<div id="newsfeatured-news' + ui.item.value + '">' + ui.item.label + '<img src="view/image/delete.png" /><input type="hidden" value="' + ui.item.value + '" /></div>');

		$('#newsfeatured-news div:odd').attr('class', 'odd');
		$('#newsfeatured-news div:even').attr('class', 'even');

		data = $.map($('#newsfeatured-news input'), function(element){
			return $(element).attr('value');
		});

		$('input[name=\'newsfeatured_news\']').attr('value', data.join());

		return false;
	}
});
$(document).on('click', '#newsfeatured-news div img', function() {
	$(this).parent().remove();

	$('#newsfeatured-news div:odd').attr('class', 'odd');
	$('#newsfeatured-news div:even').attr('class', 'even');

	data = $.map($('#newsfeatured-news input'), function(element){
		return $(element).attr('value');
	});

	$('input[name=\'newsfeatured_news\']').attr('value', data.join());
});

$(document).on('click', '#newsfeatured-news div', function() {
				$this = $(this);

				if($this.hasClass('marked'))
					$(this).removeClass('marked');
				else
					$(this).addClass('marked');
			});

			function moveMarkedItemsUp() {
				$('.marked').each(function(index) {
					$this = $(this);
					$this.prev().before($this);
				});

				reColorThenReMap();
			}

			jQuery.fn.reverse = function() {
				return this.pushStack(this.get().reverse(), arguments);
			};

			function moveMarkedItemsDown() {
				$('.marked').reverse().each(function(index) {
					$this = $(this);
					$this.next().after($this);
				});

				reColorThenReMap();
			}

			function reColorThenReMap()
			{
				$('#newsfeatured-news div').removeClass('odd').removeClass('even');
				$('#newsfeatured-news div:odd').addClass('odd');
				$('#newsfeatured-news div:even').addClass('even');

				data = $.map($('#newsfeatured-news input'), function(element){
					return $(element).attr('value');
				});

				$('input[name=\'newsfeatured_news\']').attr('value', data.join());
			}
//--></script>
<script type="text/javascript"><!--
var module_row = <?php echo $module_row; ?>;

function addModule() {
	html  = '<tbody id="module-row' + module_row + '">';
	html += '  <tr>';
	html += '    <td class="left"><input type="text" name="newsfeatured_module[' + module_row + '][limit]" value="5" size="3" /></td>';
	html += '    <td class="left"><input type="text" name="newsfeatured_module[' + module_row + '][limitdescription]" value="200" size="3" /></td>';
	html += '    <td class="left"><input type="text" name="newsfeatured_module[' + module_row + '][image_width]" value="80" size="3" /> <input type="text" name="newsfeatured_module[' + module_row + '][image_height]" value="80" size="3" /></td>';
	html += '    <td class="left"><select name="newsfeatured_module[' + module_row + '][layout_id]">';
	<?php foreach ($layouts as $layout) { ?>
	html += '      <option value="<?php echo $layout['layout_id']; ?>"><?php echo addslashes($layout['name']); ?></option>';
	<?php } ?>
	html += '    </select></td>';
	html += '    <td class="left"><select name="newsfeatured_module[' + module_row + '][position]">';
	<?php foreach($positions as $position){ ?>
	html += '      <option value="<?php echo $position['value'];?>"><?php echo $position['text'];?></option>';
	<?php } ?>
	html += '    </select></td>';
	html += '    <td class="left"><select name="newsfeatured_module[' + module_row + '][description]">';
    html += '      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
    html += '      <option value="0"><?php echo $text_disabled; ?></option>';
    html += '    </select></td>';
	html += '    <td class="left"><select name="newsfeatured_module[' + module_row + '][imagestatus]">';
    html += '      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
    html += '      <option value="0"><?php echo $text_disabled; ?></option>';
    html += '    </select></td>';
	html += '    <td class="left"><select name="newsfeatured_module[' + module_row + '][status]">';
    html += '      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
    html += '      <option value="0"><?php echo $text_disabled; ?></option>';
    html += '    </select></td>';
	html += '    <td class="right"><input type="text" name="newsfeatured_module[' + module_row + '][sort_order]" value="" size="3" /></td>';
	html += '    <td class="left"><a onclick="$(\'#module-row' + module_row + '\').remove();" class="btn btn-sm btn-default" title="<?php echo $button_remove; ?>"><i class="fa fa-minus"></i></a><?php echo $button_remove; ?></a></td>';
	html += '  </tr>';
	html += '</tbody>';

	$('#module tfoot').before(html);

	module_row++;
}
//--></script>
<style>
				.marked {
					border-left: 5px solid orange;
				}

				.scrollbox
				{
					height: 200px;
				}
				div#newsfeatured-news {
					float: left;
				}
				a.m_mover {
					float: left;
					margin-left: 15px;
					text-decoration: none;
					text-align: center;
					outline: none;
				}
				a.m_mover span {
					display:block;
					margin: 5px auto;
					color:#666;
				}
				a.m_mover span:before {
					content: '';
					width: 22px;
					height: 27px;
					display: block;
					margin: 0 auto;
					background: url(view/image/mover_up_bg.png) left top no-repeat;
				}
				a + a.m_mover span:before {
					background-image: url(view/image/mover_down_bg.png);
				}
				a.m_mover:hover span {color: #333;}
				a.m_mover:hover span:before {background-position: center top}
				a.m_mover:active span{color: #5c9bc8;}
				a.m_mover:active span:before {background-position: right top}
			</style>
<?php echo $footer; ?>
