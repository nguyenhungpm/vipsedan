<?php echo $header; ?>
<div id="content" class="content-wrapper">
	<section class="content-header">
		<h1><?php echo $heading_title; ?><small>(Danh sách)</small></h1>
		<ol class="breadcrumb">
			<?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
				<li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
			<?php $k++; } ?>
		</ol>
	</section>
    <?php if ($error_warning) { ?>
		<div class="warning"><?php echo $error_warning; ?></div>
	<?php } ?>
	<?php if ($success) { ?>
		<div class="success"><?php echo $success; ?></div>
	<?php } ?>
	
	<div class="content">
		<div class="box box-primary">
			<div class="box-header">
				<div class="pull-left">
					<a onclick="$('#form').submit();" class="btn btn-default"><i class="fa fa-times"></i> <?php echo $button_delete; ?></a>
				</div>
				<div class="pull-right">
					<a href="#myModal" data-toggle="modal" title="Thêm nội dung" class="btn btn-primary"><i class="fa fa-plus"></i> Thêm nội dung</a>
				</div>
			</div>
			<div class="box-body">
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
					<table class="list">
						<thead>
							<tr>
								<td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
								<td class="center" style="width: 90px;">Hình ảnh</td>
								<td class="left">Tên</td>
								<td class="center">Thứ tự</td>
								<td class="center">Trạng thái</td>
								<td class="center">Thao tác</td>
							</tr>
						</thead>
						<tbody>
							<?php if ($megamenus) { ?>
								<?php foreach ($megamenus as $megamenu) { ?>
									<?php if(isset($this->request->get['megamenu_id']) && $megamenu['megamenu_id'] == $this->request->get['megamenu_id']){ ?>
										<tr>
											<td style="text-align: center;"><input type="checkbox" name="selected[]" value="<?php echo $megamenu['megamenu_id']; ?>"/></td>
											<td class="text-center">
											  <div class="image"><img src="<?php echo $megamenu['thumb']; ?>" alt="" id="thumb<?php echo $megamenu['megamenu_id']; ?>" /><br />
												  <input type="hidden" name="image" value="<?php echo $megamenu['image']; ?>" id="image<?php echo $megamenu['megamenu_id']; ?>" />
												  <a onclick="image_upload('image<?php echo $megamenu['megamenu_id']; ?>', 'thumb<?php echo $megamenu['megamenu_id']; ?>');">Chọn ảnh</a><br /><a onclick="$('#thumb<?php echo $megamenu['megamenu_id']; ?>').attr('src', '<?php echo $no_image; ?>'); $('#image<?php echo $megamenu['megamenu_id']; ?>').attr('value', '');">Xóa ảnh</a>
											  </div>
											</td>
											<td class="left">
												<?php foreach ($languages as $key=> $language) { ?>
													<img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <input class="form-control" placeholder="Tên (<?php echo $language['name']; ?>)" type="text" name="detail[<?php echo $language['language_id'];?>][name]" value="<?php echo !empty($megamenu['detail'][$language['language_id']]['name']) ? $megamenu['detail'][$language['language_id']]['name']:''; ?>"/>
												<?php } ?>
											</td>
											<td class="text-center">
												<select name="parent_id" class="form-control">
												<option <?php echo $megamenu['parent_id']==0 ? 'selected':''; ?> value="0">--root--</option>
												<?php foreach ($list as $megamenu_2) { if($megamenu_2['megamenu_id']==$megamenu['megamenu_id']) continue; ?>
												<option <?php echo $megamenu['parent_id']==$megamenu_2['megamenu_id'] ? 'selected':''; ?> value="<?php echo $megamenu_2['megamenu_id'];?>"><?php echo $megamenu_2['name'];?></option>
												<?php } ?>
												</selected>
												<input class="form-control" placeholder="Liên kết" type="text" name="url" value="<?php echo $megamenu['url']; ?>"/>
												<input class="form-control" placeholder="Thứ tự" type="text" name="sort_order" value="<?php echo $megamenu['sort_order']; ?>"/>
											</td>
											<td class="text-center">
												<select name="status" class="form-control">
													<option value="1" <?php echo $megamenu['status']==1 ? 'selected':''; ?>>Bật</option>
													<option value="0" <?php echo $megamenu['status']==0 ? 'selected':''; ?>>Tắt</option>
												</select>
											</td>
											<td class="center"><a onclick="save('<?php echo $megamenu['megamenu_id']; ?>')" class="button">Lưu</a></td>
										</tr>
										<?php }else{ ?>
										<tr>
											<td style="text-align: center;"><input type="checkbox" name="selected[]" value="<?php echo $megamenu['megamenu_id']; ?>"/></td>
											<td class="center"><img src="<?php echo $megamenu['thumb']; ?>" alt=""/></td>
											<td class="left"><strong><?php echo $megamenu['name']; ?></strong></td>
											<td class="text-center"><?php echo $megamenu['sort_order']; ?></td>
											<td class="text-center"><?php echo $megamenu['status'] == 1 ? 'Bật':'Tắt'; ?></td>
											<td class="center"><a href="<?php echo $megamenu['edit']; ?>">Sửa</a></td>
										</tr>
									<?php } ?>
								<?php } ?>
								<?php } else { ?>
								<tr>
									<td class="center" colspan="7"><?php echo $text_no_results; ?></td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</form>
				<div class="pagination"><?php echo $pagination; ?></div>
			</div>
		</div>
	</div>
	<div id="myModal" class="modal fade">
		<div class="modal-dialog form-sale">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Thêm nội dung</h4>
				</div>
				<div class="modal-body">
					<div class="tab-content">
						<form method="POST" action="" name="form-megamenu" id="form-megamenu">
							<?php foreach ($languages as $key=> $language) { ?>
								<div class="form-group required">
									<label class="control-label text-left"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> Tên (<?php echo $language['name']; ?>)</label>
									<input class="form-control" type="text" name="detail[<?php echo $language['language_id'];?>][name]"/>
								</div>
							<?php } ?>
							<div class="form-group">
								<label class="control-label text-left">Cấp menu</label>
								<select name="parent_id" class="form-control">
									<option value="0">--root--</option>
									<?php foreach ($list as $megamenu_2) { ?>
									<option value="<?php echo $megamenu_2['megamenu_id'];?>"><?php echo $megamenu_2['name'];?></option>
									<?php } ?>
								</select>
							</div>
							<div class="form-group">
								<label class="control-label text-left">Liên kết</label>
								<input class="form-control" type="text" name="url"/>
							</div>
							<div class="row clear">
							<div class="col-md-6">
							<div class="form-group">
								<label class="control-label text-left">Icon/Hình ảnh</label>
								<div class="image text-center">
								  <img src="<?php echo $thumb; ?>" alt="" id="thumb" /><br />
								  <input type="hidden" name="image" value="" id="image" />
								  <a onclick="image_upload('image', 'thumb');">Chọn</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#thumb').attr('src', '<?php echo $no_image; ?>'); $('#image').attr('value', '');">Xóa</a>
								</div>
							</div>
							</div>
							<div class="col-md-6">
							<div class="form-group">
								<label class="control-label text-left">Thứ tự</label>
								<input class="form-control" type="text" name="sort_order"/>
							</div>
							<div class="form-group">
								<label class="control-label text-left">Trạng thái</label>
								<select name="status" class="form-control">
									<option value="1">Bật</option>
									<option value="0">Tắt</option>
								</select>
							</div>
							</div>
							</div>
						</form>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
					<button type="button" id="submit" onclick="submit_form()" data-dismiss="modal" class="btn btn-primary">Lưu</button>
				</div>
			</div>
		</div>
	</div>
	<script>
		$("#myModal").draggable({
			handle: ".modal-header"
		});
		function submit_form(){
			$.ajax({
				url: 'index.php?route=extension/megamenu/insert&token=<?php echo $token; ?>',
				type: 'post',
				dataType: 'json',
				data : $("#form-megamenu").serialize(),
				complete: function() {
					$('#submit').button('reset');
				},
				success: function(json) {
					var href = 'index.php?route=extension/megamenu&token=<?php echo $token; ?>';
					<?php if(isset($page) && !empty($page)){ ?>
						href += '&page=<?php echo $page; ?>';
					<?php } ?>
					window.location = href;
				}
			});
		}
		function save(id){
		
			var url = 'index.php?route=extension/megamenu/edit&token=<?php echo $token; ?>&megamenu_id=' +  id;
			<?php if(isset($page) && !empty($page)){ ?>
				url += '&page=<?php echo $page; ?>';
			<?php } ?>
			$.ajax({
				url: url,
				type: 'POST',
				data : $("#form").serialize(),
				dataType: 'json',
				success: function(json) {
					var href = 'index.php?route=extension/megamenu&token=<?php echo $token; ?>';
					<?php if(isset($page) && !empty($page)){ ?>
						href += '&page=<?php echo $page; ?>';
					<?php } ?>
					window.location = href;
				}
			});
		}
	</script>
	<script type="text/javascript"><!--
function image_upload(field, thumb) {

  $('#dialog').remove();

  $('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token; ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');

  $('#dialog').dialog({
    title: 'Quản lí hình ảnh',
    close: function (event, ui) {
      if ($('#' + field).attr('value')) {
        $.ajax({
          url: 'index.php?route=common/filemanager/image&token=<?php echo $token; ?>&image=' + encodeURIComponent($('#' + field).attr('value')),
          dataType: 'text',
          success: function(text) {
            $('#' + thumb).replaceWith('<img src="' + text + '" alt="" id="' + thumb + '" />');
          }
        });
      }
    },
    bgiframe: false,
    width: 1002,
    height: 600,
    resizable: false,
    modal: false
  });
};
//--></script>
<script type="text/javascript"><!--
    $('input[name=\'url\']').autocomplete({
        delay: 0,
        source: function (request, response) {
            $.ajax({
                url: 'index.php?route=extension/megamenu/autocomplete&token=<?php echo $token; ?>',
                type: 'POST',
                dataType: 'json',
                data: 'filter_name=' + encodeURIComponent(request.term),
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item.name,
                            value: item.news_id
                        }
                    }));
                }
            });
 
        },
        select: function (event, ui) {
            $('input[name=\'url\']').val(ui.item.label)
            return false;
        }
    });
//--></script> 
	<?php echo $footer; ?>
