<?php echo $header; ?>
<script src="view/javascript/codemirror.js"></script>
<script src="view/javascript/mode/css/css.js"></script>
<script src="view/javascript/mode/xml/xml.js"></script>
<script src="view/javascript/mode/showhint.js"></script>
<script src="view/javascript/mode/csshint.js"></script>
<link rel="stylesheet" href="view/stylesheet/codemirror.css">
<div class="content-wrapper">
  <section class="content-header">
    <h1><?php echo $heading_title; ?><small>(<?php echo $text_configuration; ?>)</small></h1>
  </section>
  <div id="alert"></div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="content" id="content">
  <div class="box box-primary">
    <div class="box-header">
      <div class="pull-right">
        <a href="<?php echo $cancel; ?>" class="btn btn-default"><i class="fa fa-reply" aria-hidden="true"></i> <?php echo $button_cancel; ?></a>
        <a onclick="$('#form').submit();" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> <?php echo $button_save; ?></a>
      </div>
    </div>
    <div class="box-body">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
          <div class="form-group">
              <label for="config_script_header" class="control-label">
                  <?php echo $entry_header; ?>
              </label>
              <textarea class="form-control" name="config_script_header" id="config_script_header" cols="120" rows="8"><?php echo $config_script_header; ?></textarea>
          </div>
          <div class="form-group">
              <label for="config_script_footer" class="control-label">
                <?php echo $entry_footer; ?>
              </label>
              <textarea class="form-control" name="config_script_footer" id="config_script_footer" cols="120" rows="8"><?php echo $config_script_footer; ?></textarea>
          </div>
          <div class="form-group">
              <label for="config_custom_css" class="control-label">Custom CSS</label>
              <textarea id="config_custom_css" name="config_custom_css" cols="120" rows="8"><?php echo $config_custom_css; ?></textarea>
          </div>
      </form>
    </div>
  </div>
</div>
</div>
<script>
  var editor = CodeMirror.fromTextArea(document.getElementById("config_script_header"), {
    mode: "text/html",
  });
  var editor = CodeMirror.fromTextArea(document.getElementById("config_script_footer"), {
    mode: "text/html",
  });
  var editor = CodeMirror.fromTextArea(document.getElementById("config_custom_css"), {
    mode:'css',
    lineNumbers: true,
    matchBrackets: true
  });
</script>

<?php echo $footer; ?>
