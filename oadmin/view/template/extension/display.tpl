<?php echo $header; ?>
<div class="content-wrapper">
  <section class="content-header">
    <h1><?php echo $heading_title; ?><small>(<?php echo $text_configuration; ?>)</small></h1>
    <ol class="breadcrumb">
      <?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
        <li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
      <?php $k++; } ?>
    </ol>
  </section>
  <div id="alert"></div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="content" id="content">
  <div class="box box-primary">
    <div class="box-header">
      <div class="pull-right">
        <a onclick="$('#form').submit();" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> <?php echo $button_save; ?></a>
      </div>
    </div>
    <div class="box-body">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" class="form-horizontal">
          <div class="form-group">
                <label class="col-sm-3"><?php echo $entry_product_date; ?></label>
                <div class="col-sm-9">
                  <div class="clearfix">
    								<label class="switch">
    								  <input type="checkbox" name="config_dateog" <?php echo $config_dateog ? 'checked':''; ?> value="1" />
    								  <span class="slider round"></span>
    								</label>
    					    </div>
                </div>
          </div>
          <div class="form-group">
                <label class="col-sm-3"><?php echo $entry_product_price; ?></label>
                <div class="col-sm-9">
                  <div class="clearfix">
    								<label class="switch">
    								  <input type="checkbox" name="config_product_price" <?php echo $config_product_price ? 'checked':''; ?> value="1" />
    								  <span class="slider round"></span>
    								</label>
    					    </div>
                </div>
          </div>
          <div class="form-group">
                <label class="col-sm-3"><?php echo $entry_product_status; ?></label>
                <div class="col-sm-9">
                  <div class="clearfix">
    								<label class="switch">
    								  <input type="checkbox" name="config_product_stock" <?php echo $config_product_stock ? 'checked':''; ?> value="1" />
    								  <span class="slider round"></span>
    								</label>
    					    </div>
                </div>
          </div>
          <div class="form-group">
                <label class="col-sm-3"><span class="required">*</span> <?php echo $entry_catalog_limit; ?></label>
                <div class="col-sm-1"><input type="text" name="config_catalog_limit" value="<?php echo $config_catalog_limit; ?>" size="3" class="form-control"/>
                  <?php if ($error_catalog_limit) { ?>
                  <span class="error"><?php echo $error_catalog_limit; ?></span>
                  <?php } ?>
                </div>
          </div>
          <div class="form-group">
                <label class="col-sm-3"><span class="required">*</span> <?php echo $entry_news_limit; ?></label>
                <div class="col-sm-1"><input type="text" name="config_news_limit" value="<?php echo $config_news_limit; ?>" size="3" class="form-control"/>
                </div>
          </div>
          <div class="form-group">
                <label class="col-sm-3"><span class="required">*</span> <?php echo $entry_admin_limit; ?></label>
                <div class="col-sm-1"><input type="text" name="config_admin_limit" value="<?php echo $config_admin_limit; ?>" size="3" class="form-control control-inline" />
                  <?php if ($error_admin_limit) { ?>
                  <span class="error"><?php echo $error_admin_limit; ?></span>
                  <?php } ?>
                </div>
          </div>
      </form>
    </div>
  </div>
</div>
</div>
<?php echo $footer; ?>
