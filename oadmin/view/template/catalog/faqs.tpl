<?php echo $header; ?>
<style type='text/css'>
table.form > tbody > tr > td:first-child {
	width: 300px;
}
.q-heading {
    margin-bottom: 10px;
    padding: 10px 10px 10px 33px;
    color: #555555;
    cursor: pointer;
}

.q-heading.red {
	border: 1px solid #F8ACAC;
	background-color: #FFD1D1;
}

.q-heading.green {
   background-color: #EAF7D9;
   border: 1px solid #BBDF8D;
}

.q-heading.loading {
	background-image: url(view/image/loading.gif);
	background-position: center;
	background-repeat: no-repeat;
}

.q-heading .lt {
	text-align: left;
	display: inline-block;
	width: 60%;
}

.q-heading .rt {
	text-align: right;
	display: inline-block;	
	width: 40%;
}

.q-content {
	margin-bottom: 5px;
	padding: 10px;
	display: none;
}

.q-content.red {
	border: 1px solid #F8ACAC;
}

.q-content.green {
	border: 1px solid #BBDF8D;
}

table.form > tbody > tr {
    border-bottom: 1px dotted #CCCCCC;
    color: #000000;
    padding: 10px;
}

table.form > tbody > tr > td:first-child {
    width: 200px;
}

table.form {
	width: 100%;
	text-align: left;
}

.htabs .buttons .button {
    background: none repeat scroll 0 0 #003A88;
    border-radius: 10px 10px 10px 10px;
    color: #FFFFFF;
    display: inline-block;
    padding: 5px 15px;
    text-decoration: none;
    font-weight: normal;
}

.pagination {
	border-top: none;
}

tr.ffSample {
        display: none;
}
</style>
<div class="content-wrapper">
	<section class="content-header">
	<h1><?php echo $heading_title; ?><small>(Danh sách)</small></h1>
	<ol class="breadcrumb">
			<?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
				<li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
			<?php $k++; } ?>
		</ol>
	</section>
	<?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
    <?php if ($success) { ?>
        <div class="success"><?php echo $success; ?></div>
    <?php } ?>
	<div class="content">
		<div class="box box-default">
			<div class="box-body">
				<script type="text/javascript" src="view/javascript/ckeditor/ckeditor.js"></script>
				<?php if (!empty($faqs)) {
						$i=0;
				        foreach($faqs as $q) {
				        $qId = $q['faq_id'];
			        ?>
			                <div>
			                <form action="index.php?route=catalog/faqs/editquestion&token=<?php echo $token; ?>" method="post" enctype="multipart/form-data" id="pqForm<?php echo $qId; ?>">
							<div class="q-heading <?php echo (empty($q['answer']) ? 'red' : 'green'); ?>"><span class="lt"><?php echo '<b>' . $qId . ': ' . $q['title']; ?></b></span><span class="rt"><?php echo $q['name'] . ' &lt;' . $q['email'] . '  |  ' . date('d-m-Y, H:i:s', $q['create_time']); ?></span></div>
			                <div class="q-content <?php echo (empty($q['answer']) ? 'red' : 'green'); ?>">
								<input type='hidden' name='pq[<?php echo $i; ?>][faq_id]' value='<?php echo $qId; ?>'/>			
								<div id="htabs<?php echo $qId; ?>">
									<div class="pull-right">
										<a onclick="deleteQuestion(<?php echo $qId; ?>)" class="btn btn-sm btn-default"><?php echo $text_delete; ?></a>
										&nbsp; &nbsp; 
										<a onclick="saveQuestion(<?php echo $qId; ?>)" class="btn btn-sm btn-primary"><?php echo $text_save; ?></a>
									</div>
								</div>
								<?php
								$checked = $q['display'] ? "checked = 'checked'" : '';
								?>
								<div id="q<?php echo $qId; ?>">
								
				                <table class="form">
								<tr>					
			                        <td><?php echo $text_display; ?></td>
									<td><input type='checkbox' name='pq[<?php echo $i; ?>][disp]' <?php echo $checked; ?> />
										&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
										<label for="emailalert">Gửi email cho người hỏi</label>
										<input type="checkbox" checked="checked" name="pq[<?php echo $i; ?>][emailalert]" />
									</td>
								</tr>
								<tr>
									<td>Thông tin người hỏi</td>
									<td>
										<?php 
										 	if (!empty($q['name'])) { 
										    	echo '<strong>Tên</strong>: '.$q['name'] .'; ';
										    	}
										    if (!empty($q['email'])) { 
										    	echo '<strong>Email</strong>: '.$q['email'] .'; ';
										    	}
										    if (!empty($q['phone'])) { 
											    echo '<strong>Điện thoại </strong>: '.$q['phone'];
										  	  } 
										  ?>
									</td>
								</tr>
			                    <tr>
			                        <td><?php echo $text_question; ?></td>
									<td><textarea rows="5" cols="80" name='pq[<?php echo $i; ?>][question]'><?php echo $q['question']; ?></textarea></td>
								</tr>
		
			                    <tr>
			                        <td><?php echo $text_answer; ?></td>
									<td><textarea class="ckeditor" name='pq[<?php echo $i; ?>][answer]' id='pq[<?php echo $qId; ?>][<?php echo $i; ?>][answer]'><?php echo  $q['answer']; ?></textarea></td>	                        
								</tr>
			                    </table>
			                	</div>
			               		<?php $i++; ?>	                    
			                </div>
			                </form>
							</div>
							<script type="text/javascript">
								CKEDITOR.replaceClass = 'ckeditor';
							</script>
			        <?php } ?>
				<?php } else {
					echo "$text_no_questions_added</fieldset>";
				} ?>
				<div class="pagination"><?php echo $pagination; ?></div>
			</div>
			
	</div>
	
  </div>
</div>

<script type="text/javascript">
function saveQuestion(qId) {
	//alert($("#pqForm"+qId).serialize());
	$("#pqForm" + qId + ' textarea.ckeditor').each(function () {
		$(this).val(CKEDITOR.instances[$(this).attr('id')].getData());   
	});

    $.ajax({
    	type: "POST",
      	url: 'index.php?route=catalog/faqs/editquestion&token=<?php echo $token; ?>',
      	data: $("#pqForm"+qId).serialize(),
      	dataType: 'json',
      	beforeSend: function() {
	      	$("#pqForm" + qId).find('.q-content').slideToggle('fast');
	      	$("#pqForm" + qId).find('.q-heading').addClass("loading");
      	},
      	complete: function() {
      		$("#pqForm" + qId).find('.q-heading').removeClass("loading").removeClass("red").addClass("green");
      		$("#pqForm" + qId).find('.q-content').removeClass("red").addClass("green");
      		$("#pqForm" + qId).find('.q-heading').effect("highlight", {color: '#BBDF8D'}, 2000);
      	},
      	success: function(json) {
      		console.log(json);
      	},
      	error: function(json) {
      		console.log(json);
      	}
    });
}

function deleteQuestion(qId) {
    $.ajax({
    	type: "GET",
      	url: 'index.php?route=catalog/faqs/deletequestion&faq_id='+ qId +'&token=<?php echo $token; ?>',
      	beforeSend: function() {
	      	$("#pqForm" + qId).find('.q-content').slideToggle('fast');
	      	$("#pqForm" + qId).find('.q-heading').addClass("loading");
      	},
      	complete: function() {
      		$("#pqForm" + qId).parent().fadeOut(500, function() { $("#pqForm" + qId).parent().remove(); });
      		//$("#pqForm" + qId).find('.q-heading').removeClass("loading");
      		//$("#pqForm" + qId).find('.q-heading').effect("highlight", {color: '#BBDF8D'}, 2000);
      	},
      	success: function(json) {

      	}
    });
}

$('.q-heading').bind('click', function() {
	if ($(this).hasClass('active')) {
		$(this).removeClass('active');
	} else {
		$(this).addClass('active');
	}
	
	$(this).parent().find('.q-content').slideToggle('slow');
	
});

$('body').delegate("a.ffRemove", "click", function() {
	$(this).parents('tr').remove();
});

$('body').delegate("a.ffClone", "click", function() {
	var lastRow = $(this).parents('table').find('tbody tr:last input:last').attr('name');
	if (typeof lastRow == "undefined") {
		var newRowNum = 1;
	} else {
		var newRowNum = parseInt(lastRow.match(/[0-9]+/)) + 1;
	}
	var newRow = $(this).parents('table').find('tbody tr.ffSample').clone();
	newRow.find('input,select').attr('name', function(i,name) {
		return name.replace('[0]','[' + newRowNum + ']');
	});
	
	$(this).parents('table').find('tbody').append(newRow.removeAttr('class'));
});
</script> 

</div>
<?php echo $footer; ?>	