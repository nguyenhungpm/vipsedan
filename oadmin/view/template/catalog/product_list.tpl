<?php echo $header; ?>
<div class="content-wrapper">
    <section class="content-header">
        <h1><?php echo $heading_title; ?><small>(<?php echo $text_list; ?>)</small></h1>
        <ol class="breadcrumb">
            <?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
                <li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
            <?php $k++; } ?>
        </ol>
    </section>
    <?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
    <?php if ($success) { ?>
        <div class="success"><?php echo $success; ?></div>
    <?php } ?>
    <div class="content">
        <div id="fake"></div>
        <div class="box box-default">
        <div class="box-header" id="toolbar">
                <div class="pull-right">
                    <a href="<?php echo $insert; ?>" class="btn btn-primary"><i class="fa fa-file-text" aria-hidden="true"></i> <?php echo $button_insert; ?></a>
                </div>
                <div class="pull-left">
                        <a style="display:none" onclick="$('#form').attr('action', '<?php echo $copy; ?>'); $('#form').submit();" class="btn btn-default btn-sm"><i class="fa fa-files-o" aria-hidden="true"></i> <?php echo $button_copy; ?>
                        </a>
                        <a onclick="$('form').submit();" class="btn btn-default btn-sm"><i class="fa fa-trash" aria-hidden="true"></i> <?php echo $button_delete; ?></a>
                </div>
        </div>
        <div class="box-body">
            <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                <table class="table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <td width="1" style="text-align: center;">
                                <input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" />
                            </td>
                            <td class="center hidden-xs"><?php echo $column_image; ?></td>
                            <td class="left"><?php if ($sort == 'pd.name') { ?>
                                    <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                                <?php } else { ?>
                                    <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                                <?php } ?></td>
                            <td class="left">Giá tiền</td>
                            <td class="right hidden-xs">Mã sản phẩm</td>
                            <td class="left"><?php if ($sort == 'p.status') { ?>
                                    <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                                <?php } else { ?>
                                    <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                                <?php } ?></td>
                            <td class="text-center"><?php echo $column_action; ?></td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="filter">
                            <td></td>
                            <td class="hidden-xs"></td>
                            <td>
                                <div class="input-group input-group-sm"><input type="text" name="filter_name" value="<?php echo $filter_name; ?>" class="form-control" /></div>
                            </td>
                            <td>
                                
                            </td>
                            <td align="right" class="hidden-xs"></td>
                            <td>
                                <div class="input-group input-group-sm">
                                <select name="filter_status" class="form-control">
                                    <option value="*"></option>
                                    <?php if ($filter_status) { ?>
                                        <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                    <?php } else { ?>
                                        <option value="1"><?php echo $text_enabled; ?></option>
                                    <?php } ?>
                                    <?php if (!is_null($filter_status) && !$filter_status) { ?>
                                        <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                    <?php } else { ?>
                                        <option value="0"><?php echo $text_disabled; ?></option>
                                    <?php } ?>
                                </select>
                                </div>
                            </td>
                            <td align="text-center"><a onclick="filter();" class="btn btn-block btn-default btn-s"><?php echo $button_filter; ?></a></td>
                        </tr>
                        <?php if ($products) { ?>
                            <?php foreach ($products as $product) { ?>
                                <tr>
                                    <td style="text-align: center;"><?php if ($product['selected']) { ?>
                                            <input type="checkbox" name="selected[]" value="<?php echo $product['product_id']; ?>" checked="checked" />
                                        <?php } else { ?>
                                            <input type="checkbox" name="selected[]" value="<?php echo $product['product_id']; ?>" />
                                        <?php } ?></td>
                                    <td class="center hidden-xs"><img src="<?php echo $product['image']; ?>" alt="<?php echo $product['name']; ?>" style="padding: 1px; border: 1px solid #DDDDDD;" /></td>
                                    <td class="left"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></td>
                                    <td class="left"><?php echo $product['price']; ?></td>
                                    <td class="right hidden-xs">
                                        <?php echo $product['model']; ?>
                                    </td>
                                    <td class="left text-center"><a class="columnstatus" id="status-<?php echo $product['product_id']; ?>"><?php echo $product['status']; ?></a></td>
                                    <td class="text-center"><?php foreach ($product['action'] as $action) { ?>
                                            [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                                        <?php } ?></td>
                                </tr>
                            <?php } ?>
                        <?php } else { ?>
                            <tr>
                                <td class="center" colspan="8"><?php echo $text_no_results; ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </form>
            <div class="pagination"><?php echo $pagination; ?></div>
        </div>
        </div>
    </div>
</div>
<script type="text/javascript"><!--
function filter() {
        url = 'index.php?route=catalog/product&token=<?php echo $token; ?>';

        var filter_name = $('input[name=\'filter_name\']').val();

        if (filter_name) {
            url += '&filter_name=' + encodeURIComponent(filter_name);
        }

        var filter_status = $('select[name=\'filter_status\']').val();

        if (filter_status != '*') {
            url += '&filter_status=' + encodeURIComponent(filter_status);
        }

        location = url;
    }
//--></script>
<script type="text/javascript"><!--
    $('#form input').keydown(function (e) {
        if (e.keyCode == 13) {
            filter();
        }
    });
//--></script>
<script type="text/javascript"><!--
    $('input[name=\'filter_name\']').autocomplete({
        delay: 500,
        source: function (request, response) {
            $.ajax({
                url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request.term),
                dataType: 'json',
                success: function (json) {
                    response($.map(json, function (item) {
                        return {
                            label: item.name,
                            value: item.product_id
                        }
                    }));
                }
            });
        },
        select: function (event, ui) {
            $('input[name=\'filter_name\']').val(ui.item.label);

            return false;
        },
        focus: function (event, ui) {
            return false;
        }
    });

    $('input[name=\'filter_model\']').autocomplete({
        delay: 500,
        source: function (request, response) {
            $.ajax({
                url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_model=' + encodeURIComponent(request.term),
                dataType: 'json',
                success: function (json) {
                    response($.map(json, function (item) {
                        return {
                            label: item.model,
                            value: item.product_id
                        }
                    }));
                }
            });
        },
        select: function (event, ui) {
            $('input[name=\'filter_model\']').val(ui.item.label);

            return false;
        },
        focus: function (event, ui) {
            return false;
        }
    });
//--></script>
<script type="text/javascript"><!--
                $('.columnstatus').click(function() {
                    var object_id=$(this).attr('id');
                    $.ajax({
                        url: 'index.php?route=catalog/product/updatestatus&token=<?php echo $token; ?>',
                        type: 'get',
                        data: {object_id:object_id},
                        dataType: 'html',
                        success: function(html) {
                            if(html!=''){
                                $('#'+object_id).html(html);
                            }
                        }
                    });
                });
                //--></script>
<?php echo $footer; ?>