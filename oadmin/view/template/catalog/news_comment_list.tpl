<?php echo $header; ?>
<div class="content-wrapper">
	<section class="content-header">
		<h1><?php echo $heading_title; ?><small>(Danh sách)</small></h1>
		<ol class="breadcrumb">
			<?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
				<li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
			<?php $k++; } ?>
		</ol>
	</section>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="content">	
    <div class="box">
    <div class="box-header">
		<div class="pull-right">
						<a onclick="location = '<?php echo $insert; ?>'" class="btn btn-primary"><i class="fa fa-file-text" aria-hidden="true"></i> <?php echo $button_insert; ?></a>
				</div>
		<div class="pull-left">
		 <a onclick="$('form').submit();" class="btn btn-default"><span><i class="fa fa-trash" aria-hidden="true"></i> <?php echo $button_delete; ?></span></a>
		</div>
    </div>
    <div class="content">
      <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="list">
          <thead>
            <tr>
              <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
              <td class="left"><?php if ($sort == 'nd.name') { ?>
                <a href="<?php echo $sort_news; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_news; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_news; ?>"><?php echo $column_news; ?></a>
                <?php } ?></td>
              <td class="left"><?php if ($sort == 'c.author') { ?>
                <a href="<?php echo $sort_author; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_author; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_author; ?>"><?php echo $column_author; ?></a>
                <?php } ?></td>
              <td class="email"><?php if ($sort == 'c.email') { ?>
                <a href="<?php echo $sort_email; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_email; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_email; ?>"><?php echo $column_email; ?></a>
                <?php } ?></td>
              <td class="left"><?php if ($sort == 'c.status') { ?>
                <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                <?php } ?></td>
              <td class="left"><?php if ($sort == 'c.date_added') { ?>
                <a href="<?php echo $sort_date_added; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_added; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_date_added; ?>"><?php echo $column_date_added; ?></a>
                <?php } ?></td>
              <td class="right"><?php echo $column_action; ?></td>
            </tr>
          </thead>
          <tbody>
            <?php if ($comments) { ?>
            <?php foreach ($comments as $comment) { ?>
            <tr>
              <td style="text-align: center;"><?php if ($comment['selected']) { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $comment['comment_id']; ?>" checked="checked" />
                <?php } else { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $comment['comment_id']; ?>" />
                <?php } ?></td>
              <td class="left"><?php echo $comment['name']; ?></td>
              <td class="left"><?php echo $comment['author']; ?></td>
              <td class="left"><?php echo $comment['email']; ?></td>
              <td class="left"><?php echo $comment['status']; ?></td>
              <td class="left"><?php echo $comment['date_added']; ?></td>
              <td class="right"><?php foreach ($comment['action'] as $action) { ?>
                [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                <?php } ?></td>
            </tr>
            <?php } ?>
            <?php } else { ?>
            <tr>
              <td class="center" colspan="7"><?php echo $text_no_results; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </form>
      <div class="pagination"><?php echo $pagination; ?></div>
    </div>
  </div>
  </div>
</div>
<?php echo $footer; ?>