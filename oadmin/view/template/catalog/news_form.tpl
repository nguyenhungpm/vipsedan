<?php echo $header; ?>
<script src="//cdn.jsdelivr.net/gh/osdvn/mkmate/ckeditor-full.js"></script>
<link rel="stylesheet" href="//cdn.jsdelivr.net/gh/osdvn/mkmate/finder/css/elfinder.min.css" />
<script src="//cdn.jsdelivr.net/gh/osdvn/mkmate/finder/js/elfinder.min.js"></script>
<script src="asset/initck5.js"></script>
<div class="content-wrapper" id="content" >
  <section class="content-header">
  		<h1><?php echo $heading_title; ?><small>(<?php echo $text_page_details; ?>)</small></h1>
  		<ol class="breadcrumb">
  			<?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
  				<li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
  			<?php $k++; } ?>
  		</ol>
  </section>
<?php if (isset($success)) { ?>
  <div class="success"><?php echo $success; ?></div>
<?php } ?>
<?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
  <div class="content">
	<div id="fake"></div>
	<div class="box">
		<div class="box-header" >
			<div class="pull-right">
			     <a onclick="$('#form').submit();" class="btn btn-sm"><i class="fa fa-floppy-o" aria-hidden="true"></i> <?php echo $button_saveclose; ?></a>
           <a id="add-new" class="btn btn-sm"><i class="fa fa-file-text" aria-hidden="true"></i> <?php echo $button_saveadd; ?></a>
           <button id="continue" class="btn btn-primary"><i class="fa fa-check" aria-hidden="true"></i> <?php echo $button_save; ?></button>
			</div>
      <div class="pull-left">
          <a href="<?php echo $cancel; ?>" class="btn btn-default btn-sm"><i class="fa fa-reply"></i> <?php echo $button_cancel; ?></a>
          <button type="button" onclick="<?php if(isset($news_id) && !empty($news_id)){ ?>if(navigator.userAgent.toLowerCase().indexOf('opera') != -1 &amp;&amp; window.event.preventDefault) window.event.preventDefault();this.newWindow = window.open('<?php echo HTTP_CATALOG; ?>index.php?route=news/news&news_id=<?php echo $_GET['news_id']; ?>','_blank','toolbar=0, scrollbars=0, location=0, status=1, menubar=0, left=100, top=50, width=1300, height=700, resizable=1');this.newWindow.focus();this.newWindow.opener=window;return false;<?php } ?>" class="btn btn-default btn-sm" target="_blank" <?php echo isset($news_id) && !empty($news_id) ? '':'style="background:red;"';?>><i class="fa fa-eye" aria-hidden="true"></i> <?php echo $button_preview; ?></button>
		  <?php if(isset($news_id) && !empty($news_id)){ ?>
		  <a class="btn btn-default" onclick="$('#form_delete').submit();" ><i class="fa fa-trash" aria-hidden="true"></i> Xóa</a>
		  <?php } ?>
      </div>
	  <?php if(isset($news_id) && !empty($news_id)){ ?>
					<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form_delete">
						<input type="hidden" name="selected[]" value="<?php echo $news_id; ?>"/>
					</form>
				<?php } ?>
		</div>
  </div>
	<?php if(isset($news_id) && !empty($news_id)){ ?>
		<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form_delete">
			<input type="hidden" name="selected[]" value="<?php echo $news_id; ?>"/>
		</form>
		<?php } ?>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
      <div class="row">
			<div class="col-md-9">
        <div class="box box-danger">
				<div class="box-body">
							<ul class="nav nav-tabs <?php echo count($languages)==1 ? 'hide':''; ?>">
								<?php foreach ($languages as $key=> $language) { ?>
									<li class="<?php echo $language['language_id'] == $this->config->get('config_language_id') ? 'active':'';?>"><a data-toggle="tab" aria-expanded="true" href="#language<?php echo $language['language_id']; ?>"><img <?php echo count($languages) ==1 ? 'class="hide"':''; ?> src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
								<?php } ?>
							</ul>
							<div class="tab-content">
								<?php foreach ($languages as $key=> $language) { ?>
									<div class="tab-pane <?php echo $language['language_id'] == $this->config->get('config_language_id') ? 'active':'';?>" id="language<?php echo $language['language_id']; ?>">
										<div class="form-group required">
											<label class="control-label"><?php echo $entry_name; ?></label>
											<input type="text" class="form-control" name="news_description[<?php echo $language['language_id']; ?>][name]" size="100" value="<?php echo isset($news_description[$language['language_id']]) ? $news_description[$language['language_id']]['name'] : ''; ?>">
											<?php if (isset($error_name[$language['language_id']])) { ?>
												<div class="text-danger"><?php echo $error_name[$language['language_id']]; ?></div>
											<?php } ?>
										</div>
										<div class="form-group">
											<label class="control-label"><?php echo $entry_short_description; ?></label>
											<textarea class="form-control" name="news_description[<?php echo $language['language_id']; ?>][short_description]" cols="120" rows="5"><?php echo isset($news_description[$language['language_id']]) ? $news_description[$language['language_id']]['short_description'] : ''; ?></textarea>
										</div>
										<div class="form-group">
											<label class="control-label"><?php echo $entry_description; ?></label>
											<textarea class="ckeditor5" name="news_description[<?php echo $language['language_id']; ?>][description]" id="description<?php echo $language['language_id']; ?>"><?php echo isset($news_description[$language['language_id']]) ? $news_description[$language['language_id']]['description'] : ''; ?></textarea>
										</div>
										<div class="form-group">
											<label class="control-label"><?php echo $entry_tag; ?></label>
											<input  class="form-control" type="text" name="news_tag[<?php echo $language['language_id']; ?>]" value="<?php echo isset($news_tag[$language['language_id']]) ? $news_tag[$language['language_id']] : ''; ?>" size="80" />
										</div>
									</div>
								<?php } ?>
							</div>

						</div>
      </div>
			<div class="box box-success">
						<div class="box-header">
							<h3 class="box-title">SEO</h3>
						</div>
						<div class="box-body">
							<ul class="nav nav-tabs <?php echo count($languages)==1 ? 'hide':''; ?>">
								<?php foreach ($languages as $key=> $language) { ?>
									<li class="<?php echo $language['language_id'] == $this->config->get('config_language_id') ? 'active':'';?>"><a data-toggle="tab" aria-expanded="true" href="#seo<?php echo $language['language_id']; ?>"><img <?php echo count($languages) ==1 ? 'class="hide"':''; ?> src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
								<?php } ?>
							</ul>
							<div class="tab-content">
								<?php foreach ($languages as $key=> $language) { ?>
									<div class="tab-pane <?php echo $language['language_id'] == $this->config->get('config_language_id') ? 'active':'';?>" id="seo<?php echo $language['language_id']; ?>">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label">Meta title</label>
													<input placeholder="Meta title" type="text" class="form-control" name="news_description[<?php echo $language['language_id']; ?>][meta_title]" value="<?php echo isset($news_description[$language['language_id']]) ? $news_description[$language['language_id']]['meta_title'] : ''; ?>" size="100" onKeyDown="textCounter(this, 70);" onKeyUp="textCounter(this, 'titleseo', 70)"/>
													<span class="small"><?php echo $text_remain; ?> <input style="font-weight:bold;border:none" disabled size="2" id="titleseo" value="70" /> <?php echo $text_characters; ?></span>
												</div>
												<div class="form-group">
													<label class="control-label">Meta keyword</label>
													<input placeholder="Meta keyword" class="form-control" type="text" name="news_description[<?php echo $language['language_id']; ?>][meta_keyword]" size="100" />
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label">Meta Description</label>
													<textarea placeholder="Meta Description" class="form-control" onKeyDown="textCounter(this, 160);" onKeyUp="textCounter(this, 'descseo', 160)" size="100" name="news_description[<?php echo $language['language_id']; ?>][meta_description]" cols="45" rows="5"><?php echo isset($news_description[$language['language_id']]) ? $news_description[$language['language_id']]['meta_description'] : ''; ?></textarea>
													<span class="small"><?php echo $text_remain; ?> <input style="font-weight:bold;border:none" disabled size="3" id="descseo" value="160" /> <?php echo $text_characters; ?></span>
												</div>
											</div>
										</div>
									</div>
								<?php } ?>
							</div>

							<div class="row">
								<div class="col-md-9">
									<div class="form-group">
										<input class="form-control" placeholder="SEO URL" size="60" type="text" name="keyword" value="<?php echo $keyword; ?>" id="seo_keyword" />
											<div id="seo_keyword_msg" style="margin-top: 12px;"></div>
											<?php if ($error_seo) { ?>
												<div class="text-danger"><?php echo $error_seo; ?></div>
											<?php } ?>
									</div>
								</div>
								<div class="col-md-3">
									<input type="text" class="form-control" name="robots" value="<?php echo $robots; ?>" placeholder="Robot tag" />
								</div>
                <div class="col-md-12">
                    <label class="control-label">Mã nhúng</label>
                    <textarea class="form-control" id="embed" name="embed" rows="5"><?php echo $embed; ?></textarea>
                </div>
							</div>
						</div>
					</div>

        </div>
				<div class="col-md-3">
					<div class="box box-primary">
						<div class="box-body">
							<div class="form-group text-center">
								<div class="image">
									<a class="pull-right" onclick="$('#thumb').attr('src', '<?php echo $no_image; ?>'); $('#image').attr('value', '');"><?php echo $text_clear; ?></a>
									<a onclick="filebrowser('image', 'thumb');">
										<img src="<?php echo $thumb; ?>" alt="" id="thumb" />
									</a>
								  	<input type="hidden" name="image" value="<?php echo $image; ?>" id="image" />
								  </div>
							</div>
              <div class="form-group text-center">
								<div class="image">
									<a class="pull-right" onclick="$('#thumb_ogimage').attr('src', '<?php echo $no_image; ?>'); $('#ogimage').attr('value', '');"><?php echo $text_clear; ?></a>
									<a onclick="filebrowser('ogimage', 'thumb_ogimage');">
										<img src="<?php echo $thumb_ogimage; ?>" alt="" id="thumb_ogimage" />
									</a>
								  	<input type="hidden" name="ogimage" value="<?php echo $ogimage; ?>" id="ogimage" />
								  </div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="box box-primary">
						<div class="box-body">
							<div class="s2-example">
							<select class="js-example-basic-multiple js-states form-control select2-hidden-accessible" multiple="" tabindex="-1" aria-hidden="true">
								<?php foreach ($categories as $category) { ?>
								<option value="<?php echo $category['news_category_id']; ?>"><?php echo $category['name']; ?></option>
								<?php } ?>
							</select>
							</div>
							<input type="hidden" name="news_category" value="<?php echo $news_category; ?>"/>
							<br />
							<div class="form-group clearfix">
								<label class="control-label pull-left"><?php echo $entry_status; ?></label>
								<label class="switch pull-right">
								  <input type="checkbox" name="status" <?php echo $status ? 'checked':''; ?> value="1">
								  <span class="slider round"></span>
								</label>
							</div>
							<div class="form-group">
								<label><?php echo $entry_date_available; ?></label>
								<div class="input-group date">
									<div class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</div>
									<input type="text" name="date_available" value="<?php echo $date_available; ?>" class="form-control pull-right date" id="datepicker">
								</div>
								<!-- /.input group -->
							</div>
							<div class="form-group">
								<label class="control-label"><?php echo $entry_sort_order; ?></label>
								<input class="form-control" type="text" name="sort_order" value="<?php echo $sort_order; ?>" size="2" />
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="box box-primary">
						<div class="box-header">
							<h3 class="box-title"><?php echo $entry_related_news; ?></h3>
						</div>
						<div class="box-body">
							<div class="form-group">
								<input class="form-control" type="text" name="related" value="" />
								<?php if (!isset($news_related)) { ?>
									<?php $news_related = array(); ?>
								<?php } ?>
								<div class="scrollbox" id="news-related">
									  <?php $class = 'odd'; ?>
									  <?php foreach ($news_related as $news_related) { ?>
									  <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
									  <div id="news-related<?php echo $news_related['news_id']; ?>" class="<?php echo $class; ?>"> <?php echo $news_related['name']; ?><img src="view/image/delete.png" />
										<input type="hidden" name="news_related[]" value="<?php echo $news_related['news_id']; ?>" />
									  </div>
									  <?php } ?>
								</div>
							</div>
						</div>
					</div>
				</div>
		</div>
      </form>
  </div>
</div>

<script type="text/javascript"><!--
$('input[name=\'related\']').autocomplete({
	delay: 0,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/news/autocomplete&token=<?php echo $token; ?>',
			type: 'POST',
			dataType: 'json',
			data: 'filter_name=' +  encodeURIComponent(request.term),
			success: function(data) {
				response($.map(data, function(item) {
					return {
						label: item.name,
						value: item.news_id
					}
				}));
			}
		});

	},
	select: function(event, ui) {
		$('#news-related' + ui.item.value).remove();
		$('#news-related').append('<div id="news-related' + ui.item.value + '">' + ui.item.label + '<img src="view/image/delete.png" /><input type="hidden" name="news_related[]" value="' + ui.item.value + '" /></div>');
		$('#news-related div:odd').attr('class', 'odd');
		$('#news-related div:even').attr('class', 'even');
		return false;
	}
});
// $('#news-related div img').live('click', function() {
$(document).on('click', '#news-related div img', function() {
	$(this).parent().remove();
	$('#news-related div:odd').attr('class', 'odd');
	$('#news-related div:even').attr('class', 'even');
});
//--></script>
<script type="text/javascript"><!--
$.widget('custom.catcomplete', $.ui.autocomplete, {
	_renderMenu: function(ul, items) {
		var self = this, currentCategory = '';
		$.each(items, function(index, item) {
			if (item.category != currentCategory) {
				ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');

				currentCategory = item.category;
			}
			self._renderItem(ul, item);
		});
	}
});
//--></script>
<script type="text/javascript"><!--

$('#form').submit(function(){
	if($('input[name="news_category"]').val() ==""){
		if (!confirm('Bạn chưa chọn danh mục tin tức! Bạn có chắc chắn bỏ qua?')) {
			return false;
		}
	}
});
//--></script>
<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript"><!--
	$('.date').datepicker({format: 'yyyy-mm-dd'});
    $('.datetime').datetimepicker({
        dateFormat: 'yy-mm-dd',
        timeFormat: 'h:m'
	});
    $('.time').timepicker({timeFormat: 'h:m'});
//--></script>
<script type="text/javascript"><!--
	function trim_name(input_name){
		var re=new RegExp("([0-9])+");
		var temp= re.exec(input_name);
		var lang_id='';
		for(i=0; i<temp.length-1; i++){
			lang_id = lang_id + temp[i];
		}
		var txtname_trim=erase($("#news_description_"+lang_id+"_name").val());
		$("#seo_keyword").val(seo_write(txtname_trim)+'.html');

	}
//--></script>
<script type="text/javascript">
	$( "#seo_keyword" ).keyup(function() {
		$.ajax({
		    dataType: 'json',
		    type: 'POST',
    		url: 'index.php?route=catalog/news/checkKeyword&token=<?php echo $this->request->get['token']; ?>',
    		data: 'news_id=<?php echo isset($this->request->get['news_id']) ? $this->request->get['news_id']: ''; ?>&keyword=' + encodeURIComponent($('input[name=\'keyword\']').val()),
		    success: function(result){
		      if (result.error) {
		        $('#seo_keyword_msg').html('<div class="warning">' + result.error + '</div><input size="60" type="hidden" name="seo" value="0" id="seo_url" />');
		      }
		      if (result.success) {
		        $('#seo_keyword_msg').html('<div class="success">' + result.success + '<input size="60" type="hidden" name="seo" value="1" id="seo_url" /></div>');
		      }
		    }
    });
});
</script>
<script type="text/javascript"><!--
$('#continue').click(function(e) {
	e.preventDefault();
	$('#form').attr('action', $('#form').attr('action') + '&continue=true');
	$('#form').submit();
});
$('#add-new').click(function(e) {
	e.preventDefault();
	$('#form').attr('action', $('#form').attr('action') + '&new=true');
	$('#form').submit();
});
//--></script>
<script type="text/javascript">
  $.fn.select2.amd.require([
    "select2/core",
    "select2/utils",
    "select2/compat/matcher"
  ], function (Select2, Utils, oldMatcher) {
    var $basicMultiple = $(".js-example-basic-multiple");
    $.fn.select2.defaults.set("width", "100%");
    $basicMultiple.select2({
		placeholder: '<?php echo $entry_category; ?>',
	});
	$basicMultiple.val([<?php echo $news_category; ?>]).trigger("change");
  });
var $eventSelect = $(".js-example-basic-multiple");
$eventSelect.on("select2:select", function (e) { log("select2:select", e); });
$eventSelect.on("select2:unselect", function (e) { log("select2:unselect", e); });
function log (name, evt) {
	var id='';
	if (!evt) {
		var args = "{}";
	} else {
		var args = JSON.stringify(evt.params, function (key, value) {
		if (value && value.nodeName) return "[DOM node]";
		if (value instanceof $.Event) return "[$.Event]";
		return value;
		});
	}
	var obj = JSON.parse(args);
	for (var key in obj["data"]) {
		if (Object.prototype.hasOwnProperty.call(obj["data"], key)) {
			id = obj["data"]["id"];
		}
	}
	data =  $('input[name=\'news_category\']').attr('value');
	if(data ==''){
		data = id;
	}else{
		if(obj["_type"] == 'select'){
			data += ","+id;
		}else{
			var arr = data.split(',');
			var index = arr.indexOf(id);
			arr.splice(index, 1);
			data = arr.join();
		}
	}
	$('input[name=\'news_category\']').val(data);
}
</script>
<style type="text/css">
	.markup{
		background: #ddd;
		padding: 3px 5px;
		margin-right: 5px;
		border-radius: 5px;
		position: relative;
	}
	.markup:hover .fa{
		display: block;
	}
	.markup .fa{
		position: absolute;
		display: none;
		right: -5px;
		top: -5px;
		cursor: pointer;
	}
</style>
<?php echo $footer; ?>
