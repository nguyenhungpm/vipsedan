<?php echo $header; ?>
<link rel="stylesheet" href="//cdn.jsdelivr.net/gh/osdvn/mkmate/finder/css/elfinder.min.css" />
<script src="//cdn.jsdelivr.net/gh/osdvn/mkmate/finder/js/elfinder.min.js"></script>
<script src="asset/initck5.js"></script>
<div id="content" class="content-wrapper">
  <section class="content-header">
		<h1><?php echo $heading_title; ?><small>(Danh sách)</small></h1>
		<ol class="breadcrumb">
			<?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
				<li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
			<?php $k++; } ?>
		</ol>
	</section>
    <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>

<div class="content">
	<div class="box box-primary">
    <div class="box-header">
      <div class="pull-left">
        <a onclick="$('#form').submit();" class="btn btn-default"><i class="fa fa-times"></i> <?php echo $button_delete; ?></a>
      </div>
      <div class="pull-right">
          <a href="#myModal" data-toggle="modal" title="Thêm nội dung" class="btn btn-primary"><i class="fa fa-plus"></i> Thêm nội dung</a>
      </div>
    </div>
  <div class="box-body">
    <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
      <table class="list"> 
        <thead>
          <tr>
            <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
            <td class="left">Hình ảnh</td>
            <td class="left">Loại xe</td>
            <td class="center">Sức chứa (người x hành lí)</td>
            <td class="center">Giá tiện chuyến</td>
            <td class="center">Giá đi tỉnh</td>
            <td class="center">Thứ tự</td>
            <td class="center">Bài viết</td>
            <td class="left">Trạng thái</td>
            <td class="center">Thao tác</td>
          </tr>
        </thead>
        <tbody>
          <?php if ($type_cars) { ?>
          <?php foreach ($type_cars as $type_car) { ?>
  				<?php if(isset($this->request->get['type_car_id']) && $type_car['type_car_id'] == $this->request->get['type_car_id']){ ?>
          <tr>
            <td style="text-align: center;"><input type="checkbox" name="selected[]" value="<?php echo $type_car['type_car_id']; ?>"/></td> 
            <td class="left">
				<div class="form-group text-center">
					<div class="image">
						<a class="pull-right" onclick="$('#thumb').attr('src', '<?php echo $no_image; ?>'); $('#image').attr('value', '');">Xóa ảnh</a>
						<a onclick="filebrowser('image', 'thumb');">
							<img src="<?php echo $type_car['image'];?>" alt="" id="thumb" />
						</a>
						<input type="hidden" name="image" value="<?php echo $type_car['origin_image'];?>" id="image" />
					</div>
				</div>
			</td> 
            <td class="left">
			<?php foreach ($languages as $key=> $language) { ?>
				<?php if(count($languages)>1){ ?><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php } ?>
				<input class="form-control" placeholder="Loại xe" type="text" name="detail[<?php echo $language['language_id'];?>][name]" value="<?php echo !empty($type_car['detail'][$language['language_id']]['name']) ? $type_car['detail'][$language['language_id']]['name']:''; ?>"/>
			<?php } ?>
				</td>
            <td class="text-center">
				<input name="seat" value="<?php echo $type_car['seat'] ? $type_car['seat'] : '';?>"/> x
				<input name="luggage" value="<?php echo $type_car['luggage'] ? $type_car['luggage'] : '';?>"/>
			</td>
            <td class="text-center">
				<input class="form-control" placeholder="Giá chính thức" type="text" name="cost_tienchuyen" value="<?php echo $type_car['cost_tienchuyen'] ? $type_car['cost_tienchuyen'] : ''; ?>"/>
				<input class="form-control" placeholder="Giá ngoài giờ" type="text" name="cost_tienchuyen_ov" value="<?php echo $type_car['cost_tienchuyen_ov'] ? $type_car['cost_tienchuyen_ov'] : ''; ?>"/>
			</td>
            <td class="text-center">
				<input class="form-control" placeholder="Giá chính thức" type="text" name="cost_tinh" value="<?php echo $type_car['cost_tinh'] ? $type_car['cost_tinh'] : ''; ?>"/>
				<input class="form-control" placeholder="Giá ngoài giờ" type="text" name="cost_tinh_ov" value="<?php echo $type_car['cost_tinh_ov'] ? $type_car['cost_tinh_ov'] : ''; ?>"/>
			</td>
      <td class="text-center">
				<input class="form-control" placeholder="Thứ tự" type="text" name="sort_order" value="<?php echo $type_car['sort_order']; ?>"/>
			</td>
      <td class="text-left">
				<input class="form-control" placeholder="Bài viết" type="text" name="link" value="<?php echo $type_car['link']; ?>"/>
			</td>
            <td class="text-center">
              <select name="status" class="form-control">
                <option value="1" <?php echo $type_car['status']==1 ? 'selected':''; ?>>Bật</option>
                <option value="0" <?php echo $type_car['status']==0 ? 'selected':''; ?>>Tắt</option>
              </select>
            </td>
            <td class="center"><a onclick="save('<?php echo $type_car['type_car_id']; ?>')" class="button">Lưu</a></td>
			</tr>
          <?php }else{ ?>
          <tr>
            <td style="text-align: center;"><input type="checkbox" name="selected[]" value="<?php echo $type_car['type_car_id']; ?>"/></td>
			<td class="center"><img src="<?php echo $type_car['image']; ?>" alt="<?php echo $type_car['name']; ?>" style="padding: 1px; border: 1px solid #DDDDDD;" /></td>
            <td class="left"><strong><?php echo !empty($type_car['detail'][$this->config->get('config_language_id')]['name']) ? $type_car['detail'][$this->config->get('config_language_id')]['name']:''; ?></strong></td>
            <td class="text-center">
				<?php echo $type_car['seat']; ?> người x <?php echo $type_car['luggage']; ?> hành lí
			</td>
            <td class="text-left">
				Giá chính thức: <?php echo $type_car['cost_tienchuyen']; ?><br />
				Giá ngoài giờ: <?php echo $type_car['cost_tienchuyen_ov']; ?>
			</td>
            <td class="text-left">
				Giá chính thức: <?php echo $type_car['cost_tinh']; ?><br />
				Giá ngoài giờ: <?php echo $type_car['cost_tinh_ov']; ?>
			</td>
            <td class="text-center"><?php echo $type_car['sort_order']; ?></td>
            <td class="text-left"><?php echo $type_car['link']; ?></td>
            <td class="text-center"><?php echo $type_car['status'] == 1 ? 'Bật':'Tắt'; ?></td>
            <td class="center"><a href="<?php echo $type_car['edit']; ?>">Sửa</a></td>
          </tr>
              <?php } ?>
          <?php } ?>
          <?php } else { ?>
          <tr>
            <td class="center" colspan="7"><?php echo $text_no_results; ?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </form>
  <div class="pagination"><?php echo $pagination; ?></div>
  </div>
  </div>
</div>
<div id="myModal" class="modal fade">
  <div class="modal-dialog form-sale">
        <div class="modal-content">
            <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Thêm nội dung</h4>
      </div>
      <div class="modal-body">
                <div class="tab-content">
             <form method="POST" action="" name="form-type_car" id="form-type_car">
		  
		  <?php foreach ($languages as $key=> $language) { ?>
		  <div class="form-group required">
            <label class="control-label text-left">
				<?php if(count($languages)>1){ ?><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><?php } ?> Loại xe</label>
            <input class="form-control" type="text" name="detail[<?php echo $language['language_id'];?>][name]"/>
          </div>
			<?php } ?>
			<div class="row">
				<div class="col-md-12">
					<label class="control-label text-left">Sức chứa (người x hành lí)</label>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<input placeholder="Số chỗ ngồi" type="text" class="form-control" name="seat" />
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<input placeholder="Số hành lí" type="text" class="form-control" name="luggage" />
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<label class="control-label text-left">Giá tiện chuyến</label>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<input placeholder="Giá chính thức" type="text" class="form-control" name="cost_tienchuyen" />
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<input placeholder="Giá ngoài giờ" type="text" class="form-control" name="cost_tienchuyen_ov" />
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<label class="control-label text-left">Giá đi tỉnh</label>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<input placeholder="Giá chính thức" type="text" class="form-control" name="cost_tinh" />
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<input placeholder="Giá ngoài giờ" type="text" class="form-control" name="cost_tinh_ov" />
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
				  <div class="form-group">
					<label class="control-label text-left">Thứ tự</label>
					<input class="form-control" type="text" name="sort_order"/>
				  </div>
				  <div class="form-group">
					<label class="control-label text-left">Trạng thái</label>
					<select name="status" class="form-control">
					  <option value="1">Bật</option>
					  <option value="0">Tắt</option>
					</select>
				  </div>
				</div>
				<div class="col-md-6">
					<div class="form-group text-center">
						<div class="image">
							<a class="pull-right" onclick="$('#thumb').attr('src', '<?php echo $no_image; ?>'); $('#image').attr('value', '');"><?php echo $text_clear; ?></a>
							<a onclick="filebrowser('image', 'thumb');">
								<img src="<?php echo $no_image; ?>" alt="" id="thumb" />
							</a>
							<input type="hidden" name="image" value="" id="image" />
						</div>
					</div>
				</div>
			</div>
          </form>
        </div>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
          <button type="button" id="submit" onclick="submit_form()" data-dismiss="modal" class="btn btn-primary">Lưu</button>
      </div>
    </div>
  </div>
</div>
<script>
$("#myModal").draggable({
  handle: ".modal-header"
});
function submit_form(){
  $.ajax({
    url: 'index.php?route=catalog/type_car/insert&token=<?php echo $token; ?>',
    type: 'post',
    dataType: 'json',
    data : $("#form-type_car").serialize(),
    complete: function() {
      $('#submit').button('reset');
    },
    success: function(json) {
      var href = 'index.php?route=catalog/type_car&token=<?php echo $token; ?>';
      <?php if(isset($page) && !empty($page)){ ?>
      href += '&page=<?php echo $page; ?>';
      <?php } ?>
      window.location = href;
    }
  });
}
function save(id){
	var url = 'index.php?route=catalog/type_car/edit&token=<?php echo $token; ?>&type_car_id=' +  id;
	<?php if(isset($page) && !empty($page)){ ?>
	url += '&page=<?php echo $page; ?>';
	<?php } ?>
	$.ajax({
		url: url,
		type: 'POST',
    data : $("#form").serialize(),
		dataType: 'json',
		success: function(json) {
			var href = 'index.php?route=catalog/type_car&token=<?php echo $token; ?>';
			<?php if(isset($page) && !empty($page)){ ?>
			href += '&page=<?php echo $page; ?>';
			<?php } ?>
			window.location = href;
		}
	});
}
</script>

<?php echo $footer; ?>
