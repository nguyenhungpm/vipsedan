<?php echo $header; ?>
<link href="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.10.2/skins/ui/oxide/skin.min.css">
<script src="//cdn.jsdelivr.net/gh/osdvn/mkmate/ckeditor-full.js"></script>
<link rel="stylesheet" href="//cdn.jsdelivr.net/gh/osdvn/mkmate/finder/css/elfinder.min.css" />
<script src="//cdn.jsdelivr.net/gh/osdvn/mkmate/finder/js/elfinder.min.js"></script>
<script src="asset/initck5.js"></script>
<style type="text/css">
	.s2-example{pading:2px}
</style>
<div class="content-wrapper" id="content">
	<section class="content-header">
		<h1><?php echo $heading_title; ?><small>(<?php echo $text_page_details; ?>)</small></h1>
		<ol class="breadcrumb">
			<?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
				<li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
			<?php $k++; } ?>
		</ol>
	</section>
	<?php if (isset($success)) { ?>
		<div class="success"><?php echo $success; ?></div>
	<?php } ?>
	<?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
	<?php } ?>
	<div class="content">
		<div id="fake"></div>
		<div class="box box-default">
			<div class="box-header">
				<div class="pull-right">
					<?php if(isset($product_id) && !empty($product_id)){ ?>
					<a onclick="<?php if(isset($product_id) && !empty($product_id)){ ?>if(navigator.userAgent.toLowerCase().indexOf('opera') != -1 &amp;&amp; window.event.preventDefault) window.event.preventDefault();this.newWindow = window.open('<?php echo HTTP_CATALOG; ?>index.php?route=product/product&product_id=<?php echo $_GET['product_id']; ?>','_blank','toolbar=0, scrollbars=0, location=0, status=1, menubar=0, left=100, top=50, width=1300, height=700, resizable=1');this.newWindow.focus();this.newWindow.opener=window;return false;<?php } ?>" class="btn btn-sm" target="_blank"><i class="fa fa-play" aria-hidden="true"></i> Xem trước</a>
					<?php } ?>
					<a id="saveonly" class="btn btn-sm"><i class="fa fa-floppy-o" aria-hidden="true"></i> <?php echo $button_saveclose; ?></a>
					<a id="add-new" class="btn btn-sm"><i class="fa fa-file-text" aria-hidden="true"></i> <?php echo $button_saveadd; ?></a>
					<button type="button" id="continue" class="btn btn-primary"><i class="fa fa-check" aria-hidden="true"></i> <?php echo $button_save; ?></button>
				</div>
				<div class="pull-left">
					<a href="<?php echo $cancel; ?>" class="btn btn-default btn-sm"><i class="fa fa-reply"></i> <?php echo $button_close; ?></a>
					<?php if(isset($product_id) && !empty($product_id)){ ?>
						<a class="btn btn-default" onclick="$('#form_delete').submit();" ><i class="fa fa-trash" aria-hidden="true"></i> <?php echo $button_remove; ?></a>
					<?php } ?>
				</div>
				<?php if(isset($product_id) && !empty($product_id)){ ?>
					<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form_delete">
						<input type="hidden" name="selected[]" value="<?php echo $product_id; ?>"/>
					</form>
				<?php } ?>
			</div>
		</div>
		<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
			<div class="row">
				<div class="col-md-9">
					<div class="box box-danger">
					<!-- 	<div class="box-header hide">
							<h3 class="box-title"><?php echo $text_overview; ?></h3>
						</div> -->
						<div class="box-body">
							<ul class="nav nav-tabs <?php echo count($languages)==1 ? 'hide':''; ?>">
								<?php foreach ($languages as $key=> $language) { ?>
									<li class="<?php echo $language['language_id'] == $this->config->get('config_language_id') ? 'active':'';?>"><a data-toggle="tab" aria-expanded="true" href="#language<?php echo $language['language_id']; ?>"><img <?php echo count($languages) ==1 ? 'class="hide"':''; ?> src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
								<?php } ?>
							</ul>
							<div class="tab-content">
								<?php foreach ($languages as $key=> $language) { ?>
									<div class="tab-pane <?php echo $language['language_id'] == $this->config->get('config_language_id') ? 'active':'';?>" id="language<?php echo $language['language_id']; ?>">
										<div class="form-group required">
											<label class="control-label"><?php echo $entry_name; ?></label>
											<input type="text" class="form-control" name="product_description[<?php echo $language['language_id']; ?>][name]" size="100" value="<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['name'] : ''; ?>">
											<?php if (isset($error_name[$language['language_id']])) { ?>
												<div class="text-danger"><?php echo $error_name[$language['language_id']]; ?></div>
											<?php } ?>
										</div>
										<div class="form-group">
											<label class="control-label">Tiêu đề ngắn</label>
											<input type="text" class="form-control" name="product_description[<?php echo $language['language_id']; ?>][title]" size="100" value="<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['title'] : ''; ?>">
											<?php if (isset($error_name[$language['language_id']])) { ?>
												<div class="text-danger"><?php echo $error_name[$language['language_id']]; ?></div>
											<?php } ?>
										</div>
										<div class="form-group">
											<label class="control-label"><?php echo $entry_short_description; ?></label>
											<textarea class="form-control" name="product_description[<?php echo $language['language_id']; ?>][short_description]" cols="120" rows="5"><?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['short_description'] : ''; ?></textarea>
										</div>
										<div class="form-group">
											<label class="control-label"><?php echo $entry_description; ?></label>
											<textarea class="ckeditor5" name="product_description[<?php echo $language['language_id']; ?>][description]" id="description<?php echo $language['language_id']; ?>"><?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['description'] : ''; ?></textarea>
										</div>
										<div class="form-group">
											<label class="control-label"><?php echo $entry_tag; ?></label>
											<input class="form-control" type="text" name="product_description[<?php echo $language['language_id']; ?>][tag]" value="<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['tag'] : ''; ?>" size="80" />
										</div>
									</div>
								<?php } ?>
							</div>

						</div>
					</div>
					<div class="box box-default">
						<div class="box-header">
							<h3 class="box-title"><?php echo $text_gallery; ?></h3>
							<a onclick="addImage();" class="btn btn-primary pull-right"><?php echo $button_add_image; ?></a>
						</div>
						<div class="box-body">
							<div class="row" id="list-images">
							<?php $image_row = 0;foreach ($product_images as $product_image) { ?>
							<div class="col-md-2 text-center" id="image-row<?php echo $image_row; ?>">
								<div class="image">
									<img style="width: 100%" src="<?php echo $product_image['thumb']; ?>" alt="" id="thumb<?php echo $image_row; ?>" />
									<input type="hidden" name="product_image[<?php echo $image_row; ?>][image]" value="<?php echo $product_image['image']; ?>" id="image<?php echo $image_row; ?>" />
										<br />
									<a onclick="filebrowser('image<?php echo $image_row; ?>', 'thumb<?php echo $image_row; ?>');"><?php echo $text_browse; ?></a><br />
									<input class="text-center" type="text" name="product_image[<?php echo $image_row; ?>][sort_order]" value="<?php echo $product_image['sort_order']; ?>" placeholder="0" size="8" />
									<a onclick="$('#image-row<?php echo $image_row; ?>').remove();" class="remove"><i class="fa fa-close"></i></a>
								</div>
							</div>
							<?php $image_row++;} ?>
							</div>
						</div>
					</div>
					<div class="box box-danger">
						<div class="box-header">
							<h3 class="box-title">SEO</h3>
						</div>
						<div class="box-body">
							<ul class="nav nav-tabs <?php echo count($languages)==1 ? 'hide':''; ?>">
								<?php foreach ($languages as $key=> $language) { ?>
									<li class="<?php echo $language['language_id'] == $this->config->get('config_language_id') ? 'active':'';?>"><a data-toggle="tab" aria-expanded="true" href="#seo<?php echo $language['language_id']; ?>"><img <?php echo count($languages) ==1 ? 'class="hide"':''; ?> src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
								<?php } ?>
							</ul>
							<div class="tab-content">
								<?php foreach ($languages as $key=> $language) { ?>
									<div class="tab-pane <?php echo $language['language_id'] == $this->config->get('config_language_id') ? 'active':'';?>" id="seo<?php echo $language['language_id']; ?>">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<input placeholder="Meta title" type="text" class="form-control" name="product_description[<?php echo $language['language_id']; ?>][meta_title]" value="<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['meta_title'] : ''; ?>" size="100" onKeyDown="textCounter(this, 70);" onKeyUp="textCounter(this, 'titleseo', 70)"/>
													<span class="small"><?php echo $text_remain; ?> <input style="font-weight:bold;border:none" disabled size="2" id="titleseo" value="70" /> <?php echo $text_characters; ?></span>
												</div>
												<div class="form-group">
													<input placeholder="Meta keyword" class="form-control" type="text" name="product_description[<?php echo $language['language_id']; ?>][meta_keyword]" value="<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['meta_keyword'] : ''; ?>" size="100" />
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<textarea placeholder="Meta Description" class="form-control" onKeyDown="textCounter(this, 160);" onKeyUp="textCounter(this, 'descseo', 160)" size="100" name="product_description[<?php echo $language['language_id']; ?>][meta_description]" cols="45" rows="5"><?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['meta_description'] : ''; ?></textarea>
													<span class="small"><?php echo $text_remain; ?> <input style="font-weight:bold;border:none" disabled size="3" id="descseo" value="160" /> <?php echo $text_characters; ?></span>
												</div>
											</div>
										</div>
									</div>
								<?php } ?>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-9">
									<input class="form-control" size="60" type="text" name="keyword" value="<?php echo $keyword; ?>" id="seo_keyword" placeholder="SEO url" />
									<div id="seo_keyword_msg" style="margin-top: 12px;"></div>
									<?php if ($error_seo) { ?>
										<div class="text-danger"><?php echo $error_seo; ?></div>
									<?php } ?>
									</div>
									<div class="col-md-3">
										<input type="text" placeholder="Robots tag" class="form-control" name="robots" value="<?php echo $robots; ?>">
									</div>
									<div class="col-md-12">
											<label class="control-label">Mã nhúng</label>
											<textarea class="form-control" id="embed" name="embed" rows="5"><?php echo $embed; ?></textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="box box-primary">
						<div class="box-body">
							<div class="form-group text-center">
								<div class="">
									<a class="pull-right" onclick="$('#thumb').attr('src', '<?php echo $no_image; ?>'); $('#image').attr('value', '');"><?php echo $text_clear; ?></a>
									<a onclick="filebrowser('image', 'thumb');"><img style="border:1px solid #ddd; margin-bottom:7px;padding: 5px;" src="<?php echo $thumb; ?>" alt="" id="thumb" />
									</a>

									<input type="hidden" name="image" value="<?php echo $image; ?>" id="image" />

								</div>
							</div>
							<div class="form-group text-center">
								<div class="">
									<a class="pull-right" onclick="$('#thumb_ogimage').attr('src', '<?php echo $no_image; ?>'); $('#ogimage').attr('value', '');"><?php echo $text_clear; ?></a>
									<a onclick="filebrowser('ogimage', 'thumb_ogimage');"><img style="border:1px solid #ddd; margin-bottom:7px;padding: 5px;" src="<?php echo $thumb_ogimage; ?>" alt="" id="thumb_ogimage" />
									</a>
									<input type="hidden" name="ogimage" value="<?php echo $ogimage; ?>" id="ogimage" />
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="box box-primary">
						<div class="box-body">
							<div class="s2-example">
								<select class="js-example-basic-multiple js-states form-control select2-hidden-accessible" multiple="" tabindex="-1" aria-hidden="true">
									<?php foreach ($categories as $value) { ?>
										<!-- <optgroup label="<?php echo $value['name'];?>"> -->
											<option class="bold" value="<?php echo $value['category_id'];?>"><?php echo $value['name']?></option>
											<?php foreach ($value['group'] as $rs) { ?>
											<option value="<?php echo $rs['category_id'];?>">&nbsp&nbsp&nbsp&nbsp<?php echo $rs['name']?></option>
											<?php } ?>
										<!-- </optgroup> -->
									<?php } ?>
								</select>
							</div>
							<input type="hidden" name="category" id="category" value="<?php echo $category; ?>"/>
							<br />
							<div class="form-group clearfix">
								<label class="control-label pull-left"><?php echo $entry_status; ?></label>
								<label class="switch pull-right">
								  <input type="checkbox" name="status" <?php echo $status ? 'checked':''; ?> value="1">
								  <span class="slider round"></span>
								</label>
							</div>
							<div class="form-group required">
								<label class="control-label"><?php echo $entry_model; ?></label>
								<input class="form-control" type="text" name="model" value="<?php echo $model; ?>" />
								<?php if ($error_model) { ?>
									<div class="text-danger"><?php echo $error_model; ?></div>
								<?php } ?>
							</div>
							<div class="form-group clearfix">
								<label class="control-label pull-left"><?php echo $entry_stock_status; ?></label>
								<label class="switch pull-right">
								  <input type="checkbox" name="quantity" <?php echo $quantity ? 'checked':''; ?> value="1">
								  <span class="slider round"></span>
								</label>
							</div>
							<div class="form-group hide">
								<label class="control-label"><?php echo $entry_manufacturer; ?></label>
								<input class="form-control" type="text" name="manufacturer" value="<?php echo $manufacturer ?>" /><input type="hidden" name="manufacturer_id" value="<?php echo $manufacturer_id; ?>" />
							</div>
							<div class="form-group">
								<label class="control-label"><?php echo $entry_price; ?></label>
								<input class="form-control" type="text" name="price" value="<?php echo $price; ?>" />
							</div>
							<div class="form-group">
								<label><?php echo $entry_date_available; ?></label>
								<div class="input-group date">
									<div class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</div>
									<input type="text" name="date_available" value="<?php echo $date_available; ?>" class="form-control pull-right date" id="datepicker">
								</div>
								<!-- /.input group -->
							</div>
							<div class="form-group">
								<label class="control-label"><?php echo $entry_sort_order; ?></label>
								<input class="form-control" type="text" name="sort_order" value="<?php echo $sort_order; ?>" size="2" />
							</div>
							<div class="form-group">
								<label class="control-label">Ghi chú</label>
								<textarea class="form-control" type="text" name="note"><?php echo $note; ?></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="box box-primary">
						<div class="box-header">
							<h3 class="box-title"><?php echo $entry_related_news; ?></h3>
						</div>
						<div class="box-body">
							<div class="form-group">
								<input class="form-control" type="text" name="news" value="" />
								<?php if (!isset($product_newss)) { ?>
									<?php $product_newss = array(); ?>
								<?php } ?>
								<div id="product-news" class="scrollbox">
										<?php $class = 'odd'; ?>
										<?php foreach ($product_newss as $product_news) { ?>
											<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
											<div id="product-news<?php echo $product_news['news_id']; ?>" class="<?php echo $class; ?>"><?php echo $product_news['name']; ?><img src="view/image/delete.png" alt="" />
												<input type="hidden" name="product_news[]" value="<?php echo $product_news['news_id']; ?>" />
											</div>
										<?php } ?>
								</div>
							</div>
						</div>
						<div class="box-header">
							<h3 class="box-title"><?php echo $entry_related_products; ?></h3>
						</div>
						<div class="box-body">
							<div class="form-group">
								<input class="form-control" type="text" name="related" value="" />
								<?php if (!isset($product_related)) { ?>
									<?php $product_related = array(); ?>
								<?php } ?>
								<div id="product-related" class="scrollbox">
								  <?php $class = 'odd'; ?>
								  <?php foreach ($product_related as $product_related) { ?>
								  <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
								  <div id="product-related<?php echo $product_related['product_id']; ?>" class="<?php echo $class; ?>"> <?php echo $product_related['name']; ?><img src="view/image/delete.png" alt="" />
									<input type="hidden" name="product_related[]" value="<?php echo $product_related['product_id']; ?>" />
								  </div>
								  <?php } ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript"><!--
$.widget('custom.catcomplete', $.ui.autocomplete, {
      _renderMenu: function (ul, items) {
      var self = this, currentCategory = '';
      $.each(items, function (index, item) {
      if (item.category != currentCategory) {
          ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');
          currentCategory = item.category;
			}
      self._renderItem(ul, item);
		});
	}
});
	// Manufacturer
	$('input[name=\'manufacturer\']').autocomplete({
		delay: 500,
		source: function(request, response) {
			$.ajax({
				url: 'index.php?route=catalog/manufacturer/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
				dataType: 'json',
				success: function(json) {
					response($.map(json, function(item) {
						return {
							label: item.name,
							value: item.manufacturer_id
						}
					}));
				}
			});
		},
		select: function(event, ui) {
			$('input[name=\'manufacturer\']').val(ui.item.label);
			// $('input[name=\'manufacturer\']').attr('value', ui.item.label);
			$('input[name=\'manufacturer_id\']').attr('value', ui.item.value);

			return false;
		},
		focus: function(event, ui) {
			return false;
		}
	});

	$(function(){
		$(document).on('click', '#product-category div img', function(){
			$(this).parent().remove();
			$('#product-category div:odd').attr('class', 'odd');
			$('#product-category div:even').attr('class', 'even');
		})
	})
	// Related
    $('input[name=\'related\']').autocomplete({
        delay: 100,
        source: function (request, response) {
            $.ajax({
                url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request.term),
                dataType: 'json',
                success: function (json) {
                    response($.map(json, function (item) {
                        return {
                            label: item.name,
                            value: item.product_id
						}
					}));
				}
			});
		},
        select: function (event, ui) {
            $('#product-related' + ui.item.value).remove();

            $('#product-related').append('<div id="product-related' + ui.item.value + '">' + ui.item.label + '<img src="view/image/delete.png" alt="" /><input type="hidden" name="product_related[]" value="' + ui.item.value + '" /></div>');

            $('#product-related div:odd').attr('class', 'odd');
            $('#product-related div:even').attr('class', 'even');

            return false;
		},
        focus: function (event, ui) {
            return false;
		}
	});


	$(function(){
		$(document).on('click', '#product-related div img', function(){
			$(this).parent().remove();
			$('#product-related div:odd').attr('class', 'odd');
			$('#product-related div:even').attr('class', 'even');
		})
	})
    // $('#product-related div img').live('click', function () {
	// $(this).parent().remove();

	// $('#product-related div:odd').attr('class', 'odd');
	// $('#product-related div:even').attr('class', 'even');
    // });
//--></script>
<script type="text/javascript"><!--
    $('#tabs a').tabs();
    $('#languages a').tabs();
    $('#vtab-option a').tabs();
//--></script>



<script type="text/javascript"><!--
    var image_row = <?php echo $image_row; ?>;

    function addImage() {
		html = '<div class="col-md-2 text-center" id="image-row' + image_row + '">';
        html += '	<div class="image">';
        html += '		<img style="width:100%" src="<?php echo $no_image; ?>" alt="" id="thumb' + image_row + '" />';
        html += '		<input type="hidden" name="product_image[' + image_row + '][image]" value="" id="image' + image_row + '" /><br />';
        html += '		<a onclick="filebrowser(\'image' + image_row + '\', \'thumb' + image_row + '\');"><?php echo $text_browse; ?></a><br /><input class="text-center" type="text" name="product_image[' + image_row + '][sort_order]" value="" placeholder="0" size="8" />';
        html += '		<a onclick="$(\'#image-row' + image_row + '\').remove();" class="remove"><i class="fa fa-close"></i></a>';
        html += '	</div>';
        html += '</div>';

        $('#list-images').append(html);

        image_row++;
	}
//--></script>
<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript"><!--
	$('.date').datepicker({format: 'yyyy-mm-dd'});
    $('.datetime').datetimepicker({
        dateFormat: 'yy-mm-dd',
        timeFormat: 'h:m'
	});
    $('.time').timepicker({timeFormat: 'h:m'});
//--></script>

<script type="text/javascript"><!--
	// News
    $('input[name=\'news\']').autocomplete({
        delay: 500,
        source: function (request, response) {
            $.ajax({
                url: 'index.php?route=catalog/news/autocomplete&token=<?php echo $token; ?>',
                type: 'post',
                dataType: 'json',
                data: 'filter_name=' + encodeURIComponent($('input[name=\'news\']').val()),
                success: function (json) {
                    response($.map(json, function (item) {
                        return {
                            label: item.name,
                            value: item.news_id
						}
					}));
				}
			});
		},
        select: function (event, ui) {
            $('#product-news' + ui.item.value).remove();

            $('#product-news').append('<div id="product-news' + ui.item.value + '">' + ui.item.label + '<img src="view/image/delete.png" alt="" /><input type="hidden" name="product_news[]" value="' + ui.item.value + '" /></div>');

            $('#product-news div:odd').attr('class', 'odd');
            $('#product-news div:even').attr('class', 'even');

            return false;
		},
        focus: function (event, ui) {
            return false;
		}
	});

	$(function(){
		$(document).on('click', '#product-news div img', function(){
			$(this).parent().remove();
			$('#product-news div:odd').attr('class', 'odd');
			$('#product-news div:even').attr('class', 'even');
		})
	})
//--></script>
<script type="text/javascript">
    $("#seo_keyword").keyup(function () {
        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: 'index.php?route=catalog/product/checkKeyword&token=<?php echo $this->request->get['token']; ?>',
            data: 'product_id=<?php echo isset($_GET['product_id']) ? $_GET['product_id'] : ''; ?>&keyword=' + encodeURIComponent($('input[name=\'keyword\']').val()),
            success: function (result) {
                if (result.success) {
                    $('#seo_keyword_msg').html('<div class="success">' + result.success + '<input size="60" type="hidden" name="seo" value="1" id="seo_url" /></div>');
				}
                if (result.error) {
                    $('#seo_keyword_msg').html('<div class="warning">' + result.error + '</div><input size="60" type="hidden" name="seo" value="0" id="seo_url" />');
				}
			}
		});
	});

</script>
<script type="text/javascript"><!--
	$('#saveonly').click(function(e) {
		e.preventDefault();
		$('#form').submit();
	});
	$('#continue').click(function(e) {
		e.preventDefault();
		$('#form').attr('action', $('#form').attr('action') + '&continue=true');
		$('#form').submit();

	});
	$('#add-new').click(function(e) {
		e.preventDefault();
		$('#form').attr('action', $('#form').attr('action') + '&new=true');
		$('#form').submit();
	});
//--></script>
<script type="text/javascript"><!--

	$('input[name=\'related\']').autocomplete({
        delay: 500,
        source: function (request, response) {
            $.ajax({
                url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request.term),
                dataType: 'json',
                success: function (json) {
                    response($.map(json, function (item) {
                        return {
                            label: item.name,
                            value: item.product_id
                        }
                    }));
                }
            });
        },
        select: function (event, ui) {
            $('#product-related' + ui.item.value).remove();

            $('#product-related').append('<div id="product-related' + ui.item.value + '">' + ui.item.label + '<img src="view/image/delete.png" alt="" /><input type="hidden" name="product_related[]" value="' + ui.item.value + '" /></div>');

            $('#product-related div:odd').attr('class', 'odd');
            $('#product-related div:even').attr('class', 'even');

            return false;
        },
        focus: function (event, ui) {
            return false;
        }
    });

	$(function(){
		$(document).on('click', '#product-related div img', function(){
			$(this).parent().remove();
			$('#product-related div:odd').attr('class', 'odd');
			$('#product-related div:even').attr('class', 'even');
		})
	})
//--></script>
<script type="text/javascript"><!--
	$.fn.select2.amd.require([
    "select2/core",
    "select2/utils",
    "select2/compat/matcher"
	], function (Select2, Utils, oldMatcher) {
		var $basicMultiple = $(".js-example-basic-multiple");
		$.fn.select2.defaults.set("width", "100%");
		$basicMultiple.select2({
			placeholder: '<?php echo $entry_category; ?>',
			/* ajax: {
				data: function (term) {
					return {
						q: term // search term
					};
				},
				url: function(term) {
					url="http://fr.openfoodfacts.org/api/v0/produit/" + term +".json";
					return url;
				},
				dataType: 'json',
				results: function(data) {
					if (data.status!=1) {return;}
					if (data.product.complete!=1) {return;}

//                        return {results : [data]};
					return {
						results: $.map([data], function (item) {
							return {
								text: item.product.product_name,
								id: item.code,
								data: item
							}
						})
					};
				}
			},
			formatResult : function(response){
				alert(1);
				data=response.data;
				console.log(data);
				this.description =
					'<div id="fmu_select2_ajax_result">' +
						"<div>Nom du produit : " + data.product.product_name + "</div>" +
						"<div>"+
							"<img src='"+data.product.image_small_url+"'>"+
							"<ul>" +
								"<li><span>Catégories</span> : " + data.product.categories + "</li>" +
								"<li><span>Quantité</span> : " + data.product.quantity + " - " + data.product.serving_quantity + "</li>" +
							"</ul>" +
							"<div>" + data.product.brands + "</div>" +
					"</div>" +
					'</div>'
				;
				return this.description;
			}, */
		});
		$basicMultiple.val([<?php echo $category; ?>]).trigger("change");
	});

	var $eventSelect = $(".js-example-basic-multiple");

	$eventSelect.on("select2:select", function (e) { log("select2:select", e); });
	$eventSelect.on("select2:unselect", function (e) { log("select2:unselect", e); });

	function log (name, evt) {
		var id='';
		if (!evt) {
			var args = "{}";
			} else {
			var args = JSON.stringify(evt.params, function (key, value) {
				if (value && value.nodeName) return "[DOM node]";
				if (value instanceof $.Event) return "[$.Event]";
				return value;
			});
		}
		var obj = JSON.parse(args);
		for (var key in obj["data"]) {
			if (Object.prototype.hasOwnProperty.call(obj["data"], key)) {
				id = obj["data"]["id"];
			}
		}
		data =  $('input[name=\'category\']').attr('value');
		if(data ==''){
			data = id;
			}else{
			if(obj["_type"] == 'select'){
				data += ","+id;
				}else{
				var arr = data.split(',');
				var index = arr.indexOf(id);
				arr.splice(index, 1);
				data = arr.join();
			}
		}
		$('input[name=\'category\']').val(data);
	}




/* $(document).ready(function() {

    var markMatch = function (result, term, markup) {
        var text = result.text;
        var match=text.toUpperCase().indexOf(term.toUpperCase()),
            tl=term.length;

        if (match<0) {
            markup.push(text);
            return;
        }
console.log(result);
        markup.push(text.substring(0, match));
        markup.push("<span class='select2-match' style='width: 100%' title='"+result.element[0]['title']+"'>");
        markup.push(text.substring(match, match + tl));
        markup.push(text.substring(match + tl, text.length));
        markup.push("</span>");
    };

    $(".js-example-basic-multiple").select2({
        placeholder: "Chọn nhóm sản phẩm",
        width: "100%",
        formatResult: function(result, container, query) {
			alert(1);
            var markup=[];
            markMatch(result, query.term, markup);
            var m = markup.join("");
            console.log(m);
            return m;
        }
    });
}); */
//--></script>

<style type="text/css">
	.markup{
		background: #ddd;
		padding: 3px 5px;
		margin-right: 5px;
		border-radius: 5px;
		position: relative;
	}
	.markup:hover .fa{
		display: block;
	}
	.markup .fa{
		position: absolute;
		display: none;
		right: -5px;
		top: -5px;
		cursor: pointer;
	}
</style>
<?php echo $footer; ?>
