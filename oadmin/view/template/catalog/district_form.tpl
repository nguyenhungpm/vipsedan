<?php echo $header; ?>
<style type="text/css">
  label{cursor: pointer;margin-right: 10px}
</style>
<div class="content-wrapper">
  <section class="content-header">
    <h1><?php echo $heading_title; ?></h1>
    <ol class="breadcrumb">
      <?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
        <li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
      <?php $k++; } ?>
    </ol>
  </section>
  <?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
<div class="content">
  <div class="box box-default">
    <div class="box-header">
      <div class="pull-right"><a onclick="$('#form').submit();" class="btn btn-primary btn-sm"><span><i class="fa fa-floppy-o" aria-hidden="true"></i> <?php echo $button_save; ?></span></a></div>
      <div class="pull-left"><a onclick="location = '<?php echo $cancel; ?>';" class="btn btn-default btn-sm"><span><i class="fa fa-reply"></i> <?php echo $button_cancel; ?></span></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
            <table class="form">
              <tr>
                <td><span class="required">*</span> <?php echo 'Tên quận / huyện'; ?></td>
                <td><input type="text" name="name"  size="100" value="<?php echo $name; ?>" /></td>
              </tr>
		            <tr>
              <td><?php echo 'Thứ tự'; ?></td>
              <td><input type="text" name="sort_order" value="<?php echo $sort_order; ?>" size="1" /></td>
            </tr>
			<tr>
              <td><?php echo 'Trạng thái'; ?></td>
              <td><select name="status">
                  <?php if ($status) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
            </tr>
			      <tr>
              <td><?php echo "Khu vực"; ?></td>
              <td>
      				<select name="province">
      					<option>--Chọn tỉnh thành--</option>
      					<?php foreach($provinces as $rs){ ?>
      					<?php if($rs['province_id']==$province){ ?>
      					<option selected value="<?php echo $rs['province_id']?>"><?php echo $rs['name']?></option>
      					<?php }else{ ?>
      					<option value="<?php echo $rs['province_id']?>"><?php echo $rs['name']?></option>
      					<?php } ?>
      					<?php } ?>
      				</select>
      			  </td>
            </tr>
		       </table>
         </div>
        </form>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
$('#tabs a').tabs();
$('#languages a').tabs();
//--></script>
<?php echo $footer; ?>
