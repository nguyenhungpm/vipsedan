<?php echo $header; ?>
<script src="//cdn.jsdelivr.net/gh/osdvn/mkmate/ckeditor-full.js"></script>
<link rel="stylesheet" href="//cdn.jsdelivr.net/gh/osdvn/mkmate/finder/css/elfinder.min.css" />
<script src="//cdn.jsdelivr.net/gh/osdvn/mkmate/finder/js/elfinder.min.js"></script>
<script src="asset/initck5.js"></script>
<style type="text/css">
  .image{width:auto !important}
</style>
<div class="content-wrapper" id="content">
  <section class="content-header">
    <h1><?php echo $heading_title; ?><small>(Thêm)</small></h1>
    <ol class="breadcrumb">
      <?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
        <li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
      <?php $k++; } ?>
    </ol>
  </section>
  <?php if (isset($success)) { ?>
    <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="content">
    <div class="box box-default">
      <div class="box-header">
        <div class="pull-right">
           <a onclick="$('#form').submit();" class="btn btn-primary"><i class="fa fa-check"></i> <?php echo $button_save; ?></a>
        </div>
        <div class="pull-left">
          <a onclick="location = '<?php echo $cancel; ?>';" class="btn btn-default"><i class="fa fa-reply"></i> <?php echo $button_cancel; ?></a>
        </div>
      </div>
    </div>
    <div class="box box-body">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <div id="tab-general">
          <div id="languages" class="htabs" style="display:none">
            <?php foreach ($languages as $language) { ?>
            <a href="#language<?php echo $language['language_id']; ?>"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a>
            <?php } ?>
          </div>
          <?php foreach ($languages as $language) { ?>
          <div id="language<?php echo $language['language_id']; ?>">

            <table class="">

	          <tr>
	            <td><?php echo $entry_title; ?></td>

	            <td><input type="text" name="testimonial_description[<?php echo $language['language_id']; ?>][title]" size="100" value="<?php echo isset($testimonial_description[$language['language_id']]) ? $testimonial_description[$language['language_id']]['title'] : ''; ?>" class="form-control" />
	          </tr>

	          <tr>
	            <td> <?php echo $entry_description; ?><span class="required">*</span></td>
	            <td>
        			  <textarea class="ckeditor5" rows="10" cols="95" name="testimonial_description[<?php echo $language['language_id']; ?>][description]" id="description<?php echo $language['language_id']; ?>"><?php echo isset($testimonial_description[$language['language_id']]) ? $testimonial_description[$language['language_id']]['description'] : ''; ?></textarea>

        				<?php if (isset($error_description[$language['language_id']])) { ?>
        	              <span class="error"><?php echo $error_description[$language['language_id']]; ?></span>
        	              <?php } ?>
        			</td>
	          </tr>
            </table>
          </div>
          <?php } ?>
      </div>
      <table class="form">
		    <tr>
		         <td><?php echo $entry_name; ?></td>
    				 <td><input type="text" name="name" value="<?php echo $name; ?>" />
    			              <?php if (isset($error_name)) { ?>
    			              <span class="error"><?php echo $error_name; ?></span>
    			              <?php } ?>
    				</td>
		    </tr>
        <tr style="display:none">
          <td>Ảnh đại diện</td>
          <td>
              <div class="image pull-left">
                  <a onclick="filebrowser('image', 'thumb');">
                    <img src="<?php echo $thumb; ?>" alt="" id="thumb" />
                  </a>
                      <a class="pull-left"  onclick="filebrowser('image', 'thumb');">Chọn</a> &nbsp; &nbsp; | &nbsp; &nbsp;
                      <a class="pull-right" onclick="$('#thumb').attr('src', '<?php echo $no_image; ?>'); $('#image').attr('value', '');">Xóa</a>
                      <input type="hidden" name="image" value="<?php echo $image; ?>" id="image" />
              </div>
          </td>
        </tr>
			  <tr>
		      <td><?php echo $entry_city; ?></td>
			    <td><input type="text" name="city" value="<?php echo $city; ?>"></td>
		    </tr>
			  <tr style="display:none">
		       <td><?php echo $entry_email; ?></td>
			    <td><input type="text" name="email" value="<?php echo $email; ?>"></td>
		    </tr>
        <tr>
          <td><?php echo $entry_status; ?></td>
          <td><select name="status">
              <?php if ($status) { ?>
              <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
              <option value="0"><?php echo $text_disabled; ?></option>
              <?php } else { ?>
              <option value="1"><?php echo $text_enabled; ?></option>
              <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
              <?php } ?>
            </select></td>
        </tr>
        <tr>
          <td><?php echo $entry_date_added; ?></td>
     	    <td><input type="text" name="date_added" value="<?php echo $date_added; ?>"></td>
        </tr>
          <tr>
            <td><?php echo $entry_rating; ?></td>
		<td><span><?php echo $entry_bad; ?></span>&nbsp;
        		<input type="radio" name="rating" value="1" style="margin: 0;" <?php if ( $rating == 1 ) echo 'checked="checked"';?> />
        		&nbsp;
        		<input type="radio" name="rating" value="2" style="margin: 0;" <?php if ( $rating == 2 ) echo 'checked="checked"';?> />
        		&nbsp;
        		<input type="radio" name="rating" value="3" style="margin: 0;" <?php if ( $rating == 3 ) echo 'checked="checked"';?> />
        		&nbsp;
        		<input type="radio" name="rating" value="4" style="margin: 0;" <?php if ( $rating == 4 ) echo 'checked="checked"';?> />
        		&nbsp;
        		<input type="radio" name="rating" value="5" style="margin: 0;" <?php if ( $rating == 5 ) echo 'checked="checked"';?> />
        		&nbsp; <span><?php echo $entry_good; ?></span>

          	</td>
          </tr>


      </table>


      </form>
    </div>
  </div>
  </div>
</div>



<script type="text/javascript"><!--
$('#tabs a').tabs();
$('#languages a').tabs();
//--></script>
<?php echo $footer; ?>
