<?php echo $header; ?>
<div class="content-wrapper" id="content">
 <section class="content-header">
    <h1><?php echo $heading_title; ?></h1>
    <ol class="breadcrumb">
      <?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
        <li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
      <?php $k++; } ?>
    </ol>
  </section>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <?php if ($error_warning) { ?> 
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="content">
    <div class="box box-default">
      <div class="box-header">
        <div class="pull-right"><a onclick="location = '<?php echo $insert; ?>'" class="btn btn-primary btn-sm"><i class="fa fa-file-text" aria-hidden="true"></i>  <span><?php echo $button_insert; ?></span></a></div>
        <div class="pull-left"><a onclick="$('#form').submit();" class="btn btn-sm btn-default"><i class="fa fa-trash" aria-hidden="true"></i> <span><?php echo $button_delete; ?></span></a></div>
    </div>
    <div class="box box-body">
      <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="list">
          <thead>
            <tr>
              <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
              <td class="left"><?php echo $column_name; ?></td>
              <td class="right"><?php echo $column_sort_order; ?></td>
              <td class="right"><?php echo $column_action; ?></td>
            </tr>
          </thead>
          <tbody>
            <?php if ($categories) { ?>
            <?php foreach ($categories as $category) { ?>
            <tr>
              <td style="text-align: center;"><?php if ($category['selected']) { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $category['area_id']; ?>" checked="checked" />
                <?php } else { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $category['area_id']; ?>" />
                <?php } ?></td>
              <td class="left"><?php echo $category['name']; ?></td>
              <td class="right"><?php echo $category['sort_order']; ?></td>
              <td class="right"><?php foreach ($category['action'] as $action) { ?>
                [ <a href="<?php echo $category['view']; ?>">Xem</a> ]
                [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                <?php } ?></td>
            </tr>
            <?php } ?>
            <?php } else { ?>
            <tr>
              <td class="center" colspan="4"><?php echo $text_no_results; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </form>
    </div>
  </div>
</div>
<?php echo $footer; ?>