<?php echo $header; ?>
<link href="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.10.2/skins/ui/oxide/skin.min.css">
<script src="//cdn.jsdelivr.net/gh/osdvn/mkmate/ckeditor-full.js"></script>
<link rel="stylesheet" href="//cdn.jsdelivr.net/gh/osdvn/mkmate/finder/css/elfinder.min.css" />
<script src="//cdn.jsdelivr.net/gh/osdvn/mkmate/finder/js/elfinder.min.js"></script>
<script src="asset/initck5.js"></script>
<div class="content-wrapper" id="content">
	<section class="content-header">
		<h1><?php echo $heading_title; ?><small>(<?php echo $text_page_details; ?>)</small></h1>
		<ol class="breadcrumb">
			<?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
			<li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
			<?php $k++; } ?>
		</ol>
	</section>
	<?php if ($error_warning) { ?>
	<div class="warning"><?php echo $error_warning; ?></div>
	<?php } ?>
	<div class="content">
		<div class="box box-default">
			<div class="box-header">
				<div class="pull-right">
					<a onclick="$('#form').submit();" class="btn btn-primary"><i class="fa fa-check" aria-hidden="true"></i> <?php echo $button_save; ?></a>
				</div>
				<div class="pull-left">
					<a href="<?php echo $cancel; ?>" class="btn btn-default btn-sm"><i class="fa fa-reply"></i> <?php echo $button_close; ?></a>
					<?php if(isset($information_id) && !empty($information_id)){ ?>
					<a class="btn btn-default btn-sm" onclick="$('#form_delete').submit();" ><i class="fa fa-trash" aria-hidden="true" ></i> <?php echo $button_delete; ?></a>
					<?php } ?>
				</div>
				<?php if(isset($information_id) && !empty($information_id)){ ?>
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form_delete">
					<input type="hidden" name="selected[]" value="<?php echo $information_id; ?>"/>
				</form>
				<?php } ?>
			</div>
		</div>

		<div class="row">
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
				<div class="col-md-9" id="tab-general">
					<div class="box box-primary">
						<div class="box-body">
							<ul class="nav nav-tabs <?php echo count($languages)==1 ? 'hide':''; ?>">
								<?php foreach ($languages as $key=> $language) { ?>
									<li class="<?php echo $language['language_id'] == $this->config->get('config_language_id') ? 'active':'';?>"><a data-toggle="tab" aria-expanded="true" href="#language<?php echo $language['language_id']; ?>"><img <?php echo count($languages) ==1 ? 'class="hide"':''; ?> src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
								<?php } ?>
							</ul>
							<div class="tab-content">
							<?php foreach ($languages as $language) { ?>
							<div class="tab-pane <?php echo $language['language_id'] == $this->config->get('config_language_id') ? 'active':'';?>" id="language<?php echo $language['language_id']; ?>">
								<div class="form-group">
									<span class="required">*</span> <label class="control-label"><?php echo $entry_title; ?></label>
									<input class="form-control" type="text" name="information_description[<?php echo $language['language_id']; ?>][title]" value="<?php echo isset($information_description[$language['language_id']]) ? $information_description[$language['language_id']]['title'] : ''; ?>" />
									<?php if (isset($error_title[$language['language_id']])) { ?>
									<span class="error"><?php echo $error_title[$language['language_id']]; ?></span>
									<?php } ?>
								</div>
								<div class="form-group">
									<span class="required">*</span> <label class="control-label"><?php echo $entry_description; ?></label>
									<textarea class="ckeditor5" name="information_description[<?php echo $language['language_id']; ?>][description]" id="description<?php echo $language['language_id']; ?>"><?php echo isset($information_description[$language['language_id']]) ? $information_description[$language['language_id']]['description'] : ''; ?></textarea>
									<?php if (isset($error_description[$language['language_id']])) { ?>
									<span class="error"><?php echo $error_description[$language['language_id']]; ?></span>
									<?php } ?>
								</div>
							</div>
							<?php } ?>
							</div>
						</div>
					</div>

					<div class="box box-info">
						<div class="box-header with-border">
							<h3 class="box-title">SEO</h3>
						</div>
						<div class="box-body">
							<ul class="nav nav-tabs <?php echo count($languages)==1 ? 'hide':''; ?>">
								<?php foreach ($languages as $key=> $language) { ?>
									<li class="<?php echo $language['language_id'] == $this->config->get('config_language_id') ? 'active':'';?>"><a data-toggle="tab" aria-expanded="true" href="#seo<?php echo $language['language_id']; ?>"><img <?php echo count($languages) ==1 ? 'class="hide"':''; ?> src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
								<?php } ?>
							</ul>
							<div class="tab-content">
							<?php foreach ($languages as $language) { ?>
							<div class="tab-pane <?php echo $language['language_id'] == $this->config->get('config_language_id') ? 'active':'';?>" id="seo<?php echo $language['language_id']; ?>">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<input placeholder="Meta title" class="form-control" onKeyDown="textCounter(this, 'titleseo', 70);"
											onKeyUp="textCounter(this,'titleseo',70)" name="information_description[<?php echo $language['language_id']; ?>][meta_title]" value="<?php echo isset($information_description[$language['language_id']]) ? $information_description[$language['language_id']]['meta_title'] : ''; ?>" />
											<label class="control-label"><span class="small help"><?php echo $text_remain; ?> <input style="font-weight:bold;border:none" disabled size="3" id="titleseo" value="70" /> <?php echo $text_characters; ?></span></label>

										</div>
										<div class="form-group">
											<input placeholder="Meta keyword" class="form-control" name="information_description[<?php echo $language['language_id']; ?>][meta_keyword]" value="<?php echo isset($information_description[$language['language_id']]) ? $information_description[$language['language_id']]['meta_keyword'] : ''; ?>"/>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<textarea placeholder="Meta Description" class="form-control" onKeyDown="textCounter(this,'descseo',160);"
											onKeyUp="textCounter(this,'descseo',160)" name="information_description[<?php echo $language['language_id']; ?>][meta_description]" rows="4"><?php echo isset($information_description[$language['language_id']]) ? $information_description[$language['language_id']]['meta_description'] : ''; ?></textarea>
											<label class="control-label"><span class="small help"><?php echo $text_remain; ?> <input style="font-weight:bold;border:none" disabled size="3" id="descseo" value="160" /> <?php echo $text_characters; ?></span></label>
										</div>
									</div>
								</div>
							</div>
							<?php } ?>
							</div>
							<div class="row">
								<div class="col-md-9">
									<div class="form-group">
										<input type="text" name="keyword" value="<?php echo $keyword; ?>" class="form-control" placeholder="SEO URL" />
									</div>
								</div>
								<div class="col-md-3">
									<input type="text" class="form-control" name="robots" value="<?php echo $robots; ?>" placeholder="robots tag" />
								</div>
							</div>
						</div>
					</div>
				</div>

				<div id="tab-data" class="col-md-3">
					<div class="box box-info">
						<div class="box-body">
							<div class="form-group">
								<label class="control-label pull-left"><?php echo $entry_status; ?></label>
								<label class="switch pull pull-right">
									<input type="checkbox" name="status" <?php echo $status ? 'checked':''; ?> value="1">
									<span class="slider round"></span>
								</label>
							</div>
							<div class="form-group">
								<label class="control-label"><?php echo $entry_sort_order; ?></label>
								<input type="text" name="sort_order" size="4" value="<?php echo $sort_order; ?>" />
							</div>
							<div class="form-group" style="display:none">
								<?php if ($bottom) { ?>
								<input type="checkbox" name="bottom" value="1" checked="checked" />
								<?php } else { ?>
								<input type="checkbox" name="bottom" value="1" />
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript" src="view/javascript/ckeditor/ckeditor.js"></script>

<script type="text/javascript"><!--
	$('#tabs a').tabs();
	$('#languages a').tabs();
//--></script>
<style type="text/css">
	.markup{
		background: #ddd;
		padding: 3px 5px;
		margin-right: 5px;
		border-radius: 5px;
		position: relative;
	}
	.markup:hover .fa{
		display: block;
	}
	.markup .fa{
		position: absolute;
		display: none;
		right: -5px;
		top: -5px;
		cursor: pointer;
	}
</style>
<?php echo $footer; ?>
