<?php echo $header; ?>
<div class="content-wrapper">
  <section class="content-header">
    <h1><?php echo $heading_title; ?></h1>
    <ol class="breadcrumb">
      <?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
        <li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
      <?php $k++; } ?>
    </ol>
  </section>
  <?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
  <div class="content">
    <div class="box box-default">
    <div class="box-header">
      <div class="pull-right"><a onclick="$('#form').submit();" class="btn btn-primary btn-sm"><span><?php echo $button_save; ?></span></a></div>
      <div class="pull-left"><a onclick="location = '<?php echo $cancel; ?>';" class="btn btn-sm btn-default"><span><?php echo $button_cancel; ?></span></a></div>
    </div>
    <div class="box box-body">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <div id="tab-general">
          <div id="languages" class="htabs">
            <?php foreach ($languages as $language) { ?>
            <a href="#language<?php echo $language['language_id']; ?>"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a>
            <?php } ?>
          </div>
          <?php foreach ($languages as $language) { ?>
          <div id="language<?php echo $language['language_id']; ?>">
            <table class="form">
              <tr>
                <td><span class="required">*</span> <?php echo $entry_name; ?></td>
                <td><input type="text" name="province_description[<?php echo $language['language_id']; ?>][name]" id="province_description<?php echo $language['language_id']; ?>_name" onkeyup="trim_name(this.id);" size="100" value="<?php echo isset($province_description[$language['language_id']]) ? $province_description[$language['language_id']]['name'] : ''; ?>" />
                  <?php if (isset($error_name[$language['language_id']])) { ?>
                  <span class="error"><?php echo $error_name[$language['language_id']]; ?></span>
                  <?php } ?></td>
              </tr>
              </table>
          </div>
          <?php } ?>
		  <table class="form">
		  <tr>
              <td><?php echo $entry_sort_order; ?></td>
              <td><input type="text" name="sort_order" value="<?php echo $sort_order; ?>" size="5" /></td>
            </tr>
			<tr>
              <td><?php echo $entry_status; ?></td>
              <td><select name="status">
                  <?php if ($status) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
      </tr>
			<tr style="display:none;">
              <td><?php echo $entry_keyword; ?></td>
              <td><input type="text" name="keyword" id="seo_keyword" value="<?php echo $keyword; ?>" /></td>
            </tr>
			<tr>
              <td><?php echo "Khu vực"; ?></td>
              <td>
				<select name="area">
					<option>--Chọn khu vực--</option>
					<?php foreach($areas as $rs){ ?>
					<?php if($rs['area_id']==$area){ ?>
					<option selected value="<?php echo $rs['area_id']?>"><?php echo $rs['name']?></option>
					<?php }else{ ?>
					<option value="<?php echo $rs['area_id']?>"><?php echo $rs['name']?></option>
					<?php } ?>
					<?php } ?>
				</select>
			  </td>
            </tr>
		</table>
        </div>
        </form>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
$('#tabs a').tabs(); 
$('#languages a').tabs();
//--></script>
<?php echo $footer; ?>