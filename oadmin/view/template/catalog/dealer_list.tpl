<?php echo $header; ?>
<div class="content-wrapper" id="content">
  <section class="content-header">
    <h1><?php echo $heading_title; ?></h1>
    <ol class="breadcrumb">
      <?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
        <li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
      <?php $k++; } ?>
    </ol>
  </section>
  <?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
        <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box box-default">
    <div class="box-header">
      <div class="pull-right">
        <a onclick="$('#form').attr('action', '<?php echo $save_sort; ?>'); $('#form').submit();" class="btn btn-default btn-sm"><i class="fa fa-sort" aria-hidden="true"></i> Lưu sắp xếp</a>
        <a onclick="location = '<?php echo $insert; ?>'" class="btn btn-primary btn-sm"><span><?php echo $button_insert; ?></span></a>
      </div>
      <div class="pull-left">
		      <a onclick="$('#form').submit();" class="btn btn-sm btn-default"><span><?php echo $button_delete; ?></span></a>
		  </div>
    </div>
    <div class="box-body">
      <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="list">
          <thead>
            <tr>
              <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
              <td class="left"><?php echo $column_name; ?></td>
              <td class="right"><?php echo 'Số điện thoại'; ?></td>
              <td class="right"><?php echo $column_sort_order; ?></td>
              <td class="right"><?php echo $column_action; ?></td>
            </tr>
          </thead>
          <tbody>
            <tr class="filter">
              <td></td>
              <td class="left"><input class="form-control" placeholder="nhập tên và nhấn enter" type="text" value="<?php echo $filter_name; ?>" name="filter_name" /></td>
              <td colspan="2"></td>
              <td><a class="btn btn-sm btn-primary pull-right" href="javascript:filter();">Lọc</a></td>
            </tr>
            <?php if ($dealers) { ?>
            <?php foreach ($dealers as $dealer) { ?>
            <tr>
              <td style="text-align: center;"><?php if ($dealer['selected']) { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $dealer['dealer_id']; ?>" checked="checked" />
                <?php } else { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $dealer['dealer_id']; ?>" />
                <?php } ?></td>
              <td class="left"><?php echo $dealer['name']; ?></td>
              <td class="right"><?php echo $dealer['telephone']; ?></td>
			  <?php  $sort_order = "sort_order[". $dealer['dealer_id'] ."]"; ?>
              <td class="right"><input type="text" class="text-center" size="3" name="<?php echo  $sort_order ?>"  value="<?php echo $dealer['sort_order']; ?>" /></td>
              <td class="right"><?php foreach ($dealer['action'] as $action) { ?>
                [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                <?php } ?></td>
            </tr>
            <?php } ?>
            <?php } else { ?>
            <tr>
              <td class="center" colspan="4"><?php echo $text_no_results; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
        <div class="pagination"><?php echo $pagination; ?></div>
      </form>
  </div>
</div>
<script type="text/javascript"><!--
  $('input[name=\'filter_name\']').keydown(function (e) {
      if (e.keyCode == 13) {
          filter();
      }
  });
function filter() {
        url = 'index.php?route=catalog/dealer&token=<?php echo $token; ?>';

        var filter_name = $('input[name=\'filter_name\']').val();

        if (filter_name) {
            url += '&filter_name=' + encodeURIComponent(filter_name);
        }
        location = url;
    }
//--></script>

<?php echo $footer; ?>
