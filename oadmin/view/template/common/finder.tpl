<?php echo $header; ?>
<div class="content-wrapper">   
    <?php if (strlen($success)>1) { ?>
		<div class="success"><?php echo $success; ?></div>
	<?php } ?>
	<?php if ($error_warning) { ?>
		<div class="warning"><?php echo $error_warning; ?></div>
  	<?php } ?>
    <section class="content">
		<div id="finder" data-connector-url="<?php echo $connector_url;?>"></div>
	</section>
</div>
<script type="text/javascript" charset="utf-8">
  $(document).ready(function() {
    var elfinder = $('#finder');
    var elfinderConnectorUrl = elfinder.data('connector-url');
    if (!elfinderConnectorUrl) {
      var message = 'Please set connector url for elFinder!';
      alert(message);
      throw Error(message);
    }
    elfinder.elfinder({
      url: elfinderConnectorUrl,
      cssAutoLoad : ['css/Material/css/theme-light.min.css'],
      		uiOptions: {
			toolbar: [
				['home', 'back', 'forward', 'up', 'reload'],
				['mkdir', 'mkfile', 'upload'],
				['open','getfile'],
				['undo', 'redo'],
				['copy', 'cut', 'paste'],
				['duplicate', 'rename', 'edit', 'resize', 'chmod'],
				['selectall', 'selectnone', 'selectinvert'],
				['search'],
				['view', 'sort'],
				['fullscreen']
			]
		},
		contextmenu: {
			navbar: ['open', '|', 'upload', 'mkdir', '|', 'copy', 'cut', 'paste', 'duplicate', '|', '|', 'rename', '|', 'places', 'info'],
			cwd: ['undo', 'redo', '|', 'back', 'up', 'reload', '|', 'upload', 'mkdir', 'mkfile', 'paste', '|', '|', 'view', 'sort', 'selectall', 'colwidth', '|', 'info', '|', 'fullscreen', '|'],
			files: ['getfile', '|', 'open', 'download', 'opendir', '|', 'upload', 'mkdir', '|', 'copy', 'cut', 'paste', 'duplicate', '|', 'empty', '|', 'rename', 'edit', 'resize', '|', 'selectall', 'selectinvert']
		},
		width: "99%",
		height: "90%"
    });
  });
</script>

<?php echo $footer; ?>
