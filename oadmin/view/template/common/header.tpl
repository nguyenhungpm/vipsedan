<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
	<head>
		<meta charset="UTF-8" />
		<title><?php echo $title; ?></title>
		<base href="<?php echo $base; ?>" />
		<?php if ($description) { ?>
			<meta name="description" content="<?php echo $description; ?>" />
		<?php } ?>
		<?php if ($keywords) { ?>
			<meta name="keywords" content="<?php echo $keywords; ?>" />
		<?php } ?>
		<?php foreach ($links as $link) { ?>
			<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
		<?php } ?>
		<?php if( isset($this->request->get['route']) && $this->request->get['route'] != 'common/filemanager'){ ?>
			<link rel="stylesheet" type="text/css" href="view/stylesheet/stylesheet.css" class="no-filemanger"/>
			<?php } ?>
		<?php foreach ($styles as $style) { ?>
			<link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
		<?php } ?>

		<!-- lte -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

		<link rel="stylesheet" href="view/stylesheet/lte/bootstrap.min.css">
		<link rel="stylesheet" href="view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css">
		<link rel="stylesheet" href="view/stylesheet/lte/font-awesome.min.css">
		<link rel="stylesheet" href="view/stylesheet/lte/AdminLTE.min.css">
		<link rel="stylesheet" href="view/stylesheet/lte/bootstrap-datepicker.min.css">
		<link rel="stylesheet" href="view/stylesheet/lte/skins.min.css">


		<script type="text/javascript" src="view/javascript/jquery/jquery.min.js"></script>
		<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui.min.js"></script>
		<script type="text/javascript" src="view/javascript/lte/bootstrap.min.js"></script>
		<script type="text/javascript" src="view/javascript/jquery/select2.full.js"></script>
		<script type="text/javascript" src="view/javascript/lte/bootstrap-datepicker.min.js"></script>
		<script type="text/javascript" src="view/javascript/lte/adminlte.min.js"></script>
		<script type="text/javascript" src="view/javascript/lte/superfish.js"></script>
		<script type="text/javascript" src="view/javascript/common.js"></script>

		<script type="text/javascript">
			$.widget.bridge('uibutton', $.ui.button);
		</script>
		<!-- end lte -->
		<?php foreach ($scripts as $script) { ?>
			<script type="text/javascript" src="<?php echo $script; ?>"></script>
		<?php } ?>
	</head>
	<body class="skin-blue sidebar-mini wysihtml5-supported">
		<?php if ($logged) { ?>
		<header class="main-header">
			<a href="index.php?route=common/home&token=<?php echo $token; ?>" class="logo">
				<span class="logo-mini"><b>C</b>MS</span>
				<span class="logo-lg"><b>Simple</b> CMS</span>
			</a>
			<nav class="navbar navbar-static-top">
				<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
					<span class="sr-only">Toggle navigation</span>
				</a>

				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<li class="hidden-xs hidden-sm hidden-md"><a href="<?php echo $store; ?>" target="_blank"><?php echo $text_website; ?> <i class="fa fa-eye"></i></a></li>
						<li><a href="<?php echo $logout; ?>"><i class="fa fa-user"></i> <?php echo $logged; ?> >>  <?php echo $text_logout; ?></a></li>
					</ul>
				</div>
			</nav>
		</header>
			<?php echo $menu; ?>
		<?php }else{ ?>
	<style>body{background: white}</style>
	<?php } ?>
	<style>.messages-menu, .notifications-menu, .tasks-menu{display: none !important;}, html{min-height: auto;}</style>
