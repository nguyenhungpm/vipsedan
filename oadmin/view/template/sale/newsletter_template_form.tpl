<?php echo $header; ?>
<div id="content" class="content-wrapper">
	<section class="content-header">
		<h1><?php echo $heading_title; ?><small>(Trang chi tiết)</small></h1>
		<ol class="breadcrumb">
			<?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
				<li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
			<?php $k++; } ?>
		</ol>
	</section>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<div class="content">
	<div id="fake"></div>
	<div class="box box-default" id="toolbar">
		<div class="box-header">
			<div class="pull-right">
				<button onclick="$('#form').submit();" type="button" class="btn btn-primary"><i class="fa fa-check" aria-hidden="true"></i> <?php echo $button_save; ?></button>
				<a href="<?php echo $cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i> <?php echo $button_cancel; ?></a>
			</div>
		</div>
	</div>
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
		<div class="row">
			<div class="col-md-9">
				<div class="box box-danger">
					<div class="box-body">
						<div class="form-group required">
							<label class="control-label"><?php echo $entry_code; ?></label>
							<textarea name="template_code" id="template_code" cols="40" rows="10"><?php echo isset($template_code)?$template_code:''; ?></textarea>
							<?php if (!empty($error_template_code)) { ?>
							<div class="text-danger"><?php echo $error_template_code; ?></div>
							<?php  } ?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="box box-danger">
					<div class="box-body">
						<div class="form-group required">
							<label class="control-label"><?php echo $entry_name; ?></label>
							<input type="text" name="template_name" value="<?php echo $template_name; ?>"  size="100" class="form-control"/>
							<?php if (!empty($error_template_name)) { ?>
							<div class="text-danger"><?php echo $error_template_name; ?></div>
							<?php  } ?>
						</div>
						<div class="form-group">
							<label class="control-label">Chủ đề email</label>
							<input type="text" name="template_subject" value="<?php echo $template_subject; ?>" size="100" class="form-control"/>
						</div>
					</div>
				</div>
			</div>
		</div>
    </form>
</div>
</div>
<?php echo $footer; ?>
<script type="text/javascript" src="view/javascript/ckeditor/ckeditor.js"></script>
<script type="text/javascript"><!--
CKEDITOR.replace('template_code', {
	filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	enterMode : CKEDITOR.ENTER_BR,
    shiftEnterMode: CKEDITOR.ENTER_P,
	on :{
		instanceReady : function( ev ){
		this.dataProcessor.writer.setRules( 'p',
			{
				indent : false,
				breakBeforeOpen : true,
				breakAfterOpen : false,
				breakBeforeClose : false,
				breakAfterClose : true
			});
		}}
	});
//--></script>
<script type="text/javascript" src="view/javascript/jquery/ui/ui.draggable.js"></script>
<script type="text/javascript" src="view/javascript/jquery/ui/ui.resizable.js"></script>
<script type="text/javascript" src="view/javascript/jquery/ui/ui.dialog.js"></script>
<script type="text/javascript" src="view/javascript/jquery/ui/external/bgiframe/jquery.bgiframe.js"></script>
<script type="text/javascript"><!--
function image_upload(field, preview) {
	$('#dialog').remove();
	
	$('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token; ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');
	
	$('#dialog').dialog({
		title: 'Quản lý ảnh',
		close: function (event, ui) {
			if ($('#' + field).attr('value')) {
				$.ajax({
					url: 'index.php?route=common/filemanager/image&token=<?php echo $token; ?>',
					type: 'POST',
					data: 'image=' + encodeURIComponent($('#' + field).val()),
					dataType: 'text',
					success: function(data) {
						$('#' + preview).replaceWith('<img src="' + data + '" alt="" id="' + preview + '" class="image" onclick="image_upload(\'' + field + '\', \'' + preview + '\');" />');
					}
				});
			}
		},	
		bgiframe: false,
		width: 700,
		height: 400,
		resizable: false,
		modal: false
	});
};
</script>