<?php echo $header; ?>
<div class="content-wrapper">
	<section class="content-header">
		<h1><?php echo $heading_title; ?><small>(<?php echo $text_list; ?>)</small></h1>
		<ol class="breadcrumb">
			<?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
				<li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
			<?php $k++; } ?>
		</ol>
	</section>
	<?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
    <?php if ($success) { ?>
        <div class="success"><?php echo $success; ?></div>
    <?php } ?>
	<div class="content">
		<div id="fake"></div>
		<div class="box box-default">
		<div class="box-header" id="toolbar">
				<div class="pull-right">
					<a onclick="$('form').submit();" class="btn btn-default btn-sm"><i class="fa fa-trash" aria-hidden="true"></i> <?php echo $button_delete; ?></a>
				</div>
		</div>
		<div class="box-body">
            <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                <table class="table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <td width="1" style="text-align: center;">
								<input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" />
							</td>
                            <td class="text-left">Sản phẩm</td>
                            <td class="text-left"><?php echo $entry_customer; ?></td>
                            <td class="text-left">Email</td>
                            <td class="left"><?php echo $entry_phone; ?></td>
                            <td class="left"><?php echo $entry_content; ?></td>
                            <td class="right"><?php echo $entry_status; ?></td>
                            <td class="left"><?php echo $entry_date_added; ?></td>
                        </tr>
                    </thead>
                    <tbody>
					<?php if ($orders) { ?>
						<?php foreach ($orders as $order) { ?>
							<tr>
								<td><input type="checkbox" name="selected[]" value="<?php echo $order['order_id']; ?>" /></td>
								<td>
									<table style="width: 100%;"> 
										<tr>
											<td>Tên</td>
											<td class="text-right">Số lượng</td>
											<td class="text-right">Giá</td>
											<td class="text-right">Tổng tiền</td>
										</tr> 
										<?php $price = 0; foreach(unserialize($order['detail']) as $product_id => $product){$price += $product['price']*$product['quantity'];} ?>
										<?php $k=0;foreach(unserialize($order['detail']) as $product_id => $product){ ?>
										<tr>
											<td><?php echo $product['name'];?></td>
											<td class="text-right"><?php echo $product['quantity'];?></td>
											<td class="text-right"><?php echo number_format($product['price'], 0, ',','.');?></td>
											<?php if($k==0){ ?>
											<td class="text-right" rowspan="<?php echo count(unserialize($order['detail']));?>"><?php echo number_format($price, 0, ',','.');?></td>
											<?php } ?>
										</tr>
										<?php $k++;} ?>
									</table>
								</td>
								<td><?php echo $order['name']; ?></td>
								<td><?php echo $order['email']; ?></td>
								<td><?php echo $order['telephone']; ?></td>
								<td><?php echo $order['note']; ?></td>
								<td><a class="columnstatus" id="status-<?php echo $order['order_id']; ?>"><?php echo $order['status']==1 ? $text_enabled : $text_disabled ; ?></a></td>
								<td><?php echo date('d-m-Y h:i',strtotime($order['date_added'])); ?></td>
							</tr>
						<?php } ?>
					<?php } else { ?>
						<tr>
							<td class="center" colspan="8"><?php echo $text_no_results; ?></td>
						</tr>
					<?php } ?>
                    </tbody>
                </table>
            </form>
            <div class="pagination"><?php echo $pagination; ?></div>
        </div>
        </div>
	</div>
</div>

<script type="text/javascript"><!--
$('.columnstatus').click(function() {
	var object_id=$(this).attr('id');
	$.ajax({
		url: 'index.php?route=sale/order/updatestatus&token=<?php echo $token; ?>',
		type: 'get',
		data: {object_id:object_id},
		dataType: 'html',
		success: function(html) {
			if(html!=''){
				$('#'+object_id).html(html);
			}
		}
	});
});
//--></script>
<?php echo $footer; ?>
