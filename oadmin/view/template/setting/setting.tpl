<?php echo $header; ?>
<div class="content-wrapper">
  <section class="content-header">
    <h1><?php echo $heading_title; ?><small>(<?php echo $text_configuration; ?>)</small></h1>
    <ol class="breadcrumb">
      <?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
        <li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
      <?php $k++; } ?>
    </ol>
  </section>
  <div id="alert"></div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="content" id="content">
  <div class="box box-primary">
    <div class="box-header">
      <div class="pull-right">
        <a onclick="$('#form').submit();" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> <?php echo $button_save; ?></a>
      </div>
    </div>
    <div class="box-body">
      <div class="nav-tabs-custom">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
            <li><a href="#tab-local" data-toggle="tab"><?php echo $tab_local; ?></a></li>
            <li class="<?php echo $this->config->get('config_error_display') ? '':'hide'?>"><a href="#tab-option" data-toggle="tab"><?php echo $tab_option; ?></a></li>
            <li><a href="#tab-ftp" data-toggle="tab"><?php echo $tab_ftp; ?></a></li>
            <li><a href="#tab-mail" data-toggle="tab"><?php echo $tab_mail; ?></a></li>
            <li><a href="#tab-server" data-toggle="tab"><?php echo $tab_server; ?></a></li>
        </ul>
        <div class="tab-content">
        <div id="tab-general" class="tab-pane active">
          <h3 class="box-title"><?php echo $tab_general; ?></h3>
          <table class="form">
            <tr>
              <td><span class="required">*</span> <?php echo $entry_name; ?></td>
              <td>
			  <?php foreach ($languages as $language) { ?>
			  <?php $id=$language['language_id']; ?>
			  <?php if(count($languages) > 1){ ?><img <?php echo count($languages) ==1 ? 'class="hide"':''; ?> src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><?php } ?><input type="text" name="config_name[<?php echo $id; ?>]" value="<?php echo $config_name[$id]; ?>" size="33" />
                <?php if ($error_name) { ?>
                <span class="error"><?php echo $error_name; ?></span>
                <?php } ?>
				<?php } ?>
				</td>
            </tr>
            <tr>
              <td><span class="required">*</span> <?php echo $entry_owner; ?></td>
              <td>
			  <?php foreach ($languages as $language) { ?>
			  <?php $id=$language['language_id']; ?>
			  <?php if(count($languages) > 1){ ?><img <?php echo count($languages) ==1 ? 'class="hide"':''; ?> src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><?php } ?><input type="text" name="config_owner[<?php echo $id; ?>]" value="<?php echo $config_owner[$id]; ?>" size="33" />
                <?php if ($error_owner) { ?>
                <span class="error"><?php echo $error_owner; ?></span>
                <?php } ?>
				<?php } ?>
				</td>
            </tr>

            <tr>
              <td><span class="required">*</span> <?php echo $entry_address; ?></td>
              <td>
			  <?php foreach ($languages as $language) { ?>
			  <?php $id=$language['language_id']; ?>
			  <?php if(count($languages) > 1){ ?><img <?php echo count($languages) ==1 ? 'class="hide"':''; ?> src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><?php } ?><textarea name="config_address[<?php echo $id; ?>]" cols="30" rows="5"><?php echo $config_address[$id]; ?></textarea>
                <?php if ($error_address) { ?>
                <span class="error"><?php echo $error_address; ?></span>
                <?php } ?>
				<?php } ?>
			</td>
            </tr>

            <tr>
              <td><span class="required">*</span> <?php echo $entry_email; ?></td>
              <td><input type="text" name="config_email" value="<?php echo $config_email; ?>" size="33" />
                <?php if ($error_email) { ?>
                <span class="error"><?php echo $error_email; ?></span>
                <?php } ?></td>
            </tr>
            <tr>
              <td><span class="required">*</span> <?php echo $entry_telephone; ?></td>
              <td><input type="text" name="config_telephone" value="<?php echo $config_telephone; ?>" />
                <?php if ($error_telephone) { ?>
                <span class="error"><?php echo $error_telephone; ?></span>
                <?php } ?></td>
            </tr>
            <tr>
              <td><?php echo $entry_fax; ?></td>
              <td><input type="text" name="config_fax" value="<?php echo $config_fax; ?>" /></td>
            </tr>
            <tr>
              <td>Hotline</td>
              <td><input type="text" name="config_hotline" size="33" value="<?php echo $config_hotline; ?>" /></td>
            </tr>
           <tr>
              <td>Thông tin giấy phép kinh doanh</td>
              <td>
                <textarea class="form-control" id="config_giayphep" name="config_giayphep"><?php echo $config_giayphep; ?></textarea> 
              </td>
           </tr>
           <tr>
              <td>Chi nhánh HCM</td>
              <td>
                <textarea class="form-control" name="config_chinhanh"><?php echo $config_chinhanh; ?></textarea>
              </td>
           </tr>
           <tr>
              <td>Ghi chú chân trang</td>
              <td>
                <textarea class="form-control" name="config_ghichu"><?php echo $config_ghichu; ?></textarea>
              </td>
           </tr>
           <tr>
              <td>Facebook</td>
              <td><input type="text" name="config_facebook" size="33" value="<?php echo $config_facebook; ?>" /></td>
            </tr>
			     <tr>
              <td>Twitter</td>
              <td><input type="text" name="config_twitter" size="33" value="<?php echo $config_twitter; ?>" /></td>
            </tr>
			     <tr>
              <td>Google Plus</td>
              <td><input type="text" name="config_googleplus" size="33" value="<?php echo $config_googleplus; ?>" /></td>
            </tr>
			     <tr>
              <td>Youtube</td>
              <td><input type="text" name="config_youtube" size="33" value="<?php echo $config_youtube; ?>" /></td>
            </tr>
            <tr>
              <td>Instagram</td>
              <td><input type="text" name="config_pinterest" size="33" value="<?php echo $config_pinterest; ?>" /></td>
            </tr>

        </table>
        </div>


        <div id="tab-local" class="tab-pane">
          <h3 class="box-title"><?php echo $tab_local; ?></h3>
          <table class="form">
            <tr>
              <td><?php echo $entry_country; ?></td>
              <td><select name="config_country_id">
                  <?php foreach ($countries as $country) { ?>
                  <?php if ($country['country_id'] == $config_country_id) { ?>
                  <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select></td>
            </tr>
            <tr>
              <td><?php echo $entry_zone; ?></td>
              <td><select name="config_zone_id">
                </select></td>
            </tr>
            <tr>
              <td><?php echo $entry_language; ?></td>
              <td><select name="config_language">
                  <?php foreach ($languages as $language) { ?>
                  <?php if ($language['code'] == $config_language) { ?>
                  <option value="<?php echo $language['code']; ?>" selected="selected"><?php echo $language['name']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $language['code']; ?>"><?php echo $language['name']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select></td>
            </tr>
            <tr>
              <td><?php echo $entry_admin_language; ?></td>
              <td><select name="config_admin_language">
                  <?php foreach ($languages as $language) { ?>
                  <?php if ($language['code'] == $config_admin_language) { ?>
                  <option value="<?php echo $language['code']; ?>" selected="selected"><?php echo $language['name']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $language['code']; ?>"><?php echo $language['name']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select></td>
            </tr>

          </table>
        </div>
		    <div id="tab-option" class="tab-pane">
			  <h3 class="box-title">Cấu hình vị trí mô đun</h3>
        <span class="help">Chỉ dành cho lập trình viên</span>
    			<table id="positions" class="list">
    				<thead>
    					<tr>
    						<td class="left">Value</td>
    						<td class="left">Text</td>
    						<td></td>
    					</tr>
    				</thead>
    				<?php $position_row = 0; ?>
    				<?php foreach ($config_positions as $config_position) { ?>
    					<tbody id="position-row<?php echo $position_row; ?>">
    						<tr>
    							<td class="left"><input type="text" name="config_position[<?php echo $position_row; ?>][value]" value="<?php echo $config_position['value']; ?>" size="15" /></td>
    							<td class="left"><input type="text" name="config_position[<?php echo $position_row; ?>][text]" value="<?php echo $config_position['text']; ?>" size="15" /></td>
    							<td class="left"><a onclick="$('#position-row<?php echo $position_row; ?>').remove();" class="btn btn-sm btn-default"><i class="fa fa-minus"></i> Xóa</a></td>
    						</tr>
    					</tbody>
    					<?php $position_row++; ?>
    				<?php } ?>
    				<tfoot>
    					<tr>
    						<td colspan="2"></td>
    						<td class="left"><a onclick="addPosition();" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Thêm vị trí</a></td>
    					</tr>
    				</tfoot>
    			</table>
				<h3>Amp</h3>
    			<table class="form">
					<tr>
						<td>Amp page</td>
						<td>
							<div class="clearfix"> <label class="switch"> <input type="checkbox" name="config_amp" <?php echo $config_amp ? 'checked':''; ?> value="1"> <span class="slider round"></span> </label> </div>
						</td>
					</tr>
					<tr>
						<td>Amp sản phẩm</td>
						<td>
							<div class="clearfix"> <label class="switch"> <input type="checkbox" name="config_amp_product" <?php echo $config_amp_product ? 'checked':''; ?> value="1"> <span class="slider round"></span> </label> </div>
						</td>
					</tr>
					<tr>
						<td>Amp tin tức</td>
						<td>
							<div class="clearfix"> <label class="switch"> <input type="checkbox" name="config_amp_news" <?php echo $config_amp_news ? 'checked':''; ?> value="1"> <span class="slider round"></span> </label> </div>
						</td>
					</tr>
					<tr>
						<td>Amp giới thiệu</td>
						<td>
							<div class="clearfix"> <label class="switch"> <input type="checkbox" name="config_amp_information" <?php echo $config_amp_information ? 'checked':''; ?> value="1"> <span class="slider round"></span> </label> </div>
						</td>
					</tr>
    			</table>
				  <div class="box-body">
              <h3 class="box-title">Nghiệp vụ site</h3>
              <div class="form-group">
                  <select name="config_site_type">
                      <option value="">--Bán sản phẩm(mặc định)--</option>
                      <option value="service" <?php echo $config_site_type == 'service' ? 'selected':''; ?>>Dịch vụ</option>
                      <option value="cms" <?php echo $config_site_type == 'cms' ? 'selected':''; ?>>CMS</option>
                      <option value="mixed" <?php echo $config_site_type == 'mixed' ? 'selected':''; ?>>Cả 2</option>
                  </select>
              </div>
          </div>
        </div>
        <div id="tab-ftp" class="tab-pane">
          <h3 class="box-title"><?php echo $tab_ftp; ?></h3>
          <table class="form">
            <tr>
              <td><?php echo $entry_ftp_status; ?></td>
              <td>
                <div class="clearfix">
  								<label class="switch">
  								  <input type="checkbox" name="config_ftp_status" <?php echo $config_ftp_status ? 'checked':''; ?> value="1" />
  								  <span class="slider round"></span>
  								</label>
  					    </div>
              </td>
            </tr>
            <tr>
              <td><?php echo $entry_ftp_host; ?></td>
              <td><input type="text" name="config_ftp_host" value="<?php echo $config_ftp_host; ?>" />
                <?php if ($error_ftp_host) { ?>
                <span class="error"><?php echo $error_ftp_host; ?></span>
                <?php } ?></td>
            </tr>
            <tr>
              <td><?php echo $entry_ftp_port; ?></td>
              <td><input type="text" name="config_ftp_port" value="<?php echo $config_ftp_port; ?>" />
                <?php if ($error_ftp_port) { ?>
                <span class="error"><?php echo $error_ftp_port; ?></span>
                <?php } ?></td>
            </tr>
            <tr>
              <td><?php echo $entry_ftp_username; ?></td>
              <td><input type="text" name="config_ftp_username" value="<?php echo $config_ftp_username; ?>" />
                <?php if ($error_ftp_username) { ?>
                <span class="error"><?php echo $error_ftp_username; ?></span>
                <?php } ?></td>
            </tr>
            <tr>
              <td><?php echo $entry_ftp_password; ?></td>
              <td><input type="text" name="config_ftp_password" value="<?php echo $config_ftp_password; ?>" />
                <?php if ($error_ftp_password) { ?>
                <span class="error"><?php echo $error_ftp_password; ?></span>
                <?php } ?></td>
            </tr>
            <tr>
              <td><?php echo $entry_ftp_root; ?></td>
              <td><input type="text" name="config_ftp_root" value="<?php echo $config_ftp_root; ?>" /></td>
            </tr>
          </table>
        </div>
        <div id="tab-mail" class="tab-pane">
          <h3 class="box-title">Thiết lập Email</h3>
          <table class="form">
            <tr>
              <td><?php echo $entry_mail_protocol; ?></td>
              <td><select name="config_mail_protocol">
                  <?php if ($config_mail_protocol == 'mail') { ?>
                  <option value="mail" selected="selected"><?php echo $text_mail; ?></option>
                  <?php } else { ?>
                  <option value="mail"><?php echo $text_mail; ?></option>
                  <?php } ?>
                  <?php if ($config_mail_protocol == 'smtp') { ?>
                  <option value="smtp" selected="selected"><?php echo $text_smtp; ?></option>
                  <?php } else { ?>
                  <option value="smtp"><?php echo $text_smtp; ?></option>
                  <?php } ?>
                </select></td>
            </tr>
            <tr>
              <td>From (email)</td>
              <td><input type="text" name="config_mail_from" value="<?php echo $config_mail_from; ?>" /></td>
            </tr>
            <tr>
              <td><?php echo $entry_mail_parameter; ?></td>
              <td><input type="text" name="config_mail_parameter" value="<?php echo $config_mail_parameter; ?>" /></td>
            </tr>
            <tr>
              <td><?php echo $entry_smtp_host; ?></td>
              <td><input type="text" name="config_smtp_host" value="<?php echo $config_smtp_host; ?>" /></td>
            </tr>
            <tr>
              <td><?php echo $entry_smtp_username; ?></td>
              <td><input type="text" name="config_smtp_username" value="<?php echo $config_smtp_username; ?>" /></td>
            </tr>
            <tr>
              <td><?php echo $entry_smtp_password; ?></td>
              <td><input type="text" name="config_smtp_password" value="<?php echo $config_smtp_password; ?>" /></td>
            </tr>
            <tr>
              <td><?php echo $entry_smtp_port; ?></td>
              <td><input type="text" name="config_smtp_port" value="<?php echo $config_smtp_port; ?>" /></td>
            </tr>
            <tr>
              <td><?php echo $entry_smtp_timeout; ?></td>
              <td><input type="text" name="config_smtp_timeout" value="<?php echo $config_smtp_timeout; ?>" /></td>
            </tr>
            <tr>
              <td><?php echo $entry_alert_mail; ?></td>
              <td>
                <div class="clearfix">
                <label class="switch">
                  <input type="checkbox" name="config_order_email" <?php echo $config_order_email ? 'checked':''; ?> value="1" />
                  <span class="slider round"></span>
                </label>
              </div>
              </td>
            </tr>
            <tr>
              <td><?php echo $entry_alert_emails; ?></td>
              <td><textarea name="config_alert_emails" cols="18" rows="5"><?php echo $config_alert_emails; ?></textarea></td>
            </tr>
          </table>
        </div>
		<div id="tab-server" class="tab-pane">
        <h3 class="box-title"><?php echo $tab_server; ?></h3>
          <table class="form">
            <tr>
              <td><?php echo $entry_secure; ?></td>
              <td>
                <div class="clearfix">
                  <label class="switch">
                    <input type="checkbox" name="config_secure" <?php echo $config_secure ? 'checked':''; ?> value="1" />
                    <span class="slider round"></span>
                  </label>
                </div>
            </td>
            </tr>
            <tr>
              <td>Captcha on login?</td>
              <td>
                <div class="clearfix">
                  <label class="switch">
                    <input type="checkbox" name="config_login_captcha" <?php echo $config_login_captcha ? 'checked':''; ?> value="1" />
                    <span class="slider round"></span>
                  </label>
                </div>
            </td>
            </tr>
            <tr>
              <td>Copy protected?</td>
              <td>
                <div class="clearfix">
                  <label class="switch">
                    <input type="checkbox" name="config_copy_protect" <?php echo $config_copy_protect ? 'checked':''; ?> value="1" />
                    <span class="slider round"></span>
                  </label>
                </div>
            </td>
            </tr>
            <tr>
              <td>Full-text search</td>
              <td>
                <input onclick="open_()" type="radio" name="config_fts" value="1" <?php echo $config_fts ? 'checked':''; ?>/>
                <?php echo $text_yes; ?>
                <input onclick="open_()" type="radio" name="config_fts" value="0" <?php echo !$config_fts ? 'checked':''; ?>/>
                <?php echo $text_no; ?>
                </td>
            </tr>
			<tr id="fts" style="display:none">
				<td>Reindex full-text search</td>
				<td>
				<div class="scrollbox" style="margin-bottom: 5px;">

                <?php $class = 'odd'; ?>
                <?php foreach ($tables as $table) { ?>

				<?php if (strpos($table, '_description') !== false){?>

                <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                <div class="<?php echo $class; ?>">
                  <input type="checkbox" name="backup[]" value="<?php echo $table; ?>" <?php echo (strpos($table, 'news_description') !== false) || (strpos($table, 'product_description') !== false) || (strpos($table, 'information_description') !== false) ? 'checked="checked"':'' ?> />
                  <?php echo $table; ?></div>
                <?php } ?>
                <?php } ?>

			  </div>
              <a onclick="$(this).parent().find(':checkbox').attr('checked', true);">Chọn tất cả</a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);">Bỏ chọn tất cả</a><br /><br /><a class="button" onclick="execute()">Thực hiện</a>
			  </td>
            </tr>
<script type="text/javascript">
var val = document.querySelector('input[name="config_fts"]:checked').value;
if(val == 1){
	document.getElementById("fts").style.display = "table-row";
}else{
	document.getElementById("fts").style.display = "none";
}
function open_(){
	var val = document.querySelector('input[name="config_fts"]:checked').value;
	if(val == 1){
		document.getElementById("fts").style.display = "table-row";
	}else{
		document.getElementById("fts").style.display = "none";
	}
}
function execute() {
    $.ajax({
    	type: "POST",
      	url: 'index.php?route=setting/setting/indexsearch&token=<?php echo $token; ?>',
      	data: $("#form").serialize(),
      	dataType: 'json',
      	beforeSend: function() {
	      // alert("Bắt đầu index!");
      	},
      	complete: function() {

      	},
      	success: function(json) {
			if(json['key']=='ok'){
				$('#alert').append('<div class="success">'+json['notify']+'</div>').show();
			}else{
				$('#alert').append('<div class="warning">'+json['notify']+'</div>').show();
			}

      	}
    });
}
</script>

            <tr>
              <td><?php echo $entry_robots; ?></td>
              <td><textarea name="config_robots" cols="28" rows="5"><?php echo $config_robots; ?></textarea></td>
            </tr>
            <tr>
              <td><?php echo $entry_file_extension_allowed; ?></td>
              <td><textarea name="config_file_extension_allowed" cols="28" rows="5"><?php echo $config_file_extension_allowed; ?></textarea></td>
            </tr>
            <tr>
              <td><?php echo $entry_file_mime_allowed; ?></td>
              <td><textarea name="config_file_mime_allowed" cols="28" rows="5"><?php echo $config_file_mime_allowed; ?></textarea></td>
            </tr>
            <tr>
              <td><?php echo $entry_maintenance; ?></td>
              <td>
                <div class="form-group clearfix">
  								<label class="switch">
  								  <input type="checkbox" name="config_maintenance" <?php echo $config_maintenance ? 'checked':''; ?> value="1" />
  								  <span class="slider round"></span>
  								</label>
  					     </div>
               </td>
            </tr>
            <tr>
              <td><?php echo $entry_password; ?></td>
              <td>
                <div class="form-group clearfix">
  								<label class="switch">
  								  <input type="checkbox" name="config_password" <?php echo $config_password ? 'checked':''; ?> value="1" />
  								  <span class="slider round"></span>
  								</label>
  					     </div>
              </td>
            </tr>
            <tr>
              <td><?php echo $entry_encryption; ?></td>
              <td><input type="text" name="config_encryption" value="<?php echo $config_encryption; ?>" />
                <?php if ($error_encryption) { ?>
                <span class="error"><?php echo $error_encryption; ?></span>
                <?php } ?></td>
            </tr>
            <tr>
              <td><?php echo $entry_compression; ?></td>
              <td><input type="text" name="config_compression" value="<?php echo $config_compression; ?>" size="3" /></td>
            </tr>
            <tr>
              <td><?php echo $entry_error_display; ?></td>
              <td>
                <div class="form-group clearfix">
  								<label class="switch">
  								  <input type="checkbox" name="config_error_display" <?php echo $config_error_display ? 'checked':''; ?> value="1" />
  								  <span class="slider round"></span>
  								</label>
  					     </div>
                </td>
            </tr>
            <tr>
              <td><?php echo $entry_error_log; ?></td>
              <td>
                <div class="form-group clearfix">
  								<label class="switch">
  								  <input type="checkbox" name="config_error_log" <?php echo $config_error_log ? 'checked':''; ?> value="1" />
  								  <span class="slider round"></span>
  								</label>
  					     </div>
                </td>
            </tr>
            <tr>
              <td><span class="required">*</span> <?php echo $entry_error_filename; ?></td>
              <td><input type="text" name="config_error_filename" value="<?php echo $config_error_filename; ?>" />
                <?php if ($error_error_filename) { ?>
                <span class="error"><?php echo $error_error_filename; ?></span>
                <?php } ?></td>
            </tr>
          </table>
        </div>
        </div>
      </form>
      </div>
    </div>
  </div>
</div>
</div>
<script type="text/javascript" src="view/javascript/ckeditor/ckeditor.js"></script>
<script type="text/javascript"><!--
CKEDITOR.replace('config_giayphep', {
	filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
});
//--></script>

<script type="text/javascript"><!--
$('select[name=\'config_country_id\']').bind('change', function() {
	$.ajax({
		url: 'index.php?route=setting/setting/country&token=<?php echo $token; ?>&country_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('select[name=\'country_id\']').after('<span class="wait">&nbsp;<img src="view/image/loading.gif" alt="" /></span>');
		},
		complete: function() {
			$('.wait').remove();
		},
		success: function(json) {
			if (json['postcode_required'] == '1') {
				$('#postcode-required').show();
			} else {
				$('#postcode-required').hide();
			}

			html = '<option value=""><?php echo $text_select; ?></option>';

			if (json['zone'] != '') {
				for (i = 0; i < json['zone'].length; i++) {
        			html += '<option value="' + json['zone'][i]['zone_id'] + '"';

					if (json['zone'][i]['zone_id'] == '<?php echo $config_zone_id; ?>') {
	      				html += ' selected="selected"';
	    			}

	    			html += '>' + json['zone'][i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
			}

			$('select[name=\'config_zone_id\']').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('select[name=\'config_country_id\']').trigger('change');
//--></script>
<script type="text/javascript"><!--
function image_upload(field, thumb) {
	$('#dialog').remove();

	$('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token; ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');

	$('#dialog').dialog({
		title: '<?php echo $text_image_manager; ?>',
		close: function (event, ui) {
			if ($('#' + field).attr('value')) {
				$.ajax({
					url: 'index.php?route=common/filemanager/image&token=<?php echo $token; ?>&image=' + encodeURIComponent($('#' + field).val()),
					dataType: 'text',
					success: function(data) {
						$('#' + thumb).replaceWith('<img src="' + data + '" alt="" id="' + thumb + '" />');
					}
				});
			}
		},
		bgiframe: false,
		width: 960,
		height: 550,
		resizable: false,
		modal: false,
    dialogClass: 'dlg'
	});
};
//--></script>
<script type="text/javascript"><!--
    var position_row = <?php echo $position_row; ?>;
    function addPosition() {
        html = '<tbody id="image-row' + position_row + '">';
        html += '  <tr>';
        html += '    <td class="left"><input type="text" name="config_position[' + position_row + '][value]" value="" size="15" /></td>';
        html += '    <td class="left"><input type="text" name="config_position[' + position_row + '][text]" value=""  size="15"/></td>';
        html += '    <td class="left"><a onclick="$(\'#image-row' + position_row + '\').remove();" class="btn btn-sm btn-default"><i class="fa fa-minus"></i> Xóa</a></td>';
        html += '  </tr>';
        html += '</tbody>';

        $('#positions tfoot').before(html);

        position_row++;
    }
//--></script>
<?php echo $footer; ?>
