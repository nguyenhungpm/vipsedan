<?php echo $header; ?>
<div class="content-wrapper">
  <section class="content-header">
		<h1><?php echo $heading_title; ?><small>(Chi tiết)</small></h1>
		<ol class="breadcrumb">
			<?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
				<li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
			<?php $k++; } ?>
		</ol>
	</section>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="content">
    <div class="box box-default">
      <div class="box-header">
        <div class="pull-right">
      		<a href="<?php echo $cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i> <?php echo $button_cancel; ?></a>
      		<a onclick="$('#form').submit();" class="btn btn-primary"><i class="fa fa-check" aria-hidden="true"></i> <?php echo $button_save; ?></a>
	     </div>
    </div>
    <div class="box-body  ">
      <form action="<?php echo $action; ?>" method="post" id="form">
        <table class="form">
		<?php foreach($urls as $type => $url) { ?>
          <tr>
            <td><span class="required">*</span> <?php echo ${"entry_{$type}_url"}; ?></td>
            <td><input type="text" name="url[<?php echo $type; ?>]" value="<?php echo $url; ?>" style="width:650px" />
              <?php if (isset($error_url[$type])) { ?>
              <span class="error"><?php echo $error_url[$type]; ?></span>
              <?php } ?></td>
          </tr>
		 <?php } ?>
          <tr>
            <td><?php echo $entry_status; ?></td>
            <td><div class="form-group clearfix">
								<label class="switch pull">
								  <input type="checkbox" name="status" <?php echo $status ? 'checked':''; ?> value="1">
								  <span class="slider round"></span>
								</label>
					</div></td>
          </tr>
        </table>
      </form>
    </div>
  </div>
</div>
</div>
<?php echo $footer; ?>
