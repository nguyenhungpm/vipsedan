<?php echo $header; ?>
<link rel="stylesheet" href="//cdn.jsdelivr.net/gh/osdvn/mkmate/finder/css/elfinder.min.css" />
<script src="//cdn.jsdelivr.net/gh/osdvn/mkmate/finder/js/elfinder.min.js"></script>
<script src="asset/initck5.js"></script>
<div class="content-wrapper" id="content">
	<section class="content-header">
		<h1><?php echo $heading_title; ?><small>(<?php echo $text_page_details; ?>)</small></h1>
		<ol class="breadcrumb">
			<?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
				<li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
			<?php $k++; } ?>
		</ol>
	</section>
	<?php if (isset($success)) { ?>
		<div class="success"><?php echo $success; ?></div>
	<?php } ?>
	<?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
	<?php } ?>
  <div class="content">
		<div class="box box-default">
			<div class="box-header">
				<div class="pull-right">
					<a class="btn btn-default" href="<?php echo $cancel; ?>"><i class="fa fa-reply"></i> <?php echo $button_cancel; ?></a>
					<a class="btn btn-primary" onclick="$('#form').submit();"><i class="fa fa-check" aria-hidden="true"></i> <?php echo $button_save; ?></a>
				</div>
			</div>
    	<div class="box-body">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="form">
          <tr>
            <td><span class="required">*</span> <label class="control-label"><?php echo $entry_name; ?></label></td>
            <td><input class="form-control" type="text" name="name" value="<?php echo $name; ?>" size="100" />
              <?php if ($error_name) { ?>
              <span class="error"><?php echo $error_name; ?></span>
              <?php } ?></td>
          </tr>
          <tr>
            <td><label class="control-label"><?php echo $entry_status; ?></label></td>
            <td><div class="form-group clearfix">
								<label class="switch pull">
								  <input type="checkbox" name="status" <?php echo $status ? 'checked':''; ?> value="1">
								  <span class="slider round"></span>
								</label>
				</div>
			</td>
          </tr>
        </table>
        <table id="images" class="list">
          <thead>
            <tr>
              <td class="left"><label class="control-label"><?php echo $entry_title; ?></label></td>
              <td class="left"><?php echo $entry_description; ?></td>
              <td class="left"><?php echo $entry_link; ?></td>
              <td class="left"><?php echo $entry_image; ?></td>
							<td class="left">Ảnh cho mobile</td>
              <td></td>
            </tr>
          </thead>
          <?php $image_row = 0; ?>
          <?php foreach ($banner_images as $banner_image) { ?>
          <tbody id="image-row<?php echo $image_row; ?>">
            <tr>
			<td class="left"><?php foreach ($languages as $language) { ?>
                <input type="text" class="form-control" name="banner_image[<?php echo $image_row; ?>][banner_image_description][<?php echo $language['language_id']; ?>][title]" value="<?php echo isset($banner_image['banner_image_description'][$language['language_id']]['title']) ? $banner_image['banner_image_description'][$language['language_id']]['title'] : ''; ?>" />
                <img <?php echo count($languages) ==1 ? 'class="hide"':''; ?> src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />
                <?php if (isset($error_banner_image[$image_row][$language['language_id']])) { ?>
                <span class="error"><?php echo $error_banner_image[$image_row][$language['language_id']]; ?></span>
                <?php } ?>
                <?php } ?></td>
			<td class="left"><?php foreach ($languages as $language) { ?>
                <textarea type="text" class="form-control" name="banner_image[<?php echo $image_row; ?>][banner_image_description][<?php echo $language['language_id']; ?>][description]"><?php echo isset($banner_image['banner_image_description'][$language['language_id']]['description']) ? $banner_image['banner_image_description'][$language['language_id']]['description'] : ''; ?></textarea>
                <img <?php echo count($languages) ==1 ? 'class="hide"':''; ?> src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />
                <?php if (isset($error_banner_image[$image_row][$language['language_id']])) { ?>
                <span class="error"><?php echo $error_banner_image[$image_row][$language['language_id']]; ?></span>
                <?php } ?>
                <?php } ?></td>
           <?php $link_row = 0; ?>
           <td class="left"><?php foreach ($languages as $language) { ?>
            <input type="text" class="form-control" name="banner_image[<?php echo $image_row; ?>][banner_image_description][<?php echo $language['language_id']; ?>][link]" value="<?php echo (isset($banner_image['banner_image_description'][$language['language_id']]['link'])) ? $banner_image['banner_image_description'][$language['language_id']]['link'] : ''; ?>" />
            <img <?php echo count($languages) ==1 ? 'class="hide"':''; ?> src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />
            <?php $link_row++; ?>
           <?php } ?>
           </td>
          <td class="left">
		           <?php $language_row = 0; ?>
		           <?php foreach ($languages as $language) { ?>
		           <div class="image"><img src="<?php echo $banner_image['banner_image_description'][$language['language_id']]['thumb']; ?>" alt="" id="thumb<?php echo $image_row; ?><?php echo $language_row; ?>" />
		                  <input type="hidden" name="banner_image[<?php echo $image_row; ?>][banner_image_description][<?php echo $language['language_id']; ?>][image]" value="<?php echo (isset($banner_image['banner_image_description'][$language['language_id']]['image'])) ? $banner_image['banner_image_description'][$language['language_id']]['image'] : ''; ?>" id="image<?php echo $image_row; ?><?php echo $language_row; ?>"  />
		                  <br />
		                  <a onclick="filebrowser('image<?php echo $image_row; ?><?php echo $language_row; ?>', 'thumb<?php echo $image_row; ?><?php echo $language_row; ?>');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#thumb<?php echo $image_row; ?><?php echo $language_row; ?>').attr('src', '<?php echo $no_image; ?>'); $('#image<?php echo $image_row; ?><?php echo $language_row; ?>').attr('value', '');"><?php echo $text_clear; ?></a>
		              <img <?php echo count($languages) ==1 ? 'class="hide"':''; ?> src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />
		           </div>
		           <?php $language_row++; ?>
		           <?php } ?>
              </td>
							<td class="left">
				           <?php $language_row = 0; ?>
				           <?php foreach ($languages as $language) { ?>
				           <div class="image"><img src="<?php echo $banner_image['banner_image_description'][$language['language_id']]['thumb_small']; ?>" alt="" id="thumb_small<?php echo $image_row; ?><?php echo $language_row; ?>" />
				                  <input type="hidden" name="banner_image[<?php echo $image_row; ?>][banner_image_description][<?php echo $language['language_id']; ?>][small]" value="<?php echo (isset($banner_image['banner_image_description'][$language['language_id']]['small'])) ? $banner_image['banner_image_description'][$language['language_id']]['small'] : ''; ?>" id="small<?php echo $image_row; ?><?php echo $language_row; ?>"  />
				                  <br />
				                  <a onclick="filebrowser('small<?php echo $image_row; ?><?php echo $language_row; ?>', 'thumb_small<?php echo $image_row; ?><?php echo $language_row; ?>');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#thumb_small<?php echo $image_row; ?><?php echo $language_row; ?>').attr('src', '<?php echo $no_image; ?>'); $('#image<?php echo $image_row; ?><?php echo $language_row; ?>').attr('value', '');"><?php echo $text_clear; ?></a>
				              <img <?php echo count($languages) ==1 ? 'class="hide"':''; ?> src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />
				           </div>
				           <?php $language_row++; ?>
				           <?php } ?>
		              </td>
              <td class="left"><a class="btn btn-primary" onclick="$('#image-row<?php echo $image_row; ?>').remove();" class="btn btn-sm btn-default"><i class="fa fa-minus"></i></a></td>
            </tr>
          </tbody>
          <?php $image_row++; ?>
          <?php } ?>
          <tfoot>
            <tr>
              <td colspan="5"></td>
              <td class="left"><a class="btn btn-primary" onclick="addImage();" class="btn btn-primary"><i class="fa fa-plus"></i> <?php echo $button_add_banner; ?></a></td>
            </tr>
          </tfoot>
        </table>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
var image_row = <?php echo $image_row; ?>;

function addImage() {
    html  = '<tbody id="image-row' + image_row + '">';
	html += '<tr>';
    html += '<td class="left">';
	<?php foreach ($languages as $language) { ?>
	html += '<input class="form-control" type="text" name="banner_image[' + image_row + '][banner_image_description][<?php echo $language['language_id']; ?>][title]" value="" /> <img <?php echo count($languages) ==1 ? 'class="hide"':''; ?> src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />';
    <?php } ?>
	html += '</td>';
    html += '<td class="left">';
	<?php foreach ($languages as $language) { ?>
	html += '<textarea class="form-control" name="banner_image[' + image_row + '][banner_image_description][<?php echo $language['language_id']; ?>][description]"></textarea> <img <?php echo count($languages) ==1 ? 'class="hide"':''; ?> src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />';
    <?php } ?>
	html += '</td>';
    html += '<td class="left">';
	<?php foreach ($languages as $language) { ?>
	html += '<input class="form-control" type="text" name="banner_image[' + image_row + '][banner_image_description][<?php echo $language['language_id']; ?>][link]" value="" /><img <?php echo count($languages) ==1 ? 'class="hide"':''; ?> src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />';
    <?php } ?>
	html += '</td>';
    html += '<td class="left">';
    <?php $language_row = 0; ?>
	<?php foreach ($languages as $language) { ?>
	html += '<div class="image"><img src="<?php echo $no_image; ?>" alt="" id="thumb' + image_row + '<?php echo $language_row; ?>" />';
	html += '<input type="hidden" name="banner_image[' + image_row + '][banner_image_description][<?php echo $language['language_id']; ?>][image]" value="" id="image' + image_row + '<?php echo $language_row; ?>" /><br />';
	html += '<a onclick="filebrowser(\'image' + image_row + '<?php echo $language_row; ?>\', \'thumb' + image_row + '<?php echo $language_row; ?>\');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$(\'#thumb' + image_row + '<?php echo $language_row; ?>\').attr(\'src\', \'<?php echo $no_image; ?>\'); $(\'#image' + image_row + '<?php echo $language_row; ?>\').attr(\'value\', \'\');"><?php echo $text_clear; ?></a>';
	html += '<img <?php echo count($languages) ==1 ? 'class="hide"':''; ?> src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br /></div>';
	<?php $language_row++; ?>
    <?php } ?>
	html += '</td>';
	html += '<td class="left">';
	<?php $language_row = 0; ?>
<?php foreach ($languages as $language) { ?>
html += '<div class="image"><img src="<?php echo $no_image; ?>" alt="" id="thumb_small' + image_row + '<?php echo $language_row; ?>" />';
html += '<input type="hidden" name="banner_image[' + image_row + '][banner_image_description][<?php echo $language['language_id']; ?>][small]" value="" id="small' + image_row + '<?php echo $language_row; ?>" /><br />';
html += '<a onclick="filebrowser(\'small' + image_row + '<?php echo $language_row; ?>\', \'thumb_small' + image_row + '<?php echo $language_row; ?>\');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$(\'#thumb_small' + image_row + '<?php echo $language_row; ?>\').attr(\'src\', \'<?php echo $no_image; ?>\'); $(\'#small' + image_row + '<?php echo $language_row; ?>\').attr(\'value\', \'\');"><?php echo $text_clear; ?></a>';
html += '<img <?php echo count($languages) ==1 ? 'class="hide"':''; ?> src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br /></div>';
<?php $language_row++; ?>
	<?php } ?>
html += '</td>';
	html += '<td class="left"><a class="btn btn-default" onclick="$(\'#image-row' + image_row  + '\').remove();"><i class="fa fa-trash"></i> <?php echo $button_remove; ?></a></td>';
	html += '</tr>';
	html += '</tbody>';

	$('#images tfoot').before(html);

	image_row++;
}
//--></script>

<?php echo $footer; ?>
