<?php echo $header; ?>
<div class="content-wrapper">
	<section class="content-header">
		<h1><?php echo $heading_title; ?><small>(<?php echo $name; ?>)</small></h1>
		<ol class="breadcrumb">
			<?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
				<li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
			<?php $k++; } ?>
		</ol>
	</section>
	<?php if ($error_warning) { ?>
	<div class="warning"><?php echo $error_warning; ?></div>
	<?php } ?>
	<div class="content">
		<div class="box box-default">
			<div class="box-header">
				<div class="pull-right">
					<a href="<?php echo $cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i> <?php echo $button_cancel; ?></a>
					<a onclick="$('#form').submit();" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> <?php echo $button_save; ?></a>
				</div>
			</div>
		</div>
    <div class="">
		<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
		<div class="row">
			<div class="col-md-5">
				<div class="box box-danger">
					<div class="box-body">
						<div class="form-group required">
							<label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
							<input type="text" name="name" value="<?php echo $name; ?>" placeholder="Tên bố cục:" id="input-name" class="form-control">
							<?php if ($error_name) { ?>
							<span class="text-danger"><?php echo $error_name; ?></span>
							<?php } ?>
						</div>
						<table id="route" class="list">
						  <thead>
							<tr>
							  <td class="left hide"><?php echo $entry_store; ?></td>
							  <td class="left">Route</td>
							  <td></td>
							</tr>
						  </thead>
						  <?php $route_row = 0; ?>
						  <?php foreach ($layout_routes as $layout_route) { ?>
						  <tbody id="route-row<?php echo $route_row; ?>">
							<tr>
							  <td class="left hide"><select name="layout_route[<?php echo $route_row; ?>][store_id]">
								  <option value="0"><?php echo $text_default; ?></option>
								  <?php foreach ($stores as $store) { ?>
								  <?php if ($store['store_id'] == $layout_route['store_id']) { ?>
								  <option value="<?php echo $store['store_id']; ?>" selected="selected"><?php echo $store['name']; ?></option>
								  <?php } else { ?>
								  <option value="<?php echo $store['store_id']; ?>"><?php echo $store['name']; ?></option>
								  <?php } ?>
								  <?php } ?>
								</select></td>
							  <td class="left"><input class="form-control" type="text" name="layout_route[<?php echo $route_row; ?>][route]" value="<?php echo $layout_route['route']; ?>" /></td>
							  <td class="left"><a onclick="$('#route-row<?php echo $route_row; ?>').remove();" class="btn btn-default btn-sm"><?php echo $button_remove; ?></a></td>
							</tr>
						  </tbody>
						  <?php $route_row++; ?>
						  <?php } ?>
						  <tfoot>
							<tr>
							  <td colspan="1"></td>
							  <td class="left"><a onclick="addRoute();" class="btn btn-primary"><i class="fa fa-plus"></i> <?php echo $button_add_route; ?></a></td>
							</tr>
						  </tfoot>
						</table>
					</div>
				</div>
			</div>
			<div class="col-md-7">
				<div class="box box-danger">
					<div class="box-header">
						<h3 class="box-title">Modules</h3>
						<a onclick="addModule();" class="btn btn-primary pull-right hide">add module</a>
					</div>
					<div class="box-body">
						<table id="module-list" class="list">
						  <thead>
							<tr>
							  <td class="text-center">Sort Order</td>
							  <td class="left">Position</td>
							  <td class="text-center">Module</td>
							</tr>
						  </thead>
						  <tbody>
							<?php $module_row = 0; if(isset($layouts)){foreach ($layouts as $layout_module) { ?>
							<tr id="module-row<?php echo $module_row; ?>">
								<td class="text-center">
									<?php echo $layout_module['sort_order']; ?>
									<input class="text-center hide" type="text" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" size="3"	/>
								</td>
								<td class="left">
								<?php foreach($positions as $position){ ?>
									<?php echo $position['value']==$layout_module['position'] ? $position['text'] : ''; ?>
								<?php } ?>
								<select name="layout_module[<?php echo $module_row; ?>][position]" class="form-control input-sm hide">
								<?php foreach($positions as $position){ ?>
									<option value="<?php echo $position['value']; ?>" <?php echo $position['value']==$layout_module['position'] ? 'selected':''; ?>><?php echo $position['text']; ?></option>
								<?php } ?>
								</select></td>
							  <td class="text-left"><div class="">
									<?php foreach ($extensions as $extension) { ?>
									<?php echo $extension['module'] == $layout_module['module'] ? $extension['name'] . ' (<a target="_blank" href="index.php?route=module/'. $extension['code'] .'&token='. $this->session->data['token'] .'">Sửa</a>)':'';?>
									<?php } ?>
								  <select name="layout_module[<?php echo $module_row; ?>][module]" class="form-control input-sm hide">
									<?php foreach ($extensions as $extension) { ?>
									<option value="<?php echo $extension['module']; ?>" <?php echo $extension['module'] == $layout_module['module'] ? 'selected':'';?>><?php echo $extension['name']; ?></option>
									<?php } ?>
								  </select>
								  <div class="input-group-btn">
									<button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="Xóa module" class="btn btn-danger btn-sm hide"><i class="fa fa fa-minus-circle"></i></button>
								  </div>
								</div>
							  </td>
							</tr>
							<?php $module_row++; ?>
							<?php }} ?>
						  </tbody>
						</table>
					  
					</div>
				</div>
			</div>
        
		</div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
var route_row = <?php echo $route_row; ?>;

function addRoute() {
	html  = '<tbody id="route-row' + route_row + '">';
	html += '  <tr>';
	html += '    <td class="left hide"><select name="layout_route[' + route_row + '][store_id]">';
	html += '    <option value="0"><?php echo $text_default; ?></option>';
	<?php foreach ($stores as $store) { ?>
	html += '<option value="<?php echo $store['store_id']; ?>"><?php echo addslashes($store['name']); ?></option>';
	<?php } ?>   
	html += '    </select></td>';
	html += '    <td class="left"><input class="form-control" type="text" name="layout_route[' + route_row + '][route]" value="" /></td>';
	html += '    <td class="left"><a onclick="$(\'#route-row' + route_row + '\').remove();" class="btn btn-default"><i class="fa fa-trash"></i> <?php echo $button_remove; ?></a></td>';
	html += '  </tr>';
	html += '</tbody>';
	
	$('#route > tfoot').before(html);
	
	route_row++;
}
var module_row = <?php echo $module_row; ?>;
function addModule() {
	html  = '<tr id="module-row' + module_row + '">';
    html += '  <td class="text-center">';
	html += '  <input class="text-center" type="text" name="layout_module[' + module_row + '][sort_order]" value="0" size="3"	/>';
	html += '</td>';
	html += '  <td class="left"><select name="layout_module[' + module_row + '][position]" class="form-control input-sm">';
    <?php foreach($positions as $position){ ?>
	html += '    <option value="<?php echo $position['value']; ?>"><?php echo $position['text']; ?></option>';
    <?php } ?>
	html += '</select></td>';
    html += '  <td class="left"><div class=""><select name="layout_module[' + module_row + '][code]" class="form-control input-sm">';
	<?php foreach ($extensions as $extension) { ?>
	html += '<option value="<?php echo $extension['module']; ?>"><?php echo $extension['name']; ?></option>';
	<?php } ?>
	html += '  </select>';
	html += '  <div class="input-group-btn"><button type="button" onclick="$(\'#module-row' + module_row + '\').remove();" data-toggle="tooltip" title="Xóa module" class="btn btn-danger btn-sm hide"><i class="fa fa fa-minus-circle"></i></button></div></div></td>';
	html += '</tr>';
	
	$('#module-list tbody').append(html);
		
	module_row++;
}
//--></script> 
<?php echo $footer; ?>