<?php echo $header; ?>
<div class="content-wrapper">
  <section class="content-header">
    <h1><?php echo $heading_title; ?><small>(Danh sách)</small></h1>
    <ol class="breadcrumb">
      <?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
        <li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
      <?php $k++; } ?>
    </ol>
  </section> 
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="content">
    <div class="box box-default">
    <div class="box-header">
      <div class="pull-right"><a href="<?php echo $insert; ?>" class="btn btn-primary"><?php echo $button_insert; ?></a></div>
      <div class="pull-left"><a onclick="$('form').submit();" class="btn btn-default btn-sm"><?php echo $button_delete; ?></a></div>
    </div>
    <div class="box-body">
      <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="table table-bordered table-hover dataTable">
          <thead>
            <tr>
              <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
              <td class="left">Old URL</td>
              <td class="left">New URL</td>
              <td class="right"><?php echo $column_action; ?></td>
            </tr>
          </thead>
          <tbody>
            <?php if ($redirects) { ?>
            <?php foreach ($redirects as $redirect) { ?>
            <tr>
              <td style="text-align: center;"><?php if ($redirect['selected']) { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $redirect['redirect_id']; ?>" checked="checked" />
                <?php } else { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $redirect['redirect_id']; ?>" />
                <?php } ?></td>
              <td class="left"><?php echo $redirect['old_url']; ?></td>
              <td class="left"><?php echo $redirect['new_url']; ?></td>
              <td class="right"><?php foreach ($redirect['action'] as $action) { ?>
                [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                <?php } ?></td>
            </tr>
            <?php } ?>
            <?php } else { ?>
            <tr>
              <td class="center" colspan="4"><?php echo $text_no_results; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </form>
      <div class="pagination"><?php echo $pagination; ?></div>
    </div>
  </div>
</div>
</div>
<?php echo $footer; ?>