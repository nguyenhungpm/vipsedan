<?php

class ControllerCatalogProduct extends Controller {

	private $error = array();

	public function index() {
		$this->language->load('catalog/product');
		$this->data['button_preview']           = $this->language->get('button_preview');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/product');

		$this->getList();
	}


	public function updatestatus() {
		$this->load->language('catalog/product');
		$this->load->model('catalog/product');
		$output='';
		if(isset($this->request->get['object_id'])){
			$get_request = explode('-',$this->request->get['object_id']);
			if(count($get_request)==2){
				$column_name = $get_request[0];
				$product_id = $get_request[1];
				$result = $this->model_catalog_product->getProduct($product_id);
				if($result[$column_name]){
					$this->model_catalog_product->updateStatus($product_id, $column_name, 0);
					} else {
					$this->model_catalog_product->updateStatus($product_id, $column_name, 1);
				}
				$result = $this->model_catalog_product->getProduct($product_id);
				$output = $result[$column_name] ? $this->language->get('text_enabled') : $this->language->get('text_disabled');
			}
		}
		$this->response->setOutput($output);
	}

	public function insert() {
		$this->language->load('catalog/product');

		$this->document->setTitle($this->language->get('heading_title_add'));

		$this->load->model('catalog/product');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->response->ClearCache();
			$product_id = $this->model_catalog_product->addProduct
			($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_model'])) {
				$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_price'])) {
				$url .= '&filter_price=' . $this->request->get['filter_price'];
			}

			if (isset($this->request->get['filter_quantity'])) {
				$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}


			if (!isset($this->request->get['continue']) && !isset($this->request->get['new'])) {

				$this->redirect($this->url->link('catalog/product', 'token=' . $this->session->data['token'] . $url, 'SSL'));

			}
			elseif (isset($this->request->get['continue'])) {
				$this->redirect($this->url->link('catalog/product/update', 'token=' . $this->session->data['token'] . $url . '&product_id=' . $product_id . '&continue=true', 'SSL'));
			}
			else {
				$this->redirect($this->url->link('catalog/product/insert', 'token=' . $this->session->data['token'] . '&new=true', 'SSL'));
			}

		}

		$this->getForm();
	}

	public function update() {
		$this->language->load('catalog/product');

		$this->load->model('catalog/product');
		if (isset($this->request->get['product_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$news_info = $this->model_catalog_product->getNameProduct($this->request->get['product_id']);
			$this->document->setTitle($news_info['name']);
			} else {
			$this->document->setTitle($this->language->get('heading_title'));
		}


		$this->load->model('catalog/product');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

			$this->response->ClearCache();

			$this->model_catalog_product->editProduct($this->request->get['product_id'], $this->request->post);


			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_model'])) {
				$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_price'])) {
				$url .= '&filter_price=' . $this->request->get['filter_price'];
			}

			if (isset($this->request->get['filter_quantity'])) {
				$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}


			if (!isset($this->request->get['continue']) && !isset($this->request->get['new'])) {

				$this->redirect($this->url->link('catalog/product', 'token=' . $this->session->data['token'] . $url, 'SSL'));

			}
			elseif (isset($this->request->get['new'])) {
				$this->redirect($this->url->link('catalog/product/insert', 'token=' . $this->session->data['token'] . '&new=true', 'SSL'));
			}

		}

		$this->getForm();
	}

	public function delete() {
		$this->language->load('catalog/product');
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/product');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $product_id) {
				$this->model_catalog_product->deleteProduct($product_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_model'])) {
				$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_price'])) {
				$url .= '&filter_price=' . $this->request->get['filter_price'];
			}

			if (isset($this->request->get['filter_quantity'])) {
				$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/product', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	public function copy() {
		$this->language->load('catalog/product');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/product');

		if (isset($this->request->post['selected']) && $this->validateCopy()) {
			foreach ($this->request->post['selected'] as $product_id) {
				$this->model_catalog_product->copyProduct($product_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_model'])) {
				$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_price'])) {
				$url .= '&filter_price=' . $this->request->get['filter_price'];
			}

			if (isset($this->request->get['filter_quantity'])) {
				$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/product', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
			} else {
			$filter_name = null;
		}

		if (isset($this->request->get['filter_model'])) {
			$filter_model = $this->request->get['filter_model'];
			} else {
			$filter_model = null;
		}

		if (isset($this->request->get['filter_price'])) {
			$filter_price = $this->request->get['filter_price'];
			} else {
			$filter_price = null;
		}

		if (isset($this->request->get['filter_quantity'])) {
			$filter_quantity = $this->request->get['filter_quantity'];
			} else {
			$filter_quantity = null;
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
			} else {
			$filter_status = null;
		}

		if (isset($this->request->get['filter_category'])) {
			$this->data['filter_category'] = $this->request->get['filter_category'];
			$filter_category = $this->request->get['filter_category'];
			} else {
			$this->data['filter_category'] = null;
			$filter_category = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
			} else {
			$sort = 'pd.name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
			} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
			} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/product', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->data['insert'] = $this->url->link('catalog/product/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['copy'] = $this->url->link('catalog/product/copy', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('catalog/product/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$this->data['products'] = array();

		$data = array(
		'filter_name' => $filter_name,
		'display_name' => true,
		'filter_model' => $filter_model,
		'filter_price' => $filter_price,
		'filter_quantity' => $filter_quantity,
		'filter_status' => $filter_status,
		'filter_category' => $filter_category,
		'sort' => $sort,
		'order' => $order,
		'start' => ($page - 1) * $this->config->get('config_admin_limit'),
		'limit' => $this->config->get('config_admin_limit')
		);

		$this->load->model('tool/image');
		$this->load->model('catalog/category');

		$this->data['categories'] = $this->model_catalog_category->getCategories(NULL);
		$product_total = $this->model_catalog_product->getTotalProducts($data);

		$results = $this->model_catalog_product->getProducts($data);

		foreach ($results as $result) {
			$action = array();

			$action[] = array(
			'text' => $this->language->get('text_edit'),
			'href' => $this->url->link('catalog/product/update', 'token=' . $this->session->data['token'] . '&product_id=' . $result['product_id'] . $url, 'SSL')
			);

			if ($result['image'] && file_exists(DIR_IMAGE . $result['image'])) {
				$image = $this->model_tool_image->resize($result['image'], 40, 40);
				} else {
				$image = $this->model_tool_image->resize('no_image.jpg', 40, 40);
			}

			$special = false;

			$product_specials = $this->model_catalog_product->getProductSpecials($result['product_id']);

			foreach ($product_specials as $product_special) {
				if (($product_special['date_start'] == '0000-00-00' || $product_special['date_start'] < date('Y-m-d')) && ($product_special['date_end'] == '0000-00-00' || $product_special['date_end'] > date('Y-m-d'))) {
					$special = $product_special['price'];

					break;
				}
			}

			$this->data['products'][] = array(
			'product_id' => $result['product_id'],
			'name' => $result['name'],
			'name_category' => $result['name_category'],
			'model' => $result['model'],
			'price' => $result['price'],
			'special' => $special,
			'image' => $image,
			'quantity' => $result['quantity'],
			'href' => $this->url->link('catalog/product/update', 'token=' . $this->session->data['token'] . '&product_id=' . $result['product_id'] . $url, 'SSL'),
			'status' => ($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),
			'selected' => isset($this->request->post['selected']) && in_array($result['product_id'], $this->request->post['selected']),
			'action' => $action
			);
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] 	= $this->language->get('text_enabled');
		$this->data['text_list'] 		= $this->language->get('text_list');
		$this->data['text_confuguration'] 		= $this->language->get('text_confuguration');
		$this->data['text_disabled'] 	= $this->language->get('text_disabled');
		$this->data['entry_all_category'] = $this->language->get('entry_all_category');
		$this->data['text_no_results'] 	= $this->language->get('text_no_results');
		$this->data['text_image_manager'] = $this->language->get('text_image_manager');
		$this->data['text_outofstock'] = $this->language->get('text_outofstock');
		$this->data['text_overview'] = $this->language->get('text_overview');
		$this->data['text_instock'] = $this->language->get('text_instock');

		$this->data['column_image'] = $this->language->get('column_image');
		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_category'] = $this->language->get('column_category');
		$this->data['column_price'] = $this->language->get('column_price');
		$this->data['column_quantity'] = $this->language->get('column_quantity');
		$this->data['column_status'] = $this->language->get('column_status');
		$this->data['column_action'] = $this->language->get('column_action');

		$this->data['button_copy'] = $this->language->get('button_copy');
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_filter'] = $this->language->get('button_filter');

		$this->data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
			} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
			} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
			} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['sort_name'] = $this->url->link('catalog/product', 'token=' . $this->session->data['token'] . '&sort=pd.name' . $url, 'SSL');
		$this->data['sort_model'] = $this->url->link('catalog/product', 'token=' . $this->session->data['token'] . '&sort=p.model' . $url, 'SSL');
		$this->data['sort_price'] = $this->url->link('catalog/product', 'token=' . $this->session->data['token'] . '&sort=p.price' . $url, 'SSL');
		$this->data['sort_quantity'] = $this->url->link('catalog/product', 'token=' . $this->session->data['token'] . '&sort=p.quantity' . $url, 'SSL');
		$this->data['sort_status'] = $this->url->link('catalog/product', 'token=' . $this->session->data['token'] . '&sort=p.status' . $url, 'SSL');
		$this->data['sort_order'] = $this->url->link('catalog/product', 'token=' . $this->session->data['token'] . '&sort=p.sort_order' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_category'])) {
			$url .= '&filter_category=' . urlencode(html_entity_decode($this->request->get['filter_category'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $product_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/product', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['filter_name'] = $filter_name;
		$this->data['filter_model'] = $filter_model;
		$this->data['filter_price'] = $filter_price;
		$this->data['filter_quantity'] = $filter_quantity;
		$this->data['filter_status'] = $filter_status;

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->template = 'catalog/product_list.tpl';
		$this->children = array(
		'common/header',
		'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_none'] = $this->language->get('text_none');
		$this->data['text_yes'] = $this->language->get('text_yes');
		$this->data['text_no'] = $this->language->get('text_no');
		$this->data['text_remain'] = $this->language->get('text_remain');
		$this->data['text_characters'] = $this->language->get('text_characters');
		$this->data['text_default'] = $this->language->get('text_default');
		$this->data['text_image_manager'] = $this->language->get('text_image_manager');
		$this->data['text_browse'] = $this->language->get('text_browse');
		$this->data['text_clear'] = $this->language->get('text_clear');
		$this->data['text_option'] = $this->language->get('text_option');
		$this->data['text_option_value'] = $this->language->get('text_option_value');
		$this->data['text_select'] = $this->language->get('text_select');
		$this->data['text_none'] = $this->language->get('text_none');
		$this->data['entry_short_description'] = $this->language->get('entry_short_description');
		$this->data['text_gallery'] = $this->language->get('text_gallery');


		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$this->data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$this->data['entry_description'] = $this->language->get('entry_description');
		$this->data['entry_store'] = $this->language->get('entry_store');
		$this->data['entry_keyword'] = $this->language->get('entry_keyword');
		$this->data['entry_model'] = $this->language->get('entry_model');
		$this->data['entry_sku'] = $this->language->get('entry_sku');
		$this->data['entry_location'] = $this->language->get('entry_location');
		$this->data['entry_minimum'] = $this->language->get('entry_minimum');
		$this->data['entry_manufacturer'] = $this->language->get('entry_manufacturer');
		$this->data['entry_shipping'] = $this->language->get('entry_shipping');
		$this->data['entry_date_available'] = $this->language->get('entry_date_available');
		$this->data['entry_quantity'] = $this->language->get('entry_quantity');
		$this->data['entry_stock_status'] = $this->language->get('entry_stock_status');
		$this->data['entry_price'] = $this->language->get('entry_price');
		$this->data['entry_weight_class'] = $this->language->get('entry_weight_class');
		$this->data['entry_weight'] = $this->language->get('entry_weight');
		$this->data['entry_dimension'] = $this->language->get('entry_dimension');
		$this->data['entry_length'] = $this->language->get('entry_length');
		$this->data['entry_image'] = $this->language->get('entry_image');
		$this->data['entry_category'] = $this->language->get('entry_category');
		$this->data['entry_filter'] = $this->language->get('entry_filter');
		$this->data['entry_related'] = $this->language->get('entry_related');
		$this->data['entry_attribute'] = $this->language->get('entry_attribute');
		$this->data['entry_text'] = $this->language->get('entry_text');
		$this->data['entry_option'] = $this->language->get('entry_option');
		$this->data['entry_option_value'] = $this->language->get('entry_option_value');
		$this->data['entry_required'] = $this->language->get('entry_required');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		$this->data['entry_priority'] = $this->language->get('entry_priority');
		$this->data['entry_tag'] = $this->language->get('entry_tag');
		$this->data['entry_customer_group'] = $this->language->get('entry_customer_group');
		$this->data['entry_related_products'] = $this->language->get('entry_related_products');
		$this->data['entry_related_news'] = $this->language->get('entry_related_news');
		$this->data['entry_layout'] = $this->language->get('entry_layout');
		$this->data['entry_profile'] = $this->language->get('entry_profile');
		$this->data['text_page_details'] = $this->language->get('text_page_details');
		$this->data['text_overview'] = $this->language->get('text_overview');

		$this->data['text_length_day'] = $this->language->get('text_length_day');
		$this->data['text_length_week'] = $this->language->get('text_length_week');
		$this->data['text_length_month'] = $this->language->get('text_length_month');
		$this->data['text_length_month_semi'] = $this->language->get('text_length_month_semi');
		$this->data['text_length_year'] = $this->language->get('text_length_year');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_saveadd'] = $this->language->get('button_saveadd');
		$this->data['button_saveclose'] = $this->language->get('button_saveclose');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_close'] = $this->language->get('button_close');
		$this->data['button_add_attribute'] = $this->language->get('button_add_attribute');
		$this->data['button_add_option'] = $this->language->get('button_add_option');
		$this->data['button_add_option_value'] = $this->language->get('button_add_option_value');
		$this->data['button_add_discount'] = $this->language->get('button_add_discount');
		$this->data['button_add_special'] = $this->language->get('button_add_special');
		$this->data['button_add_image'] = $this->language->get('button_add_image');
		$this->data['button_remove'] = $this->language->get('button_remove');
		$this->data['button_add_profile'] = $this->language->get('button_add_profile');



		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
			} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['seo'])) {
			$this->data['error_seo'] = $this->error['seo'];
			} else {
			$this->data['error_seo'] = array();
		}

		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
			} else {
			$this->data['error_name'] = array();
		}

		if (isset($this->error['meta_description'])) {
			$this->data['error_meta_description'] = $this->error['meta_description'];
			} else {
			$this->data['error_meta_description'] = array();
		}

		if (isset($this->error['description'])) {
			$this->data['error_description'] = $this->error['description'];
			} else {
			$this->data['error_description'] = array();
		}


		if ((isset($this->request->get['continue']) || isset($this->request->get['new'])) && (!isset($this->error) || empty($this->error))) {
			$this->data['success'] = $this->language->get('text_success');
		}
		if (isset($this->error['model'])) {

			$this->data['error_model'] = $this->error['model'];
			} else {
			$this->data['error_model'] = '';
		}

		if (isset($this->error['date_available'])) {
			$this->data['error_date_available'] = $this->error['date_available'];
			} else {
			$this->data['error_date_available'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
		'text' => $this->language->get('text_home'),
		'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
		'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
		'text' => $this->language->get('heading_title'),
		'href' => $this->url->link('catalog/product', 'token=' . $this->session->data['token'] . $url, 'SSL'),
		'separator' => ' :: '
		);

		if (!isset($this->request->get['product_id'])) {
			$this->data['action'] = $this->url->link('catalog/product/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
			} else {
			$this->data['product_id'] = $this->request->get['product_id'];
			$this->data['delete'] = $this->url->link('catalog/product/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
			$this->data['action'] = $this->url->link('catalog/product/update', 'token=' . $this->session->data['token'] . '&product_id=' . $this->request->get['product_id'] . $url, 'SSL');
		}

		$this->data['cancel'] = $this->url->link('catalog/product', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->get['product_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$product_info = $this->model_catalog_product->getProduct($this->request->get['product_id']);
		}

		$this->data['token'] = $this->session->data['token'];

		// Tin lien quan
		$this->load->model('catalog/product');
		if (isset($this->request->post['product_news'])) {
			$product_newss = $this->request->post['product_news'];
			} elseif (isset($this->request->get['product_id'])) {
			$product_newss = $this->model_catalog_product->getProductNews($this->request->get['product_id']);
			} else {
			$product_newss = array();
		}

		$this->data['product_newss'] = array();

		$this->load->model('catalog/news');
		foreach ($product_newss as $news_id) {
			$glossary_info = $this->model_catalog_news->getNew($news_id);

			if (isset($glossary_info) && !empty($glossary_info)) {
				$this->data['product_newss'][] = array(
				'news_id' => $glossary_info['news_id'],
				'name' => $glossary_info['name']
				);
			}
		}
		// Ket thuc

		$this->load->model('localisation/language');

		$this->data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->post['product_description'])) {
			$this->data['product_description'] = $this->request->post['product_description'];
			} elseif (isset($this->request->get['product_id'])) {
			$this->data['product_description'] = $this->model_catalog_product->getProductDescriptions($this->request->get['product_id']);
			} else {
			$this->data['product_description'] = array();
		}

		if (isset($this->request->post['model'])) {
			$this->data['model'] = $this->request->post['model'];
			} elseif (!empty($product_info)) {
			$this->data['model'] = $product_info['model'];
			} else {
			$this->data['model'] = '';
		}

		if (isset($this->request->post['keyword'])) {
			$this->data['keyword'] = $this->request->post['keyword'];
			} elseif (!empty($product_info)) {
			$this->data['keyword'] = $product_info['keyword'];
			} else {
			$this->data['keyword'] = '';
		}

		if (isset($this->request->post['image'])) {
			$this->data['image'] = $this->request->post['image'];
			} elseif (!empty($product_info)) {
			$this->data['image'] = $product_info['image'];
			} else {
			$this->data['image'] = '';
		}

		if (isset($this->request->post['ogimage'])) {
			$this->data['ogimage'] = $this->request->post['ogimage'];
		} elseif (!empty($product_info)) {
			$this->data['ogimage'] = $product_info['ogimage'];
		} else {
			$this->data['ogimage'] = '';
		}

		$this->load->model('tool/image');

		if (isset($this->request->post['image']) && file_exists(DIR_IMAGE . $this->request->post['image'])) {
			$this->data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
			} elseif (!empty($product_info) && $product_info['image'] && file_exists(DIR_IMAGE . $product_info['image'])) {
			$this->data['thumb'] = $this->model_tool_image->resize($product_info['image'], 100, 100);
			} else {
			$this->data['thumb'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
		}

		if (isset($this->request->post['ogimage']) && file_exists(DIR_IMAGE . $this->request->post['ogimage'])) {
			$this->data['thumb_ogimage'] = $this->model_tool_image->resize($this->request->post['ogimage'], 100, 100);
			} elseif (!empty($product_info) && $product_info['ogimage'] && file_exists(DIR_IMAGE . $product_info['ogimage'])) {
			$this->data['thumb_ogimage'] = $this->model_tool_image->resize($product_info['ogimage'], 100, 100);
			} else {
			$this->data['thumb_ogimage'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
		}

		if (isset($this->request->post['price'])) {
			$this->data['price'] = $this->request->post['price'];
			} elseif (!empty($product_info)) {
			$this->data['price'] = $product_info['price'];
			} else {
			$this->data['price'] = '';
		}

		if (isset($this->request->post['robots'])) {
			$this->data['robots'] = $this->request->post['robots'];
			} elseif (!empty($product_info)) {
			$this->data['robots'] = $product_info['robots'];
			} else {
			$this->data['robots'] = '';
		}

		if (isset($this->request->post['date_available'])) {
			$this->data['date_available'] = $this->request->post['date_available'];
			} elseif (!empty($product_info['date_available']) && $product_info['date_available']!= '0000-00-00 00:00:00') {
			$this->data['date_available'] = date('Y-m-d', strtotime($product_info['date_available']));
			} else {
			$this->data['date_available'] = date('Y-m-d', time() - 86400);
		}

		if (isset($this->request->post['quantity'])) {
			$this->data['quantity'] = $this->request->post['quantity'];
			} elseif (!empty($product_info)) {
			$this->data['quantity'] = $product_info['quantity'];
			} else {
			$this->data['quantity'] = 1;
		}

		if (isset($this->request->post['sort_order'])) {
			$this->data['sort_order'] = $this->request->post['sort_order'];
			} elseif (!empty($product_info)) {
			$this->data['sort_order'] = $product_info['sort_order'];
			} else {
			$this->data['sort_order'] = 1;
		}
		// print_r($product_info);
		if (isset($this->request->post['note'])) {
			$this->data['note'] = $this->request->post['note'];
			} elseif (!empty($product_info)) {
			$this->data['note'] = $product_info['note'];
			} else {
			$this->data['note'] = '';
		}


		if (isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
			} elseif (!empty($product_info)) {
			$this->data['status'] = $product_info['status'];
			} else {
			$this->data['status'] = 1;
		}

		if (isset($this->request->post['embed'])) {
				$this->data['embed'] = $this->request->post['embed'];
		} else if (isset($product_info)) {
				$this->data['embed'] = $product_info['embed'];
		} else {
				$this->data['embed'] = '';
		}

		if (isset($this->request->post['category'])) {
            $this->data['category'] = $this->request->post['category'];
        } elseif (isset($product_info)) {
            $this->data['category'] = join(',',$this->model_catalog_product->getProductCategories($this->request->get['product_id']));
        } else {
            $this->data['category'] = '';
        }
		// print_r($this->data['category']);

		$this->load->model('catalog/manufacturer');

		if (isset($this->request->post['manufacturer_id'])) {
			$this->data['manufacturer_id'] = $this->request->post['manufacturer_id'];
			} elseif (!empty($product_info)) {
			$this->data['manufacturer_id'] = $product_info['manufacturer_id'];
			} else {
			$this->data['manufacturer_id'] = 0;
		}

		if (isset($this->request->post['manufacturer'])) {
			$this->data['manufacturer'] = $this->request->post['manufacturer'];
			} elseif (!empty($product_info)) {
			$manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($product_info['manufacturer_id']);

			if ($manufacturer_info) {
				$this->data['manufacturer'] = $manufacturer_info['name'];
				} else {
				$this->data['manufacturer'] = '';
			}
			} else {
			$this->data['manufacturer'] = '';
		}

		// Categories
		$this->load->model('catalog/category');
		$this->document->addStyle('view/stylesheet/lte/select2.min.css');

		// $this->data['categories'] = $this->model_catalog_category->getCategories(NULL);
		$this->data['categories'] = array();
		foreach($this->model_catalog_category->getCategoryByParent(0) as $rs){
			$cats = array();
			foreach($this->model_catalog_category->getCategoryByParent($rs['category_id']) as $r){
				$cats[] = array(
					'category_id' => $r['category_id'],
					'name' => $r['name'],
				);
			}
			$this->data['categories'][] = array(
				'category_id' => $rs['category_id'],
				'name' => $rs['name'],
				'group' => $cats,
			);
		}
		// print_r($this->model_catalog_category->getCategories(0));

		if (isset($this->request->post['product_category'])) {
			$categories = $this->request->post['product_category'];
			} elseif (isset($this->request->get['product_id'])) {
			$categories = $this->model_catalog_product->getProductCategories($this->request->get['product_id']);
			} else {
			$categories = array();
		}

		$this->data['product_categories'] = array();

		foreach ($categories as $category_id) {
			$category_info = $this->model_catalog_category->getCategory($category_id);

			if ($category_info) {
				$this->data['product_categories'][] = array(
				'category_id' => $category_info['category_id'],
				'name' => ($category_info['path'] ? $category_info['path'] . ' &gt; ' : '') . $category_info['name']
				);
			}
		}

		if (isset($this->request->post['product_special'])) {
			$this->data['product_specials'] = $this->request->post['product_special'];
			} elseif (isset($this->request->get['product_id'])) {
			$this->data['product_specials'] = $this->model_catalog_product->getProductSpecials($this->request->get['product_id']);
			} else {
			$this->data['product_specials'] = array();
		}

		// Images
		if (isset($this->request->post['product_image'])) {
			$product_images = $this->request->post['product_image'];
			} elseif (isset($this->request->get['product_id'])) {
			$product_images = $this->model_catalog_product->getProductImages($this->request->get['product_id']);
			} else {
			$product_images = array();
		}

		$this->data['product_images'] = array();

		foreach ($product_images as $product_image) {
			if ($product_image['image'] && file_exists(DIR_IMAGE . $product_image['image'])) {
				$image = $product_image['image'];
				} else {
				$image = 'no_image.jpg';
			}

			$this->data['product_images'][] = array(
			'image' => $image,
			'thumb' => $this->model_tool_image->resize($image, 100, 100),
			'sort_order' => $product_image['sort_order']
			);
		}

		$this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);


		if (isset($this->request->post['product_related'])) {
			$products = $this->request->post['product_related'];
			} elseif (isset($this->request->get['product_id'])) {
			$products = $this->model_catalog_product->getProductRelated($this->request->get['product_id']);
			} else {
			$products = array();
		}

		$this->data['product_related'] = array();

		foreach ($products as $product_id) {
			$related_info = $this->model_catalog_product->getProduct($product_id);

			if ($related_info) {
				$this->data['product_related'][] = array(
				'product_id' => $related_info['product_id'],
				'name' => $related_info['name']
				);
			}
		}

		$this->load->model('design/layout');

		$this->data['layouts'] = $this->model_design_layout->getLayouts();

		$this->template = 'catalog/product_form.tpl';
		$this->children = array(
		'common/header',
		'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/product')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		foreach ($this->request->post['product_description'] as $language_id => $value) {
			if ((utf8_strlen($value['name']) < 1) || (utf8_strlen($value['name']) > 255)) {
				$this->error['name'][$language_id] = $this->language->get('error_name');
			}
		}

		if (isset($this->request->post['seo']) && $this->request->post['seo'] == 0) {
			$this->error['seo'] = "Từ khóa SEO đã tồn tại";
		}

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		if (!$this->error) {
			return true;
			} else {
			return false;
		}
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/product')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return true;
			} else {
			return false;
		}
	}

	protected function validateCopy() {
		if (!$this->user->hasPermission('modify', 'catalog/product')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return true;
			} else {
			return false;
		}
	}

	public function checkKeyword() {
		$json = array();

		if (isset($this->request->post['product_id'])) {
			$product_id = $this->request->post['product_id'];
			} else {
			$product_id = 0;
		}
		if (isset($this->request->post['keyword'])) {
			$keyword = $this->request->post['keyword'];
			} else {
			$keyword = '';
		}

		$data = array(
		'keyword' => $keyword,
		'product_id' => $product_id
		);

		if (strlen($keyword) > 0) {
			$this->load->model('catalog/product');
			$results = $this->model_catalog_product->checkKeywords($data);
			if ($results == 0) {
				$json['success'] = 'Thành công:Từ khóa mà bạn đã nhập vào được kiểm tra là duy nhất.';
				} else {
				$json['error'] = 'Cảnh báo: Từ khóa mà bạn đã nhập vào đã được sử dụng ở những nơi khác trên trang.';
			}
			$this->response->setOutput(json_encode($json));
		}
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name']) || isset($this->request->get['filter_model']) || isset($this->request->get['filter_category_id'])) {
			$this->load->model('catalog/product');

			if (isset($this->request->get['filter_name'])) {
				$filter_name = $this->request->get['filter_name'];
				} else {
				$filter_name = '';
			}

			if (isset($this->request->get['filter_model'])) {
				$filter_model = $this->request->get['filter_model'];
				} else {
				$filter_model = '';
			}

			if (isset($this->request->get['limit'])) {
				$limit = $this->request->get['limit'];
				} else {
				$limit = 20;
			}

			$data = array(
			'filter_name' => $filter_name,
			'filter_model' => $filter_model,
			'start' => 0,
			'limit' => $limit
			);

			$results = $this->model_catalog_product->getProducts($data);

			foreach ($results as $result) {
				$json[] = array(
				'product_id' => $result['product_id'],
				'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
				'model' => $result['model'],
				'price' => $result['price']
				);
			}
		}

		$this->response->setOutput(json_encode($json));
	}

}

?>
