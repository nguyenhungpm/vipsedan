<?php
class ControllerSeoLocalseo extends Controller {
	private $error = array();

	public function index() {
		$this->language->load('seo/localseo');
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			 $this->response->ClearCache();
		$this->model_setting_setting->editSetting('localseo', $this->request->post);

		$this->session->data['success'] = $this->language->get('text_success');

		$this->redirect($this->url->link('seo/localseo', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_select'] = $this->language->get('text_select');
		$this->data['text_none'] = $this->language->get('text_none');
		$this->data['text_yes'] = $this->language->get('text_yes');
		$this->data['text_configuration'] = $this->language->get('text_configuration');
		$this->data['text_no'] = $this->language->get('text_no');
		$this->data['text_success'] = $this->language->get('text_success');
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['entry_name_seo'] = $this->language->get('entry_name_seo');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_type_seo'] = $this->language->get('entry_type_seo');
		$this->data['entry_street_seo'] = $this->language->get('entry_street_seo');
		$this->data['entry_region_seo'] = $this->language->get('entry_region_seo');
		$this->data['entry_locality_seo'] = $this->language->get('entry_locality_seo');
		$this->data['entry_postalcode_seo'] = $this->language->get('entry_postalcode_seo');


		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('seo/localseo', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->data['action'] = $this->url->link('seo/localseo', 'token=' . $this->session->data['token'], 'SSL');

		if(isset($this->session->data['redirect'])){
			$this->data['cancel'] = $this->session->data['redirect'];
		}else{
			$this->data['cancel'] = $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL');
		}

		if (isset($this->request->post['config_local'])) {
			$this->data['config_local'] = $this->request->post['config_local'];
		} else {
			$this->data['config_local'] = $this->config->get('config_local');
		}

		if (isset($this->request->post['config_type_seo'])) {
			$this->data['config_type_seo'] = $this->request->post['config_type_seo'];
		} else {
			$this->data['config_type_seo'] = $this->config->get('config_type_seo');
		}
		if (isset($this->request->post['config_name_seo'])) {
			$this->data['config_name_seo'] = $this->request->post['config_name_seo'];
		} else {
			$this->data['config_name_seo'] = $this->config->get('config_name_seo');
		}

		if (isset($this->request->post['config_street_seo'])) {
			$this->data['config_street_seo'] = $this->request->post['config_street_seo'];
		} else {
			$this->data['config_street_seo'] = $this->config->get('config_street_seo');
		}

		if (isset($this->request->post['config_region_seo'])) {
			$this->data['config_region_seo'] = $this->request->post['config_region_seo'];
		} else {
			$this->data['config_region_seo'] = $this->config->get('config_region_seo');
		}

		if (isset($this->request->post['config_locality_seo'])) {
			$this->data['config_locality_seo'] = $this->request->post['config_locality_seo'];
		} else {
			$this->data['config_locality_seo'] = $this->config->get('config_locality_seo');
		}

		if (isset($this->request->post['config_postalcode_seo'])) {
			$this->data['config_postalcode_seo'] = $this->request->post['config_postalcode_seo'];
		} else {
			$this->data['config_postalcode_seo'] = $this->config->get('config_postalcode_seo');
		}

		if (isset($this->request->post['config_locality_seo'])) {
			$this->data['config_locality_seo'] = $this->request->post['config_locality_seo'];
		} else {
			$this->data['config_locality_seo'] = $this->config->get('config_locality_seo');
		}

		if (isset($this->request->post['config_postalcode_seo'])) {
			$this->data['config_postalcode_seo'] = $this->request->post['config_postalcode_seo'];
		} else {
			$this->data['config_postalcode_seo'] = $this->config->get('config_postalcode_seo');
		}

		$this->template = 'seo/localseo.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'seo/localseo')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

}
?>
