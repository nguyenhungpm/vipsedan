<?php
class ControllerSeoSeo extends Controller {
	private $error = array();

	public function index() {
		$this->language->load('seo/seo');
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			 $this->response->ClearCache();
		$this->model_setting_setting->editSetting('seo', $this->request->post);

		$this->session->data['success'] = $this->language->get('text_success');

		$this->redirect($this->url->link('seo/seo', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_select'] = $this->language->get('text_select');
		$this->data['text_none'] = $this->language->get('text_none');
		$this->data['text_or'] = $this->language->get('text_or');
		$this->data['text_yes'] = $this->language->get('text_yes');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['entry_seo_url'] = $this->language->get('entry_seo_url');
		$this->data['entry_title'] = $this->language->get('entry_title');
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['text_success'] = $this->language->get('text_success');
		$this->data['entry_meta_robots'] = $this->language->get('entry_meta_robots');
		$this->data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$this->data['entry_meta_keywords'] = $this->language->get('entry_meta_keywords');
		$this->data['text_configuration'] = $this->language->get('text_configuration');
		$this->data['entry_google_analytics'] = $this->language->get('entry_google_analytics');
		$this->data['entry_google_searchconsole'] = $this->language->get('entry_google_searchconsole');


		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('seo/seo', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['title'])) {
			$this->data['error_title'] = $this->error['error_title'];
		} else {
			$this->data['error_title'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->data['action'] = $this->url->link('seo/seo', 'token=' . $this->session->data['token'], 'SSL');

		if(isset($this->session->data['redirect'])){
			$this->data['cancel'] = $this->session->data['redirect'];
		}else{
			$this->data['cancel'] = $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL');
		}

		$this->load->model('localisation/language');

		$this->data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->post['config_seo_url'])) {
			$this->data['config_seo_url'] = $this->request->post['config_seo_url'];
		} else {
			$this->data['config_seo_url'] = $this->config->get('config_seo_url');
		}

		if (isset($this->request->post['google_sitemap_status'])) {
					$this->data['google_sitemap_status'] = $this->request->post['google_sitemap_status'];
				} else {
					$this->data['google_sitemap_status'] = $this->config->get('google_sitemap_status');
		}

		if (isset($this->request->post['config_meta_description'])) {
			$this->data['config_meta_description'] = $this->request->post['config_meta_description'];
		} else {
			$this->data['config_meta_description'] = $this->config->get('config_meta_description');
		}

		if (isset($this->request->post['config_meta_robots'])) {
			$this->data['config_meta_robots'] = $this->request->post['config_meta_robots'];
		} else {
			$this->data['config_meta_robots'] = $this->config->get('config_meta_robots');
		}
		if (isset($this->request->post['config_meta_keyword'])) {
			$this->data['config_meta_keyword'] = $this->request->post['config_meta_keyword'];
		} else {
			$this->data['config_meta_keyword'] = $this->config->get('config_meta_keyword');
		}

		if (isset($this->request->post['config_title'])) {
			$this->data['config_title'] = $this->request->post['config_title'];
		} else {
			$this->data['config_title'] = $this->config->get('config_title');
		}

		if (isset($this->request->post['config_google_analytics'])) {
			$this->data['config_google_analytics'] = $this->request->post['config_google_analytics'];
		} else {
			$this->data['config_google_analytics'] = $this->config->get('config_google_analytics');
		}

		if (isset($this->request->post['config_google_verify'])) {
			$this->data['config_google_verify'] = $this->request->post['config_google_verify'];
		} else {
			$this->data['config_google_verify'] = $this->config->get('config_google_verify');
		}
		$this->data['data_feed'] = HTTP_CATALOG . 'index.php?route=feed/google_sitemap';
		$this->data['data_feed2'] = HTTP_CATALOG . 'sitemap.xml';
		$this->template = 'seo/seo.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'seo/seo')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		if (!$this->request->post['config_title']) {
			$this->error['title'] = $this->language->get('error_title');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	public function autocomplete() {
		$json = array();

		if(!empty($this->request->post['current'])){
			$current = explode(",", $this->request->post['current']);
		}

		$results = explode("\n", $this->config->get('config_meta_robots_dictionary'));

		foreach ($results as $result) {
			$json[] = trim($result);
		}

		if(!empty($current)){
			$json = array_diff($json, $current);
		}

		$this->response->setOutput(json_encode($json));
	}

}
?>
