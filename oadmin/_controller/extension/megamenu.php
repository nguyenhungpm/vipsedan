<?php
class ControllerExtensionMegamenu extends Controller {
	private $error = array();
 
	public function index() {
		$this->load->language('extension/megamenu');
 // print_r($this->session->data['d']);
		$this->document->setTitle($this->language->get('heading_title'));
 		
		$this->load->model('extension/megamenu');
		
		$this->getList();
	}

	public function insert() {
		$json = array();
		$this->load->language('extension/megamenu');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('extension/megamenu');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_extension_megamenu->addMegamenu($this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			
			// $this->redirect($this->url->link('extension/megamenu', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}

		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
	}
	
	public function edit(){
        $json = array();
		$this->load->model('extension/megamenu');

		$this->model_extension_megamenu->editMegamenu($this->request->get['megamenu_id'], $this->request->post);

		/* $this->session->data['d'] = $this->session->data['success']; */

		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
	}
	
	public function delete() { 
		$this->load->language('extension/megamenu');

		$this->document->setTitle($this->language->get('heading_title'));		
		
		$this->load->model('extension/megamenu');
		
		if (isset($this->request->post['selected'])) {
      		foreach ($this->request->post['selected'] as $id) {
				$this->model_extension_megamenu->deleteMegamenu($id);	
			}
						
			$this->session->data['success'] = $this->language->get('text_success');
			
			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			
			$this->redirect($this->url->link('extension/megamenu', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}

		$this->getList();
	}

	private function getList() {
	
		$this->load->model('localisation/language');
		$this->data['languages'] = $this->model_localisation_language->getLanguages();

		$this->load->model('tool/image');
	
  		$this->data['breadcrumbs'] = array();

  		$this->data['breadcrumbs'][] = array(
       		'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
       		'text'      => $this->language->get('text_home'),
      		'separator' => FALSE
   		);

  		$this->data['breadcrumbs'][] = array(
       		'href'      => $this->url->link('extension/megamenu', 'token=' . $this->session->data['token'], 'SSL'),
       		'text'      => $this->language->get('heading_title'),
      		'separator' => ' :: '
   		);
							
		
		$this->data['insert'] = $this->url->link('extension/megamenu/insert', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['delete'] = $this->url->link('extension/megamenu/delete', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['export'] = $this->url->link('extension/megamenu/export', 'token=' . $this->session->data['token'], 'SSL');	
		
		$this->data['token'] = $this->session->data['token'];
	
		$this->data['text_export'] = $this->language->get('text_export');
		$url = '';
		if(isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
			$this->data['page'] = $this->request->get['page'];
			$url .= '&page='. $this->request->get['page'];
		}else{
			$page = 1;
			$this->data['page'] = 1;
		}
	
		$this->data['megamenus'] = array();
		
		$data = array(
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);
		$this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 80, 80);
		$this->data['thumb'] = $this->model_tool_image->resize('no_image.jpg', 80, 80);
		$megamenu_total = $this->model_extension_megamenu->getTotalMegamenus($data);
		
		$results = $this->model_extension_megamenu->getMegamenus($data);
		$this->data['list'] = $this->model_extension_megamenu->getParent(0);
		$this->data['megamenus'] = $this->model_extension_megamenu->getParent(0);
		/* foreach ($results as $result) {
			$detail = $this->model_extension_megamenu->getDetail($result['megamenu_id']);
			
			if (isset($result) && $result['image'] && file_exists(DIR_IMAGE . $result['image'])) {
	            $thumb = $this->model_tool_image->resize($result['image'], 40, 40);
	        } else {
	            $thumb = $this->model_tool_image->resize('no_image.jpg', 40, 40);
	        }
			$this->data['megamenus'][] = array(
				'megamenu_id' 		=> $result['megamenu_id'],
				'url' 	=> $result['url'],
				'parent_id' 	=> $result['parent_id'],
				'image' 	=> !empty($result['image']) ? $result['image'] : '',
				'thumb' 	=> $thumb,
				'detail' 	=> $detail,
				'sort_order' 	=> $result['sort_order'],
				'status' 	=> $result['status'],
				'edit' => $this->url->link('extension/megamenu', 'token=' . $this->session->data['token'] . '&megamenu_id=' . $result['megamenu_id'] . $url, 'SSL')
			);
		} */	
	
		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_megamenu'] = $this->language->get('column_megamenu');
		
		$this->data['column_action'] = $this->language->get('column_action');

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
 
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		$url = "";
		
		
		$pagination = new Pagination();
		$pagination->total = $megamenu_total;
		$pagination->page  = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text  = $this->language->get('text_pagination');
		$pagination->url   = $this->url->link('extension/megamenu/', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['sort_name'] = $this->url->link('extension/megamenu/', 'token=' . $this->session->data['token']);
		
		$this->template = 'extension/megamenu.tpl';
		$this->children = array(
			'common/header',	
			'common/footer'	
		);
		
		$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
 	}

	private function validateForm() {
		
		$this->load->model('extension/megamenu');
		
		if (!$this->user->hasPermission('modify', 'extension/megamenu')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
	 	

		if (!$this->error) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	public function autocomplete() {
		$json = array();
		
		if (isset($this->request->post['filter_name'])) {
	
			$this->load->model('extension/megamenu');
			
			$data = array(
				'filter_name' => $this->request->post['filter_name'],
				'start'       => 0,
				'limit'       => 10
			);
			$results = $this->model_extension_megamenu->getLink($data);
			
			foreach ($results as $result) {
				$json[] = array(
					'news_id' => html_entity_decode($result['keyword'], ENT_QUOTES, 'UTF-8'),
					'name'       => html_entity_decode($result['keyword'], ENT_QUOTES, 'UTF-8')
				);	
			}
		}
		
		$this->response->setOutput(json_encode($json));	
	}

}
?>