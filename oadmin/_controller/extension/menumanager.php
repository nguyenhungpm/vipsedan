<?php
class ControllerExtensionMenumanager extends Controller
{
	function index()
	{
		$this->load->language('module/menumanager');
		$this->load->model('menumanager/menu_group');
		$this->load->model('menumanager/menu');
		$this->load->model('catalog/category');
		$this->load->model('catalog/news_category');
		$this->load->model('catalog/information');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->data['menu_groups'] = $this->model_menumanager_menu_group->getMenuGroups();
// print_r($this->session->data['menu']);
		if(isset($this->request->get['group_id']))
		{
			$this->data['group_id'] = (int)$this->request->get['group_id'];
		}elseif(isset($this->data['menu_groups'][0]['id'])){
			$this->data['group_id'] = $this->data['menu_groups'][0]['id'];

		}else{
			$this->data['group_id'] = 0;
		}

		$this->load->model('localisation/language');
		$this->data['languages'] = $this->model_localisation_language->getLanguages();
		$code_lang = array();
		foreach($this->data['languages'] as $language){
			$code_lang[] = $language['language_id'];
		}
		$this->data['code_lang'] = join('_', $code_lang);

		$menu = $this->model_menumanager_menu->getMenuByGroup($this->data['group_id']);
		// print_r($menu);
		$this->data['menu_ul'] = '<ul id="easymm"></ul>';
		if ($menu) {

			$tree = new Tree;

			foreach ($menu as $row) {
				$tree->add_row(
						$row['id'],
						$row['parent_id'],
						' id="menu-'.$row['id'].'" class="sortable"',
						$this->model_menumanager_menu->get_label($row)
				);
			}

			$this->data['menu_ul'] = $tree->generate_list('id="easymm"');
		}



		$this->data['current_menu'] =  $this->model_menumanager_menu_group->get_current_menu_group($this->data['group_id']);
		// print_r($this->data['current_menu']);
		$current_menu =  $this->model_menumanager_menu_group->get_menu_group($this->data['group_id']);
		$this->data['group_title'] = $current_menu ? $current_menu['title'] : '';
		$this->data['group_title_en'] = $current_menu ? $current_menu['title_en'] : '';
		$this->load->model('menumanager/menu_group');
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			if(isset($this->request->post['action']) )
			{

				switch($this->request->post['action'])
				{
					case 'save_group':
						$k = 0;
						$title_group = array();
						foreach($this->data['languages'] as $language){
							$title_group[$language['language_id']] = trim($this->request->post['title_group_'.$language['language_id']]);
							if(!empty($title_group[$language['language_id']])){
								$k += 1;
							}
						}

						if($k==0)
						{
							$response['status'] = 2;
							$response['msg'] = 'Add group error.';
						}else{
							// $group_id = $this->model_menumanager_menu_group->save_group($title, $title_en);
							$group_id = $this->model_menumanager_menu_group->save_group($title_group);
							$name =  $this->model_menumanager_menu_group->get_menu_group($group_id)['title'];
							$response['status'] = 1;
							$response['id'] = $group_id;
							$response['name'] = $name;
						}
						header('Content-type: application/json');
						echo json_encode($response);
						exit;
					case 'update_menu_group':
						$k = 0;
						$title_group = array();
						foreach($this->data['languages'] as $language){
							$title_group[$language['language_id']] = trim($this->request->post['title_group_'.$language['language_id']]);
							if(!empty($title_group[$language['language_id']])){
								$k += 1;
							}
						}
						// $title = trim($this->request->post['title']);
						// $title_en = trim($this->request->post['title_en']);
						$group_id = $this->request->post['id'];
						if($k==0)
						{
							$response['success'] = false;
						}else{
							$group_id = $this->model_menumanager_menu_group->update_menu_group($group_id,$title_group);
							$response['success'] = true;
						}
						header('Content-type: application/json');
						echo json_encode($response);
						exit;
					case 'delete_menu_group':
						$group_id = $this->request->post['id'];
						if($group_id=='')
						{
							$response['success'] = false;
						}else{
							$this->model_menumanager_menu_group->delete_menu_group($group_id);
							$response['success'] = true;
						}
						header('Content-type: application/json');
						echo json_encode($response);
						exit;
					case 'delete_menu_item':
						$menu_id = $this->request->post['id'];
						$this->model_menumanager_menu->delete_menu($menu_id);
						$response['success'] = true;
						header('Content-type: application/json');
						echo json_encode($response);
						exit;
				}
			}
		}

		$this->load->language('module/menumanager');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_home'),
				'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
				'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('heading_title'),
				'href'      => $this->url->link('extension/menumanager', 'token=' . $this->session->data['token'], 'SSL'),
				'separator' => ' :: '
		);

		$this->data['action'] = $this->url->link('extension/menumanager', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		//news_categories
		$this->data['news_categories'] = array();

		$results = $this->model_catalog_news_category->getNewsCategories(0);

		foreach ($results as $result) {
			$action = array();

			$action[] = array(
					'text' => $this->language->get('text_edit'),
					'href' => $this->url->link('catalog/news_category/update', 'token=' . $this->session->data['token'] . '&news_category_id=' . $result['news_category_id'], 'SSL')
			);

			$this->data['news_categories'][] = array(
					'news_category_id' => $result['news_category_id'],
					'name'        => $result['name'],
					'sort_order'  => $result['sort_order'],
					'selected'    => isset($this->request->post['selected']) && in_array($result['news_category_id'], $this->request->post['selected']),
					'action'      => $action
			);
		}

		//categories
		$this->data['categories'] = array();

		$results = $this->model_catalog_category->getCategories(0);

		foreach ($results as $result) {
			$action = array();

			$action[] = array(
					'text' => $this->language->get('text_edit'),
					'href' => $this->url->link('catalog/category/update', 'token=' . $this->session->data['token'] . '&category_id=' . $result['category_id'], 'SSL')
			);

			$this->data['categories'][] = array(
					'category_id' => $result['category_id'],
					'name'        => $result['name'],
					'sort_order'  => $result['sort_order'],
					'selected'    => isset($this->request->post['selected']) && in_array($result['category_id'], $this->request->post['selected']),
					'action'      => $action
			);
		}

		//page informations
		$this->data['informations'] = array();

		$pdata = array(	);


		$results = $this->model_catalog_information->getInformations($pdata);

		foreach ($results as $result) {
			$this->data['informations'][] = array(
					'information_id' => $result['information_id'],
					'title'          => $result['title'],
					'sort_order'     => $result['sort_order'],
					'selected'       => isset($this->request->post['selected']) && in_array($result['information_id'], $this->request->post['selected']),
					'action'         => $action
			);
		}

		$this->template = 'menumanager/menumanager.tpl';
		$this->children = array(
				'common/header',
				'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	function addgroup(){
		$this->load->model('localisation/language');
		$this->data['languages'] = $this->model_localisation_language->getLanguages();
		
		$this->template = 'menumanager/new_menu_group.tpl';
		$this->children = array(
				'common/header',
				'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	function addmenu()
	{
		$this->load->model('menumanager/menu');
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			$mid = $this->model_menumanager_menu->insertMenu($this->request->post);
			if($mid)
			{
				$response['status'] = 1;
				$li_id = 'menu-'.$mid;
				$data = $this->model_menumanager_menu->getMenu($mid);
				$response['li'] = '<li id="'.$li_id.'" class="sortable">'.$this->model_menumanager_menu->get_label($data).'</li>';
				$response['li_id'] = $li_id;
			}else{
				$response['status'] = 2;
				$response['msg'] = 'Add menu error.';
			}
			header('Content-type: application/json');
			echo json_encode($response);
			exit;
		}
	}

	function quick_add()
	{
		$this->load->model('menumanager/menu');
		$this->load->model('catalog/category');
		$this->load->model('catalog/news_category');
		$this->load->model('catalog/information');
		$url = new Url('');
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {

			$group_id = $this->request->post['menu_group_id'];
			$tmp = $url->link('product/category', 'path=1');

			if(isset($this->request->post['category']))
			{
				$categories = $this->request->post['category'];
				foreach($categories as $cat)
				{
					$category = $this->model_catalog_category->getCategory($cat);
					$cat_desc = $this->model_catalog_category->getCategoryDescriptions($cat);
					$name = array();
					foreach($cat_desc as $language_id => $val){
						$name[$language_id] = $val['name'];
					}
					$lang_id = $this->config->get('config_language_id');
					$category = array_merge($category,$cat_desc[$lang_id]);
					$data = array(
								'group_id' => $group_id,
								'title' => $category['name'],
								'name' => $name,
								'class' => '',
								'url' => $category['keyword'] ? $category['keyword'] : $url->link('product/category', 'path=' . $category['category_id']),
							);
					$this->model_menumanager_menu->insertMenu($data);
				}
			}

			if(isset($this->request->post['news_category']))
			{
				$news_categories = $this->request->post['news_category'];
				foreach($news_categories as $cat)
				{
					$category = $this->model_catalog_news_category->getNewsCategory($cat);
					$cat_desc = $this->model_catalog_news_category->getNewsCategoryDescriptions($cat);
					$name = array();
					foreach($cat_desc as $language_id => $val){
						$name[$language_id] = $val['name'];
					}
					$lang_id = $this->config->get('config_language_id');
					$category = array_merge($category,$cat_desc[$lang_id]);
					$data = array(
								'group_id' => $group_id,
								'title' => $category['name'],
								'name' => $name,
								'class' => '',
								'url' => $category['keyword'] ? $category['keyword'] : $url->link('news/news_category', 'cat_id=' . $category['news_category_id']),
							);
					$this->model_menumanager_menu->insertMenu($data);
				}
			}

			if(isset($this->request->post['information']))
			{
				$informations = $this->request->post['information'];
				foreach($informations as $info)
				{
					$information = $this->model_catalog_information->getInformation($info);
					$info_desc = $this->model_catalog_information->getInformationDescriptions($info);
					$name = array();
					foreach($info_desc as $language_id => $val){
						$name[$language_id] = $val['title'];
					}
					$lang_id = $this->config->get('config_language_id');
					$information = array_merge($information,$info_desc[$lang_id]);
					$data = array(
							'group_id' => $group_id,
							'title' => $information['title'],
							'name' => $name,
							'class' => '',
							'url' => $information['keyword'] ? $information['keyword'] : $url->link('information/information', 'information_id=' . $info),
					);
					$this->model_menumanager_menu->insertMenu($data);
				}
			}

			if(isset($this->request->post['page_route']))
			{
				$url_routes = $this->request->post['page_route'];
				foreach($url_routes as $rou)
				{
					$title = "";
					$link = "";
					switch ($rou){
						case 'information/contact':
							$title = "Contact Us";
							$link = $url->link('information/contact', '');
							break;
						case 'information/sitemap':
							$title = "Site Map";
							$link = $url->link('information/sitemap', '');
							break;
						case 'product/manufacturer':
							$title = "Brands";
							$link = $url->link('product/manufacturer', '');
							break;
						case 'account/account':
							$title = "My Account";
							$link = $url->link('account/account', '');
							break;
					}

					if($title !="")
					{
						$data = array(
								'group_id' => $group_id,
								'title' => $title,
								'class' => '',
								'url' => $link,
						);
						$this->model_menumanager_menu->insertMenu($data);
					}
				}
			}

		}
		exit;
	}

	function qaddmenu()
	{
		$this->load->model('menumanager/menu');
		$this->load->model('catalog/category');
		$url = new Url(HTTP_CATALOG,'');
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			$menus = $this->model_menumanager_menu->getCategories();
			$tmp = $url->link('product/category', 'path=1');
			// print_r($menus);
			/* $mid = $this->model_menumanager_menu->insertMenu($this->request->post);
			if($mid)
			{
				$response['status'] = 1;
				$li_id = 'menu-'.$mid;
				$data = $this->model_menumanager_menu->getMenu($mid);
				$response['li'] = '<li id="'.$li_id.'" class="sortable">'.$this->model_menumanager_menu->get_label($data).'</li>';
				$response['li_id'] = $li_id;
			}else{
				$response['status'] = 2;
				$response['msg'] = 'Add menu error.';
			}
			header('Content-type: application/json');
			echo json_encode($response);
			exit; */
		}
	}

	function updatemenupos()
	{
		$this->load->model('menumanager/menu_group');
		$this->load->model('menumanager/menu');
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			// $this->session->data['db'] = $this->request->post;
			$this->model_menumanager_menu->save_position($this->request->post);
			exit;
		}
	}

	function update_menu_item()
	{
		$this->load->model('localisation/language');
        $this->data['languages'] = $this->model_localisation_language->getLanguages();
        
		$this->load->model('menumanager/menu');
		$menu_id = $this->request->get['menu_id'];
		$menu = $this->model_menumanager_menu->getMenu((int)$menu_id);
		$this->data['row'] = $menu;
		// print_r($this->data['row']);
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			$data['title'] = trim($this->request->post['title']);

			$k=0;
			foreach ($this->request->post['name'] as $key => $value) {
				if(!empty($value)) $k++;
			}

			// if (!empty($data['title'])) {
			if ($k>0) {
				$data['id'] = $this->request->post['menu_id'];
				$data['url'] = $this->request->post['url'];
				$data['class'] = $this->request->post['class'];
				$data['name'] = $this->request->post['name'];

				$item_moved = false;

				if (isset($this->request->post['group_id'])) {
					$group_id = $this->request->post['group_id'];
					$old_group_id = $this->request->post['old_group_id'];

					//if group changed
					if ($group_id != $old_group_id) {
						$data['group_id'] = $group_id;
						$data['position'] = $this->model_menumanager_menu->get_last_position($group_id) + 1;
						$item_moved = true;
					}
				}

				if ($this->model_menumanager_menu->update_menu($data['id'],$data)) {
					if ($item_moved) {
						//move sub items
						$this->get_descendants($data['id']);
						if (!empty($this->ids)) {
							$ids = implode(', ', $this->ids);
							$sql = sprintf('UPDATE %s SET %s = %s WHERE %s IN (%s)', DB_PREFIX.'menu', 'group_id', $group_id, 'id', $ids);
							$update_sub = $this->db->query($sql);
						}
						$response['status'] = 4;
					} else {
						$response['status'] = 1;
						$d['title'] = $data['title'];
						$d['name'] = $data['name'];
						$d['url'] = $data['url'];
						$d['klass'] = $data['class']; //klass instead of class because of an error in js
						$response['menu'] = $d;
					}
				} else {
					$response['status'] = 2;
					$response['msg'] = 'Edit menu item error.';
				}
			} else {
				$response['status'] = 3;
			}
			header('Content-type: application/json');
			echo json_encode($response);
			exit;
		}
		$this->data['action'] = $this->url->link('extension/menumanager/update_menu_item', 'menu_id='.$menu_id.'&token=' . $this->session->data['token'], 'SSL');
		$this->template = 'menumanager/new_menu_item.tpl';
		$this->children = array(
				'common/header',
				'common/footer'
		);

		$this->response->setOutput($this->render());
	}
}

class Tree {

	/**
	 * variable to store temporary data to be processed later
	 *
	 * @var array
	 */
	var $data;

	/**
	 * Add an item
	 *
	 * @param int $id 			ID of the item
	 * @param int $parent 		parent ID of the item
	 * @param string $li_attr 	attributes for <li>
	 * @param string $label		text inside <li></li>
	 */
	function add_row($id, $parent, $li_attr, $label) {
		$this->data[$parent][] = array('id' => $id, 'li_attr' => $li_attr, 'label' => $label);
	}

	/**
	 * Generates nested lists
	 *
	 * @param string $ul_attr
	 * @return string
	 */
	function generate_list($ul_attr = '') {
		return $this->ul(0, $ul_attr);
	}

	/**
	 * Recursive method for generating nested lists
	 *
	 * @param int $parent
	 * @param string $attr
	 * @return string
	 */
	function ul($parent = 0, $attr = '') {
		static $i = 1;
		$indent = str_repeat("\t\t", $i);
		if (isset($this->data[$parent])) {
			if ($attr) {
				$attr = ' ' . $attr;
			}
			$html = "\n$indent";
			$html .= "<ul$attr>";
			$i++;
			foreach ($this->data[$parent] as $row) {
				$child = $this->ul($row['id']);
				$html .= "\n\t$indent";
				$html .= '<li'. $row['li_attr'] . '>';
				$html .= $row['label'];
				if ($child) {
					$i--;
					$html .= $child;
					$html .= "\n\t$indent";
				}
				$html .= '</li>';
			}
			$html .= "\n$indent</ul>";
			return $html;
		} else {
			return false;
		}
	}

	/**
	 * Clear the temporary data
	 *
	 */
	function clear() {
		$this->data = array();
	}
}
