<?php
class ControllerExtensionCustom extends Controller {
	private $error = array();

	public function index() {
		$this->language->load('extension/custom');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('setting/setting');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			 $this->response->ClearCache();
		$this->model_setting_setting->editSetting('custom_scss', $this->request->post);

		$this->session->data['success'] = $this->language->get('text_success');

		$this->redirect($this->url->link('extension/custom', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_configuration'] = $this->language->get('text_configuration');
		$this->data['entry_header'] = $this->language->get('entry_header');
		$this->data['entry_footer'] = $this->language->get('entry_footer');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		$this->data['action'] = $this->url->link('extension/custom', 'token=' . $this->session->data['token'], 'SSL');

		if(isset($this->session->data['redirect'])){
			$this->data['cancel'] = $this->session->data['redirect'];
		}else{
			$this->data['cancel'] = $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL');
		}

		$this->data['token'] = $this->session->data['token'];
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		if (isset($this->request->post['config_script_footer'])) {
			$this->data['config_script_footer'] = $this->request->post['config_script_footer'];
		} else {
			$this->data['config_script_footer'] = $this->config->get('config_script_footer');
		}

		if (isset($this->request->post['config_script_header'])) {
			$this->data['config_script_header'] = $this->request->post['config_script_header'];
		} else {
			$this->data['config_script_header'] = $this->config->get('config_script_header');
		}

		if (isset($this->request->post['config_custom_css'])) {
			$this->data['config_custom_css'] = $this->request->post['config_custom_css'];
		} else {
			$this->data['config_custom_css'] = $this->config->get('config_custom_css');
		}


		$this->template = 'extension/custom.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/custom')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
}
?>
