<?php

class ControllerCommonFinder extends Controller {
    public function index(){
        $this->document->setTitle('Finder');

        $this->data['heading_title'] = 'Finder';

        $this->data['breadcrumbs'] = [];

        $this->data['breadcrumbs'][] = [
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/finder', 'token='.$this->session->data['token'], true),
        ];

        $this->data['breadcrumbs'][] = [
            'text' => 'Finder',
            'href' => $this->url->link('common/finder', 'token='.$this->session->data['token'], true),
        ];

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }

        $this->data['connector_url'] = $this->url->link('common/finder/connector', 'token='.$this->session->data['token']);

        // Attach styles

        $this->document->addStyle('asset/finder/css/elfinder.min.css');
        $this->document->addStyle('asset/finder/css/theme.css');
        $this->document->addScript('asset/finder/js/elfinder.min.js');


        $this->template = 'common/finder.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
    }

    /**
     * finder connector
     */
    public function connector()
    {
        $autoload_path = 'library/finder/autoload.php';
        $autoload_full_path = DIR_SYSTEM.$autoload_path;

        if (!file_exists($autoload_full_path)) {
            echo 'Error! File '.$autoload_path.' not exists!';
            exit(1);
        }
        require $autoload_full_path;

        if($this->user->getGroupId()=='1') {
            $opts = [
                //'debug' => true,
                'bind' => array(
                  'upload.pre mkdir.pre mkfile.pre rename.pre archive.pre ls.pre' => array(
                        'Plugin.Normalizer.cmdPreprocess',
                        'Plugin.Sanitizer.cmdPreprocess'
                    ),
                    'ls' => array(
                        'Plugin.Normalizer.cmdPostprocess',
                        'Plugin.Sanitizer.cmdPostprocess'
                    ),
                    'upload.presave' => array(
                        'Plugin.AutoResize.onUpLoadPreSave',
                        'Plugin.Normalizer.onUpLoadPreSave'
                    )
                ),
                'plugin' => array(
                    'AutoResize' => array(
                        'enable' => true,
                        'maxWidth'  => 1920,
                        'maxHeight'  => 1920,
                        'quality' => 95
                    ),
                    'Normalizer' => array(
                      'convmap' => array(
                          ' ' => '_',
                          ',' => '_',
                          '^' => '_',
                          ',' => '-',
                          'à' => 'a',
                          'ä' => 'a',
                          'é' => 'e',
                          'è' => 'e',
                          'ü' => 'u',
                          'ö' => 'o'
                      )
                  ),
                ),
                'roots' => [
                    [
                        'driver' => 'Osdvn',
                        'path' => DIR_IMAGE.'data/',
                        'URL' => HTTP_IMAGE.'data/',
                        'tmbPath' 		=> '.tmp',
                        'tmbCrop'		=> false,
                        'tmbSize'		=> '100',
                        'treeDeep'		=> 0,
                        'checkSubfolders' => false,
                        'uploadDeny' => ['all'],
                        'uploadAllow' => ['image', 'text/plain'],
                        'uploadOrder' => ['deny', 'allow'],
                        'accessControl' => [$this, 'access'],
                    ],
                    [
                        'driver' => 'Osdvn',
                        'path' => DIR_IMAGE.'users/',
                        'URL' => HTTP_IMAGE.'users/',
                        'tmbPath' 		=> '.tmp',
                        'tmbCrop'		=> false,
                        'tmbSize'		=> '100',
                        'treeDeep'		=> 0,
                        'checkSubfolders' => false,
                        'uploadDeny' => ['all'],
                        'uploadAllow' => ['image', 'text/plain'],
                        'uploadOrder' => ['deny', 'allow'],
                        'accessControl' => [$this, 'access'],
                    ],
                    [
                        // Trash volume
                        'id'            => '1',
                        'driver'        => 'Trash',
                        'path'          => DIR_IMAGE.'trash/',
                        'tmbSize'	    => '100',
                        'winHashFix'    => DIRECTORY_SEPARATOR !== '/', // to make hash same to Linux one on windows too
                        'uploadDeny'    => array('all'),                // Recomend the same settings as the original volume that uses the trash
                        'uploadAllow'   => array('image/x-ms-bmp', 'image/gif', 'image/jpeg', 'image/png', 'image/x-icon', 'text/plain'), // Same as above
                        'uploadOrder'   => array('deny', 'allow'),      // Same as above
                        'accessControl' => 'access',                    // Same as above
                    ],
                ]
            ];
        } else {
          $opts = [
              //'debug' => true,
              'bind' => array(
                  'upload.pre mkdir.pre mkfile.pre rename.pre archive.pre ls.pre' => array(
                        'Plugin.Normalizer.cmdPreprocess',
                        'Plugin.Sanitizer.cmdPreprocess'
                    ),
                    'ls' => array(
                        'Plugin.Normalizer.cmdPostprocess',
                        'Plugin.Sanitizer.cmdPostprocess'
                    ),
                    'upload.presave' => array(
                        'Plugin.AutoResize.onUpLoadPreSave',
                        'Plugin.Normalizer.onUpLoadPreSave'
                    )
              ),
              'plugin' => array(
                  'AutoResize' => array(
                      'enable' => true,
                      'maxWidth'  => 1920,
                      'maxHeight'  => 1920,
                      'quality' => 95
                  ),
                  'Normalizer' => array(
                      'convmap' => array(
                          ' ' => '_',
                          ',' => '_',
                          '^' => '_',
                          ',' => '-',
                          'à' => 'a',
                          'ä' => 'a',
                          'é' => 'e',
                          'è' => 'e',
                          'ü' => 'u',
                          'ö' => 'o'
                      )
                  ),
              ),
              'roots' => [
                  [
                      'driver' => 'Osdvn',
                      'path' => DIR_IMAGE.'users/'.$this->user->getId().'/',
                      'URL' => HTTP_IMAGE.'users/'.$this->user->getId().'/',
                      'tmbPath' 		=> '.tmp',
                      'tmbCrop'		=> false,
                      'tmbSize'		=> '100',
                      'treeDeep'		=> 0,
                      'checkSubfolders' => false,
                      'uploadDeny' => ['all'],
                      'uploadAllow' => ['image', 'text/plain'],
                      'uploadOrder' => ['deny', 'allow'],
                      'accessControl' => [$this, 'access'],
                  ]
              ]
          ];
        }

        // Run finder
        $connector = new elFinderConnector(new elFinder($opts));
        $connector->run();
    }

    /**
     * Simple function to demonstrate how to control file access using "accessControl" callback.
     * This method will disable accessing files/folders starting from '.' (dot)
     *
     * @param  string $attr attribute name (read|write|locked|hidden)
     * @param  string $path file path relative to volume root directory started with directory separator
     *
     * @return bool|null
     **/
    public function access($attr, $path, $data, $volume)
    {
        // if file/folder begins with '.' (dot)
        if (strpos(basename($path), '.') === 0) {
            // set read+write to false, other (locked+hidden) set to true
            return !($attr == 'read' || $attr == 'write');
        }

        // finder decide it itself
        return null;
    }
}
