<?php

class ControllerSaleOrder extends Controller {

    private $error = array();

    public function index() {
        $this->language->load('sale/order');
        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('sale/order');

        $this->getList();
    }

    protected function getList() {

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';


        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('sale/order', 'token=' . $this->session->data['token'] . $url, 'SSL'),
            'separator' => ' :: '
        );

        $this->data['orders'] = array();

        $this->load->model('tool/image');
        $this->load->model('sale/order');

		$data = array(
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

        $this->data['orders'] = $this->model_sale_order->getOrders($data);
        $order_total = $this->model_sale_order->getTotalOrders($data);
		$this->data['delete'] = $this->url->link('sale/order/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');


        $this->data['heading_title'] = $this->language->get('heading_title');
        $this->data['text_enabled'] = $this->language->get('text_enabled');
        $this->data['text_disabled'] = $this->language->get('text_disabled');
        $this->data['text_no_results'] = $this->language->get('text_no_results');
        $this->data['text_image_manager'] = $this->language->get('text_image_manager');
        $this->data['button_delete'] = $this->language->get('button_delete');
        $this->data['text_list'] = $this->language->get('text_list');
        $this->data['entry_phone'] = $this->language->get('entry_phone');
        $this->data['entry_customer'] = $this->language->get('entry_customer');
        $this->data['entry_content'] = $this->language->get('entry_content');
        $this->data['entry_status'] = $this->language->get('entry_status');
        $this->data['entry_date_added'] = $this->language->get('entry_date_added');

        $this->data['token'] = $this->session->data['token'];

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }

        $url = '';

        $pagination = new Pagination();
        $pagination->total = $order_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_admin_limit');
        $pagination->text = $this->language->get('text_pagination');
        $pagination->url = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

        $this->data['pagination'] = $pagination->render();

        $this->template = 'sale/order_list.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }

	public function updatestatus() {
		$this->load->language('sale/order');
		$this->load->model('sale/order');
		$output='';
		if(isset($this->request->get['object_id'])){
			$get_request = explode('-',$this->request->get['object_id']);
			if(count($get_request)==2){
				$column_name = $get_request[0];
				$order_id = $get_request[1];
				$result = $this->model_sale_order->getorder($order_id);
				if($result[$column_name]){
					$this->model_sale_order->updateStatus($order_id, $column_name, 0);
					} else {
					$this->model_sale_order->updateStatus($order_id, $column_name, 1);
				}
				$result = $this->model_sale_order->getorder($order_id);
				$output = $result[$column_name] ? $this->language->get('text_enabled') : $this->language->get('text_disabled');
			}
		}
		$this->response->setOutput($output);
	}

  public function delete(){
		$this->load->model('sale/order');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $order_id) {
				$this->model_sale_order->deleteorder($order_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');
			$this->redirect($this->url->link('sale/order', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'sale/order')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return true;
			} else {
			return false;
		}
	}
}

?>
