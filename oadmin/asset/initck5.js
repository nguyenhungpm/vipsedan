/*build* 16072021*/
function getURLVar(key) {
    var value = [];
    var query = String(document.location).split('?');
    if (query[1]) {
      var part = query[1].split('&');
      for (i = 0; i < part.length; i++) {
        var data = part[i].split('=');
        if (data[0] && data[1]) {
            value[data[0]] = data[1];
        }
      }
      if (value[key]) {
            return value[key];
        } else {
            return '';
      }
    }
}

function filebrowser(image,thumb,start = false) {
	var thumburl = '',  fileo = '', token = getURLVar('token');
	var fb = $('<div id="elfinder-dialog"/>').dialogelfinder({
		cssAutoLoad : ['//cdn.jsdelivr.net/gh/osdvn/mkmate/finder/css/Material/css/theme-light.min.css'],
		url: 'index.php?route=common/finder/connector&token='+token,
		uiOptions: {
			toolbar: [
				['reload'],
				['mkdir', 'upload'],
				['open','getfile'],
				['undo', 'redo','rm'],
				['copy', 'cut', 'paste'],
				['duplicate', 'rename', 'edit', 'resize'],
				['selectall', 'selectnone', 'selectinvert'],
				['search'],
				['view', 'sort'],
				['fullscreen']
			]
		},
		contextmenu: {
			navbar: ['open', '|', 'upload', 'mkdir', '|', 'copy', 'cut', 'paste', 'duplicate', '|', '|', 'rename', '|', 'rm'],
			cwd: ['undo', 'redo', '|', 'back', 'up', 'reload', '|', 'upload', 'mkdir', 'paste', '|', '|', 'view', 'sort', 'selectall', 'colwidth', '|', 'info', '|', 'fullscreen', '|'],
			files: ['getfile', '|', 'open', 'download', 'opendir', '|', 'upload', 'mkdir', '|', 'copy', 'cut', 'paste','|', 'empty', '|', 'rename', 'edit', 'resize', 'rm','|', 'selectall', 'selectinvert']
		},
		width: "80%",
		height: "550px",
		commandsOptions : {
			getfile : {
                multiple: (start>0)?true:false,
				oncomplete : 'close',
				folders : false
			}
		},
		destroyOnClose : true,
        useBrowserHistory : false,
		getFileCallback: function(filecalback,fb) {
            len = filecalback.length;
            if(len > 1) {
                var index, len;
                for (index = 0; index < len; ++index) {
                    if(index>0){
                        addImage();
                    }
                    let loop = parseInt(start) + parseInt(index),
                        image_target = image + loop,
                        thumb_target = thumb + loop,
                        thumburl = fb.convAbsUrl(filecalback[index].tmb),
                        path = filecalback[index].path;
                    $('#' + image_target).attr('value',path);
                    if (thumburl) {
                        $('#' + thumb_target).attr("src", thumburl);
                    }
                }
            } else {
                let url = filecalback.url,
                    thumburl = fb.convAbsUrl(filecalback.tmb),
                    path = filecalback.path;
                if(filecalback.mime != 'image/svg+xml'){
                        $('#' + thumb).replaceWith('<img src="' + thumburl + '" alt="" id="' + thumb + '" />');
                }else{
                        $('#' + thumb).replaceWith('<img src="' + url + '" alt="" id="' + thumb + '" />');
                }
                $('#' + image).attr('value',path);
            }
           $("#elfinder-dialog").remove();
		}
	});
}

$(document).ready(function() {
	getCKEditor();
});
function getCKEditor(__class=false){
    if(__class) {
        var allEditors = document.querySelectorAll(__class);
      }else{
        var allEditors = document.querySelectorAll('.ckeditor5');
    }
    const   token = getURLVar('token'),
            uploadTargetHash = 'l1_Lw',
            connectorUrl = 'index.php?route=common/finder/connector&token='+token,
            customColorPalette = [
                    {
                        color: 'hsl(4, 90%, 58%)',
                        label: 'Red'
                    },
                    {
                        color: 'hsl(340, 82%, 52%)',
                        label: 'Pink'
                    },
                    {
                        color: 'hsl(291, 64%, 42%)',
                        label: 'Purple'
                    },
                    {
                        color: 'hsl(262, 52%, 47%)',
                        label: 'Deep Purple'
                    },
                    {
                        color: 'hsl(231, 48%, 48%)',
                        label: 'Indigo'
                    },
                    {
                        color: 'hsl(207, 90%, 54%)',
                        label: 'Blue'
                    },

                ];

    for (var i = 0; i < allEditors.length; ++i) {
    // To create CKEditor 5 classic editor
    ClassicEditor.create(allEditors[i], {
        toolbar: {
            items: [
                'heading',
                '|',
                'bold',
                'italic',
                'underline',
                'removeFormat',
                'blockQuote',
                'link',
                'CKFinder',
                'bulletedList',
                'numberedList',
                'alignment',
                '|',
                'fontFamily',
                'fontSize',
                'fontColor',
                'fontBackgroundColor',
                'outdent',
                'indent',
                '|',
                'insertTable',
                'mediaEmbed',
                'horizontalLine',
                '|',
                'strikethrough',
                'undo',
                'redo',
                'superscript',
                'subscript',
                'htmlEmbed'
            ],
            shouldNotGroupWhenFull: true
        },
        table: {
            contentToolbar: [
                'tableColumn', 'tableRow', 'mergeTableCells',
                'tableProperties', 'tableCellProperties'
            ],

            // Set the palettes for tables.
            tableProperties: {
                borderColors: customColorPalette,
                backgroundColors: customColorPalette
            },

            // Set the palettes for table cells.
            tableCellProperties: {
                borderColors: customColorPalette,
                backgroundColors: customColorPalette
            }
        },

        //plugins: [ Table, TableToolbar, TableProperties, TableCellProperties],
        image: {
                toolbar: [ 'imageTextAlternative', '|', 'imageStyle:alignLeft',  'imageStyle:alignCenter', 'imageStyle:alignRight' ],
                styles: [
                    'full',
                    'block',
                    "inline",
                    'side',
                    'alignCenter',
                    'alignLeft',
                    'alignRight'
                ]
        },
        mediaEmbed: {
            removeProviders: [ 'instagram']
        },
        heading: {
            options: [
                { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
                { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
                { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' },
                { model: 'heading3', view: 'h3', title: 'Heading 3', class: 'ck-heading_heading3' },
                { model: 'heading4', view: 'h4', title: 'Heading 4', class: 'ck-heading_heading4' },
                { model: 'heading5', view: 'h5', title: 'Heading 5', class: 'ck-heading_heading5' },
                { model: 'heading6', view: 'h6', title: 'Heading 6', class: 'ck-heading_heading6' },
                { model: 'code', view: 'code', title: 'Code', class: 'ck-code' }
            ]
        },
        link: {
            decorators: {
                openInNewTab: {
                    mode: 'manual',
                    label: 'Open in a new tab',
                    attributes: {
                        target: '_blank',
                        rel: 'noopener noreferrer'
                    }
                }
            }
        }
    } )
    .then(editor => {
        const ckf = editor.commands.get('ckfinder'),
            fileRepo = editor.plugins.get('FileRepository'),
            ntf = editor.plugins.get('Notification'),
            i18 = editor.locale.t,
            // Insert images to editor window
            insertImages = urls => {
                const imgCmd = editor.commands.get('imageUpload');
                if (!imgCmd.isEnabled) {
                    ntf.showWarning(i18('Could not insert image at the current position.'), {
                        title: i18('Inserting image failed'),
                        namespace: 'ckfinder'
                    });
                    return;
                }
                editor.execute('imageInsert', { source: urls });
            },
            // To get elFinder instance
            getfm = open => {
                return new Promise((resolve, reject) => {
                    // Execute when the elFinder instance is created
                    const done = () => {
                        if (open) {
                            // request to open folder specify
                            if (!Object.keys(_fm.files()).length) {
                                // when initial request
                                _fm.one('open', () => {
                                    _fm.file(open)? resolve(_fm) : reject(_fm, 'errFolderNotFound');
                                });
                            } else {
                                // elFinder has already been initialized
                                new Promise((res, rej) => {
                                    if (_fm.file(open)) {
                                        res();
                                    } else {
                                        // To acquire target folder information
                                        _fm.request({cmd: 'parents', target: open}).done(e =>{
                                            _fm.file(open)? res() : rej();
                                        }).fail(() => {
                                            rej();
                                        });
                                    }
                                }).then(() => {
                                    // Open folder after folder information is acquired
                                    _fm.exec('open', open).done(() => {
                                        resolve(_fm);
                                    }).fail(err => {
                                        reject(_fm, err? err : 'errFolderNotFound');
                                    });
                                }).catch((err) => {
                                    reject(_fm, err? err : 'errFolderNotFound');
                                });
                            }
                        } else {
                            // show elFinder manager only
                            resolve(_fm);
                        }
                    };

                    // Check elFinder instance
                    if (_fm) {
                        // elFinder instance has already been created
                        done();
                    } else {
                        // To create elFinder instance
                        _fm = $('<div/>').dialogelfinder({
                            // dialog title
                            title : 'Finder',
                            cssAutoLoad : ['//cdn.jsdelivr.net/gh/osdvn/mkmate/finder/css/Material/css/theme-light.min.css'],
                            // connector URL
                            url : connectorUrl,
                            // start folder setting
                            startPathHash : open? open : void(0),
                            // Set to do not use browser history to un-use location.hash
                            useBrowserHistory : false,
                            // Disable auto open
                            autoOpen : false,
                            // elFinder dialog width
                            uiOptions: {
                                toolbar: [
                                    ['reload'],
                                    ['mkdir', 'upload'],
                                    ['open','getfile'],
                                    ['undo', 'redo','rm'],
                                    ['copy', 'cut', 'paste'],
                                    ['duplicate', 'rename', 'edit', 'resize'],
                                    ['selectall', 'selectnone', 'selectinvert'],
                                    ['search'],
                                    ['view', 'sort'],
                                    ['fullscreen']
                                ]
                            },
                            contextmenu: {
                                navbar: ['open', '|', 'upload', 'mkdir', '|', 'copy', 'cut', 'paste', 'duplicate', '|', '|', 'rename', '|', 'rm'],
                                cwd: ['undo', 'redo', '|', 'back', 'up', 'reload', '|', 'upload', 'mkdir', 'paste', '|', '|', 'view', 'sort', 'selectall', 'colwidth', '|', 'info', '|', 'fullscreen', '|'],
                                files: ['getfile', '|', 'open', 'download', 'opendir', '|', 'upload', 'mkdir', '|', 'copy', 'cut', 'paste','|', 'empty', '|', 'rename', 'edit', 'resize', 'rm','|', 'selectall', 'selectinvert']
                            },
                            width : '80%',
                            height: '550px',
                            // set getfile command options
                            commandsOptions : {
                                getfile: {
                                    oncomplete : 'close',
                                    multiple : true
                                }
                            },
                            // Insert in CKEditor when choosing files
                            getFileCallback : (files, fm) => {
                                let imgs = [];
                                fm.getUI('cwd').trigger('unselectall');
                                $.each(files, function(i, f) {
                                    if (f && f.mime.match(/^image\//i)) {
                                        imgs.push(fm.convAbsUrl(f.url));
                                    } else {
                                        editor.execute('link', fm.convAbsUrl(f.url));
                                    }
                                });
                                if (imgs.length) {
                                    insertImages(imgs);
                                }
                            }
                        }).elfinder('instance');
                        done();
                    }
                });
            };

        // elFinder instance
        let _fm;

        if (ckf) {
            // Take over ckfinder execute()
            ckf.execute = () => {
                getfm().then(fm => {
                    fm.getUI().dialogelfinder('open');
                });
            };
        }

        // Make uploader
        const uploder = function(loader) {
            let upload = function(file, resolve, reject) {
                getfm(uploadTargetHash).then(fm => {
                    let fmNode = fm.getUI();
                    fmNode.dialogelfinder('open');
                    fm.exec('upload', {files: [file], target: uploadTargetHash}, void(0), uploadTargetHash)
                        .done(data => {
                            if (data.added && data.added.length) {
                                fm.url(data.added[0].hash, { async: true }).done(function(url) {
                                    resolve({
                                        'default': fm.convAbsUrl(url)
                                    });
                                    fmNode.dialogelfinder('close');
                                }).fail(function() {
                                    reject('errFileNotFound');
                                });
                            } else {
                                reject(fm.i18n(data.error? data.error : 'errUpload'));
                                fmNode.dialogelfinder('close');
                            }
                        })
                        .fail(err => {
                            const error = fm.parseError(err);
                            reject(fm.i18n(error? (error === 'userabort'? 'errAbort' : error) : 'errUploadNoFiles'));
                        });
                }).catch((fm, err) => {
                    const error = fm.parseError(err);
                    reject(fm.i18n(error? (error === 'userabort'? 'errAbort' : error) : 'errUploadNoFiles'));
                });
            };

            this.upload = function() {
                return new Promise(function(resolve, reject) {
                    if (loader.file instanceof Promise || (loader.file && typeof loader.file.then === 'function')) {
                        loader.file.then(function(file) {
                            upload(file, resolve, reject);
                        });
                    } else {
                        upload(loader.file, resolve, reject);
                    }
                });
            };
            this.abort = function() {
                _fm && _fm.getUI().trigger('uploadabort');
            };
        };

        // Set up image uploader
        fileRepo.createUploadAdapter = loader => {
            return new uploder(loader);
        };
    })
    .catch(error => {
        console.error( error );
    });

    }
}
