<?php
class ModelExtensionMegamenu extends Model {

	public function addMegamenu($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "megamenu SET sort_order = '". (int)$data['sort_order'] ."', image = '". $data['image'] ."', parent_id = '". $data['parent_id'] ."', url = '". $data['url'] ."', status = '". (int)$data['status'] ."'");
		$megamenu_id = $this->db->getLastId();
		if(!empty($data['detail'])){
			foreach($data['detail'] as $language_id => $detail){
				$this->db->query("INSERT INTO " . DB_PREFIX . "megamenu_description SET megamenu_id = '". $megamenu_id ."', language_id = '". $language_id ."', name = '". $this->db->escape($detail['name']) ."'");
			}
		}
	}
	
	public function editMegamenu($megamenu_id, $data) {
		// $this->session->data['d'] = $data;
		$this->db->query("UPDATE " . DB_PREFIX . "megamenu SET status = '". (int)$data['status'] ."', sort_order = '". (int)$data['sort_order'] ."', image = '". $data['image'] ."', parent_id = '". $data['parent_id'] ."', url = '". $data['url'] ."' WHERE megamenu_id = '" . (int)$megamenu_id . "'");
		
		if(!empty($data['detail'])){
			$this->db->query("DELETE FROM " . DB_PREFIX . "megamenu_description WHERE megamenu_id = '". $megamenu_id ."'");
			foreach($data['detail'] as $language_id => $detail){
				$this->db->query("INSERT INTO " . DB_PREFIX . "megamenu_description SET megamenu_id = '". $megamenu_id ."', language_id = '". $language_id ."', name = '". $this->db->escape($detail['name']) ."'");
			}
		}
		
		return true;
	}
	
	public function deleteMegamenu($megamenu_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "megamenu WHERE megamenu_id = '" . (int)$megamenu_id . "'");
	}
	
	public function getMegamenu($megamenu_id) {
		$query = $this->db->query("SELECT  * FROM " . DB_PREFIX . "megamenu WHERE megamenu_id = '" . (int)$megamenu_id . "'");

		return $query->row;
	}
	
	public function getDetail($megamenu_id){
		$data = array();
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "megamenu_description WHERE megamenu_id = '". $megamenu_id ."'");
		foreach($query->rows as $rs){
			$data[$rs['language_id']] = array(
				'name' => $rs['name']
			);
		}
		return $data;
	}
	
	public function getMegamenus($data) {
		
		$sql = "SELECT * FROM " . DB_PREFIX . "megamenu ORDER BY sort_order ASC";
		if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }
			
		$query = $this->db->query($sql);
		
		return $query->rows;
	}
	
	public function getTotalMegamenus($data) {
		
		// $this->check_db();
		
		$sql = "SELECT * FROM " . DB_PREFIX . "megamenu";
			
		$query = $this->db->query($sql);
		
		return $query->num_rows;
	}
	
	/* public function getParent($megamenu_id){
		$data = array(1);
		if($megamenu_id != 0){
			$query = $this->db->query("SELECT parent_id FROM " . DB_PREFIX . "megamenu WHERE megamenu_id = '". $megamenu_id ."'");
			$this->getParent($query->row['parent_id']);
			$data[] = $query->row['parent_id'];
		}else{
			return $data;
		}
	} */
	
	public function getParent($parent_id) {
		$news_category_data = array();
		$this->load->model('tool/image');
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "megamenu c LEFT JOIN " . DB_PREFIX . "megamenu_description cd ON (c.megamenu_id = cd.megamenu_id) WHERE c.parent_id = '" . (int)$parent_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY c.sort_order, cd.name ASC");

		foreach ($query->rows as $result) {
			$detail = $this->getDetail($result['megamenu_id']);
			
			if (isset($result) && $result['image'] && file_exists(DIR_IMAGE . $result['image'])) {
	            $thumb = $this->model_tool_image->resize($result['image'], 40, 40);
	        } else {
	            $thumb = $this->model_tool_image->resize('no_image.jpg', 40, 40);
	        }
			
			$news_category_data[] = array(
				'megamenu_id' => $result['megamenu_id'],
				'name'        => $this->getPath($result['megamenu_id'], $this->config->get('config_language_id')),
				'url' 	=> $result['url'],
				'parent_id' 	=> $result['parent_id'],
				'image' 	=> !empty($result['image']) ? $result['image'] : '',
				'thumb' 	=> $thumb,
				'detail' 	=> $detail,
				'sort_order' 	=> $result['sort_order'],
				'status' 	=> $result['status'],
				'edit' => $this->url->link('extension/megamenu', 'token=' . $this->session->data['token'] . '&megamenu_id=' . $result['megamenu_id'], 'SSL')
			);

			$news_category_data = array_merge($news_category_data, $this->getParent($result['megamenu_id']));
		}
		return $news_category_data;
	}
	
	public function getLink($data){
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE LOWER(keyword) like LOWER('%" . $data['filter_name'] . "%') LIMIT ".$data['limit']." ");
        return $query->rows;
    }
	
	public function getPath($megamenu_id) {
		$query = $this->db->query("SELECT name, parent_id FROM " . DB_PREFIX . "megamenu c LEFT JOIN " . DB_PREFIX . "megamenu_description cd ON (c.megamenu_id = cd.megamenu_id) WHERE c.megamenu_id = '" . (int)$megamenu_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY c.sort_order, cd.name ASC");

		$news_category_info = $query->row;

		if ($news_category_info['parent_id']) {
			return $this->getPath($news_category_info['parent_id'], $this->config->get('config_language_id')) . $this->language->get('text_separator') . $news_category_info['name'];
		} else {
			return $news_category_info['name'];
		}
	}

}
?>