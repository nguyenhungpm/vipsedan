<?php
class ModelCatalogDistrict extends Model {

	public function getDistricts($province_id){
		$sql = "SELECT * FROM " . DB_PREFIX . "district p  WHERE 1";
		if(!empty($province_id)){
			$sql .= " AND p.province_id='". $province_id ."'";
		}
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getDistrict($district_id){
		$query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'district_id=" . (int)$district_id . "') AS keyword FROM " . DB_PREFIX . "district WHERE district_id = '" . (int)$district_id . "'");

		return $query->row;
	}
	public function getDistrictDescriptions($district_id) {
		$province_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "district_description WHERE district_id = '" . (int)$district_id . "'");

		foreach ($query->rows as $result) {
			$province_description_data[$result['language_id']] = array(
				'name'  => $result['name']
			);
		}
		return $province_description_data;
	}
	public function addDistrict($data){
		$this->db->query("INSERT INTO " . DB_PREFIX . "district SET sort_order = '" . (int)$data['sort_order'] . "', name = '" . $this->db->escape($data['name']) . "', province_id = '" . (int)$data['province'] . "', status = '" . (int)$data['status'] . "'");
		$district_id = $this->db->getLastId();
		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'district_id=" . (int)$district_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}
	}
	public function editDistrict($district_id, $data){
		$this->db->query("UPDATE " . DB_PREFIX . "district SET sort_order = '" . (int)$data['sort_order'] . "', name = '" . $this->db->escape($data['name']) . "', province_id = '" . (int)$data['province'] . "', status = '" . (int)$data['status'] . "' WHERE district_id = '" . (int)$district_id . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'district_id=" . (int)$district_id. "'");

		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'district_id=" . (int)$district_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}
	}
	public function deleteDistrict($district_id) {
		/* $query = $this->db->query("SELECT district_id FROM " . DB_PREFIX . "district WHERE district_id = '" . (int)$district_id . "'"); */
		/* foreach ($query->rows as $result) {
			$this->deleteArea($result['news_category_id']);
		} */

		$this->db->query("DELETE FROM " . DB_PREFIX . "district WHERE district_id = '" . (int)$district_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'district_id=" . (int)$district_id . "'");

	}

	public function getPath($news_category_id) {
		$query = $this->db->query("SELECT name, parent_id FROM " . DB_PREFIX . "news_category c LEFT JOIN " . DB_PREFIX . "news_category_description cd ON (c.news_category_id = cd.news_category_id) WHERE c.news_category_id = '" . (int)$news_category_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY c.sort_order, cd.name ASC");

		$news_category_info = $query->row;

		if ($news_category_info['parent_id']) {
			return $this->getPath($news_category_info['parent_id'], $this->config->get('config_language_id')) . $this->language->get('text_separator') . $news_category_info['name'];
		} else {
			return $news_category_info['name'];
		}
	}



	public function getDistrictStores($news_category_id) {
		$news_category_store_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_category_to_store WHERE news_category_id = '" . (int)$news_category_id . "'");

		foreach ($query->rows as $result) {
			$news_category_store_data[] = $result['store_id'];
		}

		return $news_category_store_data;
	}

	public function getDistrictLayouts($news_category_id) {
		$news_category_layout_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_category_to_layout WHERE news_category_id = '" . (int)$news_category_id . "'");

		foreach ($query->rows as $result) {
			$news_category_layout_data[$result['store_id']] = $result['layout_id'];
		}

		return $news_category_layout_data;
	}

	public function getTotalNewsCategories() {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "news_category");

		return $query->row['total'];
	}

	public function getTotalNewsCategoriesByImageId($image_id) {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "news_category WHERE image_id = '" . (int)$image_id . "'");

		return $query->row['total'];
	}

	public function getTotalNewsCategoriesByLayoutId($layout_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "news_category_to_layout WHERE layout_id = '" . (int)$layout_id . "'");

		return $query->row['total'];
	}

}
?>
