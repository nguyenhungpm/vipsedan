<?php
class ModelCatalogTypeCar extends Model {

	public function addTypeCar($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "type_car SET sort_order = '". (int)$data['sort_order'] ."', link = '". $this->db->escape($data['link']) ."', seat = '". (int)$data['seat'] ."', cost_tinh = '". (int)$data['cost_tinh'] ."', cost_tinh_ov = '". (int)$data['cost_tinh_ov'] ."', cost_tienchuyen = '". (int)$data['cost_tienchuyen'] ."', cost_tienchuyen_ov = '". (int)$data['cost_tienchuyen_ov'] ."', luggage = '". (int)$data['luggage'] ."', status = '". (int)$data['status'] ."'");
		$type_car_id = $this->db->getLastId();
		
		if(!empty($data['image'])) $this->db->query("UPDATE " . DB_PREFIX . "type_car SET image = '". $data['image'] ."' WHERE type_car_id = '". $type_car_id ."'");
		
		if(!empty($data['detail'])){
			foreach($data['detail'] as $language_id => $detail){
				$this->db->query("INSERT INTO " . DB_PREFIX . "type_car_description SET type_car_id = '". $type_car_id ."', language_id = '". $language_id ."', name = '". $this->db->escape($detail['name']) ."'");
			}
		}
	}
	
	public function editTypeCar($type_car_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "type_car SET sort_order = '". (int)$data['sort_order'] ."', link = '". $this->db->escape($data['link']) ."', seat = '". (int)$data['seat'] ."', cost_tinh = '". (int)$data['cost_tinh'] ."', cost_tinh_ov = '". (int)$data['cost_tinh_ov'] ."', cost_tienchuyen = '". (int)$data['cost_tienchuyen'] ."', cost_tienchuyen_ov = '". (int)$data['cost_tienchuyen_ov'] ."', luggage = '". (int)$data['luggage'] ."', status = '". (int)$data['status'] ."' WHERE type_car_id = '" . (int)$type_car_id . "'");
		
		if(!empty($data['image'])) $this->db->query("UPDATE " . DB_PREFIX . "type_car SET image = '". $data['image'] ."' WHERE type_car_id = '". $type_car_id ."'");
		
		if(!empty($data['detail'])){
			$this->db->query("DELETE FROM " . DB_PREFIX . "type_car_description WHERE type_car_id = '". $type_car_id ."'");
			foreach($data['detail'] as $language_id => $detail){
				$this->db->query("INSERT INTO " . DB_PREFIX . "type_car_description SET type_car_id = '". $type_car_id ."', language_id = '". $language_id ."', name = '". $this->db->escape($detail['name']) ."'");
			}
		}
		return true;
	}
	
	public function deleteTypeCar($type_car_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "type_car WHERE type_car_id = '" . (int)$type_car_id . "'");
	}
	
	public function getTypeCar($type_car_id) {
		$query = $this->db->query("SELECT  * FROM " . DB_PREFIX . "type_car WHERE type_car_id = '" . (int)$type_car_id . "'");

		return $query->row;
	}
	
	public function getDetail($type_car_id){
		$data = array();
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "type_car_description WHERE type_car_id = '". $type_car_id ."'");
		foreach($query->rows as $rs){
			$data[$rs['language_id']] = array(
				'name' => $rs['name']
			);
		}
		return $data;
	}
	
	public function getTypeCars($data) {
		
		$sql = "SELECT * FROM " . DB_PREFIX . "type_car ORDER BY sort_order ASC";
		if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }
			
		$query = $this->db->query($sql);
		
		return $query->rows;
	}
	
	public function getTotalTypeCars($data) {
		
		// $this->check_db();
		
		$sql = "SELECT * FROM " . DB_PREFIX . "type_car";
			
		$query = $this->db->query($sql);
		
		return $query->num_rows;
	}
}
?>