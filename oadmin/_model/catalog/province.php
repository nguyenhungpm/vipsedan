<?php
class ModelCatalogProvince extends Model {
	
	public function getProvinces($area_id){
		$sql = "SELECT * FROM " . DB_PREFIX . "province p LEFT JOIN " . DB_PREFIX . "province_description pd ON (p.provinceid=pd.province_id) WHERE pd.language_id='" . (int)$this->config->get('config_language_id') . "'";
		if(!empty($area_id)){
			$sql .= " AND p.area_id='". $area_id ."'";
		}
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getProvince($provinceid){
		$query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'provinceid=" . (int)$provinceid . "') AS keyword FROM " . DB_PREFIX . "province WHERE provinceid = '" . (int)$provinceid . "'");
		
		return $query->row;
	}
	public function getProvinceDescriptions($provinceid) {
		$province_description_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "province_description WHERE province_id = '" . (int)$provinceid . "'");
		
		foreach ($query->rows as $result) {
			$province_description_data[$result['language_id']] = array(
				'name'  => $result['name']
			);
		}
		return $province_description_data;
	}
	public function addProvince($data){
		$this->db->query("INSERT INTO " . DB_PREFIX . "province SET sort_order = '" . (int)$data['sort_order'] . "', area_id = '" . (int)$data['area'] . "', status = '" . (int)$data['status'] . "'");
		$provinceid = $this->db->getLastId();
		foreach ($data['province_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "province_description SET province_id = '" . (int)$provinceid . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "'");
		}
		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'news_category_id=" . (int)$news_category_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}
	}
	public function editProvince($provinceid, $data){
		$this->db->query("UPDATE " . DB_PREFIX . "province SET sort_order = '" . (int)$data['sort_order'] . "', area_id = '" . (int)$data['area'] . "', status = '" . (int)$data['status'] . "' WHERE provinceid = '" . (int)$provinceid . "'");
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "province_description WHERE province_id = '" . (int)$provinceid . "'");
		foreach ($data['province_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "province_description SET province_id = '" . (int)$provinceid . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "'");
		}
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'provinceid=" . (int)$provinceid. "'");
		
		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'provinceid=" . (int)$provinceid . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}
	}
	public function deleteProvince($provinceid) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "province_description WHERE province_id = '" . (int)$provinceid . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "province WHERE provinceid = '" . (int)$provinceid . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'provinceid=" . (int)$provinceid . "'");

	} 

	public function getPath($news_category_id) {
		$query = $this->db->query("SELECT name, parent_id FROM " . DB_PREFIX . "news_category c LEFT JOIN " . DB_PREFIX . "news_category_description cd ON (c.news_category_id = cd.news_category_id) WHERE c.news_category_id = '" . (int)$news_category_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY c.sort_order, cd.name ASC");
		
		$news_category_info = $query->row;
		
		if ($news_category_info['parent_id']) {
			return $this->getPath($news_category_info['parent_id'], $this->config->get('config_language_id')) . $this->language->get('text_separator') . $news_category_info['name'];
		} else {
			return $news_category_info['name'];
		}
	}
	
}
?>