<?php
class ModelCatalogDoctor extends Model {

	public function addDoctor($data) {
		
		$this->db->query("INSERT INTO " . DB_PREFIX . "doctor SET image='". $this->db->escape($data['image']) ."', chucdanh='". $this->db->escape($data['chucdanh']) ."', description='". $this->db->escape($data['description']) ."', sort_order = '". (int)$data['sort_order'] ."', status = '". (int)$data['status'] ."'");
		$doctor_id = $this->db->getLastId();
		if(!empty($data['detail'])){
			foreach($data['detail'] as $language_id => $detail){
				$this->db->query("INSERT INTO " . DB_PREFIX . "doctor_description SET doctor_id = '". $doctor_id ."', language_id = '". $language_id ."', name = '". $this->db->escape($detail['name']) ."'");
			}
		}
	}
	
	public function editDoctor($doctor_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "doctor SET image='". $this->db->escape($data['image']) ."', chucdanh='". $this->db->escape($data['chucdanh']) ."', description='". $this->db->escape($data['description']) ."', status = '". (int)$data['status'] ."', sort_order = '". (int)$data['sort_order'] ."' WHERE doctor_id = '" . (int)$doctor_id . "'");
		if(!empty($data['detail'])){
			$this->db->query("DELETE FROM " . DB_PREFIX . "doctor_description WHERE doctor_id = '". $doctor_id ."'");
			foreach($data['detail'] as $language_id => $detail){
				$this->db->query("INSERT INTO " . DB_PREFIX . "doctor_description SET doctor_id = '". $doctor_id ."', language_id = '". $language_id ."', name = '". $this->db->escape($detail['name']) ."'");
			}
		}
		return true;
	}
	
	public function deleteDoctor($doctor_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "doctor WHERE doctor_id = '" . (int)$doctor_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "doctor_description WHERE doctor_id = '" . (int)$doctor_id . "'");
	}
	
	public function getDoctor($doctor_id) {
		$query = $this->db->query("SELECT  * FROM " . DB_PREFIX . "doctor WHERE doctor_id = '" . (int)$doctor_id . "'");

		return $query->row;
	}
	
	public function getDetail($doctor_id){
		$data = array();
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "doctor_description WHERE doctor_id = '". $doctor_id ."'");
		foreach($query->rows as $rs){
			$data[$rs['language_id']] = array(
				'name' => $rs['name']
			);
		}
		return $data;
	}
	
	public function getDoctors($data) {
		
		$sql = "SELECT * FROM " . DB_PREFIX . "doctor ORDER BY sort_order ASC";
		if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }
			
		$query = $this->db->query($sql);
		
		return $query->rows;
	}
	
	public function getTotalDoctors($data) {
		
		// $this->check_db();
		
		$sql = "SELECT * FROM " . DB_PREFIX . "doctor";
			
		$query = $this->db->query($sql);
		
		return $query->num_rows;
	}
}
?>