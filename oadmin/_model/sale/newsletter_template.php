<?php
class ModelSaleNewsletterTemplate extends Model {
	public function check_db(){
		$this->db->query("
CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "mail_templates (
  `template_id` int(11) NOT NULL AUTO_INCREMENT,
  `template_name` varchar(100) NOT NULL,
  `template_subject` varchar(255) NOT NULL,
  `template_code` blob NOT NULL,
  PRIMARY KEY (`template_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;");
	}
	public function addTemplate($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "mail_templates SET template_name='". $this->db->escape($data['template_name']) ."', template_subject='". $this->db->escape($data['template_subject']) ."', template_code = '" . $this->db->escape($data['template_code']) . "'");
	}
	
	public function editTemplate($template_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "mail_templates SET template_name='". $this->db->escape($data['template_name']) ."', template_subject='". $this->db->escape($data['template_subject']) ."', template_code = '" . $this->db->escape($data['template_code']) . "' WHERE template_id = '" . (int)$template_id . "'");
	}
	
	public function deleteTemplate($template_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "mail_templates WHERE template_id = '" . (int)$template_id . "'");
	}
	
	public function getTemplate($template_id) {
		$query = $this->db->query("SELECT  * FROM " . DB_PREFIX . "mail_templates WHERE template_id = '" . (int)$template_id . "'");
		
		return $query->row;
	}
	
	public function getTemplates() {
		$sql = "SELECT * FROM " . DB_PREFIX . "mail_templates";
			
		$query = $this->db->query($sql);
		
		return $query->rows;
	}
	
	public function getTotalTemplates() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "mail_templates");
		
		return $query->row['total'];
	}
}
?>