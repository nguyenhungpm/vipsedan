<?php
// Heading
$_['heading_title']      = 'Display options';
$_['entry_catalog_limit'] 	    = 'Number records / product page';
$_['entry_product_price']   	 = 'Show product price?';
$_['entry_product_status']   	 = 'Show stock control status?';
$_['entry_product_date']   	 = 'Show date in Open Graph in product page';
$_['entry_news_limit'] 	    = 'Number records / news page';
$_['entry_admin_limit']   	    = 'Number records / admin page (Admin zone)';
$_['error_limit']        = 'Bạn chưa nhập tham số';
$_['text_select']        = 'Choose';
$_['text_no']          ='No';
// Error
$_['text_success']      = 'Saved successfully!';
$_['error_permission'] = 'Warning: You do not have permission to modify display options!';
?>
