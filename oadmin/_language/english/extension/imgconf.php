<?php
$_['heading_title']           = 'Logo and Image settings';
$_['entry_logo']              = 'Logo image:';
$_['entry_icon']              = 'Favicon:<br /><span class="help">The icon should be a PNG that is 16px x 16px.</span>';
$_['text_browse']             = 'Browse';
$_['text_image_manager']       = 'Image manager';
$_['text_clear']               = 'Clear';
$_['entry_image_category']         = 'Category Thumb Size:';
$_['entry_image_thumb']            = 'Product Thumb Size:';
$_['entry_image_popup']            = 'Product Image Popup Size:';
$_['entry_image_product']          = 'Product Image List Size:';
$_['entry_image_additional']       = 'Additional Product Image Size:';
$_['entry_image_news']        = 'Aritlce Thumb Image Size:';
$_['entry_image_related']     = 'Related Product Image Size:';
$_['entry_image_album']       = 'Album Thumb Size:';
$_['entry_image_album_related']       = 'Related Album Thumb Size:';
$_['entry_image_gallery']     = 'Gallery Thumb Size:';
$_['entry_gallery_settings']  = 'Gallery Image Settings:';

$_['error_image_thumb']       = 'Yêu cầu kích thước ảnh sản phẩm thu nhỏ!';
$_['error_image_popup']       = 'Yêu cầu kích thước ảnh sản phẩm phóng to!';
$_['error_image_product']     = 'Yêu cầu kích thước cho sản phẩm!';
$_['error_image_category']    = 'Yêu cầu kích thước cho danh mục sản phẩm!';
$_['error_image_manufacturer']= 'Danh sách các Kích thước ảnh nhà sản xuất !';
$_['error_image_related']     = 'Yêu cầu kích thước ảnh cho sản phẩm cùng loại!';
$_['error_image_compare']     = 'So sánh kích thước hình ảnh theo yêu cầu!';

?>
