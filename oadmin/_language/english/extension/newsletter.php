<?php
// Heading
$_['heading_title']    = 'Newsletter Subscribers';

// Text
$_['text_success']     = 'Successfully: You have edited it!';
$_['text_export']      = 'Excel export';

// Column
$_['column_name']      = 'Language';
$_['column_email']     = 'Email';
$_['column_action']    = 'Action';
$_['column_date_added']    = 'Date added';

// Entry
$_['entry_name']       = 'Language';
$_['entry_code']       = 'Email';



// Error
$_['error_email_exist']= 'Email Id already exist';
$_['error_email']      = 'Invalid Email Format';
$_['error_permission'] = 'Warning: You do not have permission to modify newsletter subscribers!';
?>