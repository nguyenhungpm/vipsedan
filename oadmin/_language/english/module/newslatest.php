<?php
// Heading
$_['heading_title']       = 'Latest News'; 

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified this module!';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';
$_['text_column_right']   = 'Column Right';

// Entry
$_['entry_news']    = 'News:<br /><span class="help">(Autocomplete)</span>';
$_['entry_limitdescription']      = 'Charaters';
$_['entry_imagestatus']   = 'Show image';
$_['entry_description']   = 'Show description';

$_['entry_limit']         = 'Limit:';
$_['entry_leading']         = '# leading news';
$_['entry_id_cat']         = 'Only ID cat.';
$_['entry_image']         = 'Image (W x H)';
$_['entry_layout']        = 'Layout:';
$_['entry_position']      = 'Position:';
$_['entry_status']        = 'Status:';
$_['entry_sort_order']    = 'Sort Order:';

// Error 
$_['error_permission']    = 'Warning: You do not have permission to modify this featured!';

?>