<?php
// Heading
$_['heading_title']       = '<font color = "#0099FF"><b>Danh mục sản phẩm nội bật</b></font>'; 
$_['site_title']       = 'Danh mục sản phẩm nội bật';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module Just Launched Categories!';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';
$_['text_column_right']   = 'Column Right';

// Entry
$_['entry_product']       = 'Sẩn phẩm:';
$_['entry_module_heading']	  ='Tên biển thị: ';
$_['entry_categories']       = 'Danh mục:<br /><span class="help">(Tự động điền)</span>';
$_['entry_limit']         = 'Limit:';
$_['entry_image']         = 'Image (W x H) and Resize Type:';
$_['entry_layout']        = 'Layout:';
$_['entry_position']      = 'Position:';
$_['entry_status']        = 'Status:';
$_['entry_sort_order']    = 'Sort Order:';

// Error 
$_['error_permission']    = 'Warning: You do not have permission to modify module featured!';
$_['error_image']         = 'Image width &amp; height dimensions required!';
?>