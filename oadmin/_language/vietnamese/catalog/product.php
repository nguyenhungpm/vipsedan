<?php
// Heading
$_['heading_title']          = 'Sản phẩm'; 
$_['heading_title_add']   	 = 'Thêm sản phẩm';

// Text  
$_['text_success']      	= 'Thành công: Sản phẩm đã được thay đổi!';
$_['text_plus']              = '+';
$_['text_minus']             = '-';
$_['text_default']           = 'Mặc định';
$_['text_image_manager']     = 'Quản lý ảnh';
$_['text_browse']            = 'Duyệt file';
$_['text_clear']             = 'Xóa Ảnh';
$_['text_option']            = 'Tùy chọn';
$_['text_option_value']      = 'Giá trị tùy chọn';
$_['text_percent']           = 'Tỷ lệ phần trăm';
$_['text_amount']            = 'Số lượng Cố định';
$_['text_overview']            = 'Tổng quan';

// Column
$_['column_name']            = 'Tên sản phẩm';
$_['column_category']            = 'Danh mục sản phẩm';
$_['column_model']           = 'Model';
$_['column_image']           = 'Hình ảnh';
$_['column_price']           = 'Giá';
$_['column_quantity']        = 'Số lượng';
$_['column_status']          = 'Trạng thái';
$_['column_action']          = 'Thao tác';
$_['text_outofstock']        = 'Hết hàng';
$_['text_instock']        	 = 'Còn hàng';
$_['button_preview']            = 'Xem trước';


// Entry
$_['entry_name']             = 'Tên sản phẩm:';
$_['entry_meta_keyword']     = 'Từ khóa:';
$_['entry_meta_description'] = 'Mô tả từ khóa:';
$_['entry_description']      = 'Mô tả sản phẩm:';
$_['entry_store']            = 'Website:';
$_['entry_keyword']          = 'Từ khóa SEO:';
$_['entry_model']            = 'Model:';
$_['entry_sku']              = 'SKU:';
$_['entry_upc']              = 'UPC:';
$_['entry_location']         = 'Địa điểm:';
$_['entry_manufacturer']     = 'Hãng sản xuất';
$_['entry_all_category']     = 'Mọi danh mục';
$_['entry_shipping']         = 'Yêu cầu Vận chuyển:'; 
$_['entry_date_available']   = 'Ngày xuất bản';
$_['entry_quantity']         = 'Số lượng:';
$_['entry_minimum']          = 'Số lượng tối thiểu:<br/><span class="help">Số lượng ít nhất khi đặt hàng</span>';
$_['entry_stock_status']     = 'Hết hàng:<br/><span class="help">Tình trạng hiện ra khi một sản phẩm hết hàng</span>';
$_['entry_price']            = 'Giá:';
$_['entry_tax_class']        = 'Loại Thuế:';
$_['entry_points']           = 'Điểm:<br/><span class="help">Số điểm cần thiết để mua mặt hàng này. Nếu bạn không \'muốn sản phẩm này được mua với điểm để là 0.</span>';
$_['entry_option_points']    = 'Điểm:';
$_['entry_subtract']         = 'Phần Trừ đi:';
$_['entry_weight_class']     = 'Trọng lượng:';
$_['entry_weight']           = 'Nặng:';
$_['entry_length']           = 'Chiều dài:';
$_['entry_dimension']        = 'Kích thước(L x W x H):';
$_['entry_image']            = 'Ảnh đại diện';
$_['entry_customer_group']   = 'Nhóm khách hàng:';
$_['entry_date_start']       = 'Từ ngày:';
$_['entry_date_end']         = 'Đến ngày:';
$_['entry_priority']         = 'Ưu tiên:';
$_['entry_attribute']        = 'Thuộc tính:';
$_['entry_attribute_group']  = 'Nhóm thuộc tính:';
$_['entry_text']             = 'Văn bản:';
$_['entry_option']           = 'Tùy chọn:';
$_['entry_option_value']     = 'Giá trị Tùy chọn:';
$_['entry_required']         = 'Bắt buộc:';
$_['entry_status']           = 'Trạng thái:';
$_['entry_sort_order']       = 'Thứ tự:';
$_['entry_category']         = 'Danh mục';
$_['entry_related']          = 'Sản phẩm cùng loại:';
$_['entry_tag']              = 'Tags:<br /><span class="help">Ngăn cách bằng dấu phẩy</span>';
$_['entry_reward']           = 'Điểm thưởng:';
$_['entry_layout']           = 'Bố cục:';
$_['entry_related_news']           = 'Bài viết liên quan từ blog:';
$_['entry_related_products']           = 'Sản phẩm liên quan:';

$_['entry_all_category']     = 'Mọi danh mục';
$_['entry_short_description']  = 'Mô tả ngắn';
$_['text_gallery']				= 'Thư viện ảnh';

$_['text_length_day']        = 'Day';
$_['text_length_week']       = 'Week';
$_['text_length_month']      = 'Month';
$_['text_length_month_semi'] = 'Semi Month';
$_['text_length_year']       = 'Year';

// Error
$_['error_warning']          = 'Cảnh báo: Dữ liệu bắt buộc chưa được nhập vào. Kiểm tra các ô trống!';
$_['error_permission']       = 'Cảnh báo: Bạn không được phép thay đổi các sản phẩm!';
$_['error_name']             = 'Tên sản phẩm phải lớn hơn 3 và nhỏ hơn 255 ký tự!';
$_['error_model']            = 'Model sản phẩm phải lớn hơn 3 và nhỏ hơn 64 ký tự!';
?>