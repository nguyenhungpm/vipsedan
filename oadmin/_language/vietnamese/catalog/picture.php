<?php
// Heading
$_['heading_title']      = 'Thư viện video';
$_['heading_title_add']  = 'Thêm thư video';

// Text
$_['text_success']       = 'Success: You have modified video list!';
$_['text_default']       = 'Mặc định';
$_['text_image_manager'] = 'Trình quản lý file/hình ảnh';
$_['text_browse']        = 'Chọn';
$_['text_clear']         = 'Xóa';

// Column
$_['column_name']        = 'Tên bộ sưu tập';
$_['column_sort_order']  = 'Thứ tự';
$_['column_status']      = 'Trạng thái';
$_['column_action']      = 'Chức năng';

//button
$_['button_add_picture']      = 'Thêm video';
// Entry
$_['entry_name']         = 'Tên bộ sưu tập';
$_['entry_title']        = 'Tiêu đề';
$_['entry_link']         = 'Mã video youtube';
$_['entry_image']        = 'Ảnh đại diện';
$_['entry_status']       = 'Trạng thái:';
$_['button_add_gallimage'] = 'Thêm ảnh';
$_['entry_description']   = 'Mô tả';
$_['entry_sort_order']   = 'Thứ tự';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify video!';
$_['error_name']         = 'Gallery Name must be between 3 and 64 characters!';
$_['error_title']        = 'Gallery Title must be between 2 and 64 characters!';
$_['error_default']      = 'Warning: This layout cannot be deleted as it is currently assigned as the default store layout!';
$_['error_product']      = 'Warning: This layout cannot be deleted as it is currently assigned to %s products!';
$_['error_category']     = 'Warning: This layout cannot be deleted as it is currently assigned to %s categories!';
$_['error_information']  = 'Warning: This layout cannot be deleted as it is currently assigned to %s information pages!';
?>
