<?php
$_['heading_title']           = 'Logo và kích thước ảnh';
$_['entry_logo']              = 'Logo website:';
$_['entry_icon']              = 'Icon:<br /><span class="help">icon được hiện thị với đuôi .png và kích thước 16px x 16px.</span>';
$_['text_browse']              = 'Chọn file';
$_['text_image_manager']       = 'Quản lý hình ảnh';
$_['text_clear']               = 'Xóa Ảnh';
$_['entry_image_category']    = 'Cỡ hình danh mục:';
$_['entry_image_thumb']       = 'Cỡ hình sản phẩm thu nhỏ:';
$_['entry_image_popup']       = 'Cỡ hình sản phẩm phóng to:';
$_['entry_image_product']     = 'Cỡ sản phẩm:';
$_['entry_image_additional']  = 'Cỡ ảnh sản phẩm nhập vào:';
$_['entry_image_related']     = 'Cỡ ảnh sản phẩm liên quan:';
$_['entry_image_compare']       = 'So sánh kích cỡ hình ảnh:';
$_['entry_image_gallery']      = 'Kích thước ảnh đại diện thư viện ảnh:';
$_['entry_image_cart']        = 'Cỡ hình giỏ:';
$_['error_image_thumb']       = 'Yêu cầu kích thước ảnh sản phẩm thu nhỏ!';
$_['error_image_popup']       = 'Yêu cầu kích thước ảnh sản phẩm phóng to!';
$_['error_image_product']     = 'Yêu cầu kích thước cho sản phẩm!';
$_['error_image_category']    = 'Yêu cầu kích thước cho danh mục sản phẩm!';
$_['error_image_manufacturer']= 'Danh sách các Kích thước ảnh nhà sản xuất !';
$_['error_image_additional']  = 'Yêu cầu kích thước ảnh cho sản phẩm thêm!';
$_['error_image_related']     = 'Yêu cầu kích thước ảnh cho sản phẩm cùng loại!';
$_['error_image_compare']     = 'So sánh kích thước hình ảnh theo yêu cầu!';
$_['error_image_wishlist']    = 'Danh sách Kích thước ảnh mong muốn !';
$_['error_image_cart']        = 'Yêu cầu kích thước ảnh giỏ hàng!';

?>
