<?php
// Heading
$_['heading_title']      = 'Custom Script & CSS';

// Text
$_['text_success']       = 'Bạn đã lưu lại thành công';
$_['button_save']        = 'Lưu';
$_['button_cancel']      = 'Đóng';

// Error
$_['error_permission']  = 'Cảnh báo: Bạn không có quyền sửa đổi chức năng này!';
?>
