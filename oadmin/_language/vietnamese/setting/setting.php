<?php
// Heading
$_['heading_title']            = 'Thiết lập';

// Text
$_['text_success']             = 'Thành công: Thiết lập của bạn đã được thay đổi!';
$_['text_browse']              = 'Chọn file';
$_['text_image_manager']       = 'Quản lý hình ảnh';
$_['text_clear']               = 'Xóa Ảnh';
$_['text_mail']                = 'Mail';
$_['text_smtp']                = 'SMTP';
$_['text_select']              = 'Chọn';

// Entry

$_['entry_name']                = 'Tên website:';
$_['entry_owner']               = 'Chủ website:';
$_['entry_address']             = 'Địa chỉ:';
$_['entry_email']               = 'E-Mail:';
$_['entry_telephone']           = 'Điện thoại:';
$_['entry_fax']                 = 'Fax:';
$_['entry_country']             = 'Quốc gia:';
$_['entry_zone']                = 'Tỉnh / Thành phố:';
$_['entry_language']            = 'Ngôn ngữ:';
$_['entry_admin_language']      = 'Ngôn ngữ trang quản lý:';
$_['entry_currency']          = 'Tiền tệ:<br /><span class="help">Thay đổi các loại tiền tệ mặc định. Xoá bộ nhớ cache của trình duyệt của bạn để xem những thay đổi và thiết lập lại cookie hiện tại của bạn.</span>';
$_['entry_review']       	       = 'Allow Reviews:<br /><span class="help">Enable/Disable new review entry and display of existing reviews</span>';
$_['entry_download']               = 'Allow Downloads:';
$_['entry_account']           = 'Quy định khách hàng:<br /><span class="help">Khách hàng phải đồng ý với các điều khoản trước khi tài khoản có thể được tạo ra.</span>';
$_['entry_review']       	= 'Cho phép những tổng quan:<br /><span class="help">Enable / Disable mới xem xét nhập và hiển thị các đánh giá hiện tại</span>';
$_['entry_download']            = 'Cho phép tải xuống:';
$_['entry_upload_allowed']      = 'Được phép tải lên các tệp Mở rộng:<br /><span class="help">Thêm vào đó các phần mở rộng tập tin được phép tải lên. Sử dụng các giá trị cách nhau bằng dấu phẩy.</span>';
$_['entry_mail_protocol']     = 'Giao thức Thư:<span class="help">Chỉ chọn \'Thư\' trừ khi máy chủ lưu trữ của bạn đã bị vô hiệu hoá chức năng mail php.';
$_['entry_mail_parameter']    = 'Tham số thư:<span class="help">Khi sử dụng \'Mail\', mail thông số bổ sung có thể được thêm ở đây (ví dụ "-femail@storeaddress.com".';
$_['entry_ftp_host']               = 'FTP Host:';
$_['entry_ftp_port']               = 'FTP Port:';
$_['entry_ftp_username']           = 'FTP Username:';
$_['entry_ftp_password']           = 'FTP Password:';
$_['entry_ftp_root']               = 'FTP Root:<span class="help">The directory your opencart installation is stored in normally \'public_html/\'.</span>';
$_['entry_ftp_status']             = 'Enable FTP:';
$_['entry_smtp_host']         = 'SMTP Host:';
$_['entry_smtp_username']     = 'SMTP Username:';
$_['entry_smtp_password']     = 'SMTP Password:';
$_['entry_smtp_port']         = 'SMTP Port:';


$_['tab_general'] = 'Thiết lập chung';
$_['tab_local'] = 'Địa phương';
$_['tab_option'] = 'Tùy chọn';
$_['tab_ftp'] = 'FTP';
$_['tab_mail'] = 'Email';
$_['tab_server'] = 'Bảo mật';

$_['entry_smtp_timeout']      = 'SMTP Timeout:';
$_['entry_account_mail']        = 'Thông báo có 1 tài khoản mới mở :<br /><span class="help">Gửi email cho chủ website khi một tài khoản mới được đăng ký.</span>';
$_['entry_alert_mail']          = 'Thư thông báo:<br /><span class="help">Gửi email cho chủ website khi có khách hàng đặt hàng</span>';
$_['entry_alert_emails']        = 'Thông báo bổ sung thư điện tử:<br /><span class="help">Bổ sung bất kỳ email nào bạn muốn nhận được email cảnh báo, ngoài các email lưu trữ chính. (Bằng dấu phẩy)</span>';
$_['entry_secure']                 = 'Use SSL:<br /><span class="help">To use SSL check with your host if a SSL certificate is installed and added the SSL URL to the catalog and admin config files.</span>';
$_['entry_shared']                 = 'Use Shared Sessions:<br /><span class="help">Try to share the session cookie between stores so the cart can be passed between different domains.</span>';
$_['entry_use_ssl']               = 'Dùng SSL:<br /><span class="help">Để sử dụng kiểm tra SSL với máy chủ của bạn nếu một chứng chỉ SSL được cài đặt và thêm vào các URL SSL cho người quản trị tập tin cấu hình.</span>';
$_['entry_robots']                 = 'Danh sách Robots:<br /><span class="help">A list of web crawler user agents that shared sessions will not be used with. Use separate lines for each user agent.</span>';
$_['entry_file_extension_allowed'] = 'Allowed File Extensions:<br /><span class="help">Add which file extensions are allowed to be uploaded. Use a new line for each value.</span>';
$_['entry_file_mime_allowed']      = 'Allowed File Mime Types:<br /><span class="help">Add which file mime types are allowed to be uploaded. Use a new line for each value.</span>';
$_['entry_maintenance']         = 'Chế độ bảo trì:<br /><span class="help">Ngăn chặn khách hàng từ website của bạn duyệt web. Họ thay vào đó sẽ thấy một thông báo bảo trì. Nếu đăng nhập như là admin, bạn sẽ thấy những website như bình thường.</span>';
$_['entry_password']               = 'Allow Forgotten Password:<br /><span class="help">Allow forgotten password to be used for the admin. This will be disabled automatically if the system detects a hack attempt.</span>';
$_['entry_encryption']          = 'Encryption Key:<br /><span class="help">Xin vui lòng cung cấp một khóa bí mật sẽ được sử dụng để mã hóa thông tin cá nhân khi xử lý các đơn đặt hàng.</span>';
$_['entry_compression']         = 'Kết quả mức nén:<br /><span class="help">Nén dữ liệu cho việc chuyển giao hiệu quả hơn cho các khách hàng yêu cầu. Mức độ nén phải có từ 0-9</span>';
$_['entry_error_display']       = 'Chế độ lập trình:';
$_['entry_error_log']           = 'Lịch sử lỗi:';
$_['entry_error_filename']      = 'Lịch sử lỗi tập tin:';

// Error
$_['error_warning']             = 'Cảnh báo: Dữ liệu bắt buộc chưa được nhập vào. Kiểm tra các ô trống!';
$_['error_permission']        = 'Cảnh báo: Bạn không được phép thay đổi các cài đặt!';
$_['error_name']              = 'Tên website phải lớn hơn 3 và nhỏ hơn 32 ký tự!';
$_['error_owner']               = 'Chủ website phải có từ 3 và 64 ký tự!';
$_['error_address']           = 'Địa chỉ website phải nhiều hơn 10 và ít hơn 128 ký tự!';
$_['error_email']             = 'Địa chỉ e-mail phải hợp lệ!';
$_['error_telephone']         = 'Điện thoại phải lớn hơn 3 và nhỏ hơn 32 ký tự!';
$_['error_ftp_host']               = 'FTP Host required!';
$_['error_ftp_port']               = 'FTP Port required!';
$_['error_ftp_username']           = 'FTP Username required!';
$_['error_ftp_password']           = 'FTP Password required!';
$_['error_error_filename']    = 'Yêu cầu lịch sử lỗi tên tập tin!';
$_['error_encryption']             = 'Encryption must be between 3 and 32 characters!';
?>
