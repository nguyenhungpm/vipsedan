<?php
// Heading
$_['heading_title']         = 'Quản trị website';

// Text
$_['text_contactus']                  = 'Contact to DB';
$_['text_news']        			= 'Tin tức';
$_['text_news_category']  	= 'Nhóm tin tức';
$_['text_news_comment']  	 = 'Nhận xét';
$_['text_picture']          = 'Thư viện ảnh';
$_['text_attribute_group']  = 'Nhóm thuộc tính';
$_['text_backup']           = 'Sao lưu / Phục hồi';
$_['text_banner']           = 'Banner';
$_['text_catalog']          = 'Nội dung';
$_['text_category']         = 'Nhóm sản phẩm';
$_['text_confirm']	        = 'Dữ liệu sau khi Xóa / Gỡ bỏ không thể phục hồi! Bạn có chắc chắn muốn làm điều này?';
$_['text_country']          = 'Quốc gia';
$_['text_currency']         = 'Tiền tệ';
$_['text_customer']         = 'Khách hàng';
$_['text_customer_group']   = 'Nhóm khách hàng';
$_['text_customer_blacklist']          = 'IP Blacklist';
$_['text_dashboard']        = 'Bảng chính';
$_['text_design']           = 'Thiết kế';
$_['text_download']         = 'Tải về';
$_['text_error_log']        = 'Lịch sử lỗi';
$_['text_extension']        = 'Giao diện';
$_['text_redirect_manager'] = '301 Redirect';
$_['text_feed']             = 'Dữ liệu sản phẩm';
$_['text_front']            = 'Xem website';
$_['text_geo_zone']         = 'Vùng địa lý';
$_['text_help']             = 'Trợ giúp';
$_['text_information']      = 'Thông tin';
$_['text_language']         = 'Ngôn ngữ';
$_['text_layout']           = 'Bố cục';
$_['text_localisation']     = 'Địa phương';
$_['text_logged']           = 'Bạn đang đăng nhập với <span>%s</span>';
$_['text_logout']           = 'Thoát';
$_['text_contact']          = 'Gửi email';
$_['text_manager']          = 'Extension Manager';
$_['text_manufacturer']     = 'Hãng sản xuất';
$_['text_module']           = 'Mô-đun';
$_['text_sale']             = 'Quan hệ khách hàng';
$_['text_newsletter']       = 'Đăng ký nhận bản tin';
$_['text_seo']              = 'SEO';
$_['text_seo_common']       = 'Cấu hình chung';
$_['text_localseo']         = 'Local SEO';
$_['text_redirect']         = 'Redirect URL';
$_['text_order']            = 'Đơn đặt hàng';
$_['text_order_status']     = 'Tình trạng đơn đặt hàng';
$_['text_opencart']         = 'Trang chủ';
$_['text_product']          = 'Sản phẩm';
$_['text_reports']          = 'Báo cáo';
$_['text_review']           = 'Nhận xét';
$_['text_setting']          = 'Thiết lập';
$_['text_stock_status']     = 'Tình trạng kho hàng';
$_['text_system']           = 'Hệ thống';
$_['text_tax']              = 'Thuế';
$_['text_tax_class']        = 'Loại Thuế';
$_['text_tax_rate']         = 'Thuế suất';
$_['text_total']            = 'Trình tự hóa đơn';
$_['text_user']             = 'Thành viên';
$_['text_documentation']    = 'Tài liệu';
$_['text_users']            = 'Thành viên';
$_['text_user_group']       = 'Nhóm thành viên';

$_['text_logoimage']		= 'Logo và hình ảnh';
$_['text_menu']		        = 'Trình đơn';
$_['text_options']		    = 'Tùy chọn';
$_['text_embedcode']		= 'Mã nhúng';

$_['text_voucher']          = 'Phiếu quà tặng';
$_['text_voucher_theme']    = 'Giao diện phiếu quà tặng';
$_['text_weight_class']     = 'Trọng Lượng';
$_['text_length_class']     = 'Chiều dài';
$_['text_zone']             = 'Vùng';
$_['text_website'] 			= 'Xem website';
$_['text_logout'] 			= 'Thoát';

?>
