<?php
// Heading
$_['heading_title']                 = 'Bảng Quản trị';

// Text
$_['text_overview']                 = 'Tổng quan';
$_['text_statistics']				= 'Chọn 1 chức năng để bắt đầu';
// Column
$_['column_order']       	    	= 'ID Đơn đặt hàng';
$_['column_customer']               = 'Tên khách hàng';
$_['column_status']                 = 'Trạng thái';
$_['column_date_added'] 	    	= 'Ngày thêm';
$_['column_total']                  = 'Tổng';
$_['column_firstname']              = 'Họ';
$_['column_lastname']               = 'Tên';
$_['column_action']                 = 'Thao tác';

// Entry
$_['entry_range']                   = 'Chọn Phạm vi:';

// Error
$_['error_install']                 = 'Cảnh báo: Thư mục install vẫn còn tồn tại!';
$_['error_image']                   = 'Cảnh báo: Thư mục hình ảnh %s không ghi được!';
$_['error_image_cache']             = 'Cảnh báo: Cache thư mục hình ảnh  %s không ghi được!';
$_['error_cache']                   = 'Cảnh báo: Cache thư mục %s không ghi được!';
$_['error_download']                = 'Cảnh báo: Chư mục tải về %s không ghi được!';
$_['error_logs']                    = 'Cảnh báo: Thư mục đăng nhập %s không ghi được!';
?>