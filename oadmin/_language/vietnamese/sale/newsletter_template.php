<?php
// Heading
$_['heading_title']    = 'Mẫu email marketing';

// Text
$_['text_success']     = 'Thành công: Bạn vừa sửa mẫu email marketing!';

// Column
$_['column_name']      = 'Mẫu enail';
$_['column_action']    = 'Thao tác';

// Entry
$_['entry_name']       = 'Tên mẫu email:';
$_['entry_code']       = 'Nội dung mẫu email:';


// Error
$_['error_name']       = 'Vui lòng nhập tên mẫu email';
$_['error_code']       = 'Vui lòng nhập nội dung';
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền truy cập trang này!';
?>