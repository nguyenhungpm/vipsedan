<?php
// Heading
$_['heading_title']      = 'Videos';

// Text
$_['text_success']       = 'Thành công: Bạn đã sửa đổi videos!';
$_['text_default']       = 'Mặc định';
$_['text_image_manager'] = 'Quản lý hình ảnh';
$_['text_browse']        = 'Duyệt file';
$_['text_clear']         = 'Xóa Ảnh';

// Column
$_['column_name']        = 'Tên Video';
$_['column_status']      = 'Trạng thái';
$_['column_action']      = 'Thao tác';

// Entry
$_['entry_name']         = 'Tên Video:';
$_['entry_title']        = 'Tiêu đề:';
$_['entry_link']         = 'Liên kết';
$_['entry_image']        = 'Hình ảnh:';
$_['entry_status']       = 'Trạng thái:';

// Error
$_['error_permission']   = 'Cảnh báo: Bạn không có quyền sửa đổi các videos!';
$_['error_name']         = 'Tên Video phải có từ 3 đến 64 ký tự!';
$_['error_title']        = 'Tiêu đề Video phải có từ 3 đến 64 ký tự!';
$_['error_default']      = 'Cảnh báo: cách trình bày này không thể bị xóa khi nó hiện thời được gán mặc định website.!';
$_['error_product']      = 'Cảnh báo: cách trình bày này không thể bị xóa khi nó hiện thời được gán Tới %s Sản phẩm.s!';
$_['error_category']     = 'Cảnh báo: cách trình bày này không thể bị xóa khi nó hiện thời được gán Tới %s Danh mục!';
$_['error_information']  = 'Cảnh báo: cách trình bày này không thể bị xóa khi nó hiện thời được gán Tới %s Trang thông tin!';
?>