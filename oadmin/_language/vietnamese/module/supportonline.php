<?php
// Heading
$_['heading_title']       = 'Hỗ trợ trực tuyến';

// Text
$_['text_module']         = 'Mô-đun';
$_['text_success']        = 'Success: You have modified module Support Online';
$_['text_column_left']    = 'Cột trái';
$_['text_column_right']    = 'Cột phải';
$_['text_content_top']    = 'Đầu nội dung';
$_['text_content_bottom'] = 'Cuối nội dung';
$_['text_header_bottom'] = 'Header Bottom';
$_['text_image_manager']  = 'Image Manager';
$_['text_browse']		       = 'Chọn file';
$_['text_clear']   		  = 'Xóa ảnh';
$_['button_add_image']        = 'Thêm nhân viên';

// Entry
$_['entry_image']         = 'Ảnh đại diện (W x H):';
$_['entry_layout']        = 'Bố cục:';
$_['entry_position']      = 'Vị trí:';
$_['entry_status']        = 'Trạng thái:';
$_['entry_sort_order']    = 'Thứ tự:';

$_['nivo_caption']		  = 'Caption';
$_['nivo_URL']		  	  = 'URL';
$_['nivo_image']		  = 'Image';

// Error
$_['error_permission']    = 'Warning: You do not have permission to modify module Support Online!';
$_['error_image']         = 'Image width &amp; height dimensions required!';
?>
