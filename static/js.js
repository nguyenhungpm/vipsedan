window.addEventListener('mouseover', hover, false);
window.addEventListener('mouseout', removehover, false);
function hover(event) {
	if(event.target.id){
		id = event.target.id.substring(1, 2);
		for(i=1;i<parseInt(id)+1;i++){
			addClass('s'+i,"hover_green");
		}
	}
}
function removehover(event) {
	if(event.target.id){
		id = event.target.id.substring(1, 2);
		for(i=1;i<parseInt(id)+1;i++){
			removeClass('s'+i,"hover_green");
		}
	}
}
function rate(id,type,value_id) {
	if(document.getElementById("point").value == ''){
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.open("POST", 'index.php?route='+ type +'/'+ type +'/rate&'+ type +'_id='+ value_id +'&point='+id);
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == XMLHttpRequest.DONE) {
				if(xmlhttp.status == 200){
					var rs = xmlhttp.responseText.split(",");
					document.getElementById('total_vote').innerHTML = 'Thành công! Bạn đã bầu chọn <span style="color:red" itemprop="reviewCount">' + id + '</span> điểm';
					document.getElementById('point').value = 'load';
					document.getElementById('point_rs').innerHTML = 'Có <span itemprop="reviewCount">' + rs[0].replace(/"/,"") + '</span> bầu chọn / điểm trung bình: <span itemprop="ratingValue">' + rs[1].replace(/"/,"") + '</span>';
				}else{
					console.log('Error: ' + xmlhttp.statusText );
				}
			}
		}
		xmlhttp.send('');
	}
}
function addClass(id, class_) {
		if(document.getElementById(id).className.indexOf(class_) < 0){
			document.getElementById(id).className += " " + class_;
		}
	}
function removeClass(id,class_) {
		document.getElementById(id).className = (' ' + document.getElementById(id).className + ' ').replace(' ' + class_ + ' ', ' ' );
		removeSpace(id,class_);
	}
function removeSpace(id,class_){
		if(document.getElementById(id).className.indexOf("  ") >= 0){
			document.getElementById(id).className = document.getElementById(id).className.replace('  ',' ');
			removeSpace(id,class_);
		}
	}