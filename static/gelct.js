Number.prototype.formatMoney = function(c, d, t){
	var n = this, 
	c = isNaN(c = Math.abs(c)) ? 2 : c, 
	d = d == undefined ? "." : d, 
	t = t == undefined ? "," : t, 
	s = n < 0 ? "-" : "", 
	i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))), 
	j = (j = i.length) > 3 ? j % 3 : 0;
   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};
function change(obj, price, id){
	var quantity = obj.value;
	if(quantity < 0){
		quantity = 0;
		document.getElementById("detail"+id).value = quantity > 0 ? quantity : 0;
	}
	var rs = quantity*price;
	document.getElementById("price"+id).innerHTML = rs.formatMoney(0,',','.') + ' vnđ';
	calculateTotal();
	// console.log(rs);
}
function addOther(obj){
	order('model', obj.value);
	window.location = "common/cart";
}
function decrease(id, price){
	var quantity = parseInt(document.getElementById("detail"+id).value) - 1;
	var price = quantity > 0 ? quantity * price : 0;
	// console.log(price);
	document.getElementById("price"+id).innerHTML = price.formatMoney(0,',','.') + ' vnđ';
	document.getElementById("detail"+id).value = quantity > 0 ? quantity : 0;
	calculateTotal();
}
function increase(id, price){
	var quantity = parseInt(document.getElementById("detail"+id).value) + 1;
	var price = quantity > 0 ? quantity * price : 0;
	// console.log(price);
	document.getElementById("price"+id).innerHTML = price.formatMoney(0,',','.') + ' vnđ';
	document.getElementById("detail"+id).value = quantity > 0 ? quantity : 0;
	calculateTotal();
}
function calculateTotal(){
	var request = new XMLHttpRequest();
	// POST to httpbin which returns the POST data as JSON
	request.open('POST', 'index.php?route=common/cart/calculateTotal', /* async = */ false);
	
	var formData = new FormData(document.getElementById('form_contact'));
	request.send(formData);
		// console.log('Thêm vào giỏ hàng');
	if (request.status == 200) {
		var json = JSON.parse(request.responseText);
		// console.log(json);
		document.getElementById("price").innerHTML = parseInt(json.total).formatMoney(0,',','.') + ' vnđ';
	} else {
		console.log('Error: ' + request.statusText);
	}
}
function remove(product_id){
	var request = new XMLHttpRequest();
	// POST to httpbin which returns the POST data as JSON
	request.open('POST', 'index.php?route=common/cart/remove', /* async = */ false);
	request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	request.send("&product_id=" + product_id);
	if (request.status == 200) {
		var json = JSON.parse(request.responseText);
		console.log(json);
		// document.getElementById('notification').innerHTML = '<div class="success">'+json['success']+'<img src="static/close.png" class="close"/>';
		// scrollToTop();
		window.location = "common/cart";
	} else {
		console.log('Error: ' + request.statusText);
	}
}
function search(e,inputid,news_category_id){
	var n=document.getElementById(inputid).value;
	url = 'index.php?route=product/search&search='+n;
	if(news_category_id){
		var nci=document.getElementById(news_category_id).value;
		url += '&news_category_id='+nci;
	}

	13==e.keyCode&&(window.location.href= url)};

	function searchDesk(inputid,news_category_id){
		search = document.getElementById(inputid).value;

		url = 'index.php?route=product/search';
		if (search) {
			url += '&search=' + encodeURIComponent(search);
		}
		if (news_category_id) {
			news_category_id = document.getElementById(news_category_id).value;
			url += '&news_category_id=' + encodeURIComponent(news_category_id);
		}
		location = url;

	}
function toggleMenuMobile(){
	var menub = document.getElementById('menubutton');
	var menum = document.getElementById('mobile-menu');
	toogleClass(menub,'menuopen');
	toogleClass(menum,'fixedmenu');
	if(menub.innerHTML == 'MENU'){
		menub.innerHTML = '';
	}else{
		menub.innerHTML = 'MENU';
	}

}
function toogleClass(ele, class1) {
  var classes = ele.className;
  var regex = new RegExp('\\b' + class1 + '\\b');
  var hasOne = classes.match(regex);
  class1 = class1.replace(/\s+/g, '');
  if (hasOne)
    ele.className = classes.replace(regex, '');
  else
    ele.className = classes + class1;
}

function rudrSwitchTab(rudr_tab_id, rudr_tab_content, tabcontent, tabmenu) {
		var x = document.getElementsByClassName(tabcontent);
		var i;
		for (i = 0; i < x.length; i++) {
			x[i].style.display = 'none';
		}
		document.getElementById(rudr_tab_content).style.display = 'block';
		var x = document.getElementsByClassName(tabmenu);
		var i;
		for (i = 0; i < x.length; i++) {
			x[i].className = tabmenu;
		}
		document.getElementById(rudr_tab_id).className = tabmenu + ' active';
	}
function addClass(id, class_) {
		if(document.getElementById(id).className.indexOf(class_) < 0){
			document.getElementById(id).className += " " + class_;
		}
	}
function removeClass(id,class_) {
		document.getElementById(id).className = (' ' + document.getElementById(id).className + ' ').replace(' ' + class_ + ' ', ' ' );
		removeSpace(id,class_);
	}
function removeSpace(id,class_){
		if(document.getElementById(id).className.indexOf("  ") >= 0){
			document.getElementById(id).className = document.getElementById(id).className.replace('  ',' ');
			removeSpace(id,class_);
		}
	}
var mItem = document.getElementsByClassName('menuItem');
var mHD = document.getElementsByClassName('menuItemHeading');
for (i = 0; i < mHD.length; i++) {
	mHD[i].addEventListener('click', toggleItem1, false);
}
function toggleItem1() {
	var itemClass = this.parentNode.className;
	for (i = 0; i < mItem.length; i++) {
		mItem[i].className = 'relative menuItem cls';
	}
	if (itemClass == 'relative menuItem cls') {
		this.parentNode.className = 'relative menuItem open';
	}
}
window.addEventListener('mouseover', hover, false);
window.addEventListener('mouseout', removehover, false);
function hover(event) {
	if(event.target.id){
		id = event.target.id.substring(1, 2);
		for(i=1;i<parseInt(id)+1;i++){
			addClass('s'+i,"hover_green");
		}
	}
}
function removehover(event) {
	if(event.target.id){
		id = event.target.id.substring(1, 2);
		for(i=1;i<parseInt(id)+1;i++){
			removeClass('s'+i,"hover_green");
		}
	}
}
function rate(id,type,value_id) {
	if(document.getElementById("point").value == ''){
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.open("POST", 'index.php?route='+ type +'/'+ type +'/rate&'+ type +'_id='+ value_id +'&point='+id);
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == XMLHttpRequest.DONE) {
				if(xmlhttp.status == 200){
					var rs = xmlhttp.responseText.split(",");
					document.getElementById('total_vote').innerHTML = 'Thành công! Bạn đã bầu chọn <span style="color:red" itemprop="reviewCount">' + id + '</span> điểm';
					document.getElementById('point').value = 'load';
					document.getElementById('point_rs').innerHTML = 'Có <span itemprop="reviewCount">' + rs[0].replace(/"/,"") + '</span> bầu chọn / điểm trung bình: <span itemprop="ratingValue">' + rs[1].replace(/"/,"") + '</span>';
				}else{
					console.log('Error: ' + xmlhttp.statusText );
				}
			}
		}
		xmlhttp.send('');
	}
}
[].forEach.bind(document.getElementsByClassName("required"), function (itm) {
	itm.style.display = "none";
})();
function order(e, t) {
    var n = window.XMLHttpRequest ? new XMLHttpRequest : new ActiveXObject("Microsoft.XMLHTTP");
    n.onreadystatechange = function() {
        if (n.readyState == XMLHttpRequest.DONE)
            if (200 == n.status) {
                n.responseText;
                1 == n.response ? alert("Đặt hàng không thành công!") : 2 == n.response && (alert("Đặt hàng thành công"),window.location = "common/cart", document.getElementById("cart").innerHTML = '<a href="common/cart"><img src="static/cart-icon.png" alt="Giỏ hàng"/></a>')
            } else console.log("Error: " + n.statusText)
    }, n.open("POST", "index.php?route=common/cart/cart", !0), n.setRequestHeader("Content-type", "application/x-www-form-urlencoded"), n.send("&model=" + t + "&type=" + e)
}
function zoom(e){
  var zoomer = e.currentTarget;
  e.offsetX ? offsetX = e.offsetX : offsetX = e.touches[0].pageX
  e.offsetY ? offsetY = e.offsetY : offsetX = e.touches[0].pageX
  x = offsetX/zoomer.offsetWidth*100
  y = offsetY/zoomer.offsetHeight*100
  zoomer.style.backgroundPosition = x + '% ' + y + '%';
}

function submitOrder(e,t){t.preventDefault();var n=new XMLHttpRequest;n.open("POST","index.php?route=module/dathang/add",!1);var o=new FormData(document.getElementById(e));n.send(o);var a=n.responseText.replace('"',""),a=a.split(","),s=0;if(a.splice(-1,1),200==n.status){for([].forEach.bind(document.getElementsByClassName("required"),function(e){e.style.display="none"})(),i=1;i<9;i++){var r=i.toString();a.indexOf(r)>=0&&(document.getElementById(e+"error"+i)&&(document.getElementById(e+"error"+i).style.display="block"),s+=1)}0==s&&(document.getElementById("order_form").innerHTML='<div class="success roboto h1 white strong">Bạn đã đặt hàng thành công. Chúng tôi sẽ liên hệ với bạn trong thời gian sớm nhất để xác nhận thông tin</div>')}else console.log("Error: "+n.statusText)}

function email_subscribe(){
    var xmlhttp= window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == XMLHttpRequest.DONE) {
            if (xmlhttp.status == 200) {
                if(xmlhttp.response==1){
                    document.getElementById('subscribe_result').innerHTML = '<div class="required yellow">Không đúng định dạng email.<br /> Vui lòng nhập lại!</div>';
                } else if(xmlhttp.response==2){
                    document.getElementById('subscribe_result').innerHTML = '<div class="required yellow">Email của bạn đã tồn tại.</div>';
                } else {
                    document.getElementById('subscribe_result').innerHTML = '<div class="success blue">Bạn đã đăng ký nhận bản tin khuyến mại qua email thành công!</div>';
                    document.getElementById('subscribe_email').value = '';
                }

            } else {
                console.log('Error: ' + xmlhttp.statusText);
            }
        }
    }
    var email = document.getElementById('subscribe_email').value;
    xmlhttp.open("POST","index.php?route=module/newsletter/add",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlhttp.send("&subscribe_email=" + email);
}

$(function() {
	$('#captcha_img,#captchaimg').click(function(){
			$('#captchaimg').attr('src', 'index.php?route=information/contact/captcha&time=' + (new Date).getTime());
	});
});
