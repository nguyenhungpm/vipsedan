<?php

class ControllerCommonShowroom extends Controller {

    public function index() {
        $this->language->load('common/showroom');

		if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        if (isset($this->request->get['limit'])) {
            $limit = $this->request->get['limit'];
        } else {
            $limit = $this->config->get('config_catalog_limit');
        }

        if (isset($this->request->get['brand']) && strlen($this->request->get['brand'])>1) {
            $this->data['brand'] = $brand = $this->request->get['brand'];
        } else {
            $this->data['brand'] = $brand = '';
        }
        $this->data['showroom_banner'] = 'media/'.$this->config->get('config_showroom_banner');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->data['text_home'] = $this->language->get('text_home');
        $this->data['text_north'] = $this->language->get('text_north');
        $this->data['text_south'] = $this->language->get('text_south');
        $this->data['text_central'] = $this->language->get('text_central');
        $this->data['text_select'] = $this->language->get('text_select');
        $this->data['text_email'] = $this->language->get('text_email');
        $this->data['text_phone'] = $this->language->get('text_phone');
        // order
        //$this->language->load('information/contactproduct');
        if ($this->config->get('config_language_id') == 2) {
            $this->data['text'] = html_entity_decode($this->config->get('contact_textvn'));
        } else {
            $this->data['text'] = html_entity_decode($this->config->get('contact_texten'));
        }
        $this->data['telephone_support'] = html_entity_decode($this->config->get('contact_telephone'));
        $this->data['contact_buy'] = html_entity_decode($this->config->get('contact_buy'));
        $this->data['contact_free'] = html_entity_decode($this->config->get('contact_free'));
        $this->data['config_special'] = html_entity_decode($this->config->get('config_special'));
        $this->data['frees'] = $this->config->get('contact_module');

        $this->data['text_intro'] = $this->language->get('text_intro');
        $this->data['text_call'] = sprintf($this->language->get('text_call'), $this->config->get('contact_telephone'));
        $this->data['text_go'] = $this->language->get('text_go');
        $this->data['text_consult'] = $this->language->get('text_consult');
        $this->data['text_method'] = $this->language->get('text_method');
        $this->data['text_quantity'] = $this->language->get('text_quantity');
        $this->data['button_order'] = $this->language->get('button_order');
        $this->data['text_order'] = $this->language->get('text_order');
        $this->data['text_order_intro'] = $this->language->get('text_order_intro');
        $this->data['url_order'] = $this->url->link('information/contactproduct', '');
        // end order

        $this->data['heading_title'] = $this->language->get('heading_title');
        $this->load->model('catalog/product');
        $this->load->model('catalog/showroom');
        $areas = $this->model_catalog_showroom->getAreas();
        foreach ($areas as $area) {
            $province_rs = $this->model_catalog_showroom->getProvinceByArea($area['area_id']);
            $province = array();
            $data_arr = array();
            foreach ($province_rs as $pr) {
                $data_arr = array(
                    'province_id' => $pr['provinceid']
                );
    				$province[] = array(
    					'province_id' => $pr['provinceid'],
    					'href' => $this->url->link('common/showroom', 'province_id=' . $pr['provinceid']),
    					'name' => $pr['name']
    				);
            }
            $this->data['showrooms'][] = array(
                'area_id' => $area['area_id'],
                'name' => $area['name'],
                'province' => $province
            );
        }

        if(isset($this->request->get['province_id']) && ($this->request->get['province_id']>0)){
          $this->data['provinceid'] = $provinceid = $this->request->get['province_id'];
        }else{
          $this->data['provinceid'] = $provinceid = '';
        }

        if(isset($this->request->get['district_id']) && ($this->request->get['district_id']>0)){
          $this->data['district_id'] = $districtid = $this->request->get['district_id'];
        }else{
          $this->data['district_id'] = $districtid = '';
        }

        if(isset($this->request->get['man_id']) && ($this->request->get['man_id']>0)){
          $this->data['pro_id'] = $pro_id = $this->request->get['man_id'];
        }else{
          $this->data['pro_id'] = $pro_id = '';
        }

		    $array_filter = array(
			       'province_id' => $provinceid,
             'district_id' => $districtid,
             'brand' => $brand,
             'man_id' => $pro_id,
			       'start' => ($page - 1) * $limit,
			       'limit' => $limit
		    );
        $url = '';
        if(isset($provinceid) && $provinceid>0){
          $url .= '&province_id=' . $provinceid;
        }
        if(isset($districtid) && $districtid>0){
          $url .= '&district_id=' . $districtid;
        }
        if(isset($brand) && strlen($brand>1)){
          $url .= '&brand=' . $brand;
        }
        if(isset($pro_id) && strlen($pro_id>1)){
          $url .= '&man_id=' . $pro_id;
        }

    		$dealers = $this->model_catalog_showroom->getDealerByProvinceID($array_filter);
    		// print_r($dealers);
    		$dealers_total = $this->model_catalog_showroom->getTotalDealerByProvinceID($array_filter);
        //print_r($dealers);
    		foreach($dealers as $dl){
    			$this->data['dealers'][] = array(
    				        'name' => $dl['name'],
                    'address' => $dl['address'],
                    'email' => $dl['email'],
                    'telephone' => $dl['telephone']
    			);
    		}

        //$this->data['mans'] = array();
        $dataman = array(
            'start' => 0,
            'limit' => 100
        );
        $this->data['districtes'] = array();
        if(isset($this->request->get['province_id']) && ($this->request->get['province_id']>0)){
            $districtes = $this->model_catalog_showroom->getDistrictByProvince($this->request->get['province_id']);
            foreach ($districtes as $district){
              $this->data['districtes'][] = array(
                    'district_id' => $district['district_id'],
                    'name'        => $district['name']
              );
            }
        }
        $this->document->addScript('static/district.js');
        $pros = $this->model_catalog_product->getProducts($dataman);

        foreach($pros as $pro){
            $this->data['pros'][] = array(
                'name' => $pro['name'],
                'product_id' => $pro['product_id']
            );
        }


        $script ="
            baseurl = 'common/showroom';
            $('#province, #manufacturer,#district_id').on('change',function(){
                province = $('#province').val();
                manufacturer = $('#manufacturer').val();
                district_id = $('#district_id').val();
                if(province>0){
                    baseurl = baseurl + '&province_id='+province;
                }
                if(manufacturer>0){
                    baseurl = baseurl + '&man_id='+manufacturer;
                }
                if(district_id>0){
                    baseurl = baseurl + '&district_id='+district_id;
                }
                window.location = baseurl;
            });
        ";
        $this->document->addExScript($script);

		$pagination = new Pagination();
		$pagination->total = $dealers_total;
		$pagination->page = $page;
		$pagination->limit = $limit;
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('common/showroom', $url.'&page={page}');

		$this->data['pagination'] = $pagination->render();

        $this->template = 'common/showroom.tpl';
        $this->children = array(
                'common/column_left',
                'common/column_right',
                'common/content_top',
                'common/content_bottom',
                'common/footer',
                'common/header'
        );
        $this->response->setOutput($this->render());
  }

  public function province() {
    $json = array();
    $this->load->model('catalog/showroom');
    $results = $this->model_catalog_showroom->getDistrictByProvince($this->request->get['province_id']);
    if ($results) {
      $this->load->model('catalog/showroom');
      foreach ($results as $result) {
        $json[] = array(
          'district_id'       => $result['district_id'],
          'name'              => $result['name']
        );
      }
    }

    $this->response->setOutput(json_encode($json));
  }

}

?>
