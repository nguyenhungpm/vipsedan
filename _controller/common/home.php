<?php
class ControllerCommonHome extends Controller {
	public function index() {
		$this->language->load('common/home');

		if(strlen($this->config->get('config_title')>1)){
			$this->document->setTitle($this->config->get('config_title'));
		}else{
			$this->document->setTitle($this->config->get('config_name')[$this->config->get('config_language_id')]);
		}
		$meta_description = nl2br($this->config->get('config_meta_description')[$this->config->get('config_language_id')]);
		$meta_keyword = nl2br($this->config->get('config_meta_keyword')[$this->config->get('config_language_id')]);

		if($this->config->get('config_secure')==1){
			$this->document->addLink(HTTPS_SERVER, 'canonical');
		}else{
			$this->document->addLink(HTTP_SERVER, 'canonical');
		}

		$this->document->setMetanames('description',$meta_description);
		$this->document->setMetanames('keywords',$meta_keyword);

		$this->load->model('tool/image');
		if ($this->config->get('config_logo') && file_exists(DIR_IMAGE . $this->config->get('config_logo'))) {
			$this->data['logo'] = $this->config->get('config_logo');
		} else {
			$this->data['logo'] = '';
		}
		// echo $this->data['logo'];
		// $image_og = $this->model_tool_image->resize($this->data['logo'], 225,225);
		$image_og = $this->model_tool_image->resize('data/logo/logo-og.png', 225,225);
		// echo $image_og;
		$this->document->setMetapros('og:image',$image_og);
		// $this->document->setMetapros('og:image:secure_url',$image_og);

		$this->data['hotline'] = $this->config->get('config_hotline');

		$this->data['heading_title'] = $this->config->get('config_name')[$this->config->get('config_language_id')];
    $this->load->model('catalog/type_car');
    $this->data['listCars'] = $this->model_catalog_type_car->getTypeCars(array());

    // print_r($this->data['listCars']);


		$this->template = 'common/home.tpl';

		$this->children = array(
			'common/content_top',
			'common/adv_top',
			'common/content_bottom',
			'common/column_left',
			'common/column_right',
			'common/slideside',
			'common/footer',
			'common/header'
		);

		$this->response->setOutput($this->render());
	}
}
?>
