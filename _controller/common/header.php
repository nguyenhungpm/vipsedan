<?php
class ControllerCommonHeader extends Controller {
	protected function index() {
        $this->data['title'] =  $this->document->getTitle();

		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}

		if (isset($this->session->data['error']) && !empty($this->session->data['error'])) {
			$this->data['error'] = $this->session->data['error'];

			unset($this->session->data['error']);
		} else {
			$this->data['error'] = '';
		}
		/*if(strlen($this->config->get('config_meta_robots'))>3){
			$this->document->setMetapros('robots',$this->config->get('config_meta_robots'));
		}*/
		$this->data['base'] = $server;
		$this->data['links'] = $this->document->getLinks();
		$this->data['metapros'] = $this->document->getMetapros();
		$this->data['metanames'] = $this->document->getMetanames();
    // echo '<div style="display:none">';
    // print_r($this->data['metapros']);
    // echo '</div>';
		$this->data['styles'] = $this->document->getStyles();

		$this->data['lang'] = $this->language->get('code');
		$this->data['direction'] = $this->language->get('direction');
		$this->data['hotline'] = $this->config->get('config_hotline');
		$this->data['email'] = $this->config->get('config_email');

		$this->data['store'] = nl2br($this->config->get('config_owner')[$this->config->get('config_language_id')]);
		$this->data['name'] = nl2br($this->config->get('config_name')[$this->config->get('config_language_id')]);
		$this->data['telephone'] = $this->config->get('config_telephone');
		$this->data['google_verify'] = html_entity_decode($this->config->get('config_google_verify'), ENT_QUOTES, 'UTF-8');
		$this->data['script_header'] = html_entity_decode($this->config->get('config_script_header'), ENT_QUOTES, 'UTF-8');
		$this->data['custom_css'] = html_entity_decode($this->config->get('config_custom_css'), ENT_QUOTES, 'UTF-8');
		$this->data['copy_protect'] = $this->config->get('config_copy_protect');

		if(isset($this->request->get['route'])){
			$this->data['route'] = $this->request->get['route'];
		}else{
			$this->data['route'] = "common/home";
		}

		$this->load->model('menumanager/menu');
		$this->data['menus'] = $this->model_menumanager_menu->getMenuByGroup2($this->config->get('menu_module_mobile'));

		$class = explode('/',$this->data['route']);
		$this->data['class'] = join('-', $class);

        if($this->data['route'] == "common/home") {
            $curent_language = (int)$this->config->get('config_language_id');
            $arraytitle = $this->config->get('config_title');
            $this->data['title'] = $arraytitle[$curent_language];
        }


		if ($this->config->get('config_icon') && file_exists(DIR_IMAGE . $this->config->get('config_icon'))) {
			$this->data['icon'] = $server . 'media/' . $this->config->get('config_icon');
		} else {
			$this->data['icon'] = '';
		}

		if ($this->config->get('config_logo') && file_exists(DIR_IMAGE . $this->config->get('config_logo'))) {
			$this->data['logo'] = $server . 'media/' . $this->config->get('config_logo');
		} else {
			$this->data['logo'] = '';
		}

		$this->language->load('common/header');

		$this->data['text_home'] = $this->language->get('text_home');
		$this->data['text_search'] = $this->language->get('text_search');
		$this->data['text_contact'] = $this->language->get('text_contact');


		$this->data['home'] = $this->url->link('common/home');
		$this->data['opensearch'] = $this->url->link('product/opensearch', '', 'SSL');

		
		$this->data['fb'] = $this->config->get('config_facebook');
		$this->data['pt'] = $this->config->get('config_pinterest');
		$this->data['yt'] = $this->config->get('config_youtube');
		$this->data['tw'] = $this->config->get('config_twitter');
		$this->data['gl'] = $this->config->get('config_googleplus');

		// Daniel's robot detector
		$status = true;

		if (isset($this->request->server['HTTP_USER_AGENT'])) {
			$robots = explode("\n", trim($this->config->get('config_robots')));

			foreach ($robots as $robot) {
				if ($robot && strpos($this->request->server['HTTP_USER_AGENT'], trim($robot)) !== false) {
					$status = false;

					break;
				}
			}
		}

		// Search
		if (isset($this->request->get['search'])) {
			$this->data['search'] = $this->request->get['search'];
		} else {
			$this->data['search'] = '';
		}

		$this->children = array(
			'module/language',
			'common/adv_top',
			'common/top_menu'
		);
		$this->template = 'common/header.tpl';

		$this->render();
	}
}
?>
