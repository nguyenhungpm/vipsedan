<?php

class ControllerProductSearch extends Controller {

    public function index() {
        $this->language->load('product/search');

        $this->load->model('catalog/category');

        $this->load->model('catalog/product');

        $this->load->model('tool/image');
        $this->load->model('tool/t2vn');

        if (isset($this->request->get['search'])) {
            $search = $this->request->get['search'];
        } else {
            $search = '';
        }

        if (isset($this->request->get['tag'])) {
            $tag = $this->request->get['tag'];
        } elseif (isset($this->request->get['search'])) {
            $tag = $this->request->get['search'];
        } else {
            $tag = '';
        }

        if (isset($this->request->get['description'])) {
            $description = $this->request->get['description'];
        } else {
            $description = '';
        }

        if (isset($this->request->get['category_id'])) {
            $category_id = $this->request->get['category_id'];
        } else {
            $category_id = 0;
        }

        if (isset($this->request->get['sub_category'])) {
            $sub_category = $this->request->get['sub_category'];
        } else {
            $sub_category = '';
        }

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'p.sort_order';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = (int)$this->request->get['page'];
        } else {
            $page = 1;
        }

        if (isset($this->request->get['limit'])) {
            $limit = $this->request->get['limit'];
        } else {
            $limit = $this->config->get('config_catalog_limit');
        }

        if (isset($this->request->get['search'])) {
            $this->document->setTitle($this->language->get('heading_title') . ' - ' . $this->request->get['search']);
        } else {
            $this->document->setTitle($this->language->get('heading_title'));
        }

        $limit_text = 350;

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home'),
            'separator' => false
        );

        $url = '';

        if (isset($this->request->get['search'])) {
            $url .= '&search=' . urlencode(html_entity_decode($this->request->get['search'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['tag'])) {
            $url .= '&tag=' . urlencode(html_entity_decode($this->request->get['tag'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['description'])) {
            $url .= '&description=' . $this->request->get['description'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('product/search', $url),
            'separator' => $this->language->get('text_separator')
        );

        if (isset($this->request->get['search'])) {
            $this->data['heading_title'] = $this->language->get('heading_title') . ' - ' . $this->request->get['search'];
        } else {
            $this->data['heading_title'] = $this->language->get('heading_title');
        }

        $this->data['text_empty'] = $this->language->get('text_empty');
        $this->data['text_critea'] = $this->language->get('text_critea');
        $this->data['text_search'] = $this->language->get('text_search');
        $this->data['text_keyword'] = $this->language->get('text_keyword');
        $this->data['text_category'] = $this->language->get('text_category');
        $this->data['text_keyword'] = $this->language->get('text_keyword');
        $this->data['text_manufacturer'] = $this->language->get('text_manufacturer');
        $this->data['text_model'] = $this->language->get('text_model');
        $this->data['text_price'] = $this->language->get('text_price');
        $this->data['text_tax'] = $this->language->get('text_tax');

        $this->data['text_display'] = $this->language->get('text_display');
        $this->data['text_list'] = $this->language->get('text_list');
        $this->data['text_grid'] = $this->language->get('text_grid');
        $this->data['text_sort'] = $this->language->get('text_sort');
        $this->data['text_limit'] = $this->language->get('text_limit');

        $this->data['entry_search'] = $this->language->get('entry_search');
        $this->data['entry_description'] = $this->language->get('entry_description');

        $this->data['button_search'] = $this->language->get('button_search');
        $this->data['button_cart'] = $this->language->get('button_cart');

        $this->data['products'] = array();
        $this->data['news'] = array();
        $this->data['informations'] = array();
        $this->data['search_rs'] = array();
        $this->load->model('catalog/search');

        foreach ($this->model_catalog_search->getSearchProduct(array('filter_name' => $search)) as $result) {

            if (empty($result['short_description'])) {
                $short_description = $this->model_tool_t2vn->cut_string(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'), $limit_text);
            } else {
                $short_description = $this->model_tool_t2vn->cut_string(html_entity_decode($result['short_description'], ENT_QUOTES, 'UTF-8'), $limit_text);
            }

            $this->data['products'][] = array(
                'id' => $result['product_id'],
                'name' => $result['name'],
                'type' => $result['type'],
                'count' => $result['count'],
                'short_description' => $short_description,
                'href' => $this->url->link('product/product', '&product_id=' . $result['product_id'])
            );
        }

        foreach ($this->model_catalog_search->getSearchNews(array('filter_name' => $search)) as $result) {

            if (empty($result['short_description'])) {
                $short_description = $this->model_tool_t2vn->cut_string(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'), $limit_text);
            } else {
                $short_description = $this->model_tool_t2vn->cut_string(html_entity_decode($result['short_description'], ENT_QUOTES, 'UTF-8'), $limit_text);
            }

            $this->data['news'][] = array(
                'id' => $result['news_id'],
                'name' => $result['name'],
                'type' => $result['type'],
                'count' => $result['count'],
                'short_description' => $short_description,
                'href' => $this->url->link('news/news', '&news_id=' . $result['news_id'])
            );
        }


        foreach ($this->model_catalog_search->getSearchInformation(array('filter_name' => $search)) as $result) {

            if (empty($result['short_description'])) {
                $short_description = $this->model_tool_t2vn->cut_string(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'), $limit_text);
            } else {
                $short_description = $this->model_tool_t2vn->cut_string(html_entity_decode($result['short_description'], ENT_QUOTES, 'UTF-8'), $limit_text);
            }

            $this->data['informations'][] = array(
                'id' => $result['information_id'],
                'name' => $result['name'],
                'type' => $result['type'],
                'count' => $result['count'],
                'short_description' => $short_description,
                'href' => $this->url->link('information/information', '&information_id=' . $result['information_id'])
            );
        }

        // foreach ($this->model_catalog_search->getSearchDirectory(array('filter_name' => $search)) as $result) {

        //     if (empty($result['short_description'])) {
        //         $short_description = $this->model_tool_t2vn->cut_string(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'), $limit_text);
        //     } else {
        //         $short_description = $this->model_tool_t2vn->cut_string(html_entity_decode($result['short_description'], ENT_QUOTES, 'UTF-8'), $limit_text);
        //     }

        //     $this->data['directories'][] = array(
        //         'id' => $result['register_id'],
        //         'name' => $result['name'],
        //         'type' => $result['type'],
        //         'count' => $result['count'],
        //         'short_description' => $short_description,
        //         'href' => $this->url->link('form/register', '&register_id=' . $result['register_id'])
        //     );
        // }

        if (isset($this->request->get['search']) || isset($this->request->get['tag']) || isset($this->request->get['category_id'])) {

            $search_rs = array_merge($this->data['products'],$this->data['news'], $this->data['informations']);
            //$search_rs = $this->record_sort($search_rs, 'count');
            $this->data['search_rs'] =$search_rs;

            $search_total = count($search_rs);

            $this->data['limits'] = array();

            $limits = array_unique(array($this->config->get('config_catalog_limit'), 25, 50, 75, 100));

            sort($limits);

            foreach ($limits as $value) {
                $this->data['limits'][] = array(
                    'text' => $value,
                    'value' => $value,
                    'href' => $this->url->link('product/search', $url . '&limit=' . $value)
                );
            }

            $pagination = new Pagination();
            $pagination->total = $search_total;
            $pagination->page = $page;
            $pagination->limit = $limit;
            $pagination->text = $this->language->get('text_pagination');
            $pagination->url = $this->url->link('product/search', $url . '&page={page}');

            $this->data['pagination'] = $pagination->render();
        }

        $this->data['search'] = $search;
        $this->data['description'] = $description;
        $this->data['category_id'] = $category_id;
        $this->data['sub_category'] = $sub_category;

        $this->data['sort'] = $sort;
        $this->data['order'] = $order;
        $this->data['limit'] = $limit;

        $this->template = 'product/search.tpl';

        $this->children = array(
            'common/column_left',
            'common/column_right',
            'common/content_top',
            'common/slideside',
            'common/content_bottom',
            'common/footer',
            'common/header'
        );

        $this->response->setOutput($this->render());
    }

    public function record_sort($records, $field, $reverse=true){
        $hash = array();

        foreach($records as $record){
            $hash[$record[$field]] = $record;
        }

        ($reverse)? krsort($hash) : ksort($hash);

        $records = array();

        foreach($hash as $record){
            $records []= $record;
        }

        return $records;
    }

}

?>
