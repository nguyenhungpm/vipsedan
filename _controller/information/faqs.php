<?php 
class ControllerInformationFaqs extends Controller {
	private $error = array(); 

	public function index() {
		$this->language->load('information/faqs');

		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/faqs');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_catalog_faqs->addQuestion($this->request->post);
			$this->redirect($this->url->link('information/faqs/success'));
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),        	
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('information/faqs'),
			'separator' => $this->language->get('text_separator')
		);

		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['heading_title2'] = $this->language->get('heading_title2');

		$this->data['text_location'] = $this->language->get('text_location');
		$this->data['text_contact'] = $this->language->get('text_contact');
		$this->data['text_address'] = $this->language->get('text_address');
		$this->data['text_address2'] = $this->language->get('text_address2');
		$this->data['text_telephone'] = $this->language->get('text_telephone');
		$this->data['text_phone'] = $this->language->get('text_phone');
		$this->data['text_title'] = $this->language->get('text_title');
		$this->data['text_question'] = $this->language->get('text_question');
		$this->data['text_answer'] = $this->language->get('text_answer');
		$this->data['text_by'] 		= $this->language->get('text_by');
		

		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['text_email'] = $this->language->get('text_email');
		$this->data['entry_enquiry'] = $this->language->get('entry_enquiry');
		$this->data['entry_submit'] = $this->language->get('entry_submit');
		$this->data['entry_captcha'] = $this->language->get('entry_captcha');

		if (isset($this->error['name'])) {
			$this->data['error_name'] = "Tên trống hoặc quá ngắn";
		} else {
			$this->data['error_name'] = '';
		}

		if (isset($this->error['email'])) {
			$this->data['error_email'] = "Email nhập vào không có hoặc không đúng định dạng";
		} else {
			$this->data['error_email'] = '';
		}		

		if (isset($this->error['phone'])) {
			$this->data['error_phone'] = "Bạn đọc nên nhập đủ thông tin số điện thoại hoặc email để gửi tin nhắn cho chúng tôi.";
		} else {
			$this->data['error_phone'] = '';
		}		

		if (isset($this->error['question'])) {
			$this->data['error_question'] = "Nội dung nhập vào quá ngắn";
		} else {
			$this->data['error_question'] = '';
		}

		if (isset($this->error['title'])) {
			$this->data['error_title'] = "Tiêu đề phải có độ dài từ 5-128 ký tự";
		} else {
			$this->data['error_title'] = '';
		}		

		if (isset($this->error['captcha'])) {
			$this->data['error_captcha'] = $this->error['captcha'];
		} else {
			$this->data['error_captcha'] = '';
		}	

		$this->data['button_continue'] = $this->language->get('button_continue');

		$this->data['action'] = $this->url->link('information/faqs');
		if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];
		} else {
			$this->data['name'] = '';
		}

		if (isset($this->request->post['email'])) {
			$this->data['email'] = $this->request->post['email'];
		} else {
			$this->data['email'] = '';
		}

		if (isset($this->request->post['phone'])) {
			$this->data['phone'] = $this->request->post['phone'];
		} else {
			$this->data['phone'] = '';
		}

		if (isset($this->request->post['question'])) {
			$this->data['question'] = $this->request->post['question'];
		} else {
			$this->data['question'] = '';
		}

		if (isset($this->request->post['title'])) {
			$this->data['title'] = $this->request->post['title'];
		} else {
			$this->data['title'] = '';
		}

		if (isset($this->request->post['captcha'])) {
			$this->data['captcha'] = $this->request->post['captcha'];
		} else {
			$this->data['captcha'] = '';
		}
		
		
		//List 
		$questions_per_page = 20;
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}  
		$this->load->model('catalog/faqs');
		$this->data['questions'] = array();
		$questions = $this->model_catalog_faqs->getQuestions(
			array(
				'display' => 1
			),
			array(
				'order_by' => 'create_time',
				'order_way' => 'DESC',
				'offset' => ($page - 1) * $questions_per_page,
				'limit' => $questions_per_page
			)
		);
		
		foreach($questions as $result){
			$this->data['questions'][] = array(
				'title'		=> $result['title'],
				'name'		=> $result['name'],
				'create_time'=>gmdate("d.m.Y, h:i", $result['create_time']),
				'phone'		=> $result['phone'],
				'question'	=> $result['question'],
				'answer'	=> html_entity_decode($result['answer'], ENT_QUOTES, 'UTF-8'),
				'email'		=> $result['email'],
				'href'		=> 'faqs/'.$result['faq_id']
			);	
		}
		
		
		$total_questions = $this->model_catalog_faqs->getQuestionCount();
      		
		$pagination = new Pagination();
		$pagination->total = $total_questions;
		$pagination->page = $page;
		$pagination->limit = $questions_per_page; 
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = 'faqs&page={page}';
			
		$this->data['pagination'] = $pagination->render();
		
		$this->template = 'information/faqs.tpl';

		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'
		);

		$this->response->setOutput($this->render());
	}
	
	public function detail(){
		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),			
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Câu hỏi thường gặp',
			'href'      => $this->url->link('information/faqs'),			
			'separator' => $this->language->get('text_separator')
		);
		
		if(isset($this->request->get['faq_id'])){
			$this->load->model('catalog/faqs');
			$data = array(
				'faq_id' => $this->request->get['faq_id']
			);
			$question_info = $this->model_catalog_faqs->getQuestions($data)[0];
			$this->data['breadcrumbs'][] = array(
				'text'      => $question_info['title'],
				'href'      => 'faqs/'.$this->request->get['faq_id'],			
				'separator' => $this->language->get('text_separator')
			);
			$this->data['text_other_faqs'] = $this->language->get('text_other_faqs');
			$this->document->setTitle($question_info['title']);
			$this->document->setDescription($question_info['question']);
			$this->data['name'] = $question_info['name'];
			$this->data['create_time'] = gmdate("d.m.Y, h:i", $question_info['create_time']);
			$this->data['heading_title'] = $question_info['title'];
			$this->data['short_description'] = $question_info['question'];
			$this->data['description'] =  html_entity_decode($question_info['answer'], ENT_QUOTES, 'UTF-8');
			$this->data['url'] = HTTP_SERVER . 'faqs/'.$this->request->get['faq_id'];

			$this->load->model('catalog/faqs');
			$this->data['other_faqs'] = array();
			$other_faqs = $this->model_catalog_faqs->getOtherQuestions($this->request->get['faq_id']);
			
			foreach($other_faqs as $result){
				$this->data['other_faqs'][] = array(
					'name'		=> $result['name'],
					'create_time'		=> gmdate("d.m.Y, h:i", $result['create_time']),
					'href'		=> 'faqs/'.$result['faq_id']
				);
			}
		}
		
		$this->template = 'information/faq.tpl';

		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'
		);

		$this->response->setOutput($this->render());
	}
	public function add(){
		$this->load->model('information/faqs');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_information_faqs->addQuestion($this->request->post);
			$this->redirect($this->url->link('information/faqs/success'));
		}else{
		if (isset($this->error['name'])) {
			$this->data['error_name'] = "Tên trống hoặc quá ngắn";
		} else {
			$this->data['error_name'] = '';
		}

		if (isset($this->error['email'])) {
			$this->data['error_email'] = "Email nhập vào không có hoặc không đúng định dạng";
		} else {
			$this->data['error_email'] = '';
		}		

		if (isset($this->error['enquiry'])) {
			$this->data['error_enquiry'] = "Nội dung nhập vào quá ngắn";
		} else {
			$this->data['error_enquiry'] = '';
		}
		$this->redirect($this->url->link('information/faqs'));
		}
	}

	public function success() {
		$this->language->load('information/faqs');

		$this->document->setTitle($this->language->get('heading_title')); 

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('information/faqs'),
			'separator' => $this->language->get('text_separator')
		);

		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_message'] = $this->language->get('text_message');
		$this->data['button_continue'] = $this->language->get('button_continue');
		$this->data['continue'] = $this->url->link('common/home');
		$this->template = 'common/success.tpl';

		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'
		);

		$this->response->setOutput($this->render());
	}

	protected function validate() {
		if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 32)) {
			$this->error['name'] = $this->language->get('error_name');
		}
		if ((utf8_strlen($this->request->post['phone']) == null) && (utf8_strlen($this->request->post['phone']) ==null)) {
			$this->error['phone'] = $this->language->get('error_phone');
		}
		else if($this->request->post['email']!= null && !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['email'])){
			$this->error['email'] = $this->language->get('error_email');
		}
		
		if ((utf8_strlen($this->request->post['question']) < 10) || (utf8_strlen($this->request->post['question']) > 3000)) {
			$this->error['question'] = $this->language->get('error_enquiry');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}  	  
	}

	public function captcha() {
		$this->load->library('captcha');

		$captcha = new Captcha();

		$this->session->data['captcha'] = $captcha->getCode();

		$captcha->showImage();
	}
	public function yeucau(){
		$json = array();
		if(isset($this->request->post['tenban'])){
			$tenban = $this->request->post['tenban'];
			if(strlen($tenban)>64 || strlen($tenban)<1){
				$json['error'] = 'Có lỗi xảy ra, kiểm tra lại các thông tin';
			}
			$thudientu = $this->request->post['thudientu'];
			if(strlen($thudientu)>64 || strlen($thudientu)<5){
				$json['error'] = 'Có lỗi xảy ra, kiểm tra lại các thông tin';
			}
			$dienthoai = $this->request->post['dienthoai'];
			if(strlen($dienthoai)>64){
				$json['error'] = 'Có lỗi xảy ra, kiểm tra lại các thông tin';
			}
			$vande = $this->request->post['vande'];
			if(strlen($vande)>1000 || strlen($vande)<10){
				$json['error'] = 'Có lỗi xảy ra, kiểm tra lại các thông tin';
			}
			if(!isset($json['error'])){
				$subject = 'Thông báo: Có câu hỏi tư vấn trực tuyến mới';
				$json['success'] = 'Thông tin của bạn đã được gửi thành công, chúng tôi liên hệ lại với bạn trong thời gian sớm nhất!';
							
				$mail = new Mail();
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->hostname = $this->config->get('config_smtp_host');
				$mail->username = $this->config->get('config_smtp_username');
				$mail->password = $this->config->get('config_smtp_password');
				$mail->port = $this->config->get('config_smtp_port');
				$mail->timeout = $this->config->get('config_smtp_timeout');				
				$mail->setTo($this->config->get('config_email'));
				$mail->setFrom($this->config->get('config_mail_from'));
				$mail->setSender('webmaster');
				$mail->setSubject($subject);
				$mail->setText(strip_tags(html_entity_decode("Tên người hỏi: ". $tenban . "\r\nThư điện tử: ". $thudientu . "\r\nĐiện thoại: " . $dienthoai."\r\n \r\nVấn đề cần hỏi: \r\n " . $vande, ENT_QUOTES, 'UTF-8')));
				$mail->send();
			}

		}else{
			$json['error'] = 'Có lỗi xảy ra, kiểm tra lại các thông tin';
		}
		$this->response->setOutput(json_encode($json));
	}
}
?>