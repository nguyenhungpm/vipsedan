<?php
class ControllerAmpInformation extends Controller {
    private $error = array();

    public function index() {
        $this->language->load('information/information');
        $this->load->model('catalog/information');
		$this->data['text_related'] = $this->language->get('text_related');
        if (isset($this->request->get['information_id'])) {
            $information_id = $this->request->get['information_id'];
        } else {
            $information_id = 0;
        }

        $information_info = $this->model_catalog_information->getInformation($information_id);

        $this->data['information_info'] = $information_info;

        if ($information_info) {
            $url = '';

            if (strlen($information_info['meta_title']) > 1) {
                $title = $information_info['meta_title'];
            } else {
                $title = $information_info['title'];
            }

            $this->document->setTitle($title);

            if (strlen($information_info['meta_description']) > 1) {
                $meta_description = $information_info['meta_description'];
            } else {
                $meta_description = $information_info['short_description'];
            }

			$this->data['meta_description'] = $meta_description;
            $this->data['short_description'] = $information_info['meta_description'];
			$description = html_entity_decode($information_info['description'], ENT_QUOTES, 'UTF-8');
			$this->data['description'] = $this->response->amphtml($description);

            $this->document->addLink($this->url->link('information/information', 'information_id=' . $this->request->get['information_id']), 'canonical');

            $this->data['heading_title'] = $information_info['title'];

            $this->load->model('tool/image');

            if (!empty($information_info['image'])) {
                $this->data['thumb'] = $this->model_tool_image->resize($information_info['image'], $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height'));
            } else {
                $this->data['thumb'] = '';
            }

            $this->data['relates'] = array();
            /* $results = $this->model_catalog_information->getOtherInformation('', $this->request->get['information_id'], 10);

            foreach ($results as $result) {
                $this->data['relates'][] = array(
                    'information_id' => $result['information_id'],
                    'name' => $result['name'],
                    'date_added' => $result['date_available'] != '0000-00-00 00:00:00' ? date($this->language->get('date_format_short'), strtotime($result['date_available'])) : date($this->language->get('date_format_short'), strtotime($result['date_added'])),
                    'href' => $this->url->link('information/information', $url . '&information_id=' . $result['information_id']),
                );
            } */
            // $this->model_catalog_information->updateViewed($this->request->get['information_id']);
			// echo 3;

            $this->template = 'amp/product.tpl';

            $this->children = array(
                'amp/footer',
                'amp/header'
            );

            $this->response->setOutput($this->render());
        } else {
            $url = '';

            if (isset($this->request->get['cat_id'])) {
                $url .= '&cat_id=' . $this->request->get['cat_id'];
            }

            if (isset($this->request->get['manufacturer_id'])) {
                $url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
            }

            if (isset($this->request->get['filter_name_information'])) {
                $url .= '&filter_name_information=' . $this->request->get['filter_name_information'];
            }

            if (isset($this->request->get['filter_tag'])) {
                $url .= '&filter_tag=' . $this->request->get['filter_tag'];
            }

            if (isset($this->request->get['filter_description'])) {
                $url .= '&filter_description=' . $this->request->get['filter_description'];
            }

            if (isset($this->request->get['filter_information_category_id'])) {
                $url .= '&filter_information_category_id=' . $this->request->get['filter_information_category_id'];
            }

            $this->data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_error'),
                'href' => $this->url->link('information/information', $url . '&information_id=' . $information_id),
                'separator' => $this->language->get('text_separator')
            );

            $this->document->setTitle($this->language->get('text_error'));

            $this->data['heading_title'] = $this->language->get('text_error');

            $this->data['text_error'] = $this->language->get('text_error');

            $this->data['button_continue'] = $this->language->get('button_continue');

            $this->data['continue'] = $this->url->link('common/home');

            $this->template = 'error/not_found.tpl';

            $this->children = array(
                'common/column_left',
                'common/column_right',
                'common/content_top',
                'common/content_bottom',
                'common/footer',
                'common/header'
            );

            $this->response->setOutput($this->render());
        }
    }

}
?>
