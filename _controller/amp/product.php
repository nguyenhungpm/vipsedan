<?php
class ControllerAmpProduct extends Controller {
    private $error = array();

    public function index() {
        $this->language->load('product/product');
        $this->load->model('catalog/product');

		   $this->data['tab_related'] = $this->language->get('tab_related');

        if (isset($this->request->get['product_id'])) {
            $product_id = $this->request->get['product_id'];
        } else {
            $product_id = 0;
        }

        $product_info = $this->model_catalog_product->getProduct($product_id);

        $this->data['product_info'] = $product_info;

        if ($product_info) {
            $url = '';

            if (strlen($product_info['meta_title']) > 1) {
                $title = $product_info['meta_title'];
            } else {
                $title = $product_info['name'];
            }

            $this->document->setTitle($title);

            if (strlen($product_info['meta_description']) > 1) {
                $meta_description = $product_info['meta_description'];
            } else {
                $meta_description = $product_info['short_description'];
            }

			$this->data['meta_description'] = $meta_description;
            $this->data['short_description'] = $product_info['short_description'];
			$description = html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8');
			$this->data['description'] = $this->response->amphtml($description);

            $this->document->addLink($this->url->link('product/product', 'product_id=' . $this->request->get['product_id']), 'canonical');

            $this->data['heading_title'] = $product_info['name'];

            $this->load->model('tool/image');
            if ($product_info['image']) {
               $image = new FastImage(HTTP_IMAGE.$product_info['image']);
               list($w, $h) = $image->getSize();
               $ratio = $w/$h;
               $this->data['target_h'] = round(480 / $ratio);
               $this->data['thumb'] = $this->model_tool_image->resize($product_info['image'], '480',$this->data['target_h']);
            } else {
                 $this->data['thumb'] = false;
            }
            $this->data['relates'] = array();
            $results = $this->model_catalog_product->getOtherproduct(array('product_id' => $this->request->get['product_id'], 'limit' => 10));

            foreach ($results as $result) {
                $this->data['relates'][] = array(
                    'product_id' => $result['product_id'],
                    'name' => $result['name'],
                    'date_added' => $result['date_available'] != '0000-00-00 00:00:00' ? date($this->language->get('date_format_short'), strtotime($result['date_available'])) : date($this->language->get('date_format_short'), strtotime($result['date_added'])),
                    'href' => $this->url->link('product/product', $url . '&product_id=' . $result['product_id']),
                );
            }
            $this->model_catalog_product->updateViewed($this->request->get['product_id']);

            $this->template = 'amp/product.tpl';

            $this->children = array(
                'amp/footer',
                'amp/header'
            );

            $this->response->setOutput($this->render());
        } else {
            $url = '';

            if (isset($this->request->get['cat_id'])) {
                $url .= '&cat_id=' . $this->request->get['cat_id'];
            }

            if (isset($this->request->get['manufacturer_id'])) {
                $url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
            }

            if (isset($this->request->get['filter_name_product'])) {
                $url .= '&filter_name_product=' . $this->request->get['filter_name_product'];
            }

            if (isset($this->request->get['filter_tag'])) {
                $url .= '&filter_tag=' . $this->request->get['filter_tag'];
            }

            if (isset($this->request->get['filter_description'])) {
                $url .= '&filter_description=' . $this->request->get['filter_description'];
            }

            if (isset($this->request->get['filter_product_category_id'])) {
                $url .= '&filter_product_category_id=' . $this->request->get['filter_product_category_id'];
            }

            $this->data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_error'),
                'href' => $this->url->link('product/product', $url . '&product_id=' . $product_id),
                'separator' => $this->language->get('text_separator')
            );

            $this->document->setTitle($this->language->get('text_error'));

            $this->data['heading_title'] = $this->language->get('text_error');

            $this->data['text_error'] = $this->language->get('text_error');

            $this->data['button_continue'] = $this->language->get('button_continue');

            $this->data['continue'] = $this->url->link('common/home');

            $this->template = 'error/not_found.tpl';

            $this->children = array(
                'common/column_left',
                'common/column_right',
                'common/content_top',
                'common/content_bottom',
                'common/footer',
                'common/header'
            );

            $this->response->setOutput($this->render());
        }
    }

    function getimgsize($url, $referer = ''){
        $headers = array(
          'Range: bytes=0-32768'
        );
        if (!empty($referer)) array_push($headers, 'Referer: '.$referer);

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($curl);
        curl_close($curl);
        $image = imagecreatefromstring($data);
        $return = array(imagesx($image), imagesy($image));
        imagedestroy($image);
        return $return;
    }
}
