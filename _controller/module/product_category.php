<?php
class ControllerModuleProductCategory extends Controller {
	protected function index($setting) {
		static $module = 0;

		$this->language->load('module/product_category');
    	$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['button_cart'] = $this->language->get('button_cart');
		$this->data['text_point'] = $this->language->get('text_point');
		$this->data['text_totalrate'] = $this->language->get('text_totalrate');

		$this->load->model('catalog/product');
		$this->load->model('catalog/category');
		$this->load->model('tool/image');
		$this->data['position'] = $setting['position'];
		$this->data['products'] = array();

		$this->data['title'] = $setting[$this->config->get('config_language_id')]['title'];

		$info_category = $this->model_catalog_category->getCategory($setting['cat_p']);

		$this->data['link'] = $this->url->link('product/category', 'path=' . $info_category['category_id']);
		$this->data['description'] = html_entity_decode($info_category['description'],ENT_QUOTES, 'UTF-8');
		if($info_category['image']){
			$this->data['image'] = 'media/'.$info_category['image'];
		}
		if(isset($info_category['name'])){
			$data = array(
				'filter_category_id' =>$info_category['category_id'],
				'sort'  => $setting['sort'],
				'order' => $setting['type'],
				'start' => 0,
				'limit' => $setting['limit']
			);

			$results = $this->model_catalog_product->getProducts($data);
			foreach ($results as $result) {
				if ($result['flag']) {
					$flag = $this->model_tool_image->cropsize($result['flag'],40,40);
				}else {
					$flag = false;
				}

				if ($result['image']) {
					$image = $this->model_tool_image->cropsize($result['image'], $setting['image_width'], $setting['image_height']);
				}else {
					$image = false;
				}
				if ($result['discount']>0) {
						$saleoff = round( (((float)$result['price'] - (float)$result['discount'])*100)/(float)$result['price']);
						$discount = $this->currency->format($result['discount']);
				} else {
						$saleoff = 0;
						$discount = 0;
				}
				if ($result['price']>0){
						$price = $this->currency->format($result['price']);
				} else {
						$price = $this->language->get('call_for_price');
				}
				$rate = array();
				$rate = $this->model_catalog_product->getRate($result['product_id']);
		        if ($rate['total_rate'] == 0) {
		            $rate['total_rate'] = 1;
		            $rate['total_point'] = 5;
		        }
		        $average = round($rate['total_point'] / $rate['total_rate']);

				$this->data['products'][] = array(
					'product_id' => $result['product_id'],
					'flag'   	 => $flag,
					'thumb'   	 => $image,
					'brand'         => $result['brand'],
					'saleoff'       => $saleoff,
					'discount'       => $discount,
					'price'   	 => $price,
					'rate'   	 => $rate,
					'rental'	=> $this->model_catalog_product->getProduct($result['product_id'])['rental'],
					'average'	 => $average,
					'name'    	 => $result['name'],
					'href'    	 => $this->url->link('product/product', 'product_id=' . $result['product_id']),
				);
			}
		}
		$this->data['module'] = $module++;
		$this->template = 'module/product_category.tpl';
		$this->render();
	}
}
?>
