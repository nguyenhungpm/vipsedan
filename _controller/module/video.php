<?php
class ControllerModuleVideo extends Controller {
	protected function index($setting) {
		$this->language->load('module/video');

    $this->data['heading_title'] =  $setting['title'];

		$this->data['position'] = $setting['position'];
		$this->data['hotline'] = $this->config->get('config_hotline');

		$this->load->model('design/banner');

		$this->data['description'] = html_entity_decode($setting['description'], ENT_QUOTES, 'UTF-8');

		$this->load->model('tool/image');

		$this->load->model('tool/t2vn');

		$results = $this->model_design_banner->getPictures();
		$this->data['videos'] = array();
		foreach($results as $rs){
			$this->data['videos'][] = array(
				'src' => "//www.youtube.com/embed/". $rs['link'] ."?showinfo=0",
				'name' => $rs['title']
			);
		}

		$this->template = 'module/video.tpl';

		$this->render();
	}
}
?>
