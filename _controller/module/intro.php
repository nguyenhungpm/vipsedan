<?php
class ControllerModuleIntro extends Controller {
	protected function index($setting) {
		$this->data['heading_title'] 	= $setting['heading_title'];
		$this->data['content'] 			= html_entity_decode($setting['content'], ENT_QUOTES, 'UTF-8');
		$this->data['intro_link'] 			= $setting['link'];
		$this->data['button'] 			= $setting['button'];

		$this->load->model('tool/image');
		$this->data['intro_image'] = 'media/'.$setting['intro_image'];
		$this->template = 'module/intro.tpl';
		$this->render();
	}
}
?>
