<?php
class ControllerModuleFacebookLB extends Controller {
	protected function index($setting) {
	$this->data['position'] = $setting['position'];	
    $this->data['page_url'] = $setting['page_url'];
		$this->data['width'] = $setting['width'];
		$this->data['height'] = $setting['height'];
		$this->data['show_faces'] = $setting['show_faces'];
		$this->data['show_stream'] = $setting['show_stream'];
		$this->data['show_header'] = $setting['show_header'];

		if (!empty($this->config->get('fdu_app_id'))){
			$this->data['flb_app_id'] = $this->config->get('fdu_app_id');
		} else {
			$this->data['flb_app_id'] = false;
		}

		$this->template = 'module/facebook_lb.tpl';

		$this->render();
	}
}
?>
