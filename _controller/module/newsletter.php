<?php  
class ControllerModuleNewsletter extends Controller {
    private $error = array();
  
  public function index(){
     $this->load->model('module/newsletter');
     $this->language->load('module/newsletter');
     $this->data['text_subscribe'] = $this->language->get('text_subscribe');  
     $this->data['text_submit'] = $this->language->get('text_submit');  
     $this->data['heading_title'] = $this->language->get('heading_title');  
     $this->data['text_email'] = $this->language->get('text_email');  
    $this->template = 'module/newsletter.tpl';
    
    $this->render();
  }
  public function addemail($data){
        $this->db->query("INSERT INTO " . DB_PREFIX . "subscribe SET email_id='" . $this->db->escape($data['subscribe_email']) . "'");
    }
  public function add() {
        $json = '';
        $this->load->model('module/newsletter');
        // if ((utf8_strlen($this->request->post['subscribe_email']) > 96) || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['subscribe_email'])) {
        if ((utf8_strlen($this->request->post['subscribe_email']) > 96) || !filter_var($this->request->post['subscribe_email'], FILTER_VALIDATE_EMAIL)) {
            $json = 1;
        } elseif ($this->model_module_newsletter->checkmailid($this->request->post)) {
            $json = 2;
        } else {
            $json = 3;
            $this->model_module_newsletter->subscribe($this->request->post);
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
  
  public function unsubscribe(){
    
    if($this->config->get('newsletter_thickbox')){
      $prefix_eval = "#TB_ajaxContent ";
    }else{
        $prefix_eval = "";
    }
    
    $this->language->load('module/newsletter');
    
    $this->load->model('module/newsletter');
    
    if(isset($this->request->post['subscribe_email']) and filter_var($this->request->post['subscribe_email'],FILTER_VALIDATE_EMAIL)){
            
        if($this->config->get('newsletter_registered') and $this->model_module_newsletter->checkRegisteredUser($this->request->post)){
         
          $this->model_module_newsletter->UpdateRegisterUsers($this->request->post,0);
        
      echo('$("'.$prefix_eval.' #subscribe_result").html("'.$this->language->get('unsubscribe').'");$("'.$prefix_eval.' #subscribe")[0].reset();');
         
        
       }else if(!$this->model_module_newsletter->checkmailid($this->request->post)){
       
         echo('$("'.$prefix_eval.' #subscribe_result").html("'.$this->language->get('notexist').'");$("'.$prefix_eval.' #subscribe")[0].reset();');
       
       }else{
         
        if($this->config->get('option_unsubscribe')) {
         $this->model_module_newsletter->unsubscribe($this->request->post);
         echo('$("'.$prefix_eval.' #subscribe_result").html("'.$this->language->get('unsubscribe').'");$("'.$prefix_eval.' #subscribe")[0].reset();');
        }
       }
       
    }else{
      echo('$("'.$prefix_eval.' #subscribe_result").html("<span class=\"error\">'.$this->language->get('error_invalid').'</span>")');
    }
  }
}
?>