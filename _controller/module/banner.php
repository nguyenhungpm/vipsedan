<?php
class ControllerModuleBanner extends Controller {
	protected function index($setting) {
		static $module = 0;
		$this->language->load('module/banner');
		$this->load->model('design/banner');
		$this->load->model('tool/image');
		$this->data['banners'] = array();
		$this->data['position'] = $setting['position'];
		$results = $this->model_design_banner->getBanner($setting['banner_id']);

		foreach ($results as $result) {
			if (file_exists(DIR_IMAGE . $result['image'])) {
				$this->data['banners'][] = array(
					'title' => $result['title'],
					'link'  => $result['link'],
					'description'  => $result['description'],
					'image' => $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height'])
				);
			}
		}
		if( ($setting['position']=='column_left') || ($setting['position']=='column_right') ) {
			$s ="
			var slider = tns({
		    container: '.banner__zone',
		    items: 2,
				autoplayButtonOutput: false,
				controls: false,
				gutter: 10,
				nav: false,
				speed: 500,
		    slideBy: 'page',
		    autoplay: true
			})";
		} else {
			$s ="
			var slider = tns({
		    container: '.banner__zone',
		    items: 2,
				autoplayButtonOutput: false,
				controls: false,
				nav: false,
				speed: 500,
		    slideBy: 'page',
		    autoplay: true,
				responsive: {
		      640: {
						gutter: 10,
		        items: 2
		      },
		      700: {
		        gutter: 20
		      },
		      900: {
		        items: 3
		      }
		    }
	  	});
			";
		}
		$this->document->addExScript($s);
		$this->document->addScript('//cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.3/tiny-slider.min.js');
		$this->document->addStyle('//cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.3/tiny-slider.min.css');
		$this->data['module'] = $module++;
		$this->template = 'module/banner.tpl';
		$this->render();
	}
}
?>
